<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="content-language" content="en-us">
<title></title>
</head>
<body ><table style="width:100%; background-color:#ffffff; " border="0" cellpadding="0" cellspacing="0" ><tr ><td style="vertical-align:top; padding:10px; " align="center" valign="top" ><table style="width:660px; text-align:left; " border="0" cellpadding="0" cellspacing="0" ><tr ><td rowspan="2">
<table style="width: 41px;" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>&nbsp;</td>
</tr>
</table>
</td>
<td style="background-color:#145385; vertical-align:top; " ><table border="0" cellpadding="0" cellspacing="0" width="100%" ><tr ><td><img height="21" width="9" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/cornerNW-white.png" alt=""><br></td>
<td style="vertical-align:middle; text-align:right; color:#FFFFFF; font-family:Tahoma,Helvetica; font-size:10px; line-height:18px; width:100%; " ><div class="mktEditable" id="volume_title" >&nbsp; &nbsp;</div>
</td>
<td><img height="21" width="9" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/cornerNE-white.png" alt=""><br></td>
</tr>
</table>
</td>
<td rowspan="2">
<table style="width: 40px;" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr ><td style="vertical-align:top; " ><table border="0" cellpadding="0" cellspacing="0" width="100%" ><tr ><td style="background-color:#f0f0f0; color:#45555f; font-family:Tahoma,Helvetica; font-size:12px; line-height:18px; vertical-align:top; padding:20px; " ><img src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/emailLogo.png" align="right" border="0" height="53" width="139" alt="skyboxsecurity" style="padding-right:15px;"><div style="color:#45555f; font-size:14px; font-weight:bold; line-height:21px; " ><div class="mktEditable" id="intro_title" >Partner Brief</div>
</div>
<div style="color:#4F4E4C; font-size:23px; font-weight:bold; line-height:26px; " ><div class="mktEditable" id="main_title" ><h1 class="with-tabs"><span style="font-size: 24px;">Knowing is Half the Battle</span></h1>
<h1 class="with-tabs"></h1></div>
</div>
<br>
<table border="0" cellpadding="0" cellspacing="0" width="100%" ><tr ><td style="vertical-align:top; font-size:12px; font-family:Tahoma,Helvetica; color:#45555f; line-height:19px; " ><div class="mktEditable" id="main_text" ><p style="color: #45555f; font-family: Tahoma, Helvetica; font-size: 12px; font-weight: normal; line-height: 19px;">Hello {{lead.First Name:default=Partner}},</p>
<p><span><span>The first step in uncovering vulnerabilities is knowing what they are</span></span></p>
<p><img src="http://www.skyboxsecurity.com/sites/default/files/image_resources/GI%20Joe.png" alt="" width="148" height="100" align="right" />GI Joe taught children of the 80s valuable life lessons and gave insights into military strategy; although I am pretty sure they didn&rsquo;t pioneer the concept of knowing your enemy. In fact, one of the famous Chinese military general, strategist and philosopher Sun Tzu&rsquo;s more popular proverbs says, &ldquo;If you know yourself but not the enemy, for every victory gained you will also suffer a defeat.&rdquo;</p>
<p>This is true for GI Joe, as well as the trenches of IT security &hellip; threats are the enemy, and every mitigated vulnerability on your network is a victory in reducing the attack surface.&nbsp; But, it doesn&rsquo;t take a rocket surgeon to figure out you can&rsquo;t fix a vulnerability if you don&rsquo;t know it exists. &nbsp;&nbsp;</p>
<p>Thus, security researchers were born to discover and disclose vulnerabilities for the masses. The most popular source is the&nbsp;<a href="http://nvd.nist.gov/">National Vulnerability Database</a>&nbsp;(NVD), a repository of vulnerability data managed by the US government.&nbsp; While the NVD is a good source of data, it&rsquo;s not an exhaustive list of vulnerabilities. A vulnerability needs to have a&nbsp;<a href="https://cve.mitre.org/">Common Vulnerabilities and Exposures</a>&nbsp;(CVE) assigned to it to be added to the NVD. And to get a CVE, the discovered vulnerability must be reported to the CVE Numbering Authority or one of the data sources it<a href="https://cve.mitre.org/cve/data_sources_product_coverage.html">follows</a>.&nbsp;</p>
<p>Not every vulnerability gets a CVE, and those that don&rsquo;t are not cataloged in the NVD. Which simply means that the NVD shouldn&rsquo;t be your&nbsp;<em>only</em>&nbsp;source of vulnerability data. If your risk analysis solution relies only on the NVD for vulnerability data, you won&rsquo;t have a&nbsp;<a href="http://www.skyboxsecurity.com/blog/are-you-using-astrolabe-navigate-network-ocean">complete picture</a>.&nbsp;</p>
<p style="text-align: center;"><img src="http://www.skyboxsecurity.com/sites/default/files/image_resources/Bicyclist_Car.JPG" alt="" width="287" height="100" /></p>
<p>The&nbsp;<a href="http://www.skyboxsecurity.com/blog/not-all-vulnerability-research-alike">Skybox Research Lab</a>&nbsp;consolidates intelligence from more than&nbsp;<a href="http://www.vulnerabilitycenter.com/methodology">20 sources</a>&nbsp;(including the NVD) into the&nbsp;<a href="http://www.vulnerabilitycenter.com/methodology">Skybox Vulnerability Database</a>&nbsp;to provide the most comprehensive view of the current state of vulnerabilities.&nbsp; Specifically, using only the NVD would get you 84 percent of the vulnerabilities in the Skybox Vulnerability Database.</p>
<p>For example, only 36 percent of all&nbsp;<a href="http://www.vulnerabilitycenter.com/#search=@phrase=JunOS">JunOS vulnerabilities</a>&nbsp;have CVEs. Or consider&nbsp;<a href="http://www.vulnerabilitycenter.com/#!vul=43432">Microsoft EMET 4.1 Local DoS Vulnerability by Bypassing It (SBV-43432</a>), another vulnerability withno assigned CVE. In some (very important) cases, using the NVD only is simply not good enough. The Skybox Vulnerability Database is incorporated into all Skybox solutions, so our customers have comprehensive visibility into all known vulnerabilities.</p>
<p>A similar comparison can be made between the Skybox Vulnerability Database and vulnerability scanners.&nbsp; A fact: none of the vulnerability scanners in the market can detect all your vulnerabilities, and the&nbsp;<a href="http://www.vulnerabilitycenter.com/#!facts=scanners">coverage varies between different vendors</a>. Therefore, it&rsquo;s important to understand the strength and weaknesses of your scanners since they may represent the primary method of detecting vulnerabilities. The Skybox Vulnerability Database includes mapping to the discoverable vulnerabilities of all popular scanners, creating a superset of vulnerability data.</p>
<p>To ensure that you are reducing your attack surface daily and reducing risk, you need to consider all known vulnerabilities. Without a complete understanding of all the potential vulnerabilities that may exist on your network you are setting yourself up for failure.</p>
<p>And now you know&hellip;</p>
<p>For more information about Skybox&rsquo;s&nbsp;<a href="http://www.skyboxsecurity.com/products/vulnerability-control">risk analysis solution</a>, download our free&nbsp;<a href="http://lp.skyboxsecurity.com/Enterprise-Suite-Trial.html">30-day trial</a>. Or check out the&nbsp;<a href="http://www.vulnerabilitycenter.com/#home">Vulnerability Center</a>&nbsp;to learn more about the Skybox Vulnerability Database.</p></div>
</td>
<td style="vertical-align:top; " ><table border="0" cellpadding="0" cellspacing="0" align="right" style="width:100px; " ><tr ><td rowspan="2">
<table style="width: 14px;" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>&nbsp;</td>
</tr>
</table>
</td>
<td style="background-color: #145385; vertical-align: top;"><img height="21" width="9" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/colCornerNW.png" alt=""><br>
<br></td>
<td style="padding-left:5px; background-color:#145385; font-family:'trebuchet ms'; font-size:17px; font-weight:bold; color:#ffffff; " ><div class="mktEditable" id="column_title" ><p style="text-align: center;">Skybox Free Trial</p></div>
</td>
<td style="background-color: #145385; vertical-align: top;"><img height="21" width="9" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/colCornerNE.png" alt="" align="right"><br></td>
<td rowspan="2">
<table style="width: 14px;" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr ><td colspan="3" style="padding:14px; background-color:#ffffff; font-size:12px; font-family:Tahoma,Helvetica; color:#45555f; line-height:19px; " ><div style="font-weight:bold; color:#45555f; " ><div class="mktEditable" id="column_subtitle" ><p>
<div style="text-align: center;">Have your customers start a<strong>&nbsp;</strong></div>
<div style="text-align: center;"><strong>Free Trial of the Skybox Risk Analytics Platform</strong></div>
<div style="text-align: center;">NOW!</div>
</p></div>
</div>
<br>
<div class="mktEditable" id="column_text" ><div style="text-align: center;"><a title="Trial" href="http://lp.skyboxsecurity.com/Enterprise-Suite-Trial.html"><img src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/StartMyTrial.gif" alt="StartMyTrial.gif" align="center" /></a></div>
<div>
<p><strong>P.S.</strong><span>&nbsp;Skybox offers online and face-to-face training. Get your&nbsp;</span><a href="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/Training Flyer Final.pdf">certification&nbsp;</a><span>now.&nbsp;</span></p>
<p>&nbsp;</p>
<p><strong>If you need assistance, please contact us!</strong></p>
<div>Farah Khan, Director NA Channel Sales, <a title="Email Ken" href="mailto: ken.wilson@skyboxsecurity.com">farah.khan@skyboxsecurity.com</a></div>
<div>John Quinn, Director of Intnl Channel Sales,&nbsp;<a title="Email John" href="mailto: john.quinn@skyboxsecurity.com">john.quinn@skyboxsecurity.com</a></div>
<div>Gina Osmond, WW Channel Marketing Manager,&nbsp;<a title="Email Gina" href="mailto: gina.osmond@skyboxsecurity.com">gina.osmond@skyboxsecurity.com</a></div>
<div>
<p>Connect with us!</p>
<table border="0" width="139" height="42">
<tbody>
<tr>
<td align="left"><a title="Follow Us on Twitter" href="https://twitter.com/SkyboxSecurity" target="_blank"><img title="Follow Us on Twitter" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/TwitterIcon.png" alt="Twitter" width="26" height="23" align="left" /></a></td>
<td align="left"><a title="Like Us on Facebook" href="https://www.facebook.com/pages/Skybox-Security-Inc/162832530411996?bookmark_t=page" target="_blank"><span class="lpContentsItem richTextSpan"><img title="Like Us on Facebook" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/FacebookIcon.png" alt="Facebook" width="26" height="24" /></span></a></td>
<td><a title="Follow Us on LinkedIn" href="http://www.linkedin.com/company/skybox-security" target="_blank"><img title="Follow Us on LinkedIn" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/LinkedInIcon.png" alt="LinkedIn" width="26" height="25" /></a></td>
<td><a title="Follow Us on Google+" href="https://plus.google.com/+Skyboxsecurity" target="_blank"><img title="Follow Us on Google+" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/Google%2BIcon.png" alt="Google+" width="26" height="24" /></a><br /></td>
</tr>
</tbody>
</table>
</div>
<div>&nbsp;</div>
</div></div>
</td>
</tr>
<tr>
<td colspan="5" style="background-color: #f1f1f1; border-collapse:collapse;"><img height="37" width="244" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/colShadow.png" style="margin:0px; padding:0px; display:block;"><br></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr ><td style="vertical-align:top; " colspan="3" ><table border="0" cellpadding="0" cellspacing="0" width="100%" ><tr ><td style="color:#45555f; font-family:Tahoma,Helvetica; font-size:11px; line-height:16px; vertical-align:top; text-align:center; border-collapse:collapse; " ><img height="99" width="660" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/bottomShadow.png" alt=""><br>
<div class="mktEditable" id="Contact" ><b>Headquarters: 2099 Gateway Place, Suite 450, San Jose, CA 95110<br>
Copyright &copy; 2015 Skybox Security Inc.</b><br>
<a href="http://www.skyboxsecurity.com/contact">Contact Us</a></div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>