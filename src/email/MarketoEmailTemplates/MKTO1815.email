<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="content-language" content="en-us">
<title></title>
</head>
<body ><table style="width:100%; background-color:#ffffff; " border="0" cellpadding="0" cellspacing="0" ><tr ><td style="vertical-align:top; padding:10px; " align="center" valign="top" ><table style="width:660px; text-align:left; " border="0" cellpadding="0" cellspacing="0" ><tr ><td rowspan="2">
<table style="width: 41px;" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>&nbsp;</td>
</tr>
</table>
</td>
<td style="background-color:#145385; vertical-align:top; " ><table border="0" cellpadding="0" cellspacing="0" width="100%" ><tr ><td><img height="21" width="9" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/cornerNW-white.png" alt=""><br></td>
<td style="vertical-align:middle; text-align:right; color:#FFFFFF; font-family:Tahoma,Helvetica; font-size:10px; line-height:18px; width:100%; " ><div class="mktEditable" id="volume_title" >&nbsp; &nbsp;</div>
</td>
<td><img height="21" width="9" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/cornerNE-white.png" alt=""><br></td>
</tr>
</table>
</td>
<td rowspan="2">
<table style="width: 40px;" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr ><td style="vertical-align:top; " ><table border="0" cellpadding="0" cellspacing="0" width="100%" ><tr ><td style="background-color:#f0f0f0; color:#45555f; font-family:Tahoma,Helvetica; font-size:12px; line-height:18px; vertical-align:top; padding:20px; " ><img src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/emailLogo.png" align="right" border="0" height="53" width="139" alt="skyboxsecurity" style="padding-right:15px;"><div style="color:#6D6C69; font-size:14px; font-weight:bold; line-height:21px; " ><div class="mktEditable" id="intro_title" >Partner Brief</div>
</div>
<div style="color:#4F4E4C; font-size:23px; font-weight:bold; line-height:26px; " ><div class="mktEditable" id="main_title" ><h1><span style="font-size: 18px;">Is there a most secure web browser?</span></h1></div>
</div>
<br>
<div class="mktEditable" id="main_text" ><p>Hello {{lead.First Name:default=Partner}},</p>
<div id="node-1794" class="node">
<div class="content">
<p>After June&rsquo;s Patch Tuesday&nbsp;<a href="http://www.skyboxsecurity.com/blog/patch-tuesday-brings-festival-browser-updates-pwn2own-ie-vulnerability-finally-0wn3d-microsoft">browser festival,</a>&nbsp;the&nbsp;<a href="http://www.skyboxsecurity.com/blog/not-all-vulnerability-research-alike">Skybox Research Lab</a>&nbsp;wondered, which web browser is the most secure?</p>
<p>To start, what defines the degree of&nbsp;<a id="FALINK_1_0_0" class="FAtxtL" href="http://www.skyboxsecurity.com/blog/there-most-secure-web-browser#">security</a>&nbsp;of software? How can one evaluate the security of a web browser? Lacking clear definition, we evaluated a few metrics.</p>
<p><strong>Fewest Exposed Vulnerabilities &ndash;&nbsp;</strong>Analyze the number of vulnerabilities published on each web browser over the past 18 months (since January 2013).&nbsp;</p>
<p style="text-align: center;"><img src="http://www.skyboxsecurity.com/sites/default/files/image_resources/Vulnerabilities%20by%20browsers%20bar%20chart.JPG" alt="" width="250" align="center" /></p>
<p>Based on this metric, one could deduce that Opera is the most secure browser. Are Opera&rsquo;s&nbsp;<a id="FALINK_4_0_3" class="FAtxtL" href="http://www.skyboxsecurity.com/blog/there-most-secure-web-browser#">software developers</a>&nbsp;doing a better job than those of Microsoft or Google? Unlikely. Given Opera&rsquo;s market share hovering around 1 percent (according to&nbsp;<a href="http://gs.statcounter.com/#browser-ww-monthly-201305-201405">StatCounter May 2014</a>), most likely there is not a lot of interest or value in finding Opera vulnerabilities. So, number of published vulnerabilities per browser is probably not the best measurement of a browser&rsquo;s security.</p>
<p><strong>Most Published Vulnerabilities &ndash;&nbsp;</strong>Let&rsquo;s turn it around and explore the browser by most published (and fixed) vulnerabilities. After all, by publishing and fixing vulnerabilities, there would be fewer attack vectors to exploit.</p>
<p>In that case, it&rsquo;s a close race between&nbsp;<a id="FALINK_3_0_2" class="FAtxtL" href="http://www.skyboxsecurity.com/blog/there-most-secure-web-browser#">Google Chrome</a>&nbsp;and Internet Explorer. However, there are still vulnerabilities discovered in Internet Explorer 6 more than 14 years after its release. So, this is probably not a good metric either.</p>
<p><strong>Shortest Time Between Security Patches &ndash;&nbsp;</strong>Perhapsthe most secure browser is the one with the shortest time between the vulnerability publication and the fix availability? Following the&nbsp;<a href="http://www.skyboxsecurity.com/blog/will-microsoft-announce-pwn2own-2014-fixes-next-week">Pwn2Own2014 statistics</a>, Chrome is the clear leader as the most responsive to security vulnerabilities.</p>
<p>Additionally, Google Chrome has issued the most frequent&nbsp;<a id="FALINK_2_0_1" class="FAtxtL" href="http://www.skyboxsecurity.com/blog/there-most-secure-web-browser#">security updates</a>&nbsp;over the past 18 months. On average, Google Chrome released a new version with security fixes every 15 days! By comparison, Internet&nbsp;<a id="FALINK_5_0_4" class="FAtxtL" href="http://www.skyboxsecurity.com/blog/there-most-secure-web-browser#">Explorer</a>&nbsp;and Firefox release security updates approximately once a month.</p>
<p style="text-align: center;"><img src="http://www.skyboxsecurity.com/sites/default/files/image_resources/Browsers%20vulnerability%20updates%20bar%20chart.JPG" alt="" width="250" /></p>
<p>Why does time between security updates matter? Time is the single biggest factor in determining the size of the&nbsp;<a href="http://www.skyboxsecurity.com/blog/incredible-expanding-attack-surface">attack surface</a>, and therefore the risk exposure an organization is facing.</p>
<p>The attack surface grows with every new vulnerability, and is intensified by the number of systems affected by that vulnerability. If exposed vulnerabilities linger unresolved for weeks and months, the likelihood of exploitation is exponentially growing. More&nbsp;<a href="http://www.skyboxsecurity.com/blog/best-practices-reducing-your-attack-surface-pangolin-your-new-security-mascot">best practices for reducing your attack surface</a>&nbsp;are outlined in this&nbsp;<a href="http://www.skyboxsecurity.com/blog/best-practices-reducing-your-attack-surface-pangolin-your-new-security-mascot">blog post</a>.</p>
<p>For more information about browser vulnerabilities, check out&nbsp;<a href="http://www.vulnerabilitycenter.com/#home">the Skybox Vulnerability Center</a>.</p>
<p><a href="mailto:vulnerabilitycenter@skyboxsecurity.com">Share with us</a>&nbsp;&hellip; which web browser do you think is the most secure? And what metrics do you use to evaluate browser security?&nbsp;</p>
</div>
</div></div>
</td>
</tr>
</table>
</td>
</tr>
<tr ><td style="vertical-align:top; " colspan="3" ><table border="0" cellpadding="0" cellspacing="0" width="100%" ><tr ><td style="color:#45555f; font-family:Tahoma,Helvetica; font-size:11px; line-height:16px; vertical-align:top; text-align:center; " ><img height="99" width="660" src="http://lp.skyboxsecurity.com/rs/skyboxsecurity/images/bottomShadow.png" alt=""><br>
<div class="mktEditable" id="Contact" ><b>Copyright &copy; 2014 Skybox Security Inc</b><br>
<a href="http://lp.skyboxsecurity.com/ContactMe.html">Contact Us</a></div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>