({
  
        
    initComponentPicklist: function(component,event,helper){ //Component is a dependant picklist which is not handled by lightning
        var dependancyMap = {
            'Change Manager': ['--- None ---', 'Tickets Management', 'Workflows', 'Browsers Support', 'Users & Permissions', 'Provisioning', 'Change Tracking & Reconciliation'],
            'Appliances': ['--- None ---', 'Web Admin', 'SNMP', 'High Availability', 'Hardware', 'Virtual Appliance', 'Logrotate', 'syslog-ng', 'Collector', 'Performance', 'Vulnerabilities'],
            'Firewall Assurance': ['--- None ---', 'Compliance', 'Change Tracking', 'Access Analyzer', 'Collection & Modeling', 'Reports', 'Tickets & Alerts', 'GUI', 'Optimization & Cleanup', 'User Management', 'Network map', 'Web UI'],
            'Network Assurance': ['--- None ---', 'Compliance', 'Access Analyzer', 'Collection & Modeling', 'Reports', 'Tickets & Alerts', 'GUI', 'User Management', 'Network map', 'Web UI'],
            'Threat Manager': ['--- None ---', 'Reports', 'Tickets & Alerts', 'GUI', 'Analysis', 'Model management', 'Dictionary'],
            'Vulnerability Control': ['--- None ---', 'Collection & Modeling', 'Tickets & Alerts', 'GUI', 'User Management', 'Analysis', 'Model management', 'Reporting', 'Dictionary', 'Web UI'],
            'Horizon': ['--- None ---'],
            'Platform': ['--- None ---', 'Collector', 'Dictionary', 'Manager', 'Server Options', 'Certificates' ,'Upgrade' ,'API'],
            'License': ['--- None ---']
        }
        var selectedModule = component.get("v.selectedModule");
        component.set("v.compPicklist", dependancyMap[selectedModule]);
    },
    
 	createCase: function(component, event) {
        var errorFlag = false;
        var inputCmp = component.find("module");
        var val = inputCmp.get("v.value");
    	if(!val){
    		inputCmp.set("v.errors", [{message:"Complete this field"}]);
            errorFlag = true;
        } else {
            inputCmp.set("v.errors", null);
        }
        inputCmp = component.find("ver");
        val = inputCmp.get("v.value");
        if(!val){
    		inputCmp.set("v.errors", [{message:"Complete this field"}]);
            errorFlag = true;
        } else {
            inputCmp.set("v.errors", null);
        }
        inputCmp = component.find("discuss");
        val = inputCmp.get("v.value");
        if(!val){
    		inputCmp.set("v.errors", [{message:"Complete this field"}]);
            errorFlag = true;
        } else {
            inputCmp.set("v.errors", null);
        }
        inputCmp = component.find("subject");
        val = inputCmp.get("v.value");
        if(!val){
    		inputCmp.set("v.errors", [{message:"Complete this field"}]);
            errorFlag = true;
        } else {
            inputCmp.set("v.errors", null);
        }
        inputCmp = component.find("description");
        val = inputCmp.get("v.value");
        if(!val){
    		inputCmp.set("v.errors", [{message:"Complete this field"}]);
            errorFlag = true;
        } else {
            inputCmp.set("v.errors", null);
        }
        inputCmp = component.find("discuss");
        val = inputCmp.get("v.value");
        var whoSpeak = component.find("speak");
        var who = whoSpeak.get("v.value");
        if(val == 'Yes' && !who){
            whoSpeak.set("v.errors", [{message:"Complete this field"}]);
            errorFlag = true;
        } else {
            whoSpeak.set("v.errors", null);
        }
        if(errorFlag){
            return;
        }
        //validateRequiredField(component, 'module');
        //helper.validateRequiredField(component, 'ver');
        //helper.validateRequiredField(component, 'subject');
        //helper.validateRequiredField(component, 'description');
        var newCase = component.get("v.newCase");
        var action = component.get("c.saveCase");
        
        //Set input values on case fields
        var selected = component.get("v.selectedType");
        component.set("v.newCase.Type", selected); 
        selected = component.get("v.selectedModule");
        component.set("v.newCase.Module__c", selected); 
        selected = component.get("v.selectedComp");
        if (selected != "--- None ---"){
        	component.set("v.newCase.Component__c", selected);
        }
        selected = component.get("v.selectedVer");
        component.set("v.newCase.Version_build__c", selected); 
        selected = component.get("v.subject");
        component.set("v.newCase.Subject", selected);
        selected = component.get("v.description");
        component.set("v.newCase.DescriptionRich__c", selected); 
        selected = component.get("v.didDiscuss");
        component.set("v.newCase.Did_you_discuss_this_with_Skybox_support__c", selected); 
        selected = component.get("v.whoSpeak");
        component.set("v.newCase.Who_did_you_speak_to__c", selected); 
        
        action.setParams({ 
            "cs": newCase
        });
        action.setCallback(this, function(a) {
               var state = a.getState();
                if (state === "SUCCESS") {
                    var caseId = a.getReturnValue();
                    console.log(caseId);
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                      "recordId": caseId,
                      "isredirect" : true
                    });
                    $A.get('e.force:refreshView').fire();                    
 
                    navEvt.fire();                                                        
                }
            });
        document.getElementById("newCaseModal").style.display = "none";
        document.getElementById("backGroundSection").style.display = "none";
        var cmpTarget = component.find('Component');
        $A.util.removeClass(cmpTarget, 'db');
        $A.util.addClass(cmpTarget, 'dn');  
        $A.enqueueAction(action);                      
	},
    
	showRTModal : function(component, event, helper) {
        var cmpTarget = component.find('Component');
        $A.util.removeClass(cmpTarget, 'dn');
        $A.util.addClass(cmpTarget, 'db');
	},
    
    defaultCloseAction : function(component, event, helper) {
        var cmpTarget = component.find('Component');
        $A.util.removeClass(cmpTarget, 'db');
        $A.util.addClass(cmpTarget, 'dn');
        $A.get('e.force:refreshView').fire();
    },
    
    hideModal : function(component, event, helper) {
		document.getElementById("newCaseModal").style.display = "none";
        document.getElementById("backGroundSection").style.display = "none";
        var cmpTarget = component.find('Component');
        $A.util.removeClass(cmpTarget, 'dn');
        $A.util.addClass(cmpTarget, 'db');
        //$A.get('e.force:refreshView').fire();
	},
    
    navigateToMyComponent : function(component, event, helper) {
    var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
        componentDef : "c:RecordTypeSelection",
        componentAttributes: {
        }
    });
    evt.fire();
	},
    
    handleRecordTypeSelected : function(component, event, helper) {
        component.set("v.selectedRecordType", event.getParam('recordTypeId'));
        var s = component.get("v.selectedRecordType");       
        component.set(("v.newCase.RecordTypeId"), s);
        document.getElementById("backGroundSection").style.display = "block";
        document.getElementById("newCaseModal").style.display = "block";
        var cmpTarget = component.find('Component');
        $A.util.removeClass(cmpTarget, 'db');
        $A.util.addClass(cmpTarget, 'dn');
        helper.buildDescriptionTemplate(component, event, helper);
        
        helper.fetchPickListVal(component, 'Type', 'type');
        helper.fetchPickListVal(component, 'Module__c', 'module');
        helper.fetchPickListVal(component, 'Version_build__c', 'ver');
        helper.fetchPickListVal(component, 'Did_you_discuss_this_with_Skybox_support__c', 'discuss');
	}
})