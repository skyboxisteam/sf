({
    fetchPickListVal: function(component, fieldName, elementId) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.newCase"),
            "fld": fieldName, 
            "recTypeId" :  component.get("v.newCase.RecordTypeId")
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                //if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                //}
                if (allValues != undefined && allValues.length > 0) {
                for (var i = 0; i < allValues.length; i++) {
                   //if (i == 0 && fieldName = 'Type'){
                       opts.push({
                            class: "optionClass",
                            label: allValues[i],
                            value: allValues[i],
                           }); 
                    		
                    /*} else {
                        opts.push({
                            class: "optionClass",
                            label: allValues[i],
                            value: allValues[i]
                        });
                    }*/
                }
                }
                component.find(elementId).set("v.options", opts);
                if (fieldName == 'Type'){
                    console.log('type');
                    component.set('v.selectedType', allValues[0]); 
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    validateRequiredField: function(component, elementId){
        alert('validating');
    	var inputCmp = component.find(elementId);
    	var val = inputCmp.get("v.value");
    	if(!val){
    		inputCmp.set("v.errors", [{message:"This field is required"}]);
            return;
        } else {
            inputCmp.set("v.errors", null);
        }
	},
    
     buildDescriptionTemplate : function(component, event, helper) {
        var desc;
        var rt = component.get("v.newCase.RecordTypeId").substring(0, 15);
        if(rt == '01232000000M4W2'){
            desc = "1.Check the Coverage Matrix if support for the product already exists." + "\n\n" +
            "Coverage Matrix - SharePoint - https://skyboxsecurityinc.sharepoint.com/sites/Solutions/_layouts/15/guestaccess.aspx?guestaccesstoken=EQpSICb4FMt70XAAxkKRe7XzGl38kZaxxkW1oiakq0o%3d&docid=2_0f44be4352938464a81be4624eaa304aa&rev=1" +
            "\n\n" +
            "2.Please Fill in the following details:" + "\n\n" + 
            
            " A.Name of product."  + "\n\n" +
            
            " B.Product Family."+ "\n\n" +
            
            " C.Vendor." + "\n\n" +
            
            " D.Version requested for support." + "\n\n" + 
            
            " E.Attach a url to the vendor product website." + "\n\n" +
            " F.Write a detailed example of the requested support." + "\n\n" +
            
            " G.Business use case: General and customer specific." + "\n\n" +
            
            " H.Skybox Modules that will support this product: FA,NA,VC." + "\n\n" +
            
            " I.Requested support level (Basic/Standard – refer to matrix)"+ "\n\n" + 
            " Please note that development time for support will increase according to the" + "\n" +
            " requested support level." + "\n\n" + 
            
            " J.Attach a full textual configuration file of the product." + "\n\n" +
            
            " K.If the device is connected via SSH/Telnet protocols enclose a putty/CRT connection" + "\n" +
            " log that includes the initial authentication phase and configuration, routing table" + "\n" + " and interface list collection."  
        } else {
            desc = "Issue and Background Info" + "\n" +
            "=========================" + "\n\n\n" + 
            
            
            "Steps to Reproduce:" + "\n" + 
            "===================" + "\n\n\n" + 
            
            
            "Expected Result:" + "\n" + 
            "================" + "\n\n\n" +
            
            "Actual Result:" + "\n" + 
            "==============" + "\n\n\n" + 
            
            "Logs/Model Attache (Yes/No)?" + "\n" +
            "Note: Providing Logs and Model will expedite handling of this case!" + "\n" + 
            "=====================================================" + "\n\n\n" + 
            
            "Issue Severity (Critical, High, Medium, Low)?" + "\n" +
            "===================================" + "\n\n\n" + 
            
            "Number of affected users:" + "\n" +
            "==============================" + "\n\n\n";
        }
        component.set("v.description", desc);
	}        
})