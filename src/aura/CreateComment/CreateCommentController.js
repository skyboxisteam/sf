({
	doInit : function(component, event, helper) {
		var oId = window.location.href.split("=").pop();
        if (oId != null) {
            component.set("v.caseId", oId);
        }
        
        var action = component.get("c.getCase");
        action.setParams({"recordId": component.get("v.caseId")});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                var c = response.getReturnValue();
                component.set("v.case", c);
            } else {
                console.log('There was a problem : '+response.getError());
            }
        });
        $A.enqueueAction(action);
	},
    cancel : function(component,event,helper){
        window.location = "/s/case/"+component.get("v.caseId");
    }, 
    createComment : function(component, event, helper){
        var errorFlag = false;
        var inputCmp = component.find("comment");
        var val = inputCmp.get("v.value");
        if(!val){
            inputCmp.set("v.errors", [{message:"Complete this field"}]);
            errorFlag = true;
        } else {
            inputCmp.set("v.errors", null);
        }
        if(errorFlag){
            return;
        }

        var action = component.get("c.insertComment");
        var newComment = {};
        newComment.ParentId = component.get("v.caseId");  
        newComment.IsPublished = true;  
        newComment.CommentBody = component.find("comment").get("v.value");
        action.setParams({"stringJson": JSON.stringify(newComment)});
        action.setCallback(this, function(response) {
            window.location = "/s/case/"+component.get("v.caseId");
        });
        $A.enqueueAction(action);
    }
})