({    
    callUpdateCase : function(component,event,helper){
        var action = component.get("c.closeCaseAndInsertComment");
        var newComment = {};
        newComment.ParentId = component.get("v.caseId");  
        newComment.IsPublished = true;  
        newComment.CommentBody = component.find("comment").get("v.value");
        
        if(newComment.CommentBody == null || newComment.CommentBody == ''){
            alert('You have to fill the comment to close the Case.');
        }else{
            action.setParams({"stringJson": JSON.stringify(component.get("v.case")),"commentJson": JSON.stringify(newComment)});

            action.setCallback(this,function(response){                        
                var state = response.getState();
                console.log('state: ' + state);
                console.log(response.getReturnValue());
                if (state === 'SUCCESS'){     
                    window.location = "/s/case/"+component.get("v.caseId");
                }           
            });
            $A.enqueueAction(action);   
		}            
    }, 
    cancel : function(component,event,helper){
        window.location = "/s/case/"+component.get("v.caseId");
    },    
    doInit : function(component, event, helper) {
		var oId = window.location.href.split("=").pop();
        if (oId != null) {
            component.set("v.caseId", oId);
        }
        
        var action = component.get("c.getCase");
        action.setParams({"recordId": component.get("v.caseId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                var c = response.getReturnValue();
                component.set("v.case", c);
                window.setTimeout(
                    $A.getCallback(function () {
                        /*var button = component.find("mybtn");
                        if(c.Status == "New" || c.Status == "Awaiting Customer"){
                            button.set("v.label","Submit");
                        }else if(c.Status == "Closed"){
                            button.set("v.label","Closed");
                        }else {
                            button.set("v.label","Withdraw Case");
                        }*/
                    })
                );
            } else {
                console.log('There was a problem : '+response.getError());
            }
        });       
        $A.enqueueAction(action);
        
        /*var action = component.get("c.getCaseReason");
        var opts = [];
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                var allValues = response.getReturnValue();
                console.log('allValues');
 				console.log(allValues);
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.find("InputSelectDynamic").set("v.options", opts);
            } else {
                console.log('There was a problem : '+response.getError());
            }
        });       
        $A.enqueueAction(action);
        f
        
        var action = component.get("c.getCaseStatuses");
        var statusOpts = [];
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                var allValues = response.getReturnValue();
                console.log('allValues');
 				console.log(allValues);
                if (allValues != undefined && allValues.length > 0) {
                    statusOpts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    statusOpts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.find("SelectStatus").set("v.options", statusOpts);
            } else {
                console.log('There was a problem : '+response.getError());
            }
        });       
        $A.enqueueAction(action);*/
	}
   
     
})