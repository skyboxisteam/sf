({
	redirect : function(component, event, helper) {        
        var action = component.get("c.getCaseForEscalation");
        action.setParams({"recordId": component.get("v.caseId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                var c = response.getReturnValue();
                if(c.Escalated__c){
                    document.getElementById('error-log').style.display = 'block';
                    document.getElementById('msg-body').innerHTML = 'Escalated already by '+c.escalated_by__r.Name+'.';
                    document.getElementById('popup-background').style.display = 'block';
                }else{
                    var urlEvent = $A.get("e.force:navigateToURL");
        
                    urlEvent.setParams({
                        "url": "/createcommment?caseId="+component.get("v.caseId")
                    });
                    urlEvent.fire();
                }
            } else {
                console.log('There was a problem : ');
                console.log(response.getError());
                document.getElementById('error-log').style.display = 'block';
                document.getElementById('msg-body').innerHTML = 'There was a problem : '+response.getError();
                document.getElementById('popup-background').style.display = 'block';
            }
        });       
        $A.enqueueAction(action);
	},
    closePopUp : function(){
        document.getElementById('error-log').style.display = 'none';
        document.getElementById('popup-background').style.display = 'none';
    }
})