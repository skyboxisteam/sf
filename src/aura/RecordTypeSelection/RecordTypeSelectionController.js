({
    doInit : function(component,event,helper){
       helper.callToServer(
            component,
            "c.findRecordTypes",
            function(response) {
                //alert(JSON.parse(response));
                var jsonObject=JSON.parse(response);
                component.set('v.recordTypeList',jsonObject);  
                component.set('v.selectedRecordType',jsonObject[0].recordTypeId); 
            }, 
            {objName: component.get('v.objType')}
        ); 
    },
     
    onChange : function(component, event, helper) {
		var value = event.getSource().get("v.text");
        component.set('v.selectedRecordType', value);
        
	},
    
    defaultCloseActionChild : function(component, event, helper) {
         var cmpTarget = component.getEvent('sampleComponentEvent');
         cmpTarget.fire();
    },
    
    onconfirm : function(component, event, helper){
        var selectedId = component.get("v.selectedRecordType");
        var selectedEvent = $A.get("e.c:recordTypeSelected");
    	selectedEvent.setParams({recordTypeId: selectedId}).fire();

    }
})