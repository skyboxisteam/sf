({
	doInit : function(component, event, helper) {
		var action = component.get("c.MakeCallOut");
        action.setParams({ 
            "caseId" : component.get("v.recordId")
        });
        action.setCallback(this, function(data){
            var myMap = data.getReturnValue();
            var returnVal = Object.keys(myMap).map(function(key){return {name: key, value: myMap[key]} });
        	component.set("v.files", returnVal); 
        });
        $A.enqueueAction(action);
	}
})