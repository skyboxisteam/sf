@isTest
private class CaseCommentTriggerTest {
    
    @testSetup static void testSetup(){
    
        UserRole R = [SELECT Id FROM UserRole WHERE DeveloperName = 'TLS' LIMIT 1];
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User U = new User(LastName = 'User 1 last name', 
                          FirstName = 'User 1 first name', 
                          WDSB_Approval_Territory__c = 'US', 
                          Alias = 'User1', 
                          Email = 'User1@skyboxsecurity.com', 
                          Username = 'User1@skyboxsecurity.com', 
                          CommunityNickname = 'User1NickName', 
                          Theater__c = 'APAC', 
                          ProfileId = profileId.id, 
                          TimeZoneSidKey = 'GMT', 
                          LanguageLocaleKey = 'en_US', 
                          EmailEncodingKey = 'UTF-8', 
                          LocaleSidKey = 'en_US', 
                          UserRoleId = R.Id
                          );
        insert U;                                         
    }
    
    @isTest static void testOne(){
    
        User us = [SELECT Id FROM User WHERE UserRole.DeveloperName = 'TLS' LIMIT 1];
        system.runas(us)
        { 
        Case C = new Case(Status = 'New', 
                          Severity1__c = '1', 
                          Subject = 'Test', 
                          Version_build__c = '10', 
                          Module__c = 'License',
                          OwnerId = us.Id
                          );
        insert C;   
          
        CaseComment CM = new CaseComment(CommentBody = 'test',
                                         ParentId = C.Id
                                         );
        insert CM;
        update C;
        }
    }
}