@isTest
private class CloneEngineerUserLookupTest {
	
	@isTest static void test_method_one() {
		Account testAccount = new Account();
		testAccount.Name = 'Test Account';
		testAccount.BillingCountryCode = 'UA';
		testAccount.Theater__c = 'EMEA';
		insert testAccount;
		Deployment__c testDeployment = new Deployment__c();
		//testDeployment.Name = 'Test Deployment';
		testDeployment.PS_Owner__c = UserInfo.getUserId();
		testDeployment.Account__c = testAccount.Id;
		testDeployment.Deployment_Type__c = 'Project';
		testDeployment.Project_Name__c = 'Test Project';
		insert testDeployment;
	}
}