public without sharing class Ctrl_AssignCases 
{
    public Case cas{get;set;}
    public List<SelectOption> departmentOpt {get;set;}
    public String selectedDepartment{get;set;}
    public String redirectUrl {public get; private set;}
    private ApexPages.StandardController stdController;
    public Boolean shouldRedirect {public get; private set;}
    public list<case> RelevantCases; 
    public static boolean run = true;
    private final string solvepending = 'Solve Pending Relesed';
    private final string support = 'Support';
    private final string Solutions = 'Solution';
    private final string Tier4 = 'Tier 4';
    private final string Tier3 = 'Tier 3';
    private final string PM = 'PM';
    private final string PS = 'PS';
    private final string CurrentUser = 'CurrentUser';
     private final string LastOwner = 'LastOwner';
     private final string OwnerRoleName = 'Owner';
    private map<id,case> oldMap = new map<id,case>();
    private map<string,string> MapRoleTeamNameToID =  new  map<string,string>();
     Map<string,string> statusPerDepartment = new Map<string,string> {support => 'Assigned to Support',
                                                                     Solutions => 'Assigned to Solutions',
                                     Tier4 => 'Assigned to Tier 4',
                                     Tier3 => 'Assigned to Tier 3', 
                                     PM=>'Assigned to PM',
                                     PS=>'Assigned to PS' };
                                     
  Map<string,string> lastOwnerPerDepartment = new Map<string,string> {support => 'Last Support Owner',
                                                                     Solutions => 'Last Solution Owner',
                                     Tier4 => 'Last Tier 4 Owner',
                                     Tier3 => 'Last Tier 3 Owner', 
                                     PM=>'Last PM Owner',
                                     PS =>'Last PS Owner'};
                                     
    Map<string,string> statusPerOwnerRole = new Map<string,string> {'PSEngineer' => 'Assigned to PS',
                                                                    'Regional_CC_Manager' => 'Assigned to Support',
                                                                    'Support_Manager' => 'Assigned to Support',
                                                                    'Support_Engineer' => 'Assigned to Support',
                                                                    'Solution_Manager' => 'Assigned to Solutions',
                                                                     'Tier_1_Support_Engineer' => 'Assigned to Support',
                                     'TLS' => 'Assigned to Tier 4',
                                      'Tier_3_Engineer' => 'Assigned to Tier 3', 
                                        'VP_Products'=>'Assigned to PM',
                                                                    'ProductManager'=>'Assigned to PM' };
                                                                    
    Map<string,string> roleToLastOwner = new Map<string,string>    {'PSEngineer' => 'Last PS Owner',
                                                                    'Regional_CC_Manager' => 'Last Support Owner',
                                                                    'Support_Manager' => 'Last Support Owner',
                                                                    'Support_Engineer' => 'Last Support Owner',
                                                                    'Solution_Manager' => 'Last Solution Owner',
                                                                    'TLS' => 'Last Tier 4 Owner',
                                                                    'Tier_3_Engineer' => 'Last Tier 3 Owner', 
                                                                    'Tier_1_Support_Engineer' => 'Last Tier 1 Owner',
                                                                    'VP_Products'=>'Last PM Owner',
                                                                    'ProductManager'=>'Last PM Owner' };
     
    Map<string,string> mapTeamToQueues = new Map<string,string>     {support => 'Incoming_Case_Support',
                                                                   Solutions => 'Incoming_Cases_Solutions',
                                                                    Tier4 => 'IncomingCasesRD',
                                                                    Tier3 => 'Incoming_Cases_Tier_3',
                                                                    PM => 'IncomingCasesPM' ,
                                                                    PS => 'Incoming_Cases_PS' };
                                                                   
    set<string> setLastOwnerRoles = new set<string>               {'Last Support Owner',
                                                                   'Last Solution Owner',
                                                                   'Last Tier 4 Owner',
                                                                   'Last Tier 3 Owner', 
                                                                   'Last PM Owner',
                                                                   'Last PS Owner',
                                                                   'Last Tier 1 Owner'};
                                                                                         
                                                                
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------Constructor for VF-----------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

    public Ctrl_AssignCases(ApexPages.StandardController stdCon) 
    {
       // run = false;
        this.stdController = stdCon;
        shouldRedirect = false;
        MapRoleTeamNameToID = getRoleTeamMap();
         if(!test.isRunningTest())
        {
            stdCon.addFields(new list<String>{'ID'});
            cas = (Case) stdCon.getRecord();
            cas = [select ID,type,OwnerId,Status,Owner.Name,Owner.UserRole.Name,Owner.UserRoleID,Owner.UserRole.DeveloperName,Severity1__c,Rejection__c,
                          (Select Id,Member.UserRoleid,Member.isActive,Member.UserRole.DeveloperName,TeamRoleId,TeamRole.Name,MemberId 
                           From TeamMembers) 
                   From Case 
                   Where ID =: cas.ID];
                 
 
         }
        else
        {
            //For test select the case you created
            cas = [Select ID,type,OwnerId,Status,Owner.Name,Owner.UserRoleID, Owner.UserRole.Name,Owner.UserRole.DeveloperName,Severity1__c,Rejection__c,
                          (Select Id,Member.UserRole.DeveloperName,Member.isActive,TeamRoleId,TeamRole.Name,MemberId 
                           From TeamMembers) 
                   From Case 
                   limit 1];
          }

        setDepartmentOpt( cas.Owner.UserRole.DeveloperName);
       
    }
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------Constructor for assign field-------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

    public Ctrl_AssignCases(list<case> listCases ) 
    {
     
       RelevantCases = [select ID,type,OwnerId,Status,assign__c,Severity1__c,Owner.Name,Owner.UserRole.Name,Owner.UserRoleID,Owner.UserRole.DeveloperName,Rejection__c,(Select Id,MemberId,Member.isActive,TeamRoleId,Member.UserRole.DeveloperName,TeamRole.Name From TeamMembers) from Case Where ID in : listCases];
       MapRoleTeamNameToID = getRoleTeamMap();
            
    }
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------runAssignmentProcess---------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
    
    public void runAssignmentProcess()
     {
        if(run == true)
      {       
         try{
           for( Case tmpCase : RelevantCases)       
             {
              selectedDepartment = tmpCase.assign__c;
              cas = tmpCase;
              assignCase();
             }
           }
          catch(Exception ex)
           {
             system.debug('Exception ' + ex.getMessage() + ex.getStackTraceString());
             throw ex;
           } 
     }
    }
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------runChangeOwnerProcess--------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
     
      public void runChangeOwnerProcess(map<id,user> newOwners, map<id,user> oldOwners, map<id,case> oldmap )
     {
         //if(run == true)
          // {
             try{              
              for( Case tmpCase : RelevantCases)       
                   {
                     cas= tmpCase;
                     
                     if(oldOwners.get(oldMap.get(tmpCase.id).ownerid) != null )
                        {
                         deleteRelevantTeam_memeber(oldMap.get(tmpCase.id).ownerId, getOldDevName(oldOwners,oldMap,tmpCase.id));                                                                                       
                         if(roleToLastOwner.get(getOldDevName(oldOwners,oldMap,tmpCase.id))!=null &&  oldOwners.get(oldMap.get(tmpCase.id).ownerId).isactive  &&  oldOwners.get(oldMap.get(tmpCase.id).ownerId).contactId == null)   
                            addTeamMember('N/A',oldMap.get(tmpCase.id).ownerId,getOldDevName(oldOwners,oldMap,tmpCase.id));                      
                        }
                      if(tmpCase.owner.UserRoleID != null )
                        {                                     
                         deleteRelevantTeam_memeber(tmpCase.ownerId, tmpCase.owner.UserRole.Developername);
                         if(newOwners.get(tmpCase.ownerid).isActive && newOwners.get(tmpCase.ownerid).contactId == null )
                            addTeamMember(currentuser,tmpCase.ownerId, tmpCase.owner.UserRole.Developername);    
                         }
                    }
                 }
               catch(Exception ex)
               {
                 system.debug('Exception ' + ex.getMessage() + ex.getStackTraceString());
                 throw ex;
               }
           // }
         //else
          run = false;
     }

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------setDepartmentOpt-------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

    private void setDepartmentOpt(string OwnerRole)
    {
        boolean lastPSExist = false;
        boolean lastOwnerExist = false;
        for(caseTeamMember ctMem : cas.TeamMembers)
        {
          if(ctMem.TeamRole.Name == 'Last PS Owner' && ctMem.Member.isActive)
            lastPSExist = true;
          if(setLastOwnerRoles.contains(ctMem.TeamRole.Name)  && ctMem.Member.isActive)
           lastOwnerExist= true;
        }
        
        departmentOpt = new list<SelectOption>();
        departmentOpt.add(new SelectOption(CurrentUser,'Current User'));
        departmentOpt.add(new SelectOption(support,'Support'));
        departmentOpt.add(new SelectOption(PM,'PM'));
        if(lastPSExist)
          departmentOpt.add(new SelectOption(PS,'PS'));
        departmentOpt.add(new SelectOption(Solutions,'Solutions'));
        departmentOpt.add(new SelectOption(Tier3,'Tier 3'));
        departmentOpt.add(new SelectOption(Tier4,'Tier 4'));
        
        if(lastOwnerExist && OwnerRole == 'TLS')
           departmentOpt.add(new SelectOption(LastOwner,'Previous Owner'));
       
    }


//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------execute_btn------------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

    public PageReference executeBtn()
    {
      run = false;
      try
      {
        MapRoleTeamNameToID = getRoleTeamMap();    
   //   updateLastOwner();
        assignCase();
        shouldRedirect = true;
        redirectUrl = stdController.view().getUrl();
      }
      catch(System.DmlException e) 
      {
          //Show in the apex:pageMessages element all the VR messages 
          //ApexPages.addMessages(e);
          cas.addError(e.getDmlMessage(0));
      }
      catch(Exception e) 
      {
          //Show in the apex:pageMessages element
          cas.addError(e);
      }
      return null;
    }

    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------deleteRelevantTeam_memeber---------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//     
  
 public void deleteRelevantTeam_memeber(id userId, string developerName)
 {
   set<CaseTeamMember> setTeamToDelete = new set<CaseTeamMember>();
   list<CaseTeamMember> teamToDelete = new list<CaseTeamMember>();
   for(caseTeamMember ctMem : cas.TeamMembers)
   {
     if(ctMem.TeamRole.Name != 'Sales Engineer' && (ctMem.MemberId == userId || ctMem.TeamRole.Name == roleToLastOwner.get(developerName)))
      {      
       setTeamToDelete.add(ctMem);      
      }
   }
     teamToDelete.addAll(setTeamToDelete);
     
     if(!teamToDelete.isempty())
     try{
       delete teamToDelete;
     }
     catch(Exception ex)
     {
     }
 }
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------addTeamMember----------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    

  private void addTeamMember(string origin, id userID,string roleDevloperName)
   {
        id roleTeamId;
       if(origin == currentUser)
         roleTeamId = MapRoleTeamNameToID.get(OwnerRoleName);
        else
        roleTeamId = MapRoleTeamNameToID.get(roleToLastOwner.get(roleDevloperName));
        
          if(!String.isBlank(roleTeamId))
                {
                  try{
                         insert new CaseTeamMember(MemberId = userId,ParentId = cas.Id,TeamRoleId = roleTeamId);
                      }
                    catch(Exception ex)
                     {
                     }
                }
         
   }
    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------assignCase-------------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//   
 
    private void assignCase()
    {
       Id casOwnerToSign;
       boolean errorPS = false;
         boolean foundUserToSign = false;
         String casStatusToUpdate;
         String userRoleDeveloperName;   
         string RoleDeveloperName;  
              
         try
          {    
          if(selectedDepartment == CurrentUser)
            {
                userRoleDeveloperName = [select developerName from UserRole  where id =: userinfo.getUserRoleId() limit 1].developerName;
            }
           else
           {
            cas = [select ID,type,OwnerId,Owner.UserRole.DeveloperName,Status,Owner.Name,Owner.UserRole.Name,Rejection__c ,(Select Id,Member.UserRole.DeveloperName,Member.isActive,TeamRoleId,TeamRole.Name,MemberId From TeamMembers order by createddate desc) from Case Where ID =: cas.ID];
            for(CaseTeamMember ctMem : cas.TeamMembers)
            {
             
                if(ctMem.Member.isActive && ((selectedDepartment == LastOwner && setLastOwnerRoles.contains(ctMem.TeamRole.Name ))  ||
                   lastOwnerPerDepartment.get(selectedDepartment) == ctMem.TeamRole.Name))
                   {
                    casOwnerToSign = ctMem.MemberId;
                    foundUserToSign = true;
                    RoleDeveloperName = ctMem.Member.UserRole.DeveloperName;
                    break;
                   }
     
            }
           }
            if(!foundUserToSign)
              if(selectedDepartment == PS)
                {  
                  cas.adderror('There is no active Last PS owner to this case');
                }
              else
                casOwnerToSign = findRelevantQueue();
              if(selectedDepartment == Tier4 && cas.type == null )
                 cas.adderror('Please fill in case type');   
            
            cas.OwnerId = selectedDepartment != CurrentUser?casOwnerToSign:userinfo.getUserId();     
            cas.Status = findStatus(selectedDepartment != CurrentUser?RoleDeveloperName:userRoleDeveloperName );
            cas.assign__c = null; 
            system.debug('@@@assignCase:cas: ' + cas); 
            system.debug('@@@assignCase:cas.Rejection__c: ' + cas.Rejection__c); 

            update cas;// will trigger the case trigger that would call to "runChangeOwnerProcess"
        
         
       }
        catch(Exception ex)
        {
            system.debug('Exception ' + ex.getMessage() + ex.getStackTraceString());
                throw  ex;
        }
    }

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------findRelevantQueue------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//   
        
    private Id findRelevantQueue ()
       {
                Map<string,string> mapQueuesToId = new Map<string,string>();                
                list<Group> listOfQueues = [Select Id, Name, DeveloperName From Group Where Type = 'Queue' and DeveloperName in: mapTeamToQueues.values()];                                            
                for(Group gr : listOfQueues)
                    mapQueuesToId.put(gr.DeveloperName, gr.id);
                    
                return mapQueuesToId.get(mapTeamToQueues.get(selectedDepartment));
       }

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------checkStatusManuallyChanged---------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//   
      
  private Boolean checkStatusManuallyChanged()
     {
        if(cas.status == 'Solved Released')   
      return true;   
      return false;
     }   

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------findStatusNeededToChange-----------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//   
    
   private string findStatusNeededToChange()
      {
       string status = '';
       string currentUserRoleDeveloperName ='';
        Bug_To_Case__c btc = new Bug_To_Case__c();
               
            try{
              btc = [select Bug_Status__c from Bug_To_Case__c where Case__c =: cas.id  order by createddate desc limit 1];
        
             if(btc.Bug_Status__c == 'VERIFIED')
               {
                 currentUserRoleDeveloperName = [select DeveloperName from userrole Where id =:userinfo.getUserRoleId()].DeveloperName;
                   if(currentUserRoleDeveloperName == 'TLS')
                       status = 'Solve Pending Relesed';
                 }
                }
          catch(Exception ex)
        {
            
        } 
        return status;
      } 

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------findStatus-------------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//   
  
        
    private string findStatus( string userRoleDeveloperName)
      {
         String findStatusChange ='';//= findStatusNeededToChange();  
  //       list<user> roleid = [select userroleid from user where id =: newOwnerid limit 1  ];
    //     list<userrole> devRole = [select DeveloperName from userrole where id =: roleid.get(0).userroleid limit 1 ];
           return findStatusChange!=''?
                  findStatusChange:checkStatusManuallyChanged()?
                  cas.Status:selectedDepartment != CurrentUser &&  selectedDepartment != LastOwner?
                  statusPerDepartment.get(selectedDepartment):statusPerOwnerRole.containsKey(userRoleDeveloperName)?
                  statusPerOwnerRole.get(userRoleDeveloperName):cas.Status;
  
      }

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------getRoleTeamMap---------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//   
   private map<string,string> getRoleTeamMap()
   {
       map<string,string> mapRoleName =  new  map<string,string>();
      list<CaseTeamRole> AllTeamRole = [select id,name from CaseTeamRole];
       
       for(CaseTeamRole ctr : AllTeamRole )
          mapRoleName.put(ctr.name,ctr.id);
          
       return mapRoleName;
   }   

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------getOldDevName----------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//   
 private string getOldDevName (map<id,user> oldOwners, map<id,case> oldmap, id caseid)
  {
    return oldOwners.get(oldMap.get(caseid).ownerid).UserRole.Developername;
  }
 
}