public with sharing class LeadHandler {
	static  map<id,RecordType> mapRecordType = new   map<id,RecordType> ([select id, DeveloperName from recordtype where SobjectType  = 'Lead']);
	static Set<String> rtSet = new Set<String>{'Deal_Registration'};
	static Set<String> leadSourceDetailSet = new Set<String>{'Magentrix Partner Portal'};

	

	public LeadHandler() {}
			
	  	public static void fieldUpdates(List<Lead> newLeadList, Map<Id, Lead> oldLeadList)
	  	{
	  		for( Lead lead : newLeadList)
		    {
		    	updateNextExpirationEmail(lead, oldLeadList);
		    }
		  
	  	}
		  //Author: Rina Nachbas      Date:  07.11.18
		  //Task:   MP-63             Event: befor update
		  //Desc:   If the new date is bigger by 7 days from previous value change "Next Expiration Email" to "Next: 7 Days Expiration"
		  //		If the new date is smaller than 7 days from previous value change "Next Expiration Email" to "Next: Expiration"
		  private static void updateNextExpirationEmail(Lead lead, Map<Id, Lead> oldLeadList){
		      if(lead.Magentrix_Partner_Expiration_Date__c >= System.today() && lead.Magentrix_Partner_Expiration_Date__c != oldLeadList.get(lead.Id).Magentrix_Partner_Expiration_Date__c)
		      {
		      	if(lead.Magentrix_Partner_Expiration_Date__c >= System.today().addDays(7) && lead.Next_Expiration_Email__c != 'Next: 7 Days Expiration'){
		      		lead.Next_Expiration_Email__c = 'Next: 7 Days Expiration';
		      	}
		      	else if(lead.Magentrix_Partner_Expiration_Date__c < System.today().addDays(7) && lead.Next_Expiration_Email__c != 'Next: Expiration'){
		      		lead.Next_Expiration_Email__c = 'Next: Expiration';
		      	}
		      }
		  }
		  //Author: Rina Nachbas      Date:  07.11.18
		  //Task:   MP-63             Event: after Insert, after update
		  //Desc:   When Deal Registration is Created and Lead source detail  is “Magentrix Partner Portal” send email to the account team
		  	public static void sendAlert(List<Lead> newLeadList){
		  		sendAlert(newLeadList,null);
		  	}
		  	public static void sendAlert(List<Lead> newLeadList, Map<Id, Lead> oldLeadList){
			    List<Lead> leadToAlert = new List<Lead>();
			    //List<String> roles = new List<String>{'Account Manager', 'Sales Engineer.'};
			    SendEmail sendEmail = new SendEmail();
			    Map<Id, String>  leadAlertMap = new Map<Id, String>();
			    
			    for( Lead lead : newLeadList)
			    {
			      if(leadSourceDetailSet.contains(lead.Lead_Source_Detail__c ) && rtSet.contains(mapRecordType.get(lead.recordtypeId).DeveloperName))
			      {
			      	if(oldLeadList == null)//insert
			      	{
			        	leadToAlert.add(lead);
			    		leadAlertMap.put(lead.Id, 'New Deal Registration Alert-Internal');
			      	}
			      	//deal reg status changed to approved
			        else if(lead.Deal_Reg_Status__c == 'Accepted' && lead.Deal_Reg_Status__c != oldLeadList.get(lead.Id).Deal_Reg_Status__c)
			        {
			        	leadToAlert.add(lead);
			    		leadAlertMap.put(lead.Id, 'Approved Deal Registration Alert-Internal');
			        }
			        //Lead deal reg status change to Decline
			        else if(lead.Deal_Reg_Status__c == 'Rejected' && lead.Deal_Reg_Status__c != oldLeadList.get(lead.Id).Deal_Reg_Status__c)
			        {
			        	leadToAlert.add(lead);
			    		leadAlertMap.put(lead.Id, 'Declined Deal Registration Alert-Internal');
			        }
			        //Lead deal reg was converted
			        else if(lead.IsConverted && lead.IsConverted != oldLeadList.get(lead.Id).IsConverted)
			        {
			        	leadToAlert.add(lead);
			    		leadAlertMap.put(lead.Id, 'Converted Deal Registration-Internal');
			        }
			        
			      }
			    }

			    system.debug('##sendAlert: leadToAlert: ' + leadToAlert);

			    for( Lead lead : leadToAlert)
			    {
			      //add recipients
			      //if(lic.Opportunity__c != null){
			      //  sendEmail.recipients = new List<String> {lic.Opportunity__r.Owner.Email};
			      //}
			      sendEmail.templateName = leadAlertMap.get(lead.Id);
			      sendEmail.whatId = lead.Id;
			      sendEmail.sendEmailToAccountTeam(lead.Name__c);//partner accountId
			    }
			}



}