public with sharing class CustomerAnalyzer {
	private ApexPages.StandardController ctrl;
	public Account  customer;
	
	public Account getCustomer(String id) {
	  	List<Account> accounts = [SELECT Id, Name FROM Account WHERE Id =: id];
	  	if (accounts != null) {
			return accounts[0];
	  	}
	  	else
	  		return null;
	}
	
	public CustomerAnalyzer(ApexPages.StandardController ctrlParam){
	  	ctrl = ctrlParam;
	  
	  	customer = getCustomer(ApexPages.currentPage().getParameters().get('id'));
	}

	public CustomerAnalyzer(String id) {
		customer = getCustomer(id);	
	}
	
	// 
	public Map<String, Integer> getPerpetualLicenses() {
		List<AggregateResult> results = [select sum(FCA_FW_nodes__c) fa, sum(NA_Nodes__c) na, sum(Change_Manager__c) cm, sum(VC_Assets__c) vc, sum(TM_Assets__c) tm
				from Contract where AccountId =: customer.id and RecordTypeId = '012600000009W1T'];
				
		if (results == null || results.size() == 0)
			return null;
			
		Map<String, Integer> perpetualLicenses = new Map<String, Integer>();

		perpetualLicenses.put('FA', getNumber(results[0].get('fa')));
		perpetualLicenses.put('NA', getNumber(results[0].get('na')));
		perpetualLicenses.put('CM', getNumber(results[0].get('cm')));
		perpetualLicenses.put('VC', getNumber(results[0].get('vc')));
		perpetualLicenses.put('TM', getNumber(results[0].get('tm')));
		
		return perpetualLicenses;
	}
	
	private Integer getnumber(Object o) {
		return (o == null) ? 0 : Integer.valueOf(o);
	}
}