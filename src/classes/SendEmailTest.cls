@isTest
private class SendEmailTest {
	static ClsObjectCreator objCreator = new ClsObjectCreator();

	@testSetup static void setupData() 
	{
		Test.startTest();
		User myAdminUser = [select id from user  where profile.Name='System Administrator' and isActive=true limit 1];
		//create Account
		Account acc = objCreator.createAccount('TestAccount');
        Account acc2 = objCreator.createAccount('TestAccount2');
        AccountTeamMember accTeam = objCreator.createAccountTeam(acc2.Id);//new AccountTeamMember(UserId=myAdminUser.Id, TeamMemberRole='Area VP', accountId=acc2.Id);// objCreator.createAccountTeam('TestAccount2');
        //insert accTeam;
        //create Contact
		Contact con = ClsObjectCreator.createContact('Test', acc.Id);
		//create opp
		Opportunity opp =  objCreator.createOpportunity(acc);
		Opportunity opp2 =  objCreator.createOpportunity(acc);
		Test.stopTest();
	}
	@isTest
	public static void SendEmailTest() {
		sendEmail email = new sendEmail();
        email.templateName = 'New Deal Registration Alert-Internal';
        List<Account> accList = [select Id from Account];
		//email.sendEmailToTemplate('Deal Registration Alerts','0016000000M3Rh0','0050e0000069nqw', new List<String>{'rina.nachbas@skyboxsecurity.com'} );
		Test.startTest();
		email.sendEmailToAccountTeam(accList[0].Id);
        email.sendEmailToAccountTeam(accList[1].Id);
		Test.stopTest();
	}
	
}