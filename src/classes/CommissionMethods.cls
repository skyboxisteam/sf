public without sharing class CommissionMethods {
 
  public static string multiType ='Multi Year Adjustment';
  public static string multiCommissionFactor ='CommissionFactor';
  public static string multiQuoteFactor ='QuoteFactor';
  public static string collectionType ='Collection Days Limit';
  public static string collectionDays ='DaysLimit';
  public static string maxAmountType ='Max Amount';
  public static string supportRenewal = 'Support Renewal';
  public static string maxAmount ='MaxAmount';
  public static string perpetualField = 'Perpetual';
  public static string perpetualValue = 'Perpetual';
  public static string subscriptionField = 'Subscription';
  public static string subscriptionValue = 'Subscription';
  public static string multiYearField = 'Multi Year';
  public static string multiYearValue = 'Multi Year';
  public static string supportRenewalmultiYear = 'Support Renewal Multi Year';
  public static string recurringIncentive = 'Recurring Incentive';
  public static string professionalServiceslValue = 'Professional Services'; 


public static boolean checkRuleExist(id planId,string typeRule , map<string,Commission_Rule_Assignment__c> mapComRule )
{
    return  mapComRule.get(planId + typeRule) == null? false: true;
}
 
/*public static decimal getRuleValue(id planId,string typeRule ,string field,  map<string,Commission_Rule_Assignment__c> mapComRule )
{
  //  system.debug('Value - '+mapComRule.get(planId + typeRule) + 'Map - ' + mapComRule);
    return  mapComRule.get(planId + typeRule) == null? 0: parseJSON(mapComRule.get(planId + typeRule).Rule__c).get(field);
}*/

public static decimal getRuleValue(id planId,string typeRule ,string field,  map<string,Commission_Rule_Assignment__c> mapComRule )
{
  //  system.debug('Value - '+mapComRule.get(planId + typeRule) + 'Map - ' + mapComRule);
     try
        {
          return decimal.valueof(getRuleString(planId,typeRule ,field, mapComRule));
        }
    catch(Exception e){

          return 0 ; 
    }
}

public static Date getRuleDate(id planId,string typeRule ,string field,  map<string,Commission_Rule_Assignment__c> mapComRule)
 {
  try
     {
      return Date.valueof(getRuleString(planId,typeRule ,field, mapComRule));
     }
     catch(Exception e)
     {
      system.debug(e.getMessage());
      return Date.valueof('1900-01-01');
     }
 }

public static string getRuleString(id planId,string typeRule ,string field,  map<string,Commission_Rule_Assignment__c> mapComRule)
 {
   return mapComRule.get(planId + typeRule) == null? '' : parseJSON(mapComRule.get(planId + typeRule).Rule__c).get(field);
 }


public static Map<string,string> parseJSON (string jstr)
  {
    
      Map<string,string> mapValue = new  Map<string,string> ();
      JSONParser parser =JSON.createParser(jstr);
         while (parser.nextToken() != null) {                               
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                    String fieldName = parser.getText();
                    parser.nextToken();
                    mapValue.put(fieldName,parser.getText());
                      
                     }
                }
        
            return mapValue;    
  }



  public static boolean isMulti(Commission_Record__c cr, Booking_Schedule__c bs, map<string,Commission_Rule_Assignment__c> mapComRule)
  {
   transient multiParams params = new multiParams(cr.Commission_Plan__c,bs,bs.Support_Renewal_Amount__c);
   return  cr.Type__c == Label.Regular &&
           bs.Type_Family__c == Label.MultiYear && 
           CommissionMethods.checkRuleExist(cr.Commission_Plan__c,Label.MultiYear,mapComRule) &&
          !isMultiFullCredit(params , mapComRule);

  }

  public static Boolean isMultiFullCredit(multiParams params, map<string,Commission_Rule_Assignment__c> mapComRule)
  {
    return CommissionMethods.checkRuleExist(params.planid,Label.MultiYearFullCredit,mapComRule) && 
           params.manualOverride ||
          (params.bsType == supportRenewalmultiYear &&
           params.amount >=  CommissionMethods.getRuleValue(params.planid,Label.MultiYearFullCredit,Label.OpportunityAmount,mapComRule) && 
           params.months >= CommissionMethods.getRuleValue(params.planid,Label.MultiYearFullCredit,Label.RenewalMonths,mapComRule) &&
           params.closeDate >=  CommissionMethods.getRuleDate(params.planid,Label.MultiYearFullCredit,Label.StartDate,mapComRule) &&
           params.closeDate <=  CommissionMethods.getRuleDate(params.planid,Label.MultiYearFullCredit,Label.EndDate,mapComRule)) ;
  }
 

  
  public static list<Commission_Rule_Assignment__c> getCommissionRule(list<id> comPlanList)
    {       
        
        return [select id,Type__c, Rule__c,Commission_Rule__c, Commission_Plan__c  from Commission_Rule_Assignment__c where Commission_Plan__c in: comPlanList];        
    }
 
   public static Boolean zeroAmount(Commission_Schedule__c cs,Id  planid ,map<string,Commission_Rule_Assignment__c> mapComRuleAss)
  {  
 // system.debug('#################' + (checkRuleExist(planid,Label.MultiYear, mapComRuleAss) && (cs.Type__c == 'Support Multi Year' || cs.Type__c =='Support Renewal Multi Year')))   ;
    return  !cs.Commissinable__c || (checkRuleExist(planid,Label.MultiYear, mapComRuleAss) && (cs.Type__c == 'Support Multi Year' || cs.Type__c =='Support Renewal Multi Year'));
  }
  
  public static commission_plan__c avoidNulls(Commission_plan__c cp)
  {     
    cp.Predecessor_Plan_Sales__c = cp.Predecessor_Plan_Sales__c == null?0:cp.Predecessor_Plan_Sales__c;
    cp.H1_Threshold__c = cp.H1_Threshold__c == null?0:cp.H1_Threshold__c;
    cp.Up_to_quota_rate__c = cp.Up_to_quota_rate__c == null?0:cp.Up_to_quota_rate__c;
    cp.Super_Acc_Threshold__c = cp.Super_Acc_Threshold__c == null?0:cp.Super_Acc_Threshold__c;
    cp.Super_Acc_Factor__c = cp.Super_Acc_Factor__c == null?0:cp.Super_Acc_Factor__c;
    cp.Sum_commission_amount__c = cp.Sum_commission_amount__c == null?0:cp.Sum_commission_amount__c;
    cp.Sum_quota_amount__c = cp.Sum_quota_amount__c == null?0:cp.Sum_quota_amount__c;
    cp.Flat_commission_amount__c = cp.Flat_commission_amount__c == null?0:cp.Flat_commission_amount__c;
    cp.Flat_quota_amount__c = cp.Flat_quota_amount__c == null?0:cp.Flat_quota_amount__c;
    cp.Beyond_quota_amount__c = cp.Beyond_quota_amount__c == null?0:cp.Beyond_quota_amount__c;
    cp.Beyond_commission_amount__c = cp.Beyond_commission_amount__c == null?0:cp.Beyond_commission_amount__c;
    return cp;
  }
  

  public static decimal acceleratorAmount(list<Commission_Schedule__c> listCommissionSchedule, Commission_Plan__c cp,string ruleType, boolean h1,map<string,Commission_Rule_Assignment__c> mapComRuleAss )
  {   
      //System.debug('@@@acceleratorAmount: ' + listCommissionSchedule.size()+ ' ruleType: ' + ruleType);
    decimal amount = 0 ,count = 0;
    set<id> countPerOppSet = new set<id>();
    //Map<id,SFLog__c> log = new  Map<id,SFLog__c>();
    for(Commission_Schedule__c cs : listCommissionSchedule)
    {
        if(cs.Commission_Plan__c == cp.id  && ((h1 && isH1(cs,mapComRuleAss)) || !h1))
        {
            if(cs.Commission_Record__r.Type__c == Label.Regular && !cs.isMulti__c)
              {
                //system.debug(cs.opportunity__r.name  + ' Type of '+ cs.Type__c + ' This is the facot '+getQuotaFactor(cp.id,cs,listCommissionSchedule,mapComRuleAss));
                amount += CommissionPlanCalc.ifnull(cs.Quota_Amount__c);
                //if(h1 && isH1(cs,mapComRuleAss))
                //  log.put( cs.Id,new SFLog__c(Msg__c = cs.Id, Location__c = 'CommissionMethods.acceleratorAmount'));
              }
            else if(!countPerOppSet.contains(cs.Opportunity__c) && cs.Commission_Record__r.Type__c == Label.PIO)
            {
                count++;
                countPerOppSet.add(cs.Opportunity__c) ;
            }
         }
        
    }
    //insert log.values();
   //   System.debug('@@@acceleratorAmount: ' + countPerOppSet.size()+ ' count: ' + count);
   // system.debug('Amountd ' + amount );
  //  h1Amount+= getRecurringIncentiveMap(listCommissionSchedule,cp).get('RecurringIncentive');
    return ruleType == Label.H1PIOAccelerator || ruleType == Label.AnnualPIOAccelerator ? count:amount + CommissionPlanCalc.ifnull(cp.Predecessor_Plan_Sales__c);
  }
  
  public static map<string,decimal> getRecurringIncentiveMap(list<Commission_Schedule__c> listCommissionSchedule ,Commission_Plan__c cp)
  {  
    decimal recurringIncentive = 0, advanceableRecurringIncetive = 0;
    map<Id,decimal> oppRecurring = new  map<Id,decimal>();
    map<Id,decimal> oppAdvance = new    map<Id,decimal>();
    map<Id,decimal> oppAmount = new     map<Id,decimal>();
    map<string,decimal> result = new  map<string,decimal>();
     
    for(Commission_Schedule__c cs : listCommissionSchedule)
    {
       if(cp.id == cs.commission_plan__c && cs.Commissinable__c)
        {
          oppAmount.put(cs.Opportunity__c,CommissionPlanCalc.ifnull(cs.Opportunity_Amount__c));
          oppRecurring.put(cs.Opportunity__c,cs.Opportunity__r.SBQQ__PrimaryQuote__r.Recurring_incentive__c);
          oppAdvance.put(cs.Opportunity__c,oppAdvance.containsKey(cs.Opportunity__c)?(oppAdvance.get(cs.Opportunity__c)+ CommissionPlanCalc.ifnull(cs.Advanceable_Amount__c)):CommissionPlanCalc.ifnull(cs.Advanceable_Amount__c));   
        }   
     }
     
     for(Id oppId :oppRecurring.keySet())
       {
         if(oppRecurring.get(oppId) == null)
           continue;
         recurringIncentive += oppRecurring.get(oppId);
         //system.debug('Recurring ' + oppAdvance.get(oppId));
         advanceableRecurringIncetive += oppAmount.get(oppId) == 0 ? 0:oppRecurring.get(oppId)*(oppAdvance.get(oppId)/oppAmount.get(oppId));
       }
       

    result.put('RecurringIncentive',recurringIncentive);
    result.put('AdvanceRecurringIncentive',advanceableRecurringIncetive);
   // system.debug(result);
    
    return result;
    
  }
  

  public static Boolean isH1(Commission_Schedule__c cs, map<string,Commission_Rule_Assignment__c> mapComRuleAss)
  {
    return cs.Commission_Record__r.Type__c == 'Regular' && cs.Opportunity__r.PO_date__c != null ? cs.Opportunity__r.PO_date__c.Month()<7 :
           cs.Commission_Record__r.Type__c == 'PIO' ? cs.Opportunity__r.Createddate.Month()<7:
           cs.Opportunity__r.CloseDate.Month()<7 ;

  }

  public static decimal getFactor(Id planId, Commission_Schedule__c cs, List<Commission_Schedule__c> csList, map<string,Commission_Rule_Assignment__c> mapComRule)
   {
    //system.debug('@@@getFactor: cs start');
    return  !cs.Commissinable__c ? 0 :
             cs.isMulti__c ? 
             getRuleValue(planId,Label.MultiYear,Label.CommissionFactor,mapComRule):
             isRenewal(planId,cs,mapComRule)?
             getRuleValue(planId,Label.RenewalAdjustment,Label.Factor,mapComRule):1;
  
   }
/*
    public static decimal getQuotaFactor(Id planId, Commission_Schedule__c cs, List<Commission_Schedule__c> csList, map<string,Commission_Rule_Assignment__c> mapComRule)
   {
    //system.debug('@@@getFactor: cs start');
    return  !cs.Commissinable__c ? 0 :
            cs.isMulti__c ? 
            getRuleValue(planId,Label.MultiYear,Label.QuotaFactor,mapComRule):
            isRenewal(planId,cs,mapComRule)?
            getRuleValue(planId,Label.RenewalAdjustment,Label.Factor,mapComRule):1;
  
   }*/
   public static decimal getFactor(Id planId,Boolean isMulti ,Booking_Schedule__c bs, List<Booking_Schedule__c> bsList, map<string,Commission_Rule_Assignment__c> mapComRule)
   {
    return !bs.Commissinable__c ? 0 :
           // isMulti(planId, bs, bsList, mapComRule) ? 
            isMulti ? 
            getRuleValue(planId,Label.MultiYear,Label.CommissionFactor,mapComRule):
            isRenewal(planId,bs, mapComRule)?
            getRuleValue(planId,Label.RenewalAdjustment,Label.Factor,mapComRule):1;
  
   }

  public static Boolean isRenewal(Id planId, Booking_Schedule__c bs, map<string,Commission_Rule_Assignment__c> mapComRule )
  {
    return bs.Opportunity__r.Is_renewal_for_commission_calculation__c && 
           checkRuleExist(planId,Label.RenewalAdjustment,mapComRule) ;//&& 
           // (bs.Type_Family__c == Label.MultiYear || bs.Type__c == supportRenewal);
  }


  public static Boolean isRenewal(Id planId, Commission_Schedule__c cs, map<string,Commission_Rule_Assignment__c> mapComRule )
  {
    return cs.Opportunity__r.Is_renewal_for_commission_calculation__c && 
           checkRuleExist(planId,Label.RenewalAdjustment,mapComRule) ;//&& 
           // (cs.Booking_Schedule__r.Type_Family__c == Label.MultiYear || cs.Type__c == supportRenewal);
  }

  public static string convertIfOldType (string type)
  {
      
      Map<string,string> mapType = 
                 new  Map<string,string> {'RMS Only' => 'Support Renewal', 
                                          'Multi-year renewal' => 'Support Renewal Multi Year', 
                                          'RMS' => 'Support Renewal', 
                                          'Professional Services' =>'PS Prepaid', 
                                          'On order' =>'PS Prepaid', 
                                          'Standard or Premium Support' => 'Support Renewal', 
                                          'Products' => 'Perpetual', 
                                          'On delivery' => 'PS on Delivery', 
                                          'Other' => 'Perpetual' , 
                                          'PS Backlog' => 'PS on Delivery', 
                                          'Products' => 'Perpetual',  
                                          'PS Billing' => 'PS on Delivery',
                                          'PS' => 'PS Prepaid',
                                          'Don\'t know' => 'PS Prepaid',
                                          'Support Renewal Standard' =>   'Support Renewal'  ,
                                          'Subscription Renewal' => 'Subscription',
                                          'Multi-Year' => 'Support Multi Year',
                                            'Product Multi-Year' => 'Support Multi Year'
                                           };

      return mapType.containsKey(type) ? mapType.get(type): type ;
    }

  public class multiParams
    {
       public Date closeDate;
       public Decimal amount;
       public Decimal months;
       public String bsType;
       public Boolean manualOverride;
     //  public Map<Id,Decimal> mapRenewAmount = new Map<Id,Decimal>();
       public Id planid;

      multiParams(Id plID, Booking_Schedule__c bs , decimal renewAmount )
       {
          this.closeDate = bs.Opportunity__r.Closedate;
          this.amount =renewAmount ;
          this.months = getOppTerm(bs);
          this.bsType = bs.Type__c;
          this.planid = plID;
          this.manualOverride = bs.opportunity__r.Quota_Credit_Override__c;
       }   

      private decimal getOppTerm( Booking_Schedule__c bs)
        {
           return getOppTerm(bs.opportunity__r.SBQQ__PrimaryQuote__r.SBQQ__StartDate__c ,
                             bs.opportunity__r.SBQQ__PrimaryQuote__r.SBQQ__EndDate__c,  
                             bs.opportunity__r.SBQQ__PrimaryQuote__r.SBQQ__SubscriptionTerm__c);
        }


      private decimal getOppTerm( Date startDate, Date endDate, decimal term)
       {
         return (endDate!= null &&  startDate!= null) ? startDate.monthsBetween(endDate + 1) : integer.valueof(term); 
       }
   }
}