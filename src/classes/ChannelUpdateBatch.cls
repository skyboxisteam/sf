global class ChannelUpdateBatch implements Database.Batchable<sObject> {
	global final String defaultQuery = 'select id, name from Account where RecordTypeId = \'012600000009O6t\' ';
	String query;

	global ChannelUpdateBatch() {
		query = defaultQuery;
	}
	
	global ChannelUpdateBatch(String q) {
		query = q;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		VR_Batch_Exclusion__c vr = VR_Batch_Exclusion__c.getOrgDefaults();
		vr.is_Batch__c = true;
		update vr;
		 
		for (sObject s : scope) {
			Account a = (Account)s;
			ChannelAnalyzer analyzer = new ChannelAnalyzer(a.id);
			analyzer.updateRecord();
		}

		vr.is_Batch__c = false;
		update vr;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
	global void runBatch() {
//    	ChannelUpdateBatch batch = new ChannelUpdateBatch();
      	Database.executeBatch(this, 10);
	}
}