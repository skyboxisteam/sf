/**
 * An apex page controller that exposes the site login functionality
 */
global class SiteLoginController {
  global String username {get; set;}
  global String password {get; set;}
  public Case c {get; set;}
  public String firstname {get; set;}
  public String lastname {get; set;}
  public String email {get; set;}
  public String company {get; set;}
  public String jobtitle {get; set;}
  public String Phone {get; set;}
  public String Country {get; set;}
  public String City {get; set;}
  public String Street {get; set;}
  public String PostalCode {get; set;}

  global PageReference login() {
    //String startUrl = System.currentPageReference().getParameters().get('startURL');
    String startUrl = '/certifiresupport/home/home.jsp';
    return Site.login(username, password, startUrl);
  }
  global SiteLoginController () {}

  global PageReference register() {
    Case c = new Case();
    c.Description = 'First Name:' + firstname + '\nLast Name: ' + lastname +
                    '\nemail: ' + email + '\nCompany: ' + company +
                    '\nJob Title: ' + jobtitle + '\nPhone: ' + Phone +
                    '\nMailingCountry: ' + country + '\nMailingCity: ' + city +
                    '\nMailingStreet: ' + street + '\nMailingPostalCode: ' + postalCode;
    c.Subject = 'New portal registration';
    c.Origin = 'Community Register';
    Database.DMLOptions dmlOpts = new Database.DMLOptions();
    dmlOpts.assignmentRuleHeader.useDefaultRule = true;
    c.setOptions(dmlOpts);
    try {
      INSERT c;
      String communityUrl = Label.Support_Community_Url;
      PageReference secondPage = new PageReference(communityUrl);
      system.debug('Case: ' + c);
      secondPage.setRedirect(true);
      return secondPage;
    } catch (Exception e) {
      ApexPages.addMessages(e);
      return null;
    }

  }
}