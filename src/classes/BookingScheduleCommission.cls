public without sharing class BookingScheduleCommission 
{
    
  
  public map<id,Booking_Schedule__c> mapOppBs = new  map<id,Booking_Schedule__c>();
  public map<id,Commission_Rule__c> mapComRule = new map<id,Commission_Rule__c>();
  public list<Commission_Record__c> crList = new list<Commission_Record__c>();
  public list<id> idPlanList = new list<id>();
  private ClsObjectCreator cls = new ClsObjectCreator();
  list<Booking_Schedule__c> bookingList = new list<Booking_Schedule__c>();

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------createFromBooking------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 
    public void createFromBooking(list<Booking_Schedule__c> bookingList)
      {
        CommissionScheduleCalc csc = new CommissionScheduleCalc(bookingList);    
        calculateCommission(csc);
      }
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------createFromCommissionRecord---------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
     
   public void createFromCommissionRecord( list<Commission_Record__c> crList)
     {
       CommissionScheduleCalc csc = new CommissionScheduleCalc(crList);              
       calculateCommission(csc,crList);
     }
     
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------createFromCommissionPlan-----------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
      
   public void createFromCommissionPlan( list<id> cpList)
     {
       CommissionScheduleCalc csc = new CommissionScheduleCalc(cpList); 
       idPlanList = cpList;          
       calculateCommission(csc);
     }
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------deletedCommission---------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 public void deletedCommission(list<Commission_Record__c> crList)
 {
     /* CommissionScheduleCalc csc = new CommissionScheduleCalc(crList);     
      csc.deleteOldComSchedule();
      crList = csc.getCommissionRecords();*/
      list<id> commissionPlanIdList = new  list<id>();
     
      for(Commission_Record__c cr :crList)  
        commissionPlanIdList.add(cr.Commission_Plan__c);
        
     delete [select id from Commission_Schedule__c where Commission_Record__c in : crList];
        
      CommissionPlanCalc cpc = new CommissionPlanCalc(commissionPlanIdList);
      cpc.calcAllComponents();   
 }

     
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------calculateCommission---------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
         
    public void calculateCommission( CommissionScheduleCalc csc )
     {
         list<Commission_Schedule__c> result  = new  list<Commission_Schedule__c>(); 
         bookingList = csc.getBookingSchedules(); 
         crList = csc.getCommissionRecords(); 
         csc.setTotalCollected(bookingList);
         csc.deleteOldComSchedule();          
         csc.createRuleMap(crList);
        
         for(Commission_Record__c cr :crList)       
          {
            for(Booking_Schedule__c bs : bookingList )
             {
            // system.debug( 'bs ' +  bs + ' cr '+ cr );
              if(cr.opportunity__c == bs.opportunity__c &&  CommissionMethods.getRuleValue(cr.Commission_Plan__c,Label.DenyBookingType,bs.Type__c,csc.mapComRule) != 1)
               {
                   result.add(csc.createCommissionSchedule(cr,bs,bookingList));
               }
             }  
          }



       //   system.debug('result ' + result);
        insert result;
     }


    //This method is for the case that the use added ac commission record 
    public void calculateCommission( CommissionScheduleCalc csc,  list<Commission_Record__c> crList)
     {
         list<Commission_Schedule__c> result  = new  list<Commission_Schedule__c>(); 
         bookingList = csc.getBookingSchedules(); 
         csc.setTotalCollected(bookingList);        
         csc.createRuleMap(crList);
        
         for(Commission_Record__c cr :crList)       
          {
            for(Booking_Schedule__c bs : bookingList )
             {
            // system.debug( 'bs ' +  bs + ' cr '+ cr );
              if(cr.opportunity__c == bs.opportunity__c &&  CommissionMethods.getRuleValue(cr.Commission_Plan__c,Label.DenyBookingType,bs.Type__c,csc.mapComRule) != 1)
               {
        
                 //Commission_Schedule__c cs = new Commission_Schedule__c(Booking_Schedule__c = bs.id,Commission_Plan__c = cr.Commission_Plan__c, credit__c = cr.Credited__c  , Opportunity__c = cr.opportunity__c );
                 result.add(csc.createCommissionSchedule(cr,bs,bookingList));
               }
             }  
          }
       //   system.debug('result ' + result);
        insert result;
     }
    
  
}