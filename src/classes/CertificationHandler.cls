public with sharing class CertificationHandler {
	
	list<Id> campaignIdList = new list<Id>();
	list<Id> contactList = new list<Id>();
	list<Campaign> campaignList = new list<Campaign>();
	list<CampaignMember> campaignMemberList = new list<CampaignMember>();
	
	
	public CertificationHandler(list<Campaign> triggerCampaignList)
	{
		   
		campaignMemberList = getExtendCampigMember(triggerCampaignList);
	}
	
	public CertificationHandler(list<CampaignMember> triggerCampaignMemberList)
	{       
        campaignMemberList = Test.isRunningTest() ? getTestExtendCampigMember(triggerCampaignMemberList): getExtendCampigMember(triggerCampaignMemberList);
	}
    
    public void generateCerification()
    {
    	map<string,certification__c > certContactMap = new map<string,certification__c >();
    	for(CampaignMember cm: campaignMemberList)
    	  contactList.add(cm.contactid);
    	  
    	list<certification__c> certList = [select Certification_type__c,contact__c from certification__c where contact__c in :contactList ]  ;
    	for(certification__c cer : certList)
    	   certContactMap.put(cer.Certification_type__c + '' +cer.contact__c ,cer );
    	   
    	
    	for(CampaignMember cm: campaignMemberList)
    	{
          system.debug(cm.status);
    	  string key = cm.Campaign.Certification_type__c + '' + cm.contactId;
    	  if(certContactMap.containsKey(key) )
    	      certContactMap.get(key).Training_Date__c = cm.Campaign.EndDate.addYears(2);
    	  else
    	      certContactMap.put(key, new Certification__c(contact__c = cm.contactId , Training_Date__c =  cm.Campaign.EndDate.addYears(2) , 
    	                                                   Certification_type__c = cm.Campaign.Certification_type__c, Campaign__c = cm.CampaignId ));
    	}    	
    	upsert certContactMap.Values();  	   	
    }
    
    private list<CampaignMember> getExtendCampigMember(list<CampaignMember> campaignMemberList)
    {
      return [SELECT id,
                     ContactId,
                     Status,
                     CampaignId,
                     Campaign.Certification_type__c,
                     Campaign.EndDate                     
              FROM CampaignMember  
              WHERE id in : campaignMemberList and contactid != null  and  Campaign.Certification_type__c != null and Status = 'Attended' ];
    	
    }
    
    
    private list<CampaignMember> getExtendCampigMember(list<Campaign> campaignList)
    {
      return [SELECT id,
                     ContactId,
                     Status,
                     CampaignId,
                     Campaign.Certification_type__c,
                     Campaign.EndDate                 
              FROM CampaignMember  
              WHERE CampaignId  in : CampaignList and contactid != null and Status = 'Attended' and  Campaign.Certification_type__c != null  ];
    	
    }
    
      private list<CampaignMember> getTestExtendCampigMember(list<CampaignMember> campaignMemberList)
    {
      return [SELECT id,
                     ContactId,
                     Status,
                     CampaignId,
                     Campaign.Certification_type__c,
                     Campaign.EndDate                     
              FROM CampaignMember  
              WHERE id in : campaignMemberList and contactid != null  and  Campaign.Certification_type__c != null ];//and Status = 'Attended' ];
    	
    }
}