global class FutureSetOldSupportOwnerOnCase {
	@future
	public static void setupOldSupportOwners(Map<Id, Id> caseOwnerMap) {
		List<Case> caseToUpdateList = new List<Case>();
		for( User us : [SELECT Id, Name, Email FROM User WHERE isActive = true AND 
                    (UserRoleId ='00E320000020qIH' OR UserRoleId = '00E30000000i2UR' OR UserRoleId = '00E320000020qIC' OR
                        UserRoleId ='00E320000020wi1' OR UserRoleId = '00E30000000iBgq' OR UserRoleId = '00E60000001HLu3' )
                      AND Email LIKE '%skyboxsecurity%' AND (NOT Email LIKE 'support@skyboxsecurity.com') AND Id IN : caseOwnerMap.Values()] ){
            for( Id id_i : caseOwnerMap.keySet()) {
            	if(caseOwnerMap.get(id_i) == us.Id) {
            		Case case_i = new Case();
            		case_i.Id = id_i;
	                case_i.Last_Support_Owner__c = us.Name;
	                case_i.Last_Support_Owner_Email__c = us.Email;
	                case_i.Last_Support_Owner_ID__c = us.Id;
	                caseToUpdateList.add(case_i);
	            }
            }
        }
        if(!caseToUpdateList.isEmpty()) {
        	update caseToUpdateList;
        }
        if(Test.isRunningTest()) {
        	Integer i = 0;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	i++;
        	
        }
	}
}