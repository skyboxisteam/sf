@isTest
private class CaseWeeklyUpdateBatchTest {
    /*
    @testSetup
    public static void insertData(){
        
        Group il2 = new Group();
        
        QueueSobject mappingObject1 = new QueueSobject();
        
        CaseTriggerSettings__c setting = new CaseTriggerSettings__c();
        setting.Name = 'Test';
        setting.QueueName__c = 'Test Queue';
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        CaseTeamRole ctr = new CaseTeamRole();
        System.runAs ( thisUser ) {
            il2.Type = 'Queue';
            il2.Name = 'Test Queue';
            insert il2;
            mappingObject1.QueueId = il2.Id;
            mappingObject1.SobjectType = 'Case';
            insert mappingObject1;
            ctr.Name = 'Test Role';
            Schema.DescribeFieldResult fieldResult = CaseTeamRole.AccessLevel.getDescribe();
            ctr.AccessLevel = fieldResult.getPicklistValues()[0].getValue();
            insert ctr;
        }
        
        caseteamtemplate ct;
        caseteamrole cr;
        caseteamtemplate c;
        try{
            cr=[select id from caseteamrole where name=:'Test Role'];
            ct=[select id,name from caseteamtemplate where name=:'test'];
            c=ct;
        } catch(Exception e) {
            if(ct==null) {
                //create case team template
                c=new caseteamtemplate(); 
                c.Description='test';
                c.Name='test';
                insert(c);
            }
        }
        
        UserRole ur = new UserRole(Name = 'TLS');
        insert ur;
        
        thisUser.userRoleId = ur.Id;
        //add case team member to pre defined case team
        caseteamtemplatemember cm=new caseteamtemplatemember();
        System.runAs ( thisUser ) {
            cm.memberid=UserInfo.getUserId();
            cm.teamroleid=cr.id;
            cm.teamtemplateid=c.id;
            insert(cm);
        }
        setting.PredefinedCaseTeamId__c = c.Id;
        User user = [SELECT Id, userRole.Name FROM User WHERE Id = :UserInfo.GetUserId()];
        user.userRoleId = ur.Id;
        setting.UserRoleName__c = ur.Name;//user.userRole.Name;
        
        
        setting.CaseTeamRoleName__c = ctr.Name;
        
        setting.LastSupportOwnerRoleId__c = ctr.Id;
        System.runAs ( thisUser ) {
            insert setting;
        }   
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        // UserRole ur = [select name from UserRole where Name = 'TLS'];
        User userTest = new User();
        userTest.LastName = 'Lname';
        userTest.Alias = 'Alias';
        userTest.Email = 'u@gmail.com';
        userTest.UserName ='u123345567UserName@mail.ru';
        userTest.CommunityNickname = 'NickName';
        userTest.profileId = p.id;
        // userTest.ContactId = c.id;
        userTest.LanguageLocaleKey = 'en_US';
        userTest.TimeZoneSidKey = 'America/Caracas';
        userTest.EmailEncodingKey = 'ISO-8859-1';
        userTest.LocaleSidKey = 'en_GB';
        userTest.UserRoleId = ur.id;
        insert userTest;    
    }
    @isTest
    static  void CaseWeeklyUpdateBatchTest() {
        String defaultQuery = 'select id from Case WHERE (Owner.UserRole.Name=\'TLS\' OR Owner.Name=\'IncomingCasesRD\') AND Severity1__c = \'1\' AND (Status = \'Work In Progress\' OR Status = \'Pending Customer\') ';
        User userTest = [select id from User where UserName='u123345567UserName@mail.ru'];
        RecordType rt = [select id from RecordType where name = 'Product Capability Request'];
        // UserRole ur = new UserRole(Name = 'TLS');
        //insert ur;
        Account acc = new Account();
        acc.Name = 'redf';
        acc.Industry = 'Education';
        acc.Theater__c = 'EMEA';
        acc.BillingCountry = 'MC';
        insert acc;
        
        Contact cont = new Contact();
        cont.LastName = 'Lname';
        cont.AccountId = acc.id;
        insert cont;
        System.runAs(userTest)   {    
            Case newCase = new Case();
            newCase.OwnerId = userTest.id;
            newCase.Origin = 'Rep';
            newCase.Severity1__c = '1';
            newCase.Status = 'Work In Progress';
            newCase.ContactId = cont.id;
            newCase.Did_you_discuss_this_with_Skybox_support__c = 'No';
            newCase.Version_build__c = 'V0.0';
            newCase.Weekly_update__c = false;
            newCase.Subject = 'newSubj1';
            newCase.RecordTypeId = rt.id;
            insert newCase;
        }
        System.debug('userTest    '+userTest.id);    
          
        Test.startTest();       
  
        Database.BatchableContext BC;
        List<Case> cases = [SELECT id FROM Case];
        CaseWeeklyBatch batch = new CaseWeeklyBatch('String');
        String jobId = System.schedule('ScheduleApexClassTest',  '0 6 14 1/1 * ? *',  new CaseWeeklyBatch('String'));
        batch.execute(BC,cases);
        Case caseTest2 = [select id,status,Weekly_update__c from case where Subject ='newSubj1'];
        System.debug('upd    '+caseTest2);
        System.assert(caseTest2.Weekly_update__c==true);
        
        Test.stopTest();     
        //Test.startTest();       
        //Database.BatchableContext BC2;
        //List<Case> cases2 = [SELECT id FROM Case];
        //CaseWeeklyBatch batch2 = new CaseWeeklyBatch('String');
        //String jobId2 = System.schedule('ScheduleApexClassTest',  '0 6 14 1/1 * ? *',  new CaseWeeklyBatch('String'));
        //batch2.execute(BC2,cases2);
        //Test.stopTest();
        //Case caseTest3 = [select id,status,Weekly_update__c from case where Subject ='newSubj1'];
        //System.debug('upd    '+caseTest3);
        //System.assert(caseTest3.Weekly_update__c==true);
       
    }
    */
    
}