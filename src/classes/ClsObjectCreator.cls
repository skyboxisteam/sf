public with sharing class ClsObjectCreator 
{
    
    public Account createAccount(String accName)
    {
        Account acc = new Account();
        acc.Name = accName;
        acc.Industry = 'Education';
        acc.BillingCountry = 'US';
        insert acc;
        return acc;
    }
    public AccountTeamMember createAccountTeam(Id accId)
    {
        User myAdminUser = [select id from user  where profile.Name='System Administrator' and isActive=true limit 1];
     	AccountTeamMember accTeam =  new AccountTeamMember();
        accTeam.accountId = accId;
        accTeam.TeamMemberRole='Area VP';
        accTeam.UserId = myAdminUser.Id;
        insert accTeam;
        return accTeam;
    }

    public static Contact createContact(String lname,Id accId)
    {
        Contact con = new Contact();
        con.AccountId = accId;
        con.LastName = lname;
        insert con;
        return con;
    }
     @future
     public static void  createUserWithRole(id roleId)
    {
                
        Profile p = [Select Id From Profile Where Name =: 'Standard User'];
        User usr = new User();
        usr.Alias = 'testusr';
        usr.Email = 'testskybox@test.com';
        usr.EmailEncodingKey = 'UTF-8';
        usr.ProfileId = p.Id;
        usr.LastName = 'Testing';
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Los_Angeles';
        usr.UserName = 'testskybox@testing.com';
        usr.UserRoleId = roleId;
        usr.contactid = null;
         insert usr;

    }
    
  /*  @future
     public static void  createUserWithRoleProfile(id roleId, id profileId, string userName, string alias)
    {
                
        //Profile p = [Select Id From Profile Where Name =: 'Standard User'];
        User usr = new User();
        usr.Alias = alias;
        usr.Email = 'testskybox@test.com';
        usr.EmailEncodingKey = 'UTF-8';
        usr.ProfileId = profileId;
        usr.LastName = 'Testing';
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Los_Angeles';
        usr.UserName = userName;
        usr.UserRoleId = roleId;
        usr.contactid = null;
         insert usr;

    }*/

    @future
    public static void createTerritory(string ter2ModelId,string ter2TypeId,String terName)
    {
        Territory2 ter = new Territory2();
    
        
        ter.Name = terName;
        ter.DeveloperName = terName;
        ter.Territory2ModelId = ter2ModelId;
        ter.Territory2TypeId = ter2TypeId;
        insert ter;
        //return ter;
    }
    
    @future
    public static void createTerritoryModel(String terName)
    {
        Territory2Model terMod = new Territory2Model();
    
        
        terMod.name = terName;
        terMod.DeveloperName = terName;
        terMod.State = 'Activate';
        insert terMod;
        
    }
    

    public ObjectTerritory2Association createJuncAccoutToTerritory(Id accID,Id territoryId,String associationCause)
    {
        ObjectTerritory2Association juncObj = new ObjectTerritory2Association();
        juncObj.ObjectId = accID;
        juncObj.Territory2Id = territoryId;
        juncObj.AssociationCause = associationCause;
        insert juncObj;
        return juncObj;
    }


  /*  public Case createCase(Id conId , Id accId, Id ownerId,Id recTypeId)
    {
        Case cas = new Case();
        cas.Status = 'New';
        cas.Severity1__c = '1';
        cas.ContactId = conId;
        cas.AccountId = accId;
        cas.Subject = 'Test';
        cas.Version_build__c = '10';
        cas.Module__c = 'License';
        cas.OwnerId = ownerId;
        cas.RecordTypeId = recTypeId;
        insert cas;
        return cas;
    }*/
    
     public Case createCasewithAccount( Id accId)
    {
        Case cas = new Case();
        cas.Status = 'New';
        cas.Severity1__c = '1';
        cas.AccountId = accId;
        cas.Subject = 'Test';
        cas.Version_build__c = '10';
        cas.Module__c = 'License';
        insert cas;
        return cas;
    }
    
    public Case createCase()
    {
        Case cas = new Case();
        cas.Status = 'New';
        cas.Severity1__c = '1';
        cas.Subject = 'Test';
        cas.Version_build__c = '10';
        cas.Module__c = 'License';
        insert cas;
        return cas;
    }
  
  public Opportunity createOpportunity(Account acc)
      {
        return createOpportunity(acc, 'test');
      }
   public Opportunity createOpportunity(Account acc, String oppName)
      {
        Contact con = ClsObjectCreator.createContact('my Con', acc.Id);
        Opportunity opp = new Opportunity();
        opp.name = oppName;
        opp.AccountId = acc.id;
        opp.Type = 'Products';
        opp.Product__c = 'Appliance';//because VR when Type = 'Products'
        opp.Amount = 56;
        opp.CloseDate = date.today()+3;
        opp.LeadSource = 'Direct Mail';
        opp.StageName = 'Potential Opportunity';
        opp.Main_Contact__c = con.Id;//beacuse OpportunityHandler.updateOppByOpportunityContactRole
        insert opp;
        return opp;
      }
      public Opportunity returnOpportunity(Account acc, String oppName)
      {
        Contact con = ClsObjectCreator.createContact('my Con', acc.Id);
        Opportunity opp = new Opportunity();
        opp.name = oppName;
        opp.AccountId = acc.id;
        opp.Type = 'Products';
        opp.Product__c = 'Appliance';//because VR when Type = 'Products'
        opp.Amount = 56;
        opp.CloseDate = date.today()+3;
        opp.LeadSource = 'Direct Mail';
        opp.StageName = 'Potential Opportunity';
        opp.Main_Contact__c = con.Id;//beacuse OpportunityHandler.updateOppByOpportunityContactRole
        //insert opp;
        return opp;
      }
    
    public Lead createLead(String recordTypeName,String leadSourceDetail){
      //Id recordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
      Lead ld = new Lead();
      ld.LastName = 'test';
      ld.Company = 'test';
      ld.status = 'In Process';
      ld.Theater__c = 'EMEA';
      ld.country = 'US';
       if(String.isNotBlank(recordTypeName))
        ld.recordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
      if(String.isNotBlank(leadSourceDetail))
        ld.Lead_Source_Detail__c = leadSourceDetail;
      insert ld;
      return ld;
    }
    public Lead createLead()
    {
        return createLead(null,null);
    }
      
    public static Task createLeadTask(id leadId, string type)
    {
        Task ts = new Task();
        ts.whoid = leadid;
        ts.type = type;
        insert ts;
        return ts;
    }

    public Deployment__c createDeployment(Id accId, string projName)
    {
      Deployment__c d = new Deployment__c();
      d.Project_Name__c = projName;
      d.Account__c = accId;
      insert d;
      return d;
    }

    public PM_Status__c createPMStatuses(Id deploymentId, string status)
    {
      PM_Status__c pms = new PM_Status__c();
      pms.Deployment__c = deploymentId;
      pms.Status__c = status;
      insert pms;
      return pms;
    }
      
   public Territory_Help__c createTerritoryHelp(Account acc, string CauseOfAssociation,string Territory_ID, string ot2aID, date LastUpdate )
    {
      Territory_Help__c th = new Territory_Help__c();
      th.Account__c = acc.id;
      th.Cause_of_association__c = CauseOfAssociation;
      th.Territory_ID__c = Territory_ID;
      th.Account_territory_Association_id__c = Territory_ID;
      th.Update_Association__c  = LastUpdate;
      insert th;
      return th;
    
    }
    
  public static opportunitycontactrole createContactRoleTo(opportunity opp, string role)
  { 
    opportunitycontactrole ocr = new opportunitycontactrole();
    ocr.contactId = opp.Main_Contact__c ;
    ocr.opportunityId = opp.id;
    ocr.role = role;
    return ocr;    
  }
  
  public static Lead_Statistics__c createLeadStatistics (Id whoid, string whoType)
  {
    Lead_Statistics__c ldSt = new Lead_Statistics__c();
    if(whoType == 'Lead')
     ldSt.Lead__c = whoid;
    if(whoType == 'Contact')
     ldSt.Contact__c = whoid;
    ldSt.Object_Type__c = whoType;
    ldSt.Object_ID__c = whoid;
    
    insert ldSt;    
    return ldSt;
    
    
  }
  
  public  PS_Tab__c createPSTab(Account acc , string psType)
  {
    PS_Tab__c psTab = new PS_Tab__c();
    psTab.Account__c = acc.id;
    psTab.type__c = psType;
    insert psTab;
    return psTab;
  }
  
   public  PS_Tab_Record__c createPSRTab(PS_Tab__c psTab , string psrType, decimal hours )
  {
    PS_Tab_Record__c psrTab = new PS_Tab_Record__c();
    psrTab.PS_Tab__c = psTab.id;
    psrTab.type__c = psrType;
    psrTab.Hours__c = hours;
    psrTab.Date__c = date.today()+3;
    insert psrTab;
    return psrTab;
  }
  
    public  License2__c createLicense2(Opportunity opp )
  {
     License2__c ls = new License2__c();
    ls.opportunity__c = opp.id;     
    ls.Expiration_Date__c = date.today()+3;
    ls.VC_Nodes__c = 5;
    ls.FA_Nodes__c = 5;
    ls.Virtual_Nodes__c = 5;
    ls.NA_Nodes__c = 5;
    ls.TM_License__c = true;
    ls.FA_license__c = true;
    ls.NA_License__c = true;
    ls.CM_License__c = true;
    ls.VC_License__c = true;
    insert ls;
    return ls;
  }

   public static License2__c createLicense2(Case cs )
  {
      Id recordTypeId = Schema.SObjectType.License2__c.getRecordTypeInfosByName()
                      .get('Support').getRecordTypeId();
 
    License2__c ls = new License2__c();
    ls.RecordTypeId = recordTypeId;
    ls.Case__c = cs.id;     
    ls.Expiration_Date__c = date.today()+3;
    ls.VC_Nodes__c = 5;
    ls.FA_Nodes__c = 5;
    ls.Virtual_Nodes__c = 5;
    ls.NA_Nodes__c = 5;
    ls.TM_License__c = true;
    ls.FA_license__c = true;
    ls.NA_License__c = true;
    ls.CM_License__c = true;
    ls.VC_License__c = true;
    insert ls;
    return ls;
  }
  
   public  contract createContract(Account acc , opportunity opp, SBQQ__Quote__c quote )
     {
      
      quote.SBQQ__Opportunity2__c = opp.id;
      update quote;
      opp.SBQQ__PrimaryQuote__c = quote.id;
      update opp;
      
      contract cr = new contract();    
      cr.Accountid = acc.id ;     
      cr.SBQQ__Quote__c =  quote.id;
      cr.ContractTerm = 12;
      cr.StartDate  = system.today();
      cr.SBQQ__Opportunity__c = opp.id;
       insert cr;
      return cr;
     }
 
  
   public  Product2 createProduct(string name )
     {
      Product2 p = new product2();
      p.name = name ;
       insert   p;
      return   p;
     }
     
    public  SBQQ__Subscription__c createSubscription(Account acc , contract cr , product2 p )
     {
      SBQQ__Subscription__c su = new SBQQ__Subscription__c();
      su.SBQQ__Quantity__c = 5;
      su.SBQQ__Product__c = p.id;
      su.SBQQ__Account__c = acc.id;
      su.SBQQ__Contract__c = cr.id;
      su.SBQQ__SubscriptionEndDate__c = system.today()+100;
       insert su;
      return su;
     }
     
     public  Asset createAsset(string name , string SN ,Account acc)
     {
      Asset ass = new Asset();
      ass.accountid = acc.id;
      ass.Appliance_Model__c = 'Custom Made';
      ass.name = name;
      ass.SerialNumber = SN;
      insert ass;
      return ass;
     }
  
   public  SBQQ__SubscribedAsset__c createSubscribedAsset(SBQQ__Subscription__c su , Asset ass  )
     {
      SBQQ__SubscribedAsset__c sua = new SBQQ__SubscribedAsset__c();
      sua.SBQQ__Asset__c = ass.id;
      sua.SBQQ__Subscription__c = su.id;
      insert sua;
      return sua;
     }
     
  public  SBQQ__Quote__c createQuote(Contact con , string status )
  {
    SBQQ__Quote__c qu = new SBQQ__Quote__c(SBQQ__SubscriptionTerm__c = 13);
    qu.SBQQ__Status__c = status;
    qu.SBQQ__Account__c = con.accountid;
    qu.WDSB_CustomerContact__c = con.id;
    qu.Pending_approval_timestamp__c = system.now() - 8;
    insert qu;
    return qu;
  }


  public Commission_Plan__c createCommissionPlan(string year, boolean adjustMulti)
  {
    Commission_Plan__c com = new Commission_Plan__c();
    com.name = 'test';
    com.Adjust_Support_Multi_Year__c = adjustMulti;
    com.Fiscal_Year__c = year;
    com.Annual_Quota__c = 25176;
    com.On_target_commission__c = 2374;
    com.Base_Salary_Currency__c = 'USD';
    com.Support_Multi_Year_Factor_Commission__c = 1;
    com.Support_Multi_Year_Factor_Quota__c = 0.5;
    com.Sales_User__c = UserInfo.getUserId();
    
    insert com;
    return com;
  }
  
  //createCommissionRule overload function
  public Commission_Rule__c createCommissionRule(String ruleType, String rule){
    return createCommissionRule(ruleType, rule, Label.Commission_Plan);
  }
  public Commission_Rule__c createCommissionRule(String ruleType, String Rule, String ruleLevel){

    Commission_Rule__c comRule = new Commission_Rule__c();
    comRule.Name = ruleType;
    comRule.Type__c = ruleType;
    comRule.Rule__c = rule;
    comRule.Rule_Level__c = ruleLevel;
    insert comRule;
    return comRule;
  }

  //overload function createCommissionRuleAssignment to enable bulk insert 
  public Commission_Rule_Assignment__c createCommissionRuleAssignment(String ruleName, Id commissionRuleId, Id commissionPlanId){
    return createCommissionRuleAssignment( ruleName,  commissionRuleId,  commissionPlanId, true);
  }
  public Commission_Rule_Assignment__c createCommissionRuleAssignment(String ruleName, Id commissionRuleId, Id commissionPlanId, boolean isInsert){
    Commission_Rule_Assignment__c cra = new Commission_Rule_Assignment__c();
    cra.Name = ruleName;
    cra.Commission_Rule__c = commissionRuleId;
    cra.Commission_Plan__c = commissionPlanId;
    if(isInsert)
      insert cra;
    return cra;
}

//overload function createCommissionRecord
public Commission_Record__c createCommissionRecord(Id oppId, Id commissionPlanId){
  return createCommissionRecord( oppId, commissionPlanId, Label.Regular);
}
public Commission_Record__c createCommissionRecord(Id oppId, Id commissionPlanId, String type){
  Commission_Record__c cr = new Commission_Record__c();
  cr.Opportunity__c = oppId;
  cr.Commission_Plan__c = commissionPlanId;
  cr.Type__c = type;
  insert cr;
  return cr;
}

//overload to function createOppLineItem
public  OpportunityLineItem createOppLineItem(Id oppId, String productClass, String bsType ){
  return createOppLineItem( oppId,  productClass,  bsType, 'Prepaid', '');
}
public  OpportunityLineItem createOppLineItem(Id oppId, String productClass, String bsType, String paymentMethod ){
  return createOppLineItem( oppId,  productClass,  bsType, paymentMethod, '');
}
  public  OpportunityLineItem createOppLineItem(Id oppId, String productClass, String bsType, String paymentMethod, String subFamily )
  {
    // This is how we get the Standard PriceBook Id.  Prior to Summer '14, we needed
    // to use SeeAllData=true, so this is a big improvement
    Id pricebookId = Test.getStandardPricebookId();

    //Create your product
    Product2 prod = new Product2(
         Name = 'Product X',
         ProductCode = 'Pro-X',
         WDSB_Product_Class__c = productClass,
         Booking_Schedule_Type__c = bsType,
         WDSB_Sub_Family__c = subFamily,
         Payment_Method__c = paymentMethod,
         isActive = true
    );
    insert prod;


    //Create your pricebook entry
    PricebookEntry pbEntry = new PricebookEntry(
         Pricebook2Id = pricebookId,
         Product2Id = prod.Id,
         UnitPrice = 100.00,
         IsActive = true
    );
    insert pbEntry;

    //create your opportunity line item.  This assumes you already have an opportunity created, called opp
    decimal quantity = 1;
    OpportunityLineItem oli = new OpportunityLineItem(
         OpportunityId = oppId,
         Quantity = 5,
         PricebookEntryId = pbEntry.Id,
         Product2Id  = prod.Id,//Guy used it
         TotalPrice = quantity * pbEntry.UnitPrice
    );
    insert oli;
    return oli;
  }
    
   public static License_Default__c createLicenseDefault(){
      License_Default__c ls = new License_Default__c();
      ls.FA_license__c = true;
      ls.VC_license__c = true;
      ls.TM_license__c = true;
      ls.NA_License__c = true;
      ls.CM_License__c = true;
      ls.Content__c = true;          
      ls.FA_Nodes__c = 7;            
      ls.VC_Nodes__c = 7;              
      ls.NA_Nodes__c = 7;   
      ls.vNA_Nodes__c = 8;           
      ls.Servers__c = 5;              
      ls.Managers__c = 5;              
      ls.Collectors__c = 5; 
      ls.Days_to_expire__c = 5;
      ls.Licesne_Type__c = 'Demo';
      insert ls;
      return ls;

  }   
  public static License2__c createLicense2(String recordTypeName, Id conId, Date expirationDate, String sourceCreation){

    Id recordTypeId = Schema.SObjectType.License2__c.getRecordTypeInfosByName()
                      .get(recordTypeName).getRecordTypeId();

    License2__c lic = new License2__c();
    lic.RecordTypeId = recordTypeId;
    lic.Partner__c = conId;
    lic.Expiration_Date__c = expirationDate;
    lic.Source_Creation__c = sourceCreation;//
    insert lic;
    return lic;
  }

  public static License2__c createLicense2(String recordTypeName, Id conId){

    Id recordTypeId = Schema.SObjectType.License2__c.getRecordTypeInfosByName()
                      .get(recordTypeName).getRecordTypeId();

    License2__c lic = new License2__c();
    lic.RecordTypeId = recordTypeId;
    if ( recordTypeName == 'POC')
      lic.Opportunity__c = conId;
    else if( recordTypeName == 'Production')
      lic.Account__c = conId;
    else
      lic.Partner__c = conId;
    insert lic;
    return lic;
  }
  
  
  
}