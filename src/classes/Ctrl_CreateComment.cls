public class Ctrl_CreateComment {
	
    @AuraEnabled
    public static Case getCase(Id recordId){
        Case mycase = [SELECT Id, isClosed, Description, Status, CaseNumber, ContactId, Contact.Name, Subject, CreatedDate, OwnerId, Reason, Status_last_modified__c, Who_did_you_speak_to__c, Version_build__c FROM Case WHERE id=:recordId];
        return mycase;
    } 
    
    @AuraEnabled 
    public static void insertComment(String stringJson){
        try{            
            CaseComment cc = (CaseComment) json.deserialize (stringJson, CaseComment.class);
            insert cc;
        }catch(Exception ex){
			system.debug('ex: ' + ex.getMessage());  
        }
    }
}