public without sharing class OpportunityBookingSchedule 
{
  final static string subscription = 'Subscription';
  final static string support = 'Support, maintenance and content';
  final static string perpetual = 'Perpetual';
  final static string supportRenewal = 'Support Renewal';
  final static string SupportMultiYear = 'Support Multi Year';
  final static string SupportRenewalMultiYear = 'Support Renewal Multi Year';
  final static string subscriptionRenewal = 'Subscription Renewal';
  final static string products = 'Products';
  final static string royalties = 'Royalties';
  final static set<string> recurringSet = new set<string>{royalties, SupportMultiYear,subscription , SupportRenewalMultiYear,subscriptionRenewal, supportRenewal,'Support' };
 
//---------------------------------------------------------------------------------------------------------------------------------------

public  static map<string,booking_schedule__c> bsMap = new map<string,booking_schedule__c>();
private static  map<string,Decimal> oppSupportRenewalMap = new map<string,Decimal>();

//----------------------------------------------------------------------------------------------------------------------------------    
//--------------------------------------------------creatFromOpportunity--------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
  
  @future
  public static void creatFromOpportunity(String strObjList, String  strOldMap, boolean DataConversion, Boolean isInsert, Boolean isUpdate)//list<opportunity> oppList, map<id,opportunity> mapOpp
  {
   system.debug(' is insert ?' + isInsert);
   system.debug(' is update ?' + isUpdate);
   
    map<id, opportunity> mapOpp = (Map<id, opportunity>) JSON.deserialize(strOldMap, Map<id, opportunity>.class);
    list<opportunity> oppList = (list<opportunity>) JSON.deserialize(strObjList, list<opportunity>.class);

    bsMap.clear();//must reset the map beacause it's static member.
    Integer NumOfMonth = 12;
    list<opportunity> relevantOpp = new list<opportunity>();
    list<opportunity> collectChangeOpp = new list<opportunity>();
    list<Id> relevantLineOppId = new list<Id>();
    list<id> oppIds = new list<id>();
    map<string,string> mapMultiType = new map<string,string> {'Multi-Year' => SupportRenewalMultiYear , 'Product Multi-Year' =>SupportMultiYear};
   
    for(opportunity opp :oppList) 
       {
         if(mapOpp != null && mapOpp.containsKey(opp.id) && Opp.Collected_Amount__c != mapOpp.get(opp.id).Collected_Amount__c && !relevantChangeMade(opp, mapOpp.get(opp.id)))
         {
           collectChangeOpp.add(opp);
          
         }
         else if((isInsert || relevantChangeMade(opp, mapOpp.get(opp.id))) && (!opp.HasOpportunityLineItem || DataConversion || ( isPreGoLive(opp.closedate) && opp.Allow_BS_Creation_On_Old_Opp__c == false)))
           {
            relevantOpp.add(opp);
            oppIds.add(opp.id);
           }       
         else if(opp.HasOpportunityLineItem && relevantChangeMade(opp, mapOpp.get(opp.id)))
           relevantLineOppId.add(opp.id);

       }
    if(!collectChangeOpp.isEmpty()) 
    {
      handleCollection(collectChangeOpp);
    }
    if(!relevantLineOppId.isempty())
        {
          creatFromOpportunityLine(getOppLine(relevantLineOppId),DataConversion);
        }
         
    if(!relevantOpp.isempty() && deleteBookingSchedule(oppIds))
     {
       
       for(opportunity opp :relevantOpp )
         {       
        //addBookingSchedule(opp.id,opp.Subtype__c != null && mapMultiType.containsKey(opp.Subtype__c) ? mapMultiType.get(opp.Subtype__c) : opp.Subtype__c!= null?opp.Subtype__c: opp.type,opp.amount,opp.closedate, opp.Collected_Amount__c, opp.Actual_Collection_Date__c, opp.Target_Collection_Date__c,DataConversion);   
          addBookingSchedule(opp.id,CommissionMethods.convertIfOldType(getType(opp)),opp.amount,opp.Booking_Date_Override__c  == null?  opp.closedate : opp.Booking_Date_Override__c , opp.Collected_Amount__c, opp.Actual_Collection_Date__c, opp.Target_Collection_Date__c,DataConversion,
                             opp.Allow_BS_Creation_On_Old_Opp__c, opp.Recurring_Amount__c);   
         }

       insert bsMap.values();
     }
    
  }
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------creatFromOpportunityLine----------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
 
 private static string getType(Opportunity opp)
 {
   
     return isPreGoLive(opp.closedate) && opp.SubType__c != null ? opp.SubType__c : opp.type;

 }
 private static boolean relevantChangeMade(opportunity newOpp , opportunity oldOpp)
 {
   return newOpp.stageName != oldOpp.stageName  
         || newOpp.closedate != oldOpp.closedate 
         || newOpp.Type != oldOpp.Type
         || newOpp.subType__c != oldOpp.subType__c
         || newOpp.Allow_BS_Creation_On_Old_Opp__c != oldOpp.Allow_BS_Creation_On_Old_Opp__c
         || newOpp.Quota_Credit_Override__c != oldOpp.Quota_Credit_Override__c
         || newOpp.Amount != oldOpp.Amount;
          
          
 }

private static Boolean isPreGoLive(Date closeDate)
{
  return closedate < date.newinstance(2018,8,13);
}
/*
private static Boolean isPostTermByLine(Date closeDate)
{
  return closedate < date.newinstance(2018,8,13);
}*/
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------creatFromOpportunityLine----------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
  
  @future
  public static void creatFromOpportunityLineFuture(String strObjList, boolean DataConversion, Boolean isInsert, Boolean isDelete, Boolean isUpdate)//(Set<Id> oppItemSet, boolean DataConversion)//
  {
    system.debug(' is insert ?' + isInsert);
    system.debug(' is update ?' + isUpdate);
    system.debug(' is delete ?' + isdelete);

    list<OpportunityLineItem> oppItemList = (list<OpportunityLineItem>) JSON.deserialize(strObjList, list<OpportunityLineItem>.class);
    creatFromOpportunityLine(oppItemList,  DataConversion);
  }
  public static void creatFromOpportunityLine(list<OpportunityLineItem> oppItemList, boolean DataConversion)//(Set<Id> oppItemSet, boolean DataConversion)
  {
    bsMap.clear();//must reset the map beacause it's static member.
    list<id> oppIds = new list<id>();
    if(DataConversion)
      return;
      
    for(OpportunityLineItem oppLine :oppItemList)  
     {
       if(isPreGoLive(oppLine.Opportunity.closedate) && oppLine.Opportunity.Allow_BS_Creation_On_Old_Opp__c == false)
           continue;      
       oppIds.add(oppLine.OpportunityId);
     }

   if(deleteBookingSchedule(oppIds))
   {  
    oppItemList = getOppLine(oppIds);
    scanLines(oppItemList);
 
      //insert new Dictionary_Log__c(ip__c='creatFromOpportunityLine:bsMap',version__c=string.valueOf(bsMap.size()));
    insert bsMap.values();
  }
  }



 //----------------------------------------------------------------------------------------------------------------------------------
 //------------------------------------------------------scanLines---------------------------------------------------------------------
 //----------------------------------------------------------------------------------------------------------------------------------
 
 private static void scanLines(list<OpportunityLineItem> oppItemList)
  {
    bsMap.clear();
    integer   NumOfMonth,historyNumOfMonth =0;
    decimal firstYearAdjust;
    string typeLine;
    boolean splitPotential;
    OpportunityLineItem opplineDiscount ;
  
    for(OpportunityLineItem oppline :oppItemList )
      {
        NumOfMonth  = getNumMonths(oppline);
        historyNumOfMonth = getHistoryNumMonths(NumOfMonth,oppline);
        typeLine = findType( NumOfMonth, historyNumOfMonth,oppline);// oppline.Product2.WDSB_License_Type__c == 'Any'?  subscription : oppline.Product2.WDSB_License_Type__c ;       
       
        system.debug( ' Type ' +typeLine);
        if(typeLine == 'Discount')
        {
          opplineDiscount = oppline;    
          continue;  
        }             
        splitPotential = oppline.Product2.Booking_Schedule_Type__c == 'Support' || oppline.Product2.Booking_Schedule_Type__c == 'Subscription';
        
  //      system.debug('Type ' + typeLine  +' Amount '+ oppline.TotalPrice );
      //  system.debug('isSplit '  + splitPotential + 'Type ' + typeLine + ' Num ' + NumOfMonth );
        if(typeLine == null)
         continue;
       
        firstYearAdjust = splitPotential && NumOfMonth>12 ? 12.00/NumOfMonth : 1;
        addBookingSchedule(oppline.OpportunityId,oppline.Product2.Booking_Schedule_Type__c,typeLine == support ? perpetual : CommissionMethods.convertIfOldType(typeLine), firstYearAdjust*oppline.TotalPrice, 
                           oppline.opportunity.Booking_Date_Override__c  == null ? oppline.opportunity.closedate : oppline.opportunity.Booking_Date_Override__c,
                           oppline.Opportunity.Collected_Amount__c, oppline.Opportunity.Actual_Collection_Date__c, oppline.Opportunity.Target_Collection_Date__c ,false,
                           oppline.Opportunity.Allow_BS_Creation_On_Old_Opp__c, oppline.Opportunity.Recurring_Amount__c);
        
      
      if((splitPotential) && NumOfMonth>12)
           splitMultiYear(NumOfMonth, oppline,typeLine,
            oppline.opportunity.closedate
           // oppline.Opportunity.SBQQ__PrimaryQuote__r.SBQQ__StartDate__c == null? oppline.opportunity.closedate : oppline.Opportunity.SBQQ__PrimaryQuote__r.SBQQ__StartDate__c
            );
      
     }
     system.debug('Splitiong ' + bsMap);
      if(opplineDiscount != null)
          proRate(opplineDiscount);
     system.debug('Splitiong ' + bsMap.values());     
    handleCollection(oppItemList);
     
  }

//----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------splitMultiYear---------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
 
 private static void splitMultiYear(integer NumOfMonth, OpportunityLineItem oppline, string typeLine, date startDate)
 {
     
     map<string,string> multiTypeMap = new map<string,string> {perpetual => SupportMultiYear , subscription =>subscription,'Support Renewal' => 'Support Renewal Multi Year'};
     system.debug('Type  ' + string.valueof(multiTypeMap.containsKey(typeLine)?multiTypeMap.get(typeLine ):typeLine));
     integer  relevantMonths,  tempNumOfMonth = 12;
     while (  NumOfMonth  > tempNumOfMonth )
       {    
         relevantMonths = tempNumOfMonth + 12 < NumOfMonth ? 12 : NumOfMonth - tempNumOfMonth;       
         addBookingSchedule(oppline.OpportunityId, multiTypeMap.containsKey(typeLine)?multiTypeMap.get(typeLine ):typeLine ,relevantMonths*oppline.TotalPrice/NumOfMonth,startDate.addMonths(tempNumOfMonth),
         oppline.Opportunity.Collected_Amount__c, oppline.Opportunity.Actual_Collection_Date__c, oppline.Opportunity.Target_Collection_Date__c, false,
         oppline.Opportunity.Allow_BS_Creation_On_Old_Opp__c, oppline.Opportunity.Recurring_Amount__c);         
         tempNumOfMonth += 12 ;
        
        }
   }
//----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------getNumMonths------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
/* private static integer getNumMonths( OpportunityLineItem oppline)
   {
    
        if(isPostTermByLine(oppline.Opportunity.closedate) && oppline.SBQQ__QuoteLine__r.SBQQ__EffectiveEndDate__c!= null && 
                                                             oppline.SBQQ__QuoteLine__r.SBQQ__EffectiveStartDate__c!= null)
             return oppline.SBQQ__QuoteLine__r.SBQQ__EffectiveStartDate__c.monthsBetween(oppline.SBQQ__QuoteLine__r.SBQQ__EffectiveEndDate__c + 1);
        else 
             return getHeaderNumMonths(oppline);
  
             
   }*/


//Find the number of months from primart qoute or from renewed contract (default 12)
 private static integer getNumMonths( OpportunityLineItem oppline)
   {
        if(oppline.opportunity.SBQQ__PrimaryQuote__r.SBQQ__EndDate__c!= null && oppline.Opportunity.SBQQ__PrimaryQuote__r.SBQQ__StartDate__c!= null)
             return oppline.Opportunity.SBQQ__PrimaryQuote__r.SBQQ__StartDate__c.monthsBetween(oppline.opportunity.SBQQ__PrimaryQuote__r.SBQQ__EndDate__c + 1);

        else if(oppline.opportunity.SBQQ__PrimaryQuote__c != null)
             return integer.valueof(oppline.opportunity.SBQQ__PrimaryQuote__r.SBQQ__SubscriptionTerm__c);
          
        else if (oppline.Opportunity.SBQQ__RenewedContract__c != null)
            return integer.valueof(oppline.Opportunity.SBQQ__RenewedContract__r.SBQQ__RenewalTerm__c );
        else 
             return 12; 
             
   }
//----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------getHistoryNumMonths------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
  
 private static integer getHistoryNumMonths(integer NumOfMonth, OpportunityLineItem oppline)
   {
        return( //integer.valueof(oppline.Opportunity.SBQQ__AmendedContract__c !=null?oppline.Opportunity.SBQQ__AmendedContract__r.ContractTerm : 
                        oppline.Opportunity.SBQQ__RenewedContract__c !=null?oppline.Opportunity.SBQQ__RenewedContract__r.ContractTerm: 0 );
   }
   
//----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------addBookingSchedule------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
 
 // create BS if the opp type and the booking type are the same
 private static void addBookingSchedule(id oppId, string oppType, decimal amount, date bookingDate, decimal collectedAmount, Date collectedDate, Date targetDate, Boolean dataConversion, Boolean allowBs , Decimal oldRecurringAmount)
  {
    addBookingSchedule(oppId, oppType,oppType, amount, bookingDate, collectedAmount, collectedDate, TargetDate,DataConversion, allowBs, oldRecurringAmount);
  }
 // create BS if the opp type and the booking type are not the same
 private static void addBookingSchedule(id oppId, string productType, string oppType, decimal amount, date bookingDate, decimal collectedAmount, date collectedDate, date TargetDate, Boolean dataConversion, Boolean allowBs , Decimal oldRecurringAmount)
   {
        
        string key = oppId + oppType + string.valueof(bookingDate.year());
        decimal totalAmount = bsMap.get(key) == null ? amount: bsMap.get(key).Amount__c + amount;
        oppSupportRenewalMap.put(oppId + oppType,totalAmount );
                 
        booking_schedule__c bs = new booking_schedule__c(Opportunity__c = oppId , Type__c = oppType, Amount__c = totalAmount, 
                                                        Booking_Date__c = bookingDate,Collected_Amount__c = collectedAmount, 
                                                        Actual_Collection_Date__c = collectedDate, Target_Collection_Date__c = TargetDate,
                                                        Data_Conversion__c = DataConversion,
                                                        Recurring_Amount__c = getRecurring(key,productType,oppType,amount, allowBs,oldRecurringAmount, bookingDate),
                                                        Support_Renewal_Amount__c = getSupportRenewal(oppId, oppType));  
       
       bsMap.put(key,bs); 
      
   }

private static decimal getSupportRenewal(Id oppId, string oppType)
{
   return (oppSupportRenewalMap.containsKey(oppId + supportRenewal ) && (oppType == SupportRenewalMultiYear  || oppType == supportRenewal))? 
                          oppSupportRenewalMap.get(oppId + supportRenewal ) : 0;
}

 
  private static decimal getRecurring(string key, string type, string bsType, Decimal amount, Boolean allowBs, Decimal oldRecurringAmount, Date bookingDate)
  {
     return (isPreGoLive(bookingDate) && !allowBs ) ? oldRecurringAmount:
            (bsMap.get(key) == null && !recurringSet.contains(type)) ? 0 :
            (bsMap.get(key) != null && !recurringSet.contains(type)) ?  bsMap.get(key).Recurring_Amount__c :
            (bsMap.get(key) == null &&  recurringSet.contains(type)) ?  amount :
            (bsMap.get(key) != null &&  recurringSet.contains(type)) ?  bsMap.get(key).Recurring_Amount__c + amount : 0;
                                 
  }
  
 private static void proRate(OpportunityLineItem oppline) 
 {
    for(string key : bsMap.keySet())
    {
      
        bsMap.get(key).Amount__c += oppline.opportunity.amount + Math.abs(oppline.TotalPrice) == 0 ? 0: bsMap.get(key).Amount__c*oppline.TotalPrice/(oppline.opportunity.amount + Math.abs(oppline.TotalPrice));
        bsMap.get(key).Recurring_Amount__c += oppline.opportunity.amount + Math.abs(oppline.TotalPrice) == 0 ? 0: bsMap.get(key).Recurring_Amount__c*oppline.TotalPrice/(oppline.opportunity.amount + Math.abs(oppline.TotalPrice));
        bsMap.put(key, bsMap.get(key));
    }
 }
//----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------findType------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
  //get the types of the non multi year BS lines
 private static string findType(integer numOfMonth, integer historyNumOfMonth, OpportunityLineItem oppline)
   {
    
        if(oppline.Opportunity.Type == royalties )  
             return royalties;
       
        if(oppline.Product2.Booking_Schedule_Type__c == 'Subscription' && (numOfMonth )<=6)
               return 'Project';
           
         if(oppline.Opportunity.SBQQ__RenewedContract__c == null && oppline.Product2.Booking_Schedule_Type__c == 'Support')
               return perpetual;
         if(oppline.Opportunity.SBQQ__RenewedContract__c != null && oppline.Product2.Booking_Schedule_Type__c == 'Support') 
               return supportRenewal;
            
       
             
        return oppline.Product2.Booking_Schedule_Type__c == null ? perpetual : oppline.Product2.Booking_Schedule_Type__c;   
        
   }
 
//----------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------handleCollection----------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
private static void handleCollection(list<OpportunityLineItem> oppItemList)
{
  list<string> orderCollectionList = new list<string> {perpetual,subscription,'Project',royalties,supportRenewal, SupportMultiYear,SupportRenewalMultiYear,'PS on Delivery','PS Prepaid' };
  map<id,decimal> oppCollectionMap = new map<id,decimal>();
  map<string,booking_schedule__c> bsMapSorted = new map<string,booking_schedule__c>();
   string key = '';
  
  for(OpportunityLineItem oppline :oppItemList )// create a map of opps with thier collected anout
     oppCollectionMap.put(oppLine.OpportunityId, oppLine.Opportunity.Collected_Amount__c); 
 // system.debug('map  ' + bsMap);
  
  for(string typeCol : orderCollectionList) // sort the booking schedule amount according to orderCollectionList
    for(Booking_Schedule__c bs : bsMap.Values())
      {
       key = bs.Opportunity__c + typeCol + string.valueof(bs.Booking_Date__c.year());
       if(bsMap.containsKey(key) && !bsMapSorted.containsKey(key))
         bsMapSorted.put(key, bsMap.get(key));
      }
    for(Booking_Schedule__c bs : bsMapSorted.Values())  
     {
        key = bs.Opportunity__c + bs.Type__c + string.valueof(bs.Booking_Date__c.year());
    //   system.debug('Key ' + key);
       if(oppCollectionMap.get(bs.Opportunity__c) < 0 )
         continue;
    //    system.debug('^^ amount ' +bsMapSorted.get(key).Amount__c + ' bsMapSorted '  + bsMapSorted.get(key).Amount__c + ' oppCollectionMap '  + oppCollectionMap.get(bs.Opportunity__c));
    //    system.debug('Collected  ' + math.min(CommissionPlanCalc.ifnull(bsMapSorted.get(key).Amount__c) , CommissionPlanCalc.ifnull(oppCollectionMap.get(bs.Opportunity__c)) ));
        bsMapSorted.get(key).Collected_Amount__c = CommissionPlanCalc.ifnull(bsMapSorted.get(key).Amount__c <= 0 ? 0 :
                                                   math.min(CommissionPlanCalc.ifnull(bsMapSorted.get(key).Amount__c) , 
                                                            CommissionPlanCalc.ifnull(oppCollectionMap.get(bs.Opportunity__c))) );

        oppCollectionMap.put(bs.Opportunity__c,CommissionPlanCalc.ifnull(oppCollectionMap.get(bs.Opportunity__c)) 
                                      - math.min(CommissionPlanCalc.ifnull(bsMapSorted.get(key).Amount__c) , 
                                                 CommissionPlanCalc.ifnull(oppCollectionMap.get(bs.Opportunity__c))));
     //   system.debug('LLLLLLL ' + oppCollectionMap.get(bs.Opportunity__c));
     }  
      system.debug('bsmap  ' + bsMap);
     bsMap = bsMapSorted;
   
}

private static void handleCollection( list<Opportunity> oppList)
{
  list<string> orderCollectionList = new list<string> {perpetual,subscription,'Project',royalties,supportRenewal, SupportMultiYear,SupportRenewalMultiYear,'PS on Delivery','PS Prepaid' };
  map<id,decimal> oppCollectionMap = new map<id,decimal>();
  map<string,booking_schedule__c> bsMapAll = new map<string,booking_schedule__c>();
  map<string,booking_schedule__c> bsMapSorted = new map<string,booking_schedule__c>();
   string key = '';
  
  
  for(Booking_schedule__c bs : [select id,Type__c,Opportunity__c,Amount__c, Booking_Date__c ,Collected_Amount__c from Booking_schedule__c where opportunity__c in :oppList ])
  {

     key = bs.Opportunity__c + bs.Type__c + string.valueof(bs.Booking_Date__c.year());
     bsMapAll.put(key,bs);
  }
  for(Opportunity opp :oppList )// create a map of opps with thier collected anout
     oppCollectionMap.put(opp.id, opp.Collected_Amount__c); 
 // system.debug('map  ' + bsMap);
  
  for(string typeCol : orderCollectionList) // sort the booking schedule amount according to orderCollectionList
    for(Booking_Schedule__c bs : bsMapAll.Values())
     {
      key = bs.Opportunity__c + typeCol + string.valueof(bs.Booking_Date__c.year());
      if(bsMapAll.containsKey(key) && !bsMapSorted.containsKey(key))
        bsMapSorted.put(key, bsMapAll.get(key));
     }
      system.debug('MAp  ' + bsMapSorted);
    for(Booking_Schedule__c bs : bsMapSorted.Values())  
     {
         system.debug('BS ' + bs);
        key = bs.Opportunity__c + bs.Type__c + string.valueof(bs.Booking_Date__c.year());
    //   system.debug('Key ' + key);
       if(oppCollectionMap.get(bs.Opportunity__c) < 0 )
         continue;
         
       bsMapSorted.get(key).Collected_Amount__c = CommissionPlanCalc.ifnull(bsMapSorted.get(key).Amount__c <= 0 ? 0 :
                                                   math.min(CommissionPlanCalc.ifnull(bsMapSorted.get(key).Amount__c) , 
                                                            CommissionPlanCalc.ifnull(oppCollectionMap.get(bs.Opportunity__c))) );

        oppCollectionMap.put(bs.Opportunity__c,CommissionPlanCalc.ifnull(oppCollectionMap.get(bs.Opportunity__c)) 
                                      - math.min(CommissionPlanCalc.ifnull(bsMapSorted.get(key).Amount__c) , 
                                                 CommissionPlanCalc.ifnull(oppCollectionMap.get(bs.Opportunity__c))));
    
     }  
      system.debug('bsmap  ' + bsMap);
     update bsMapSorted.values();
   
}
//----------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------deleteBookingSchedule----------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------

 private static Boolean deleteBookingSchedule(list<id> oppIds)  
   {
       //insert new Dictionary_Log__c(ip__c='deleteBookingSchedule',version__c=string.valueOf([select id from booking_schedule__c where opportunity__c in : oppIds].size()));
     try{
       delete [select id from booking_schedule__c where opportunity__c in : oppIds ];
       return true;
      }
     catch(exception ex)
     {
      return false;
     }
   } 
//---------------------------------------------------------------------------------------------------------------------------------------
 private static list<OpportunityLineItem> getOppLine(list<id> oppId)
 {
    return [select OpportunityId,
                   Product2.name,
                   Product2.Payment_Method__c,
                   Opportunity.SBQQ__PrimaryQuote__r.SBQQ__StartDate__c ,
                   Opportunity.SBQQ__PrimaryQuote__r.SBQQ__EndDate__c,
                   Opportunity.Amount,Product2.WDSB_Sub_Family__c, 
                   Product2.WDSB_License_Type__c,
                   Product2.WDSB_Product_Class__c,
                   Product2.Booking_Schedule_Type__c,
                   TotalPrice,
                   Opportunity.closedate, 
                   Opportunity.SBQQ__PrimaryQuote__r.SBQQ__SubscriptionTerm__c ,
                   Opportunity.SBQQ__RenewedContract__c,
                   Opportunity.SBQQ__RenewedContract__r.ContractTerm,
                   Opportunity.SBQQ__RenewedContract__r.SBQQ__RenewalTerm__c,
                   Opportunity.SBQQ__AmendedContract__c ,
                   Opportunity.SBQQ__AmendedContract__r.ContractTerm,
                   Opportunity.SBQQ__AmendedContract__r.SBQQ__RenewalTerm__c,
                   Opportunity.Actual_Collection_Date__c,
                   Opportunity.Target_Collection_Date__c,
                   Opportunity.Collected_Amount__c,
                   Opportunity.Type,
                   Opportunity.Subtype__c,
                   Opportunity.Recurring_Amount__c,
                   Opportunity.Allow_BS_Creation_On_Old_Opp__c,
                   Opportunity.createddate,
                   Opportunity.Booking_Date_Override__c,
                   SBQQ__QuoteLine__r.SBQQ__EffectiveStartDate__c,
                   SBQQ__QuoteLine__r.SBQQ__EffectiveEndDate__c
            from OpportunityLineItem 
            where OpportunityId in: oppId and IsDeleted = false ];
 }
 
 

}