@isTest
private class CommissionPlanHandlerTest {
	
    static ClsObjectCreator cls = new ClsObjectCreator();

	@testSetup static void setupData() {
       
       //create Commission Rule
       List<Commission_Rule__c>  cRuleList = new List<Commission_Rule__c>();
		cRuleList.add(cls.createCommissionRule(Label.AnnualAccelerator, '{ "Annual Quota" : 2165, "On Target Commission" : 0.5 ,"Factor" : 1.3,"Annual Quota Override":0 , "Base Rate Override":0}', Label.Commission_Plan));
		cRuleList.add(cls.createCommissionRule(Label.AnnualPIOAccelerator, '{ "Annual Quota" : 2165, "On Target Commission" : 0.5 ,"Factor" : 1.3,"Annual Quota Override":0 , "Base Rate Override":0}'));
		cRuleList.add(cls.createCommissionRule(Label.H1Accelerator, '{ "Threshold" : 0.4, "Factor" : 1.5 , "H1 Quota Override": 0 , "H1 Attainment Override" :0 }'));
		cRuleList.add(cls.createCommissionRule(Label.H1PIOAccelerator, '{ "Threshold" : 0.4, "Factor" : 1.5 , "H1 Quota Override": 0 , "H1 Attainment Override" :0 }'));
		cRuleList.add(cls.createCommissionRule(Label.SuperAccelerator, '{"Threshold":0,"Factor":0 }'));
		cRuleList.add(cls.createCommissionRule(Label.MultiYear, '{ "CommissionFactor" : 0, "QuotaFactor" : 0.5 }', Label.Commission_Schedule));
		cRuleList.add(cls.createCommissionRule(Label.SuperAccelerator, '{"Threshold":0,"Factor":0 }'));
		cRuleList.add(cls.createCommissionRule(Label.MultiYear, '{ "CommissionFactor" : 0, "QuotaFactor" : 0.5 }', Label.Commission_Schedule));

		for(Commission_Rule__c cRule: cRuleList)
			cRule.Default__c=true;

		update cRuleList;


   }


	@isTest static void createDefaultCommissionRuleTest() {
		// create cr with default - setup
		//create cp
		Commission_plan__c cp = cls.createCommissionPlan('2017',true);
		//test that was created default cra

		List<Commission_Rule_Assignment__c> craTest = [select Id from Commission_Rule_Assignment__c where Commission_Plan__c =:cp.Id];
		System.assertEquals(8,craTest.size());

	}
	
	
}