public with sharing class AccountSubscriptionController {


public Account acc {get;set;} 
  
   public AccountSubscriptionController(ApexPages.StandardController stdCon) {
      
     acc =  (Account) stdCon.getRecord();
     acc = [select id,name from account where id =: acc.id];
   }  
}