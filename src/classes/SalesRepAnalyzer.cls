public with sharing class SalesRepAnalyzer  {
    private ApexPages.StandardController ctrl;
    public User salesRep;
    private Integer pipelineAnalysisWindowQuarters = 2;
    private Integer yieldStatsWindowQuarters = 4;
    private Decimal maxProbability = 30;
    
    public User getUser(String id) {
        List<User> users = [SELECT Id, Name FROM User WHERE Id =: id];
        if (users != null) {
            return users[0];
        }
        else
            return null;
    }
    
    public SalesRepAnalyzer(ApexPages.StandardController ctrlParam){
        ctrl = ctrlParam;
      
        salesRep = getUser(ApexPages.currentPage().getParameters().get('id'));
    }
    
    public SalesRepAnalyzer(String id) {
        salesRep = getUser(id);   
    }
    
    public Integer getOpenPipeline() {
        List<AggregateResult> results = [SELECT sum(Amount) sum FROM Opportunity
            WHERE owner.id =: salesRep.id and IsClosed = false];
        if (results == null || results[0].get('sum') == null)
            return 0;
        return Integer.valueOf(results[0].get('sum'));
    }

    public Integer getProductsCountOpenPipeline() {
        List<AggregateResult> results = [SELECT count(id) opps FROM Opportunity
            WHERE owner.id =: salesRep.id and IsClosed = false and type = 'Products'];
        if (results == null || results[0].get('opps') == null)
            return 0;
        return Integer.valueOf(results[0].get('opps'));
    }

    public Integer getValueOpenPipeline() {
        List<AggregateResult> results = [SELECT sum(LT_forecasted_value__c) sum FROM Opportunity
            WHERE owner.id =: salesRep.id and IsClosed = false];
        if (results == null || results[0].get('sum') == null)
            return 0;
        return Integer.valueOf(results[0].get('sum'));
    }

   public Decimal getPipelineSnapshotCappedASP() {
        List<AggregateResult> results = [SELECT sum(LT_Capped_Forecasted_Value__c) amount, avg(Capped_Amount__c) a, sum(LT_Probability__c) deals FROM Opportunity
            WHERE owner.id =: salesRep.id and IsClosed = false and type = 'Products' and real_product_deal__c = 1];
        if (results == null || results[0].get('deals') == null || Integer.valueOf(results[0].get('deals')) == 0)
            return 0;
        // return Integer.valueOf(results[0].get('amount')) * 1.0 / Decimal.valueOf(String.valueOf(results[0].get('deals'))) ;
        return Integer.valueOf(results[0].get('a'));
    }

    public Integer getWonThisYear() {
        List<AggregateResult> results = [SELECT sum(Amount) sum FROM Opportunity
            WHERE owner.id =: salesRep.id   and IsWon = true and CloseDate = THIS_FISCAL_YEAR] ;
        if (results == null || results[0].get('sum') == null)
            return 0;
        return Integer.valueOf(results[0].get('sum'));
    }   
    
    public Decimal getProductOppsQA() {
        Date d = System.today() - (Integer)(pipelineAnalysisWindowQuarters/4.0*365);
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
   
        List<AggregateResult> results = [SELECT count(id) opps FROM Opportunity 
                    WHERE owner.id =: salesRep.id and
                    real_product_deal__c = 1 and
                    Type = 'Products' and CreatedDate >= :dt ];
            
        if (results == null || results[0].get('opps') == null)
            return 0;
        return Integer.valueOf(results[0].get('opps'))*1.0/pipelineAnalysisWindowQuarters;
    }
    
    public Decimal getRecentPipelineCappedASP() {
        Date d = System.today() - (Integer)(pipelineAnalysisWindowQuarters/4.0*365);
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
   
        List<AggregateResult> results = [SELECT avg(Capped_Amount__c) asp FROM Opportunity 
                    WHERE owner.id =: salesRep.id and
                    real_product_deal__c = 1 and
                    Type = 'Products' and CreatedDate >= :dt ];
            
        if (results == null || results[0].get('asp') == null)
            return 0;
        return Integer.valueOf(results[0].get('asp'));
    }


    public Decimal getNewCustomerOppsQA() {
        Date d = System.today() - (Integer)(pipelineAnalysisWindowQuarters/4.0*365);
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
   
        List<AggregateResult> results = [SELECT count(id) opps FROM Opportunity 
                    WHERE owner.id =: salesRep.id and
                    real_product_deal__c = 1 and is_first_deal_with_customer__c = 'yes' and
                    Type = 'Products' and CreatedDate >= :dt ];
        if (results == null || results[0].get('opps') == null)
            return 0;    
        return Integer.valueOf(results[0].get('opps'))*1.0/pipelineAnalysisWindowQuarters;
    }
    
    public Decimal getNewAddOnOppsQA() {
        Date d = System.today() - (Integer)(pipelineAnalysisWindowQuarters/4.0*365);
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
   
        List<AggregateResult> results = [SELECT count(id) opps FROM Opportunity 
                    WHERE owner.id =: salesRep.id and
                    real_product_deal__c = 1 and is_first_deal_with_customer__c = 'no' and
                    Type = 'Products' and CreatedDate >= :dt ];
        if (results == null || results[0].get('opps') == null)
            return 0;     
        return Integer.valueOf(results[0].get('opps'))*1.0/pipelineAnalysisWindowQuarters;
    }
    
    public Decimal getNewCustomerYield() {
        Date d = System.today() - (Integer)(yieldStatsWindowQuarters/4.0*365);
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
   
        List<AggregateResult> results = [SELECT avg(LT_Probability__c) yield FROM Opportunity 
                    WHERE owner.id =: salesRep.id and
                    real_product_deal__c = 1 and is_first_deal_with_customer__c = 'yes' and
                    Type = 'Products' and CreatedDate >= :dt ];
        if (results == null || results[0].get('yield') == null)
            return 0;    
        return Decimal.valueOf(String.valueOf(results[0].get('yield')));
    }
 
    public Decimal getNewCustomerYieldRecords() {
        Date d = System.today() - (Integer)(yieldStatsWindowQuarters/4.0*365);
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
   
        List<AggregateResult> results = [SELECT count(id) c FROM Opportunity 
                    WHERE owner.id =: salesRep.id and
                    real_product_deal__c = 1 and is_first_deal_with_customer__c = 'yes' and
                    Type = 'Products' and CreatedDate >= :dt ];
        if (results == null || results[0].get('c') == null)
            return 0;    
        return Decimal.valueOf(String.valueOf(results[0].get('c')));
    }
      
    public Decimal getAddOnYield() {
        Date d = System.today() - (Integer)(yieldStatsWindowQuarters/4.0*365);
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
   
        List<AggregateResult> results = [SELECT avg(LT_Probability__c) yield FROM Opportunity 
                    WHERE owner.id =: salesRep.id and
                    real_product_deal__c = 1 and is_first_deal_with_customer__c = 'no' and
                    Type = 'Products' and CreatedDate >= :dt ];
        if (results == null || results[0].get('yield') == null)
            return 0;    
        return Decimal.valueOf(String.valueOf(results[0].get('yield')));
    }
  
    public Decimal getAddOnYieldRecords() {
        Date d = System.today() - (Integer)(yieldStatsWindowQuarters/4.0*365);
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
   
        List<AggregateResult> results = [SELECT count(id) c FROM Opportunity 
                    WHERE owner.id =: salesRep.id and
                    real_product_deal__c = 1 and is_first_deal_with_customer__c = 'no' and
                    Type = 'Products' and CreatedDate >= :dt ];
        if (results == null || results[0].get('c') == null)
            return 0;    
        return Decimal.valueOf(String.valueOf(results[0].get('c')));
    }
    
    public Decimal getWonCappedASP() {
        Date d = System.today() - (Integer)(yieldStatsWindowQuarters/4.0*365);
   
        List<AggregateResult> results = [SELECT avg(Capped_Amount__c) asp FROM Opportunity 
                    WHERE owner.id =: salesRep.id and IsWon = true and 
                    real_product_deal__c = 1 and
                    Type = 'Products' and CloseDate >= :d ];
            
        if (results == null || results[0].get('asp') == null)
            return 0;
        return Decimal.valueOf(String.valueOf(results[0].get('asp')));
    }

      
    // unlike other methods, win/loss is calculated based on the closedate and not createddate
    public Decimal getNewCustomerWinLoss() {
        Date d = System.today() - (Integer)(yieldStatsWindowQuarters/4.0*365);
   
        List<AggregateResult> results = [SELECT avg(LT_Probability__c) yield FROM Opportunity 
                    WHERE owner.id =: salesRep.id and IsCLosed = true and 
                    (IsWon = true or StageName = 'Closed Lost') and
                    real_product_deal__c = 1 and is_first_deal_with_customer__c = 'yes' and
                    Type = 'Products' and CloseDate >= :d ];
            
        if (results == null || results[0].get('yield') == null)
            return 0;
        return Decimal.valueOf(String.valueOf(results[0].get('yield')));
    }
    
    public Decimal getNewCustomerWinLossRecords() {
        Date d = System.today() - (Integer)(yieldStatsWindowQuarters/4.0*365);
   
        List<AggregateResult> results = [SELECT count(id) c FROM Opportunity 
                    WHERE owner.id =: salesRep.id and IsCLosed = true and 
                    (IsWon = true or StageName = 'Closed Lost') and
                    real_product_deal__c = 1 and is_first_deal_with_customer__c = 'yes' and
                    Type = 'Products' and CloseDate >= :d ];
            
        if (results == null || results[0].get('c') == null)
            return 0;
        return Decimal.valueOf(String.valueOf(results[0].get('c')));
    }
     
    public Decimal getNewCustomerYieldQualified() {
        Date d = System.today() - (Integer)(yieldStatsWindowQuarters/4.0*365);
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
   
        List<AggregateResult> results = [SELECT avg(LT_Probability__c) yield FROM Opportunity 
                    WHERE owner.id =: salesRep.id and
                    real_product_deal__c = 1 and is_first_deal_with_customer__c = 'yes' and
                    Max_Probability__c >= :maxProbability and
                    Type = 'Products' and CreatedDate >= :dt ];
        if (results == null || results[0].get('yield') == null)
            return 0;    
        return Decimal.valueOf(String.valueOf(results[0].get('yield')));
    }
    
    public Decimal getNewCustomerYieldQualifiedRecords() {
        Date d = System.today() - (Integer)(yieldStatsWindowQuarters/4.0*365);
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
   
        List<AggregateResult> results = [SELECT count(id) c FROM Opportunity 
                    WHERE owner.id =: salesRep.id and
                    real_product_deal__c = 1 and is_first_deal_with_customer__c = 'yes' and
                    Max_Probability__c >= :maxProbability and
                    Type = 'Products' and CreatedDate >= :dt ];
        if (results == null || results[0].get('c') == null)
            return 0;    
        return Decimal.valueOf(String.valueOf(results[0].get('c')));
    }
    
    public Decimal getAddOnYieldQualified() {
        Date d = System.today() - (Integer)(yieldStatsWindowQuarters/4.0*365);
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
   
        List<AggregateResult> results = [SELECT avg(LT_Probability__c) yield FROM Opportunity 
                    WHERE owner.id =: salesRep.id and
                    real_product_deal__c = 1 and is_first_deal_with_customer__c = 'no' and
                    Max_Probability__c >= :maxProbability and
                    Type = 'Products' and CreatedDate >= :dt ];
        if (results == null || results[0].get('yield') == null)
            return 0;    
        return Decimal.valueOf(String.valueOf(results[0].get('yield')));
    }
    
    public Decimal getAddOnYieldQualifiedRecords() {
        Date d = System.today() - (Integer)(yieldStatsWindowQuarters/4.0*365);
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
   
        List<AggregateResult> results = [SELECT count(id) c FROM Opportunity 
                    WHERE owner.id =: salesRep.id and
                    real_product_deal__c = 1 and is_first_deal_with_customer__c = 'no' and
                    Max_Probability__c >= :maxProbability and
                    Type = 'Products' and CreatedDate >= :dt ];
        if (results == null || results[0].get('c') == null)
            return 0;    
        return Decimal.valueOf(String.valueOf(results[0].get('c')));
     }
 
     public void updateRecord() {
        if (salesRep == null)
            return;
            
        salesRep.SRS_Add_On_yield__c = getAddOnYield();
        salesRep.SRS_New_Customer_yield__c = getNewCustomerYield();
        salesRep.SRS_New_Customer_Win_Loss__c = getNewCustomerWinLoss();
        salesRep.SRS_Add_On_yield_qualified__c = getAddOnYieldQualified();
        salesRep.SRS_New_Customer_yield_qualified__c = getNewCustomerYieldQualified();

        salesRep.SRS_Add_On_yield_records__c = getAddOnYieldRecords();
        salesRep.SRS_New_Customer_yield_records__c = getNewCustomerYieldRecords();
        salesRep.SRS_New_Customer_Win_Loss_records__c = getNewCustomerWinLossRecords();
        salesRep.SRS_Add_On_yield_qualified_records__c = getAddOnYieldQualifiedRecords();
        salesRep.SRS_New_Customer_yield_qualified_records__c = getNewCustomerYieldQualifiedRecords();
 
        salesRep.SRS_Add_On_Opps__c = getNewAddOnOppsQA();
        salesRep.SRS_New_Customer_Opps__c = getNewCustomerOppsQA();
        salesRep.SRS_Pipeline_Snapshot_Future_Prod_Deals__c = getProductsCountOpenPipeline();
        salesRep.SRS_Pipeline_Snapshot_Future_Value__c = getValueOpenPipeline();
        salesRep.SRS_Pipeline_Snapshot_Total__c = getOpenPipeline();
        
        salesRep.SRS_Won_Capped_ASP__c = getWonCappedASP();
        salesRep.SRS_Pipeline_Snapshot_Capped_ASP__c = getPipelineSnapshotCappedASP();
        salesRep.SRS_Recent_Pipeline_Capped_ASP__C = getRecentPipelineCappedASP();
        
        
               
 /*    System.debug('Channel: ' + channel.name + ' engagement score: ' + channel.Channel_Engagement_Score__c);*/
        update salesRep;
    }
   
    public void callBatch() {
        SalesRepUpdateBatch batch = new SalesRepUpdateBatch();
        Database.executeBatch(batch, 10);
    }

}