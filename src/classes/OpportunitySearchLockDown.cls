public with sharing class OpportunitySearchLockDown {
    
  public static void SearchLockDown(list<opportunity> triggerOppList, map<id,Opportunity> triggerOppOldMap)
  {
   set<string> keySet = new set<string>();
   list<Opportunity_Lockdown__c> oppLD = [select Key_Year_Month__c from Opportunity_Lockdown__c where lock__c = true];

  for(Opportunity_Lockdown__c oppl : oppLD)
       keySet.add(oppl.Key_Year_Month__c);

  for(Opportunity opp : triggerOppList)
     if((triggerOppOldMap.get(opp.id).StageName == 'Closed /Won' || triggerOppOldMap.get(opp.id).StageName == 'Ready to book' || 
         opp.StageName == 'Closed /Won' || opp.StageName == 'Ready to book')
         &&  userinfo.getProfileId() != '00e30000000ceeiAAA' && keySet.contains(String.valueOf(opp.CloseDate.year())+String.valueOf(opp.CloseDate.month())))
      opp.adderror('The period of the close date is locked. if you need to edit, please consult with finance'); 
  }
}