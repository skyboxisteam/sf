public with sharing class ChannelAnalyzer extends CommonChannelHandler {
    private ApexPages.StandardController ctrl;

    
    public ChannelAnalyzer(ApexPages.StandardController ctrlParam){
        ctrl = ctrlParam;
      
        channel = getChannel(ApexPages.currentPage().getParameters().get('id'));
        getAllIds();
//      updateRecord();
    }
    
    public ChannelAnalyzer(String id) {
        channel = getChannel(id);   
        getAllIds();
    //      updateRecord();
    }
    
    public Integer getOpenPipeline() {
        List<AggregateResult> results = [SELECT sum(Amount) sum FROM Opportunity
            WHERE (AccountId in: ids or Partner__c in: ids or Distributor__c in: ids) and IsClosed = false];
        return Integer.valueOf(results[0].get('sum'));
    }

    public Integer getOpenPipelineThisQuarter() {
        List<AggregateResult> results = [SELECT sum(Amount) sum FROM Opportunity
            WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and IsClosed = false and CloseDate = THIS_FISCAL_QUARTER] ;
        return Integer.valueOf(results[0].get('sum'));
    }

    public Integer getWonThisYear() {
        List<AggregateResult> results = [SELECT sum(Amount) sum FROM Opportunity
//          WHERE (Accountid =: channel.id or Partner__c =: channel.id or Partner__r.ParentId =: channel.id )  and IsWon = true and CloseDate = THIS_FISCAL_YEAR] ;
            WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and IsWon = true and CloseDate = THIS_FISCAL_YEAR] ;
        return Integer.valueOf(results[0].get('sum'));
    }
    
    
    public Integer getWon365D() {
        List<AggregateResult> results = [SELECT sum(Amount) sum FROM Opportunity
           WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and IsWon = true and CloseDate >= LAST_N_DAYS:365] ;
        return Integer.valueOf(results[0].get('sum'));
    }
    
    // including where the channel is the customer
    
    public List<AggregateResult> getSalesByYear() {
        List<AggregateResult> results = [SELECT FISCAL_YEAR(CloseDate) year, sum(Amount) amount FROM Opportunity 
                    WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and IsWon = true and
                    CloseDate >= LAST_N_FISCAL_YEARS:5 group by FISCAL_YEAR(CloseDate) order by FISCAL_YEAR(CloseDate) ];
            
        return results;
    }

// including only new customers deals brought by the channel (lead source type is either "partner" or marketing, in case the campaign's partner refer to this channel

    public List<AggregateResult> getNewCustomerPipelineByYear() {
        List<AggregateResult> results = [SELECT FISCAL_YEAR(CreatedDate) year, sum(Amount) amount, count(id) opps FROM Opportunity 
                    WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and StageName <> 'Closed / Duplicate' and
                    (Lead_Source_Type__c = 'partner' or (Lead_Source_Type__c = 'marketing' and Campaign.Partner__c in: ids)) and
                    real_product_deal__c = 1 and CreatedDate >= LAST_N_FISCAL_YEARS:5 group by FISCAL_YEAR(CreatedDate) order by FISCAL_YEAR(CreatedDate) ];
            
        return results;
    }
    
    public List<AggregateResult> getCampaignsByYear() {
        List<AggregateResult> results = [SELECT FISCAL_YEAR(StartDate) year, count(id) campaigns FROM Campaign
                    WHERE (Partner__c in: ids) and StartDate >= LAST_N_FISCAL_YEARS:5
                    group by FISCAL_YEAR(StartDate) order by FISCAL_YEAR(StartDate) ];
            
        return results;
    }
    
    public Date getLatestCampaignDate() {
        List<AggregateResult> results = [SELECT max(StartDate) lastDate FROM Campaign WHERE (Partner__c in: ids)];
        if (results == null || results.size() == 0)
            return null;
            
        return Date.valueOf(results[0].get('lastDate'));
    }

    public Date getLatestPipelineProgressDate() {

        List<AggregateResult> results = [SELECT max(Last_Notable_Progress__c) lastDate FROM Opportunity
            WHERE (AccountId in: ids or Partner__c in: ids or Distributor__c in: ids)];

        if (results == null || results.size() == 0)
            return null;
            
        return Date.valueOf(results[0].get('lastDate'));
    }

    public Date getLatestWinDate() {

        List<AggregateResult> results = [SELECT max(CloseDate) lastDate FROM Opportunity
            WHERE (AccountId in: ids or Partner__c in: ids or Distributor__c in: ids) and IsWon = true];

        if (results == null || results.size() == 0)
            return null;
            
        return Date.valueOf(results[0].get('lastDate'));
    }

    public Date getLatestPipelineAdditionDate() {

        List<AggregateResult> results = [SELECT max(CreatedDate) lastDate FROM Opportunity
            WHERE (AccountId in: ids or Partner__c in: ids or Distributor__c in: ids)];

        if (results == null || results.size() == 0)
            return null;
            
        return Date.valueOf(results[0].get('lastDate'));
    }

// the last time partner brought us a new customer deal

    public Date getLatestNewCustomerPipelineOriginatedDate() {

        List<AggregateResult> results = [SELECT max(CreatedDate) lastDate FROM Opportunity 
                    WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids )  and StageName <> 'Closed / Duplicate' and
                    (Lead_Source_Type__c = 'partner' or (Lead_Source_Type__c = 'marketing' and Campaign.Partner__c in: ids)) and
                    Type = 'Products'];
                     
        if (results == null || results.size() == 0)
            return null;
            
        return Date.valueOf(results[0].get('lastDate'));
    }
    
    public Integer getEngagementScore() {
        Integer score = 0;
        
        score += Integer.valueOf([SELECT count(id) total FROM Opportunity 
                    WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and StageName <> 'Closed / Duplicate' and CreatedDate >= LAST_N_DAYS:365 and
                    (Lead_Source_Type__c = 'partner' or (Lead_Source_Type__c = 'marketing' and Campaign.Partner__c in: ids)) and
                    real_product_deal__c = 1][0].get('total'))*2;
        
        score += Integer.valueOf([SELECT count(id) campaigns FROM Campaign
                    WHERE (Partner__c in: ids) and StartDate >= LAST_N_dayS:365][0].get('campaigns'))*3;

        score += Integer.valueOf([SELECT count(id) total FROM Opportunity
            WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and IsWon = true and CloseDate >= LAST_N_DAYS:365 and real_product_deal__c = 1]
            [0].get('total'));
                     
        return score;       
    }

    public Integer getEngagementScore3M() {
        Integer score = 0;
        
        score += Integer.valueOf([SELECT count(id) total FROM Opportunity 
                    WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and StageName <> 'Closed / Duplicate' and CreatedDate >= LAST_N_DAYS:91 and
                    (Lead_Source_Type__c = 'partner' or (Lead_Source_Type__c = 'marketing' and Campaign.Partner__c in: ids)) and
                    real_product_deal__c = 1][0].get('total'))*2;
        
        score += Integer.valueOf([SELECT count(id) campaigns FROM Campaign
                    WHERE (Partner__c in: ids) and StartDate >= LAST_N_DAYS:91][0].get('campaigns'))*3;

        score += Integer.valueOf([SELECT count(id) total FROM Opportunity
            WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and IsWon = true and CloseDate >= LAST_N_DAYS:91 and real_product_deal__c = 1]
            [0].get('total'));
                     
        return score;       
    }

    public Integer getHistoricEngagementScore() {
        Integer score = 0;
        
        score += Integer.valueOf([SELECT count(id) total FROM Opportunity 
                    WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and StageName <> 'Closed / Duplicate' and
                    (Lead_Source_Type__c = 'partner' or (Lead_Source_Type__c = 'marketing' and Campaign.Partner__c in: ids)) and
                    real_product_deal__c = 1][0].get('total'))*2;
        
        score += Integer.valueOf([SELECT count(id) campaigns FROM Campaign
                    WHERE (Partner__c in: ids) ][0].get('campaigns'))*3;

        score += Integer.valueOf([SELECT count(id) total FROM Opportunity
                    WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and IsWon = true and real_product_deal__c = 1][0].get('total'));
                             
        return score;       
    }
    
    public void updateRecord() {
        if (channel == null)
            return;
        
        channel.Channel_Engagement_Score__c = getEngagementScore();
        channel.Partner_Engagement__c = channel.Channel_Engagement_Score__c > 0 ? 'Active' : 'Inactive';
        channel.Channel_Engagement_Score_3M__c = getEngagementScore3M();
        channel.Channel_Engaged_Last_3M__c = channel.Channel_Engagement_Score_3M__c > 0; 
        channel.Channel_Won_365D__c = getWon365D();
        
        channel.Channel_Historic_Engagement_Score__c = getHistoricEngagementScore();
        
        System.debug('Channel: ' + channel.name + ' engagement score: ' + channel.Channel_Engagement_Score__c);
        update channel;
    }

    
}