public with sharing class CommissionRecordHandler {
	public CommissionRecordHandler() {
		
	}

	/**
    *@Author Rina Nachbas                           events: befor Insert
    *@Date 1.11.18
    *@Task: CM-64
    *@Description : If the opportunity probability is less than 95% and a user tries to created a commission record will get an error.
    */ 
  
   public static void probabilityValidation(list<Commission_Record__c> newList)
  {
    Map<Id, Id> crOppMap = new Map<Id, Id>();

     for(Commission_Record__c cr : newList)
     {
     	if(cr.Type__c != 'PIO'){
          crOppMap.put(cr.Id, cr.Opportunity__c	);
     	}
     }

     Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([select Id, Probability from Opportunity where Id in :crOppMap.values() AND Probability < 95]);
     for(Commission_Record__c cr : newList)
     {
        if(oppMap.containsKey(cr.Opportunity__c) && cr.Type__c != 'PIO')
          cr.addError(Label.CROpportunityProbabilityError);
     }
  }
}