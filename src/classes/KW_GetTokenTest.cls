@isTest(SeeAllData=true) 
private class KW_GetTokenTest {

    @isTest
    static void KW_GetTokenTest() {
        Test.startTest();
        KW_GetToken.GetToken();
		Test.setMock(HttpCalloutMock.class, new KW_GetTokenCalloutMockTest());
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        HttpResponse response = KW_GetToken.MakeCallOut('sss','https://skyboxsecurity.kiteworks.com');
        // Verify that the response received contains fake values
        String contentType = response.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = response.getBody();
        System.debug(response.getBody());
        String expectedValue = '{"access_token":"72e1e44cd1757831832140f5c754b39ba0ba525d","expires_in":86400,"token_type":"bearer","scope":"**","refresh_token":"826457e53812fe5dacc7d7606ba8a79be3d2f8cd"}';
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(200, response.getStatusCode());
        
        if (response.getStatusCode() == 200) {
            System.debug(response.getBody());
            JSONParser parser = JSON.createParser(response.getBody());
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if(fieldName == 'access_token'){
                        break;
                    }
                }
            }
        }

        Test.stopTest();
    }

	@isTest
    static void TriggerTest() {
    	test.startTest();
    	KW_GetToken_Scheduler_Trigger GTS = new KW_GetToken_Scheduler_Trigger();
    	String schedule = '0 0 23 * * ?';
    	system.schedule('Nightly Update', schedule, GTS);
    	test.stopTest();
    }

}