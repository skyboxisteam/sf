global class LeadJobSchedulable implements  Schedulable {
	
	//Call from Anonymous: LeadJobSchedulable.scheduleIt();

	/*
    Run at 8:30 am every day. Remember this is
    Scheduled in the User’s timezone who schedules the job, not UTC
    */
    // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
  	public static final String CRON_EXPR = '0 9 13 * * ?';

   	global void execute(SchedulableContext ctx) {
        ScheduledAlert sa = new ScheduledAlert();
        sa.sendAlertToDealRegApprovedNotConvert();
        sa.sendAlertToDealRegistrationExpiryWarning();
        sa.sendAlertToDealRegistrationExpired();
        sa.sendAlertToOppExpiryWarning();
        sa.sendAlertToOppExpired();

    }
    /*
    Call this from Anonymous Apex to schedule at the default regularity
  	*/
  	global static String scheduleIt() {
  	  LeadJobSchedulable job = new LeadJobSchedulable();
  	  return System.schedule('Schedule lead alerts', CRON_EXPR, job);
  	}

	
	global void finish(Database.BatchableContext BC) {
		
	}

  public class ScheduledAlert{

   Set<String> rtSet = new Set<String>{'Deal_Registration'};
   Set<String> leadSourceDetailSet = new Set<String>{'Magentrix Partner Portal'};


      //Send alert if lead is RT='Deal_Registration', leadSourceDetail='Magentrix Partner Portal', lead is NOT converted, status='Accepted'
      // and Deal_Reg_Accept_Reject_Date__c is befor 7 days or after 14 days or multiply of 14, send after 7 days, and every 14 days
      //By virgini: on Accepted date and every 14 day.
      public void sendAlertToDealRegApprovedNotConvert(){
          //List<String> roles = new List<String>{'Account Manager', 'Sales Engineer.'};
          List<Lead> leadToAlert = new List<Lead>();
          SendEmail sendEmail = new SendEmail();
          sendEmail.templateName = 'Approved & Attributed Deal Registration Alert-Internal';
          
          //Date warnDate = date.today().addDays(7);
          Integer warnInterval = 14;
          //Integer warnFirst = 7;
          List<Lead> leads = [select Id, Name__c, Deal_Reg_Accept_Reject_Date__c from Lead 
                      where IsConverted = false AND Lead_Source_Detail__c in :leadSourceDetailSet AND recordType.DeveloperName in :rtSet 
                          AND Deal_Reg_Status__c = 'Accepted' AND Deal_Reg_Accept_Reject_Date__c != null];
          
          for( Lead lead : leads)
          {
            //system.debug('@@@sendAlertToDealRegApprovedNotConvert: lead.Deal_Reg_Accept_Reject_Date__c: ' + lead.Deal_Reg_Accept_Reject_Date__c);
            //system.debug('@@@sendAlertToDealRegApprovedNotConvert: date.today(): ' + date.today());
            //system.debug('@@@sendAlertToDealRegApprovedNotConvert: math.mod(date.today().daysBetween(lead.Deal_Reg_Accept_Reject_Date__c),14): ' + math.mod(date.today().daysBetween(lead.Deal_Reg_Accept_Reject_Date__c),14));
            //Deal_Reg_Accept_Reject_Date__c is befor 7 days or befor 14 days or multiply of 14, send after 7 days, and every 14 days
            if(lead.Deal_Reg_Accept_Reject_Date__c == date.today() || //lead.Deal_Reg_Accept_Reject_Date__c == warnDate ||
              (lead.Deal_Reg_Accept_Reject_Date__c < date.today() && math.mod(date.today().daysBetween(lead.Deal_Reg_Accept_Reject_Date__c),warnInterval) == 0 ) )
              leadToAlert.add(lead);
          }
          system.debug('##sendAlert: leadToAlert: ' + leadToAlert);

          for( Lead lead : leadToAlert)
          {
            sendEmail.whatId = lead.Id;
            sendEmail.sendEmailToAccountTeam(lead.Name__c);//accountId
          }
      }

      //Send alert if lead is RT='Deal_Registration', leadSourceDetail='Magentrix Partner Portal', lead is NOT converted, status='Accepted'
      // and Magentrix_Partner_Expiration_Date__c is expired in more 7 days
      public void sendAlertToDealRegistrationExpiryWarning(){
          //List<String> roles = new List<String>{'Account Manager', 'Sales Engineer.'};
          SendEmail sendEmail = new SendEmail();
          sendEmail.templateName = 'Deal Registration Expiry Warning-INTERNAL&EXTERNAL';
          
          Date warnDate = date.today().addDays(7);
          List<Lead> leadToAlert = [select Id, Name__c from Lead 
                      where IsConverted = false AND Lead_Source_Detail__c in :leadSourceDetailSet AND recordType.DeveloperName in :rtSet 
                          AND Magentrix_Partner_Expiration_Date__c = :warnDate
                          AND Next_Expiration_Email__c = 'Next: 7 Days Expiration'];
          

          system.debug('##sendAlert: leadToAlert: ' + leadToAlert);

          for( Lead lead : leadToAlert)
          {
            lead.Next_Expiration_Email__c = 'Next: Expiration';
            sendEmail.whatId = lead.Id;
            sendEmail.sendEmailToAccountTeam(lead.Name__c);//accountId
          }
          if(leadToAlert.size() > 0)
            update leadToAlert;
      }

      //Send alert if lead is RT='Deal_Registration', leadSourceDetail='Magentrix Partner Portal', lead is NOT converted, status='Accepted'
      // and Magentrix_Partner_Expiration_Date__c is expired Today
      public void sendAlertToDealRegistrationExpired()
      {
          //List<String> roles = new List<String>{'Account Manager', 'Sales Engineer.'};
          SendEmail sendEmail = new SendEmail();
          sendEmail.templateName = 'Deal Registration Expired-INTERNAL&EXTERNAL';
          
          Date warnDate = date.today();
          List<Lead> leadToAlert = [select Id, Name__c from Lead 
                        where IsConverted = false AND Lead_Source_Detail__c in :leadSourceDetailSet AND recordType.DeveloperName in :rtSet 
                          AND Magentrix_Partner_Expiration_Date__c = :warnDate
                          AND Next_Expiration_Email__c = 'Next: Expiration'];
          

          system.debug('##sendAlert: leadToAlert: ' + leadToAlert);

          for( Lead lead : leadToAlert)
          {
            lead.Next_Expiration_Email__c = 'Expired';
            lead.Deal_Reg_Status__c = 'Expired';
            sendEmail.whatId = lead.Id;
            sendEmail.sendEmailToAccountTeam(lead.Name__c);//accountId
          }
          if(leadToAlert.size() > 0)
            update leadToAlert;
      }


      //Send alert to Opportunity if Not Closed
      // and Magentrix_Partner_Expiration_Date__c is expired in more 7 days
      public void sendAlertToOppExpiryWarning(){
          //List<String> roles = new List<String>{'Account Manager', 'Sales Engineer.'};
          SendEmail sendEmail = new SendEmail();
          sendEmail.templateName = 'Opportunity Expiry Warning-INTERNAL&EXTERNAL';
          
          Date warnDate = date.today().addDays(7);
          List<Opportunity> oppToAlert = [select Id,Partner__c from Opportunity 
                          where IsClosed = false 
                              AND MagentrixOne__ExclusivityExpirationDate_mgtrx__c = :warnDate
                              AND Next_Expiration_Email__c = 'Next: 7 Days Expiration'];
          

          system.debug('##sendAlert: oppToAlert: ' + oppToAlert);

          for( Opportunity opp : oppToAlert)
          {
            opp.Next_Expiration_Email__c = 'Next: Expiration';
            sendEmail.whatId = opp.Id;
            sendEmail.sendEmailToAccountTeam(opp.Partner__c);
          }
          if(oppToAlert.size() > 0)
            update oppToAlert;
      }

      //Send alert to Opportunity if Not Closed
      // and Magentrix_Partner_Expiration_Date__c is expired Today
      public void sendAlertToOppExpired()
      {
          //List<String> roles = new List<String>{'Account Manager', 'Sales Engineer.'};
          SendEmail sendEmail = new SendEmail();
          sendEmail.templateName = 'Opportunity Expired-INTERNAL&EXTERNAL';
          
          Date warnDate = date.today();
          List<Opportunity> oppToAlert = [select Id,Partner__c from Opportunity 
                          where IsClosed = false 
                              AND MagentrixOne__ExclusivityExpirationDate_mgtrx__c = :warnDate
                              AND Next_Expiration_Email__c = 'Next: Expiration'];
          

          system.debug('##sendAlert: oppToAlert: ' + oppToAlert);

          for( Opportunity opp : oppToAlert)
          {
            opp.Next_Expiration_Email__c = 'Expired';
            opp.Deal_Reg_Status__c = 'Expired';
            sendEmail.whatId = opp.Id;
            sendEmail.sendEmailToAccountTeam(opp.Partner__c);
          }
          if(oppToAlert.size() > 0)
            update oppToAlert;
      }
    }
	
}