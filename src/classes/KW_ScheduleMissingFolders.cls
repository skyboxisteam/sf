global with sharing class KW_ScheduleMissingFolders implements Schedulable {
  
    global void execute(SchedulableContext sc)
	  {
	    list<case> caseList = [Select id,caseNumber,KW_CaseFolderUploadLink__c , KW_FolderID__c, KW_Folder_Link2__c from Case where KW_CaseFolderUploadLink__c = '' and createdDate = last_N_days:2];
	    for(Case updateCase : caseList)
	    if (!Test.isRunningTest())
        KW_NewCase.FolderCreation( string.valueOf(updateCase.CaseNumber));
	  }
}