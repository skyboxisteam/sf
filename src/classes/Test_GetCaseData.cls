@isTest
public class Test_GetCaseData {
    
    static testMethod void testGetCaseData() {
        CaseTriggerSettings__c appConfig = new CaseTriggerSettings__c();
        appConfig.Name = 'Case Owner Trigger';
        appConfig.UserRoleName__c = 'TLS';
        appConfig.QueueName__c = 'Incoming Cases - R&D';
        insert appConfig;
        Group g = new Group(Type = 'Queue', Name = 'Incoming Cases - R&D'); 
        insert g;
        Case cas = new Case(Status ='New', Priority = 'Medium', Origin = 'Email', Subject='test');
        insert cas;
        cas = getCaseData.getCase(cas.Id);
        cas = getCaseData.getCaseForEscalation(cas.Id);

        CaseComment casCom = new CaseComment(CommentBody = 'CommentBody', ParentId = cas.id, isPublished = true);
        insert casCom;
        String status = getCaseData.closeCaseAndInsertComment(JSON.serialize(cas), JSON.serialize(casCom));
        getCaseData.getCaseReason();
        getCaseData.getCaseStatuses();
    }
}