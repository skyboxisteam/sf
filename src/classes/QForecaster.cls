public with sharing class QForecaster  {
    private ApexPages.StandardController ctrl;
    
    public QForecaster(ApexPages.StandardController ctrlParam){
    }
    
    public QForecaster(){
    }
    
  public Integer getCurrentWonAndForecast() {

        List<AggregateResult> results = [SELECT COUNT_DISTINCT(Opportunity__r.id) deals FROM Booking_schedule__c
                    WHERE  (Opportunity__r.Skybox_Forecast_Category__c = 'won' or Opportunity__r.Skybox_Forecast_Category__c = 'forecast') and real_product_booking__c = true and
                            Opportunity__r.Adjusted_Close_Date__c = THIS_FISCAL_QUARTER];
        return Integer.valueOf(results[0].get('deals'));
    }

   // getProductsStatsByQuarter from booking schedule according to Jira case SF-1083
    public Map<String, Integer> getProductsStatsByQuarter() {
        Map<String,Integer> m = new Map<String,Integer>();
        for (Integer q = 1 ; q <=4 ; q++) {
            List<AggregateResult> results = [SELECT FISCAL_YEAR(Opportunity__r.Adjusted_Close_Date__c) year, COUNT_DISTINCT(Opportunity__r.id) deals FROM Booking_schedule__c 
                    WHERE  Opportunity__r.Skybox_Forecast_Category__c = 'won' and Real_Product_Booking__c = true and
                    Opportunity__r.Adjusted_Close_Date__c >= LAST_N_FISCAL_QUARTERS:20 and Opportunity__r.Adjusted_Close_Date__c < THIS_FISCAL_QUARTER and
                    FISCAL_QUARTER(Opportunity__r.Adjusted_Close_Date__c) = : q
                    group by FISCAL_YEAR(Opportunity__r.Adjusted_Close_Date__c) order by FISCAL_YEAR(Opportunity__r.Adjusted_Close_Date__c) ];
                    
            for (Integer j = 0 ; j < results.size() ; j++ ) {
                Integer deals = Integer.valueOf(results[j].get('deals'));
                Integer year = Integer.valueOf(results[j].get('year'));
                String s = year + '-' + q;
                m.put(s, deals);
            }        
        }
        
        return m;
    }
   //ProductsForecsastedBefore from booking schedule according to Jira case SF-1083
   public Map<String, Integer> getProductsForecsastedBeforeEOQ(long d) {
        Map<String,Integer> m = new Map<String,Integer>();
        for (Integer q = 1 ; q <=4 ; q++) {
            List<AggregateResult> results = [SELECT FISCAL_YEAR(Opportunity__r.Adjusted_Close_Date__c) year,  COUNT_DISTINCT (Opportunity__r.id) deals FROM Booking_schedule__c 
                    WHERE  Opportunity__r.Skybox_Forecast_Category__c = 'won' and Real_Product_Booking__c = true and
                    Opportunity__r.Days_from_forecast_to_quarter_end__c <=: d and 
                    Opportunity__r.Adjusted_Close_Date__c >= LAST_N_FISCAL_QUARTERS:20 and Opportunity__r.Adjusted_Close_Date__c < THIS_FISCAL_QUARTER and
                    FISCAL_QUARTER(Opportunity__r.Adjusted_Close_Date__c) =: q
                    group by FISCAL_YEAR(Opportunity__r.Adjusted_Close_Date__c) order by FISCAL_YEAR(Opportunity__r.Adjusted_Close_Date__c) ];
                    
            for (Integer j = 0 ; j < results.size() ; j++ ) {
                Integer deals = Integer.valueOf(results[j].get('deals'));
                Integer year = Integer.valueOf(results[j].get('year'));
                String s = year + '-' + q;
                m.put(s, deals);
            }        
        }

        
        return m;
    }


 /* Commented due to change request in Jira case SF-1083
    public Integer getCurrentWonAndForecast() {

        List<AggregateResult> results = [SELECT count(id) deals FROM Booking_schedule__c
                    WHERE  (Opportunity__r.Skybox_Forecast_Category__c = 'won' or Opportunity__r.Skybox_Forecast_Category__c = 'forecast') and real_product_booking__c = true and
                    Opportunity__r.Current_booking__c = true and Opportunity__r.Adjusted_Close_Date__c = THIS_FISCAL_QUARTER];
        return Integer.valueOf(results[0].get('deals'));
    }
       
    public Map<String, Integer> getProductsStatsByQuarter() {
        Map<String,Integer> m = new Map<String,Integer>();
        for (Integer q = 1 ; q <=4 ; q++) {
            List<AggregateResult> results = [SELECT FISCAL_YEAR(Adjusted_Close_Date__c) year, count(id) deals FROM Opportunity 
                    WHERE  Skybox_Forecast_Category__c = 'won' and real_product_deal__c = 1 and
                    Adjusted_Close_Date__c >= LAST_N_FISCAL_QUARTERS:20 and Adjusted_Close_Date__c < THIS_FISCAL_QUARTER and
                    FISCAL_QUARTER(Adjusted_Close_Date__c) =: q and Current_booking__c = true 
                    group by FISCAL_YEAR(Adjusted_Close_date__c) order by FISCAL_YEAR(Adjusted_Close_date__c) ];
                    
            for (Integer j = 0 ; j < results.size() ; j++ ) {
                Integer deals = Integer.valueOf(results[j].get('deals'));
                Integer year = Integer.valueOf(results[j].get('year'));
                String s = year + '-' + q;
                m.put(s, deals);
            }        
        }
        
        return m;
    }

   public Map<String, Integer> getProductsForecsastedBeforeEOQ(long d) {
        Map<String,Integer> m = new Map<String,Integer>();
        for (Integer q = 1 ; q <=4 ; q++) {
            List<AggregateResult> results = [SELECT FISCAL_YEAR(Adjusted_Close_Date__c) year, count(id) deals FROM Opportunity 
                    WHERE  Skybox_Forecast_Category__c = 'won' and real_product_deal__c = 1 and
                    Days_from_forecast_to_quarter_end__c <=: d and Current_booking__c = true and 
                    Adjusted_Close_Date__c >= LAST_N_FISCAL_QUARTERS:20 and Adjusted_Close_Date__c < THIS_FISCAL_QUARTER and
                    FISCAL_QUARTER(Adjusted_Close_Date__c) =: q
                    group by FISCAL_YEAR(Adjusted_Close_date__c) order by FISCAL_YEAR(Adjusted_Close_date__c) ];
                    
            for (Integer j = 0 ; j < results.size() ; j++ ) {
                Integer deals = Integer.valueOf(results[j].get('deals'));
                Integer year = Integer.valueOf(results[j].get('year'));
                String s = year + '-' + q;
                m.put(s, deals);
            }        
        }
        
        return m;
    }*/
    
    public List<Record> getRecords() {
        Map<String,Integer> deals = getProductsStatsByQuarter();
        Map<String,Integer> additionalForecast = getProductsForecsastedBeforeEOQ(getDaysToEOQ());
        List<Record> records = new List<Record>(deals.size());
 
         Integer i = 0;
        for (String q : deals.keySet()) {
            Record r = new Record();
            r.quarter = q;
            r.deals = deals.get(q)==null ? 0:deals.get(q);
            r.additions = additionalForecast.get(q) == null ? 0:additionalForecast.get(q) ;
         
            r.ratio = (r.deals == r.additions) ? 0 : r.additions * 1.0 / (r.deals - r.additions);
            records[i++] = r;
        
        }
        return records;
    }
    
    public Stats getStats() {
        Stats stats = new Stats(0,0);
        
        List<Record> records = getRecords();
        Integer c = 0;
        Decimal expected = 0;
        List<Decimal> values = new List<Decimal>(records.size());
        
        for (Integer i = 0 ; i < records.size() ; i++ ) {
            Decimal d = records[i].deals;
            Decimal f = records[i].additions;
            if (d == 0)
                continue;
            if (d == f) 
                continue;
            
            Decimal v = records[i].ratio;
            values[c++] = v; 
            expected += v;
        }
        
        if (c == 0)
            return stats;
            
        expected = expected / c;
        
        Decimal variance = 0;
        
        for (Integer i = 0 ; i < c ; i++ )
            variance += (values[i]- expected) * (values[i] - expected);
 
        stats.expected = expected;
        stats.stdDev = math.sqrt(variance/c);
        return stats;
    }

    public Integer getDaysToEOQ() {
        Integer y = system.today().year() + (system.today().month()>=10 ? 1 :0);
        Integer m = math.mod(math.round((system.today().month()+1.0)/3)*3,12)+1;
            
        return system.today().daysBetween(Date.newInstance(y,m,1)) - 1;
    }
    
    public Integer getExpectedProductDeals() {
       Stats s = getStats();
       Integer p = math.round(getCurrentWonAndForecast() * (1 + s.expected));

        return p;
    }
    
    public Integer getStandardDeviation() {
        Stats s = getStats();
        Integer p = math.round(getCurrentWonAndForecast() * s.stdDev);

        return p;
    }

    public class Stats {
        public Decimal stdDev { get; set; }
        public Decimal expected { get; set; }

    
        public Stats(Decimal expected, Decimal stdDev) {
            this.expected = expected;
            this.stdDev = stdDev;
        }

    }
    
    public class Record {
        public Integer deals  { get; set; }
        public Integer additions  { get; set; }
        public Decimal ratio  { get; set; }
        public String quarter { get; set; }
    }
//
    
}