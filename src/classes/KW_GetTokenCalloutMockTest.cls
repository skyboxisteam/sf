@isTest
global class KW_GetTokenCalloutMockTest implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"access_token":"72e1e44cd1757831832140f5c754b39ba0ba525d","expires_in":86400,"token_type":"bearer","scope":"**","refresh_token":"826457e53812fe5dacc7d7606ba8a79be3d2f8cd"}');
        response.setStatusCode(200);
        return response;
    }
}