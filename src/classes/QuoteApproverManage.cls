global without sharing class QuoteApproverManage implements  Database.Stateful,Schedulable{
  
 global void execute(SchedulableContext sc)
    { 
      list<id> objectId = new list<id>();
      Decimal hourGap ;
      map<id,id> mapQuoteApprover = new map<id,id>();
      list<string> emails = new list<string>();
      list<SBQQ__Quote__c> result = new list<SBQQ__Quote__c>();
      list<SBQQ__Quote__c> listQuote = [select id, Current_Approver__c ,Current_Approver__r.email ,Pending_approval_timestamp__c
                                        from SBQQ__Quote__c 
                                        where (SBQQ__Status__c = 'In Review' OR Current_Approver__c != null )and Pending_approval_timestamp__c != null]; 
      
      for(SBQQ__Quote__c q :listQuote ) 
          objectId.add(q.id);
      
      List<ProcessInstance> pis = [Select TargetObjectId ,(Select Id, Actorid From Workitems) From ProcessInstance p WHERE p.TargetObjectId in:objectid AND  p.Status = 'Pending'];
       for(ProcessInstance pi : pis)
         for(List<ProcessInstanceWorkitem> wis : pi.Workitems) 
            for (ProcessInstanceWorkitem wi : wis ) 
                mapQuoteApprover.put(pi.TargetObjectId , wi.Actorid);
      
    for(SBQQ__Quote__c q :listQuote)
     {
  
       if(mapQuoteApprover.containsKey(q.id))
       {
          q.Current_Approver__c = mapQuoteApprover.get(q.id);
          hourGap = decimal.valueof(system.now().getTime() - q.Pending_approval_timestamp__c.getTime()) ;
          if(hourGap/3600000>8)
           {
           	q.Pending_approval_timestamp__c = system.now();
            q.Send_Reminder__c = true;
           }
       }
       else
          q.Current_Approver__c = null;
  
         result.add(q);
      }
     update result;

      
      
    }  
}