public class KW_NewCase {

    public static String GetKWFileRequestLink ( String RequestFileID , KiteWorks_Integration__c KW ) {
        System.debug('KW New Case - GetKWFileRequestLink started for RequestFileID :' + RequestFileID);
        String RequestLink = '-1';
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        request.setHeader('Accept', 'application/json');
		request.setHeader('Authorization', 'Bearer '+ KW.Token__c);
		request.setHeader('X-Accellion-Version' , KW.API_Version__c);
		request.setMethod('GET');
        request.setTimeout(120000);
        
        request.setEndpoint(KW.Host__c+'rest/requestFile/'+RequestFileID);
        HttpResponse response = http.send(request);
        
        // If success - parsing token from JSON and get the RequestLink
        if (response.getStatusCode() == 200) {
            // Deserialize the JSON string into collections of primitive data types.
            System.debug(response.getBody());
            JSONParser parser = JSON.createParser(response.getBody());
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                    String fieldName = parser.getText();
                    System.debug('fieldName: ' + fieldName);
                    parser.nextToken();
                    if(fieldName == 'uploadLink'){
                        RequestLink = parser.getText() ;
                        System.debug( RequestLink );
                        break;
                    }
                }
            }
        }
        else {
            System.debug( 'KW Error in GetKWFileRequestLink ' + response.getStatus() );
        }
    System.debug('KW New Case - GetKWFileRequestLink Finished for RequestFileID :' + RequestFileID);    
	return RequestLink;
    }
    
    
    // Get request ID from activities:
    public static String GetKWFileRequestID ( String FolderID , KiteWorks_Integration__c KW) {
        System.debug('KW New Case - GetKWFileRequestID started for Folder ID :' + FolderID);
        String RequestFileID = '-1';
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        request.setHeader('Accept', 'application/json');
		request.setHeader('Authorization', 'Bearer '+ KW.Token__c);
		request.setHeader('X-Accellion-Version' , KW.API_Version__c);
		request.setMethod('GET');
        request.setTimeout(120000);

        request.setEndpoint(KW.Host__c+'rest/folders/'+FolderID+'/activities?noDayBack=1&filter=all&type=mail&orderBy=created%3Adesc&mode=full');
        
        HttpResponse response = http.send(request);
        
        // Debug status
        System.debug( 'KW GetKWFileRequestID exit code For Folder ID ' + FolderID + ':' + response.getStatusCode() + response.getStatus() );
		
        // If success - parsing token from JSON and get the request_file_ref
        if (response.getStatusCode() == 200) {
            // Deserialize the JSON string into collections of primitive data types.
            System.debug(response.getBody());
            JSONParser parser = JSON.createParser(response.getBody());
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                    String fieldName = parser.getText();
                    System.debug('fieldName: ' + fieldName);
                    parser.nextToken();
                    if(fieldName == 'request_file_ref'){
                        RequestFileID = parser.getText();
                        System.debug( RequestFileID );
                        break;
                    }
                }
            }
        }
        else {
            System.debug( 'Error ' + response.getStatus() );
        }
        System.debug('KW New Case - GetKWFileRequestID finished for Folder ID :' + FolderID);
        return RequestFileID;
    }
    
    
    public static Integer CreateKWFileRequest ( String FolderID , KiteWorks_Integration__c KW) {   
        System.debug('KW New Case - CreateKWFileRequest started for Folder ID :' + FolderID);

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setTimeout(120000);

        request.setEndpoint(KW.Host__c+'rest/folders/'+FolderID+'/actions/requestFile');
        
        request.setMethod('POST');
        
        request.setHeader('content-Type', 'application/json');
        request.setHeader('Accept', 'application/json');
		request.setHeader('Authorization', 'Bearer '+KW.Token__c);
		request.setHeader('X-Accellion-Version' , KW.API_Version__c);
        
        String ReqBody = '{"to":["KW_SF_APIUser@skyboxsecurity.com"],"subject":"UploadFileRequest","body":"UploadFileRequest","expire":"2216-07-17T12:42:06.621Z","count": 20,"requireAuth":false,"actionId":"1","files":[],"secureBody":false}';        
        
        request.setBody(ReqBody);

        HttpResponse response = http.send(request);
        
        // Debug status
        System.debug( 'KW CreateKWFileRequest For ' + FolderID + ' Exit code: ' + response.getStatusCode() + response.getStatus() );
        System.debug('KW New Case - CreateKWFileRequest finished for Folder ID :' + FolderID);
		return response.getStatusCode();
    }


    public static String MakeCallOutCreateCurrentFolder(String CaseNumber, KiteWorks_Integration__c KW){
        // Creating Request and sending
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        request.setEndpoint(KW.Host__c+'rest/folders/'+KW.RootFolderID__c+'/folders?returnEntity=true');
        request.setTimeout(120000);
        
        request.setMethod('POST');
        
        request.setHeader('content-Type', 'application/json');
        request.setHeader('Accept', 'application/json');
		request.setHeader('Authorization', 'Bearer '+KW.Token__c);
		request.setHeader('X-Accellion-Version' , '4.1');
        
        String ReqBody = '{"name":"'+CaseNumber+'-'+CaseNumber.removeEnd('000')+'999","description":"Parent Folder for cases Numbers '+CaseNumber+' To ","syncable":false,"expire":"2116-07-17","fileLifetime": 0,"secure":false}';
        request.setBody(ReqBody);
        
        HttpResponse response = http.send(request);
        
        if (response.getStatusCode() == 201) {
        // Get Case Folder ID:
        	System.debug(response.getHeaderKeys());
            System.debug(response.getHeader('X-Accellion-Location'));
            String NewCurrentFolderId = response.getHeader('X-Accellion-Location').split('/').get( response.getHeader('X-Accellion-Location').split('/').size()-1 ) ;
            System.debug('KW Parent Folder creation for Cases Numbers '+CaseNumber+' To - folder creation in KW response: ' + response.getStatus() );
            return NewCurrentFolderId;
        }
        else {
            System.debug('KW Issue in Create Parent folder '+CaseNumber);
            KW_SendEmail.SendEmail('KW Issue in Create Parent folder','Case Number: '+CaseNumber);
        	return KW.CurrentFolderID__c;
        }
    }
    
    @future(callout=true)
    public static void FolderCreation( String CaseNumber) {
        KiteWorks_Integration__c KW = KiteWorks_Integration__c.getValues('TestCloud');
        String permalink;
        String CurrentFolderID = KW.CurrentFolderID__c;
        
        System.debug('KW Folder creation started for Case Number:' + CaseNumber);
        //System.debug('Token: '+KW.Token__c);
        // Creating folder
        // POST /rest/folders/{parent_id}/folders

        if ( CaseNumber.endsWith('000') )
        	CurrentFolderID = MakeCallOutCreateCurrentFolder(CaseNumber , KW);
        
        // Creating Request and sending
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        request.setEndpoint(KW.Host__c+'rest/folders/'+CurrentFolderID+'/folders?returnEntity=true');
        request.setTimeout(120000);
        
        request.setMethod('POST');
        
        request.setHeader('content-Type', 'application/json');
        request.setHeader('Accept', 'application/json');
		request.setHeader('Authorization', 'Bearer '+KW.Token__c);
		request.setHeader('X-Accellion-Version' , '4.1');
        
        
        String ReqBody = '{"name":"'+CaseNumber+'","description":"Case Number '+CaseNumber+'","syncable":false,"expire":"2116-07-17","fileLifetime": 0,"secure":false}';
        
        request.setBody(ReqBody);

        HttpResponse response = http.send(request);
        
        // Debug status
        System.debug('KW Folder creation for Case Number '+CaseNumber+' - folder creation in KW response: ' + response.getStatus() );
        
        // If success
        if (response.getStatusCode() == 201) {
       
            // Get Case Folder ID:
            String CaseFolderId = response.getHeader('X-Accellion-Location').split('/').get( response.getHeader('X-Accellion-Location').split('/').size()-1 ) ;
			System.debug( 'KW Folder Creation succeed. folder id: '+ CaseFolderId );
            
            JSONParser parser = JSON.createParser(response.getBody());
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                    String fieldName = parser.getText();
                    System.debug('fieldName: ' + fieldName);
                    parser.nextToken();
                    if(fieldName == 'permalink'){
                        permalink = parser.getText();
                        System.debug( 'PermLINKKKKK: '+permalink );
                        break;
                    }
                }
            }
            
            // create File Request:
            Integer CreateKWFExitCode = KW_NewCase.CreateKWFileRequest( CaseFolderId , KW );
            
            if (CreateKWFExitCode == 200) {
            	// Get File Request ID:
                String RequestFileID = KW_NewCase.GetKWFileRequestID( CaseFolderId , KW );
                
                if (RequestFileID != '-1') {
                    // Get File Request Link:
                    String RequestLink = KW_NewCase.GetKWFileRequestLink ( RequestFileID, KW );
        			if (RequestLink != '-1') {
                        Case CaseToUpdate = [select id from case where CaseNumber = :CaseNumber];    
                        
                        CaseToUpdate.KW_CaseFolderUploadLink__c=RequestLink;
                        CaseToUpdate.KW_FolderID__c=CaseFolderId;
                        CaseToUpdate.KW_Folder_Link2__c = permalink;
                        update CaseToUpdate;
                    }
                    else {
                        System.debug('KW Issue in RequestLink '+CaseFolderId);
                        KW_SendEmail.SendEmail('KW Issue in New Case - RequestLink','Case Folder ID: '+CaseFolderId+' Case Number: '+CaseNumber);
                    }
                }
                else {
                    System.debug('KW Issue in RequestFileID '+CaseFolderId);
                    KW_SendEmail.SendEmail('KW Issue in New Case - RequestFileID ','Case Folder ID: '+CaseFolderId+' Case Number: '+CaseNumber);
                }
            }
            else {
                System.debug('KW Issue in CreateKWFileRequest '+CaseFolderId);
                KW_SendEmail.SendEmail('KW Issue in New Case - CreateKWFileRequest ','Case Folder ID: '+CaseFolderId+' Case Number: '+CaseNumber);
            }
        }
        else {
            System.debug('KW Issue in New Case - Folder Creation for case: '+CaseNumber);
            KW_SendEmail.SendEmail('KW Issue in New Case - Folder Creation','Case Number: '+CaseNumber);
        }
        System.debug('Folder creation finished for Case Number:' + CaseNumber);
        KW.CurrentFolderID__c = CurrentFolderID;
        update KW;
    }
}