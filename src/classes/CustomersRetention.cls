public with sharing class CustomersRetention {
  private ApexPages.StandardController ctrl;

  public CustomersRetention(ApexPages.StandardController ctrlParam){
    ctrl = ctrlParam;
  }
  
  public CustomersRetention() {
    
  }
  
    public List<AggregateResult> getAddOnWonDeals() {
    return [SELECT FISCAL_YEAR(Booking_Date__c) year, count(Id) deals FROM Booking_Schedule__c
          WHERE Opportunity__r.IsWon = true and Real_Product_Booking__c = true and Opportunity__r.First_year_deal_0_1__c = 0         
          group by FISCAL_YEAR(Booking_Date__c) 
          order by FISCAL_YEAR(Booking_Date__c) ];
  }

  public List<AggregateResult> getNewLongTermCustomers() {
    return [SELECT FISCAL_YEAR(Group_First_booked__c) year, count(Id) customers FROM Account 
          WHERE Group_first_year__c > 0 
          and Group_Long_Term_Use__c = true 
          and Top_Level_Account__c = true
          group by FISCAL_YEAR(Group_First_booked__c) 
          order by FISCAL_YEAR(Group_First_booked__c) ];
  }

  public List<AggregateResult> getLostLongTermCustomers() {
    return [SELECT FISCAL_YEAR(Group_Last_Won__c) year, count(Id) customers FROM Account 
          WHERE Group_first_year__c > 0 and Group_Active_Customer__c = 0 
          and Group_Long_Term_Use__c = true 
          and Top_Level_Account__c = true
          group by FISCAL_YEAR(Group_Last_Won__c)
          order by FISCAL_YEAR(Group_Last_Won__c) ];
  }
  
  public List<Data> getInstallbase() {
    List<AggregateResult> newCustomers = getNewLongTermCustomers();
    List<AggregateResult> lostCustomers = getLostLongTermCustomers();
    List<AggregateResult> addOnDeals = getAddOnWonDeals();
        
    Integer currentYear = System.Today().year();
    Integer lastYear = Integer.valueOf(newCustomers[newCustomers.size()-1].get('year'));
    Integer size = newCustomers.size() + (lastYear < currentYear ? 1 : 0);
 
    List<Data> installbase = new List<Data>(size);
       
    Map<Integer, Integer> lost = new Map<Integer,Integer>();

    for (Integer i = 0 ; i < lostCustomers.size() ; i++ ) {
        Integer year = Integer.valueOf(lostCustomers[i].get('year'));
        Integer c = Integer.valueOf(lostCustomers[i].get('customers'));
        lost.put(year+1, c);
    } 

    Map<Integer, Integer> addOns = new Map<Integer, Integer>();
        
    for (Integer i = 0 ; i < addOnDeals.size() ; i++ ) {
        Integer year = Integer.valueOf(addOnDeals[i].get('year'));
        Integer c = Integer.valueOf(addOnDeals[i].get('deals'));
        addOns.put(year, c);
    } 
    
    Integer totalInstallbase = 0;
    Integer maxYear = 0;
    
    Integer i = 0;
    while (i < newCustomers.size()) {
        Integer year = Integer.valueOf(newCustomers[i].get('year'));
        if (year > maxYear)
            maxYear = year;
        Integer c = Integer.valueOf(newCustomers[i].get('customers'));
        
        Integer l = 0;
        Integer a = 0;
        
        if (lost.containsKey(year)) {
            l = lost.get(year);
        }
        
        if (addOns.containsKey(year)) {
            a = addOns.get(year);
        }
        
        Decimal retention = totalInstallbase != 0 ? (totalInstallbase - l) * 100 / totalInstallbase : 0;
        Decimal expansion = math.round(totalInstallbase != 0 ? a * 1000.0 / totalInstallbase : 0)/10.0;
        
        installbase[i] = new Data(year, totalInstallbase, retention, expansion);
        
        totalInstallbase += c - l;
        i++;
    } 
    
    if (maxYear < currentYear)     {
        Data d = new Data(currentYear, totalInstallbase, 100, 0);
        installbase[i] = d;
    }
    
    return installbase;
  }
  
  public class Data {
    public Integer year {get; set; }
    public Integer installbase{ get; set; }
    public Decimal retention { get; set; }
    public Decimal expansionRate { get; set; } 
     
    public Data(Integer year) {
        this.year = year;
    }
    
    public Data(Integer year, Integer installbase, Decimal retention, Decimal expansionRate) {
        this.year = year;
        this.installbase = installbase;
        this.retention = retention;
        this.expansionRate = expansionRate;
    }
  }

}