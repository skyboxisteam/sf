global  class LeadStatisticsFindContacOpp implements Schedulable  {
	
	
	global void execute(SchedulableContext sc)
	  {
	    list<Lead_Statistics__c> ldStList = [select id, contact__c,Opportunity__c,SQL_date__c from Lead_Statistics__c where contact__c<> null and Opportunity__c =null and account_stage__c <>'Re-engage'];
	    map<id,Lead_Statistics__c> mapContactLeadStat = new map<id,Lead_Statistics__c>();
	    for(Lead_Statistics__c ldst :ldStList)
	      mapContactLeadStat.put(ldst.contact__c,ldSt);
	    list<OpportunityContactRole> oppContRoleList = [select ContactId, OpportunityId,Opportunity.createddate from OpportunityContactRole where contactId in : mapContactLeadStat.keyset()];
	    
	    for(OpportunityContactRole OppContRole :oppContRoleList)
	     {
	       mapContactLeadStat.get(OppContRole.contactId).Opportunity__c = OppContRole.OpportunityId;
	       mapContactLeadStat.get(OppContRole.contactId).SQL_date__c = OppContRole.Opportunity.createddate;
	   
	     }
	     	
	      update mapContactLeadStat.values();
	}
}