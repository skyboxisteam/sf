public with sharing class License2Genratation {

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------Generate-----------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

public static void Generate(License2__c lic)
{
  List<License2__c> licenseList = new List<License2__c>();
  licenseList.add(lic);
  Generate(licenseList);

}


public static void Generate(list<License2__c> licenseList)
 {
  string IPS = 'IPS Content';
  string PD = 'Premium Dictionary';
  string CM = 'Skybox Change Manager';
  string FA = 'Skybox Firewall Assurance';
  string NA = 'Skybox Network Assurance';
  string RC = 'Skybox Risk Control';
  string TM = 'Skybox Threat Manager';
  string VC = 'Skybox Vulnerability Control';
  string vNA = 'Network Assurance for Cloud';
  string worm = 'Worm Support';
  string reg = '[^a-zA-Z]';
  string AccountNameNoSpace;
  string getDate = DateTime.newInstance(date.today(), Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
  string InterfaceId = runningInASandbox()? '22463' : '22641'; 
  string IconductParam;
  list<License_module__c> newLicenseModule = new list<License_module__c> ();
    
    checkLimit(licenseList);
    updateExpOnOpp(licenseList);
    licenseList = extendedLicense(licenseList);
    Date maxDate; 
    system.debug('ls.Generate_License__c ' );
    for(License2__c ls : licenseList)
    {
      system.debug('ls.Generate_License__c ' + ls.Generate_License__c);
      //ls.Account__c = ls.Opportunity__c != null?ls.Opportunity__r.AccountId:ls.Partner__c != null ?ls.Partner__r.AccountId:ls.Account__c;//MP-54 moved to trigger 
      if(ls.Generate_License__c)//SF-925
      {
           if(ls.TM_License__c)
             newLicenseModule.add(getLicenseModule(TM,ls.id,ls.TM_Expiration_Date__c !=null?ls.TM_Expiration_Date__c: ls.Expiration_Date__c));
           if(ls.FA_license__c)
             newLicenseModule.add(getLicenseModule(FA,ls.id,ls.FA_Expiration_Date__c !=null?ls.FA_Expiration_Date__c: ls.Expiration_Date__c));
           if(ls.NA_License__c || (ls.vNA_License__c && ls.License_Version__r.Version_Level__c < 9))
             newLicenseModule.add(getLicenseModule(NA,ls.id,ls.NA_Expiration_Date__c !=null?ls.NA_Expiration_Date__c: ls.Expiration_Date__c));
           if(ls.vNA_License__c && ls.License_Version__r.Version_Level__c>=9)
             newLicenseModule.add(getLicenseModule(vNA,ls.id,ls.vNA_Expiration_Date__c !=null?ls.vNA_Expiration_Date__c: ls.Expiration_Date__c));
           if(ls.CM_License__c)
             newLicenseModule.add(getLicenseModule(CM,ls.id,ls.CM_Expiration_Date__c !=null?ls.CM_Expiration_Date__c: ls.Expiration_Date__c));
           if(ls.VC_License__c)
             newLicenseModule.add(getLicenseModule(VC,ls.id,ls.VC_Expiration_Date__c !=null?ls.VC_Expiration_Date__c: ls.Expiration_Date__c));
           if(ls.Content__c)
             newLicenseModule.add(getLicenseModule(PD,ls.id,ls.Content_Expatriation_Date__c !=null?ls.Content_Expatriation_Date__c: ls.Expiration_Date__c));
          
           if(ls.RecordType.Developername == 'POC' || ls.RecordType.Developername == 'Partner_Test'  || ls.RecordType.Developername == 'Support' )
           {    
              newLicenseModule.add(getLicenseModule(IPS,ls.id,getMaxDate(ls)));
              newLicenseModule.add(getLicenseModule(PD,ls.id,getMaxDate(ls)));
              newLicenseModule.add(getLicenseModule(worm,ls.id,getMaxDate(ls)));
           }
           
    
          insert newLicenseModule;
          
          
           AccountNameNoSpace  = ls.Opportunity__c != null?ls.Opportunity__r.Account.Name:ls.Partner__c != null ?ls.Partner__r.email:ls.Account__r.Name;//ls.Account_Name__c;//ls.Account__c != null? ls.Account__r.Name : ls.Opportunity__r.Account.Name;
           AccountNameNoSpace  = AccountNameNoSpace == null ? 'Skybox Security ':AccountNameNoSpace.replaceall(reg,'_') ;//ls.Account_Name__c.replaceall(reg,'_');
         
         // if(ls.RecordType.Developername == 'POC' || ls.RecordType.Developername == 'Partner_Test' )
            AccountNameNoSpace += '-' + ls.RecordType.Developername;
            string d =ls.License_Version__r.Generator_Path__c;
            system.debug('length ' + newLicenseModule.size() + 'The modules ' + newLicenseModule);
         // IconductParam = '{  "Key":"LicenseId","Value":"'+ls.id+'"},  {  "Key":"LicenseNameParam","Value":"License-' + AccountNameNoSpace +'-'+getDate+'.xml"}, {  "Key":"AccountRegName","Value":"' +AccountNameNoSpace +'"},  {  "Key":"VersionGeneratorPath","Value":"'+(ls.License_Version__c == null? defaultVersion.Generator_Path__c: ls.License_Version__r.Generator_Path__c)+'"}';
            IconductParam = '{  "Key":"LicenseId","Value":"'+ls.id+'"},  {  "Key":"LicenseNameParam","Value":"License-' + AccountNameNoSpace +'-'+getDate+'.xml"}, {  "Key":"AccountRegName","Value":"' +AccountNameNoSpace +'"},  {  "Key":"VersionGeneratorPath","Value":"'+ls.License_Version__r.Generator_Path__c+'"}';
       //   system.debug('Param ' + (ls.License_Version__c == null? defultVersionPath: ls.License_Version__r.Generator_Path__c) );
            APIToIconduct.callIconduct(InterfaceId,IconductParam );
      }
      
    }
    
    
      
 }   

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------getLicenseModule-----------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

private static License_module__c getLicenseModule(string name, Id licenseId , date experDate )
 {
   License_module__c lm = new License_module__c(Module_Name__c = name, License__c = licenseId, Expiration_Date__c = experDate);
   return lm;   
 }

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------Check Limit-----------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
 
 private static void checkLimit(list<License2__c> lsListParam)
 {
    integer maxLicensePerOpp = 3;
    map<id,License2__c> opp2Lic = new map<id,License2__c>();
    map<id,integer> opp2NumOpp = new map<id,integer>();
    
    for(License2__c ls: lsListParam)
     if(!ls.Disable_license_limitation__c && ls.opportunity__c != null)
      opp2Lic.put(ls.opportunity__c,ls);    
    
    list<License2__c> ls2OppList = [select id,opportunity__c,Disable_license_limitation__c,Generated_License__c,Generate_License__c from License2__c where opportunity__c in :opp2Lic.keySet() ];
    
    for(License2__c ls: ls2OppList)
     {    
      if(ls.Generated_License__c || ls.Generate_License__c)
        opp2NumOpp.put(ls.opportunity__c,opp2NumOpp.get(ls.opportunity__c) == null?1:opp2NumOpp.get(ls.opportunity__c)+1);
      if(opp2NumOpp.get(ls.opportunity__c) > maxLicensePerOpp)
       opp2Lic.get(ls.opportunity__c).adderror('Opportunity has max license generated - '+string.valueof(maxLicensePerOpp) );
     }   
    }
    
private static date getMaxDate(License2__c ls)
{
    list<date> dateList = new list<date>();
    dateList.add(ls.TM_Expiration_Date__c);
    dateList.add(ls.FA_Expiration_Date__c);
    dateList.add(ls.VC_Expiration_Date__c);
    dateList.add(ls.NA_Expiration_Date__c);
    dateList.add(ls.CM_Expiration_Date__c);
    dateList.add(ls.Expiration_Date__c);
    dateList.sort();
    return dateList.get(dateList.size()-1);
    
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------updateExpOnOpp-----------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

 private static void updateExpOnOpp(list<License2__c> lsListParam)
   {
    
    map<id,License2__c> opp2Lic = new map<id,License2__c>();
    list<opportunity> oppList = new list<opportunity>();
    
    for(License2__c ls: lsListParam)
      if(ls.opportunity__c != null)
       {
         opportunity opp = new opportunity(id = ls.opportunity__c, Pilot_License_Expiration__c = ls.Expiration_Date__c);
         oppList.add(opp);
       }
     update oppList;    
   } 
   
  private static list<License2__c> extendedLicense(list<License2__c> listLicense)
  {
    return [SELECT id,
                  opportunity__c,
                  opportunity__r.AccountId,
                  opportunity__r.Account.Name,
                  Generated_License__c,
                  Generate_License__c,
                  Expiration_Date__c,
                  Disable_license_limitation__c,
                  VC_License__c,
                  CM_License__c,
                  NA_License__c,
                  FA_license__c,
                  TM_License__c,
                  vNA_License__c,
                  Content__c,
                  Account__c,
                  Account__r.Name,
                  Partner__c,
                  Partner__r.AccountId,
                  Partner__r.Account.Name,
                  Partner__r.email,
                  FA_Expiration_Date__c,
                  NA_Expiration_Date__c,
                  CM_Expiration_Date__c,
                  TM_Expiration_Date__c,
                  VC_Expiration_Date__c,
                  vNA_Expiration_Date__c,
                  Content_Expatriation_Date__c,
                  License_Version__r.Generator_Path__c,
                  License_Version__r.Version_Level__c,
                  RecordType.Developername
                                  
             FROM License2__c
             WHERE id in :listLicense];
  }
 
 
  
  private static Boolean runningInASandbox() {
  return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
}
}