@isTest
public class QuoteHandlerTest {
	

static testMethod void updateQuoteTest() {  
   ClsObjectCreator cls = new ClsObjectCreator();  
   Account acc = cls.createAccount('test');
   Contact c =  ClsObjectCreator.createContact('test',acc.id);
   SBQQ__Quote__c  qu = cls.createQuote(c,'Draft');
   
   acc.Partnership_Type__c =  'Reseller';
   acc.Partnership_Level__c = 'Elite';
   update acc;

   qu.WDSB_Paying_Entity__c = 'Reseller';
   qu.SBQQ__Type__c  = 'Renewal';
   qu.SBQQ__Partner__c = acc.id;
   update qu;

   qu.WDSB_Paying_Entity__c = 'End_Customer';
   update qu;

}



   //Author: Rina Nachbas       Date:31.7.18      Event: before insert, before update
   //Task:   SF-791: Update Estimation days from primary PS_Estimation to its opportunity's primary quote
    //static testMethod void updateQuoteTest() {
    //    //  Given Setup
    //    ClsObjectCreator cls = new ClsObjectCreator();
    //     PSEstimatorHandler psh= new  PSEstimatorHandler();
    //    Account acc = cls.createAccount('test');
    //    opportunity opp = cls.createOpportunity(acc);
    //    //  When PS_Estimation__c created with Primary__c = true
    //     PS_Estimation__c ps3 = new PS_Estimation__c(name = 'test', opportunity__c = opp.id, Server_Type__c = 'virtual', Difficulty__c = 'no_poc', Total_Number_of_Servers__c = 5, Number_of_Collection_Sources__c = 2, Number_of_Vendors__c = 5, NA_Licenses__c = 10,
    //                                                 Primary__c = true, Estimation_Days__c = 30 );
    //    insert ps3;
//
    //    
//
    //    //  When PS_Estimation__c created without Primary__c
    //    Test.startTest();
    //        SBQQ__Quote__c q = cls.createQuote(ClsObjectCreator.createContact('Test',acc.id),'Draft');
	//        q.SBQQ__Opportunity2__c = opp.Id;
	//        update q;
	//        opp.SBQQ__PrimaryQuote__c = q.Id;
	//        update opp; 
    //    Test.stopTest();
//
//
//
    //    //  Then Check 1: qoute.PS_Estimation__c was updated
    //     SBQQ__Quote__c qTest = [select Id,PS_Estimation_Days__c,PS_Estimation__c  from SBQQ__Quote__c where Id = :q.Id];
    //     System.assertEquals(ps3.Estimation_Days__c, qTest.PS_Estimation_Days__c);
    //     System.assertEquals(ps3.Id, qTest.PS_Estimation__c);
    // }



}