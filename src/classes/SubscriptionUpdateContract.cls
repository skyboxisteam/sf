public with sharing class SubscriptionUpdateContract 
{
 
  public void updateContract(list<SBQQ__Subscription__c> listSub)
  {
  	list<id> listId = new list<id>();
  	for(SBQQ__Subscription__c ls : listSub)
     {
  		listId.add(ls.SBQQ__Contract__c);
     }
     
     map<id,Contract> mapContract = new map<id,Contract>( [select id, Effective_End_Date_Override__c from Contract where id in: listId]);
    
    for(SBQQ__Subscription__c ls : listSub)
     {
       mapContract.get(ls.SBQQ__Contract__c).Effective_End_Date_Override__c = ls.SBQQ__EndDate__c;
     }
    
  	update mapContract.values();
  } 
}