/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OpportunitySearchLockDownTest {

    static testMethod void myUnitTest() {
       
       ClsObjectCreator cls = new ClsObjectCreator();
       Opportunity_Lockdown__c oppLock=  new Opportunity_Lockdown__c (name = 'test', lock__c = false,Month__c = string.valueof(date.today().month()), Year__c = string.valueof(date.today().year()) );
       insert oppLock;
       Account acc = cls.createAccount('test');
       Opportunity opp =  cls.createOpportunity(acc);
       opp.Closedate = date.today();
       update opp;
       
       oppLock.lock__c = true;
       update oppLock;
       
       test.starttest();
       opp.amount = 30;
      // opp.StageName = 'Ready to book';
       try{
       update opp;
       }
       catch (exception ex)
       {
       	
       }
       test.stoptest();  
    }
}