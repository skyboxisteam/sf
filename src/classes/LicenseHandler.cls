public with sharing class LicenseHandler {

  
    static  map<id,RecordType> mapRecordType = new   map<id,RecordType> ([select id, DeveloperName from recordtype where SobjectType  = 'License2__c']);
    static final string partnerCommunity = 'Partner Community';
    static list<License_Default__c> licenseDefaultList = new list<License_Default__c>();
    static  list<License_Version__c> defaultVersion  = [select id, Generator_Path__c from License_Version__c where Default_version__c = true limit 1];
    static final String caseRT = 'Support';
   
    public static void sortToTypes(list<License2__c> licenseList, map<id,License2__c> oldMap,boolean isBefore, boolean isInsert) 
    {
    
     list<License2__c> productionList = new  list<License2__c>();
     list<License2__c> noneProductionList = new  list<License2__c>();
     list<string> recordTypeList = new list<string>();
   //  list<License_Default__c> licenseDefaultList = new list<License_Default__c>();
   //  list<License_Version__c> defaultVersion  = [select id, Generator_Path__c from License_Version__c where Default_version__c = true limit 1];
     //licenseList = getLicenseDefault(licenseList);
    
      licenseDefaultList = getLicenseDefault(createRecordTypeList(licenseList,mapRecordType)); //get all defaul values of the license created
      for(License2__c ls : licenseList)
        {
           if(isBefore)
           {
              ls = getBeforeUpdates(ls, licenseDefaultList,isInsert);                   
            }         
          else if(ls.Generate_License__c && (isInsert || !oldMap.get(ls.id).Generate_License__c )  )
                {
                   noneProductionList.add(ls);
                   system.debug('hwehw is the list ' +noneProductionList);
                }
           
        }
       if(!noneProductionList.isEmpty() && !isBefore)
       { 
      //  system.debug('A Am here');
          License2Genratation.generate(noneProductionList);
       }
    
    }


 private static list<string> createRecordTypeList(list<License2__c> licenseList, map<id,RecordType> mapRecordType)
 {
    list<string> recordTypeList = new list<string>();
    for(License2__c ls :licenseList)
      {
        recordTypeList.add(mapRecordType.get(ls.recordtypeId).DeveloperName );
      }

    return recordTypeList;
 }
 
 private static License2__c getBeforeUpdates(License2__c ls, list<License_Default__c> licenseDefaultList, boolean isInsert)
 {
       ls.License_Version__c = ls.License_Version__c == null? defaultVersion[0].id : ls.License_Version__c; 

       //Comment by Rina because MP-54 moved to trigger 
       //ls.Account__c = ls.Opportunity__c != null?ls.Opportunity__r.AccountId:ls.Partner__c != null ?ls.Partner__r.AccountId:ls.Account__c;
      
      // if(ls.Source_Creation__c == partnerCommunity && mapRecordType.get(ls.recordtypeId).DeveloperName == 'POC')   
       //    ls.Generate_License__c = false
       system.debug(mapRecordType.get(ls.recordtypeId).DeveloperName);
       if((ls.Source_Creation__c == partnerCommunity && mapRecordType.get(ls.recordtypeId).DeveloperName != 'POC') || mapRecordType.get(ls.recordtypeId).DeveloperName == 'Support')            
           ls = handleDefault(ls,mapRecordType,licenseDefaultList);
        else if(ls.Source_Creation__c == partnerCommunity && mapRecordType.get(ls.recordtypeId).DeveloperName == 'POC' && isInsert)  
           ls.Generate_License__c = false;
       else 
          {
            ls.FA_license__c =  ls.CM_License__c || ls.FA_Nodes__c>0 ?true: ls.FA_license__c;                     
            ls.NA_License__c =   ls.NA_Nodes__c >0|| ( ls.Virtual_Nodes__c >0 && ls.License_Version__r.Version_Level__c < 9 )? true:   ls.NA_License__c ;
            ls.vNA_License__c = ls.Virtual_Nodes__c > 0 && ls.License_Version__r.Version_Level__c < 9 ? true : ls.vNA_License__c;   
            ls.VC_License__c = ls.VC_Nodes__c > 0 ? true : ls.VC_License__c ;

     
          }

          return ls;
 }
 private static License2__c handleDefault(License2__c ls,  map<id,RecordType> mapRecordType, list<License_Default__c> licenseDefaultList)
    { 
      
       for(License_Default__c lsd :licenseDefaultList)
        {
         if(mapRecordType.get(ls.recordtypeId).DeveloperName == lsd.Licesne_Type__c)
            {
              system.debug('I am here');
              ls.FA_license__c = lsd.FA_license__c;
              ls.VC_license__c = lsd.VC_license__c;
              ls.TM_license__c = lsd.TM_license__c;
              ls.NA_License__c = lsd.NA_License__c;
              ls.CM_License__c = lsd.CM_License__c;
              ls.Content__c = lsd.Content__c;          
              ls.FA_Nodes__c = lsd.FA_Nodes__c;            
              ls.VC_Nodes__c = lsd.VC_Nodes__c;              
              ls.NA_Nodes__c = lsd.NA_Nodes__c;   
              ls.Virtual_Nodes__c = lsd.vNA_Nodes__c;           
              ls.Component_Limits_Servers__c = lsd.Servers__c;              
              ls.Component_Limits_Managers__c = lsd.Managers__c;              
              ls.Component_Limits_Collectors__c = lsd.Collectors__c; 
              ls.Expiration_Date__c =  ls.Expiration_Date__c == null ? date.today().addDays(lsd.Days_to_expire__c == null ? 0 : Integer.valueOf(lsd.Days_to_expire__c)) : ls.Expiration_Date__c ;
              ls.Disable_license_limitation__c= ls.Source_Creation__c == partnerCommunity ? true :  ls.Disable_license_limitation__c;
              ls.Generate_License__c = ls.Source_Creation__c == partnerCommunity ? true : ls.Generate_License__c;
            }
           
          
          
        }
        return ls;
     
  }
  
  public static list<License_Default__c> getLicenseDefault( list<string> recordTypeList)
  {
    return [SELECT  Id,
           
                      FA_license__c,
                      VC_license__c,
                      TM_license__c,
                      NA_License__c,
                      CM_License__c,
                      Content__c,
                      FA_Nodes__c,
                      VC_Nodes__c,
                      NA_Nodes__c,
                      vNA_Nodes__c,
                      Servers__c,
                      Managers__c,
                      Collectors__c,
                      Licesne_Type__c,
                      Days_to_expire__c 
              FROM License_Default__c
              WHERE Licesne_Type__c in : recordTypeList]; 
  }
  
   /* public static list<License2__c> getLicenseDefault(list<License2__c> licenseList)
    {
      return [SELECT  Id,
             
                      FA_license__c,
                      VC_license__c,
                      TM_license__c,
                      NA_License__c,
                      CM_License__c,
                      Content__c,
                      FA_Nodes__c,
                      VC_Nodes__c,
                      NA_Nodes__c,
                      Component_Limits_Servers__c,
                      Component_Limits_Managers__c,
                      Component_Limits_Collectors__c,
                      opportunity__c,
                      opportunity__r.AccountId,
                      opportunity__r.Account.Name,
                       Generated_License__c,
                      Generate_License__c,
                      Expiration_Date__c,        
                      Account__c,
                      Account__r.Name,
                      Partner__c,
                      Partner__r.AccountId,
                      Partner__r.Account.Name,
                      Partner__r.email,
                      recordtype.DeveloperName,
                      Disable_license_limitation__c,
                      FA_Expiration_Date__c,
                      NA_Expiration_Date__c,
                      CM_Expiration_Date__c,
                      VC_Expiration_Date__c,
                      TM_Expiration_Date__c,
                      Content_Expatriation_Date__c,
                      License_Version__r.Version__c
              FROM License2__c
              WHERE Id in : licenseList];   
    }*/

   
  //Author: Rina Nachbas      Date:  17.10.18
  //Task:   MP-24             Event: before Insert
  //Desc:   update License Fields: 
  //        Partner_Account_Manage_SE__c - from Partner__c.Account.Managing_SE_Ref__c or Partner__c.Account.ownerId - so we can send email alert to him(Email Alert Recipients limitatiom)
/*   public static void updateLicense(List<License2__c> newLicenseList){
    Set<Id> licensePartnerSet = new Set<Id>();
    Map<Id, Contact> partnersMap = new Map<Id, Contact>();
    List<License2__c> updateLicenseList = new List<License2__c> ();

    //newLicenseList = [select Id, Partner__c, Source_Creation__c from License2__c where Id in :newLicenseList];//if isAfter

    for( License2__c lic : newLicenseList){
       if(lic.Source_Creation__c == partnerCommunity && lic.Partner__c != null){
          licensePartnerSet.add(lic.Partner__c);
          updateLicenseList.add(lic);
        }
    }

   if(licensePartnerSet.size() > 0){
        partnersMap = new Map<Id, Contact>([select Id, Account.ownerId, Account.Managing_SE_Ref__c  from Contact where Id in :licensePartnerSet]);
        for( License2__c lic : updateLicenseList){
          if(partnersMap.get(lic.Partner__c).Account.Managing_SE_Ref__c != null ){
            lic.Partner_Account_Manage_SE__c = partnersMap.get(lic.Partner__c).Account.Managing_SE_Ref__c;
          }
          else{
            lic.Partner_Account_Manage_SE__c = partnersMap.get(lic.Partner__c).Account.ownerId;
          }
        }

        //if(updateLicenseList.size() > 0)
        //    update updateLicenseList; 
    }
  }*/


  //Author: Rina Nachbas      Date:  25.10.18
  //Task:   MP-54      Event: before Insert
  //Desc:   update License Fields: 
  //        Account__c - from Partner__c.AccountId or Opportunity__c.AccountId - so we allways have accId
  public static void updateLicenseAccount(List<License2__c> newLicenseList){
    updateLicenseAccount(newLicenseList, null);
  }
  
  public static list<License2__c> updateLicenseAccount(List<License2__c> newLicenseList, map<id,License2__c> mapOldLic){
    Set<Id> licensePartnerSet = new Set<Id>();
    Set<Id> licenseOppSet = new Set<Id>();
    Set<Id> licenseCaseSet = new Set<Id>();

    Map<Id, Contact> partnersMap = new Map<Id, Contact>();
    Map<Id, Opportunity> oppsMap = new Map<Id, Opportunity>();
    Map<Id, Case> casesMap = new Map<Id, Case>();
    List<License2__c> updateLicenseList = new List<License2__c> ();

    //newLicenseList = [select Id, Account__c, Opportunity__c, Partner__c from License2__c where Id in :newLicenseList];//if isAfter

    for( License2__c lic : newLicenseList){
      //isInsert OR isUpdate && opp or partner changed
       if(String.isBlank(lic.Account__c) && mapOldLic == null || 
            (mapOldLic != null && ( lic.Opportunity__c != mapOldLic.get(lic.Id).Opportunity__c ||  lic.Partner__c != mapOldLic.get(lic.Id).Partner__c))){
          if(String.isNotBlank(lic.Opportunity__c)){
            licenseOppSet.add(lic.Opportunity__c);
          }
          else if(String.isNotBlank(lic.Case__c)){
            licenseCaseSet.add(lic.Case__c);
          }
          else if(String.isNotBlank(lic.Partner__c)){
            licensePartnerSet.add(lic.Partner__c);
          }
          updateLicenseList.add(lic);//tere is VR that requierd at least one of account/partner/opp
        }
    }

    if(licenseOppSet.size() > 0){
      oppsMap = new Map<Id, Opportunity>([select Id, AccountId  from Opportunity where Id in :licenseOppSet]);
    }
    if(licensePartnerSet.size() > 0){
      partnersMap = new Map<Id, Contact>([select Id, AccountId  from Contact where Id in :licensePartnerSet]);
    }

    if(licenseCaseSet.size() > 0){
      casesMap = new Map<Id, Case>([select Id, AccountId  from Case where Id in :licenseCaseSet]);
    }
    
    for( License2__c lic : updateLicenseList){
      if(oppsMap.containsKey(lic.Opportunity__c) ){
      }
      else if(casesMap.containsKey(lic.Case__c)){
        lic.Account__c = casesMap.get(lic.Case__c).AccountId;
      }
      else{
        lic.Account__c = partnersMap.get(lic.Partner__c).AccountId;
      }

      system.debug('##updateLicenseAccount: lic.Account__c' + lic.Account__c);
    }

    return updateLicenseList;
    //if(updateLicenseList.size() > 0)//if isAfter
    //  update updateLicenseList;
  }
   

  //Author: Rina Nachbas      Date:  17.10.18
  //Task:   SF-925          Event: before Insert, befor Update
  //Desc:    validation on opportunity filled in POC. Partner for test/demo and account for production. 
  //Moved VR to code so the validation be before Update/Insert.
  public static void validateRequierdFields(List<License2__c> newLicenseList){

    for( License2__c lic : newLicenseList)
    {
      //SF-925 validation on opportunity filled in POC. Partner for test/demo and account for production.
      if(mapRecordType.get(lic.recordtypeId).DeveloperName == 'POC' && lic.Opportunity__c == null)
      {
          lic.addError('Please select Opportunity');
      }
      else if((mapRecordType.get(lic.recordtypeId).DeveloperName == 'Partner_Test' || mapRecordType.get(lic.recordtypeId).DeveloperName == 'Demo') && lic.Partner__c == null)
      {
          lic.addError('Please select Partner');
      }
      else if(mapRecordType.get(lic.recordtypeId).DeveloperName == 'Production' && lic.Account__c == null)
      {
          lic.addError('Please select Account');
      }
    }
  }

  //Author: Rina Nachbas      Date:  17.10.18
  //Task:   MP-24             Event: before Insert
  //Desc:   Generate license conditions
  public static void generateLicenseCond(List<License2__c> newLicenseList){
    String demo = 'Demo';
    String test = 'Partner_Test'; 
    String errorMsg = 'You already have an active ';
    String caseErrorMsg = 'Support License has been generated for this account in the last 3 months';
    Map<String, Integer> eligibleLicensePerRTMap = new Map<String, Integer>{demo => 1, test=> 1};
    Set<Id> testLicensePartnerSet = new Set<Id>();
    Set<Id> caseAccountSet = new Set<Id>();
    Set<Id> caseLicenseSet = new Set<Id>();
    Set<Id> demoLicensePartnerSet = new Set<Id>();
    List<AggregateResult> demoLicense2List = new List<AggregateResult> ();
    List<AggregateResult> testLicense2List = new List<AggregateResult> ();
    List<Contact> testLicenseContacts = new List<Contact> ();
    Map<Id, Id> testLicenseContactsAccountMap = new Map<Id, Id>();
    
    system.debug('##newLicenseList: ' +newLicenseList);

    for( License2__c lic : newLicenseList){
      if(caseRT == mapRecordType.get(lic.recordtypeId).DeveloperName && !lic.Disable_license_limitation__c)
        caseAccountSet.add(lic.Account__c);
      if(lic.Source_Creation__c == partnerCommunity){
        system.debug('##RecordTypeName: ' + mapRecordType.get(lic.recordtypeId).DeveloperName);
        //Test - 1 Active license per account
        if(test == mapRecordType.get(lic.recordtypeId).DeveloperName){
          testLicensePartnerSet.add(lic.Partner__c);
        }
        //Demo - 1 active license per contact
        else if(demo == mapRecordType.get(lic.recordtypeId).DeveloperName){
          demoLicensePartnerSet.add(lic.Partner__c);
        }
      }
    }
    //system.debug('##testLicensePartnerSet: ' + testLicensePartnerSet);

    //Test - 1 Active license per "account"
    if(testLicensePartnerSet.size() > 0){
      testLicenseContacts = [select AccountId from Contact where Id in :testLicensePartnerSet];
    }

    for(Contact con : testLicenseContacts){
      testLicenseContactsAccountMap.put(con.Id, con.AccountId);
    }
    //system.debug('##testLicenseContacts: ' + testLicenseContacts);
    //system.debug('##testLicenseContactsAccountMap.values(): ' + testLicenseContactsAccountMap.values());
    system.debug('##demoLicensePartnerSet: ' + demoLicensePartnerSet);
    if(demoLicensePartnerSet.size() > 0)
    {
      demoLicense2List = [select count(Id) cnt, RecordType.DeveloperName, Partner__c from License2__c
                                            where Partner__c in :demoLicensePartnerSet AND RecordType.DeveloperName = :demo 
                                                  AND Expiration_Date__c > :Date.Today() AND Source_Creation__c = :partnerCommunity
                                            group by Partner__c, RecordType.DeveloperName
                                            order by Partner__c   ];
    }

    if(testLicenseContactsAccountMap.Values().size() > 0)
    {
       testLicense2List = [select count(Id) cnt, RecordType.DeveloperName, Partner__r.AccountId acc from License2__c
                                          where Partner__r.AccountId in :testLicenseContactsAccountMap.Values() AND RecordType.DeveloperName = :test 
                                                AND Expiration_Date__c > :Date.Today() AND Source_Creation__c  =:partnerCommunity
                                          group by Partner__r.AccountId, RecordType.DeveloperName 
                                          order by Partner__r.AccountId  ]     ;                                      
    }
    //create map between License2 Contact / Contact.Account -> number of License 
    Map<ID, Integer> licenseCountPerConOrAccMap = new Map<ID, Integer>();
    
    //populate map between License2 Contact.Account -> number of License 
    for(AggregateResult lic : testLicense2List){
      licenseCountPerConOrAccMap.put(String.valueOf(lic.get('acc')), Integer.valueOf(lic.get('cnt')));
    }

    //populate map between License2 Contact -> number of License
    for(AggregateResult lic : demoLicense2List){
      licenseCountPerConOrAccMap.put(String.valueOf(lic.get('Partner__c')), Integer.valueOf(lic.get('cnt')));
    }

    system.debug('caseAccountSet ' + caseAccountSet);
    caseLicenseSet = getCaseCond(caseAccountSet);
    //Raise Error if contact / Account can't produce more license
    for( License2__c lic : newLicenseList)
    {
      if(needCaseError(caseLicenseSet,lic))
         lic.addError(caseErrorMsg);

      else if(test == mapRecordType.get(lic.recordtypeId).DeveloperName){
      //Test - 1 Active license per account
        if(licenseCountPerConOrAccMap.get(testLicenseContactsAccountMap.get(lic.Partner__c)) >= eligibleLicensePerRTMap.get(test))
        {
          lic.addError(errorMsg+lic.License_Type__c.toLowerCase() + ' license');// + ' ' + licenseCountPerConOrAccMap.get(testLicenseContactsAccountMap.get(lic.Partner__c)) + ' Times');
        }
      }
      else if(demo == mapRecordType.get(lic.recordtypeId).DeveloperName){
        //Demo - 1 active license per contact
        if(licenseCountPerConOrAccMap.get(lic.Partner__c) >= eligibleLicensePerRTMap.get(demo))
        {
          lic.addError(errorMsg+lic.License_Type__c.toLowerCase() + ' license');// + ' ' + licenseCountPerConOrAccMap.get(lic.Partner__c) + ' Times');
        }
      }
    }

  }

  
  //Author: Rina Nachbas      Date:  06.11.18
  //Task:   MP-56             Event: before Insert
  //Desc:   When POC license is being generated and the source is “Partner Community” send email to the account team
  public static void sendAlert(List<License2__c> newLicenseList){
    String poc = 'POC';
    Map<Id, License2__c> licenseAccountMap = new Map<Id, License2__c>();
    List<String> roles = new List<String>{'Account Manager', 'Sales Engineer.'};
    SendEmail sendEmail = new SendEmail();
    sendEmail.templateName = 'New POC License Created VF';
    
    for( License2__c lic : newLicenseList)
    {
      if(lic.Source_Creation__c == partnerCommunity && poc == mapRecordType.get(lic.recordtypeId).DeveloperName){
        licenseAccountMap.put(lic.Id, lic);
      }
    }

    system.debug('##sendAlert: licenseAccountMap: ' + licenseAccountMap);

    //find lic.opp.owner
    List<License2__c> lic2List = [select Id, Opportunity__c, Opportunity__r.Owner.Email, Account__c
                                  from License2__c 
                                  where Id in :licenseAccountMap.keySet()];
    system.debug('##sendAlert: lic2List: ' + lic2List);

    for( License2__c lic : lic2List)
    {
      if(lic.Opportunity__c != null){
        sendEmail.recipients = new List<String> {lic.Opportunity__r.Owner.Email};
      }
      sendEmail.whatId = lic.Id;
      sendEmail.sendEmailToAccountTeam(lic.Account__c, roles);
    }

     
  }

  private static Set<Id> getCaseCond(Set<Id> idSet)
  {
     List<AggregateResult> caseAggList = new List<AggregateResult> ();
     Set<Id> result = new Set<Id>();

     caseAggList = [Select COUNT(Id) cnt, 
                           Case__r.Accountid acc
                    FROM   License2__c
                    WHERE  Case__r.Accountid in : idSet AND
                           Expiration_Date__c = LAST_N_DAYS:90 
                    GROUP BY Case__r.Accountid
                    HAVING  COUNT(Id) > 0 ];
      system.debug( ' caseAggList '  + caseAggList);
      for(AggregateResult ag : caseAggList)
      {
         result.add(String.valueOf(ag.get('acc')));
      }

      return result;
  }   

  private static Boolean needCaseError( Set<Id> limitSet,License2__c lic)
   {
      return caseRT == mapRecordType.get(lic.recordtypeId).DeveloperName &&
             limitSet.size() > 0 &&
             limitSet.contains(lic.Account__c);
   }
}