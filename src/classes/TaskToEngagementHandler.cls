public with sharing class TaskToEngagementHandler {
	
	private list<Lead_Statistics__c> leadStatisticsList =  new list<Lead_Statistics__c>();
	public list<Engagement__c> engagementList =  new list<Engagement__c>();
	private Map<string,Engagement__c> taskToEngagementMap = new Map<string,Engagement__c>();
	private Map<id,Lead_Statistics__c> taskToLeadStatMap = new Map<id,Lead_Statistics__c>();
	
	
	public TaskToEngagementHandler( list<string> taskIdStringList ,list<Id> whoIdList )
	{
	    engagementList = [Select id, BDR__c,Created_date__c,Meeting_status__c, Task_id__c,relevance__c,Object_type__c,Object_ID__c,Meeting_date__c,Creator__c 
	                      From Engagement__c 
	                      Where task_id__c in: taskIdStringList];
	                                        
	    leadStatisticsList = [Select id,Object_ID__c 
	                          From Lead_Statistics__c 
	                          Where Object_ID__c in: whoIdList ];
	            
	  
	  for(Engagement__c en : engagementList)
	    taskToEngagementMap.put(en.Task_id__c, en);
	    
	  for(Lead_Statistics__c ldSt : leadStatisticsList)
	    taskToLeadStatMap.put(ldSt.object_id__c, ldSt);
	}
	
	public list<Engagement__c> convertToEngage(list<Task> taskList, boolean isInsert)
	{
		list<Engagement__c> engagementList= new list<Engagement__c>();
		for(Task ts : taskList)
		{
		  Engagement__c en = new Engagement__c();
		  en.id = taskToEngagementMap.get(ts.id) != null?taskToEngagementMap.get(ts.id).id :Null;
		
		  if(isInsert || taskToLeadStatMap.get(ts.whoid) == null || taskToEngagementMap.get(ts.id) == null || ts.Create_Lead_statistics__c ==true)
		  en.Lead_Statistics__c = taskToLeadStatMap.get(ts.whoid) == null ? ClsObjectCreator.createLeadStatistics(ts.whoId, ts.who.type).id:taskToLeadStatMap.get(ts.whoid).id;
		  en.Created_date__c = ts.createddate;
		  en.Creator__c = ts.createdById;
		  en.Meeting_date__c = ts.ActivityDate;
		  en.Object_ID__c = ts.WhoId;
		  en.OSR__c = ts.Task_OSR__c;
		  en.Object_type__c = ts.who.type;
		  en.relevance__c = ts.Meeting_relevance__c;
		  en.Engagement_type__c = ts.type;
		  en.Task_id__c = ts.id;
		  en.Meeting_status__c = ts.Status;
		  if(ts.createdby.userrole.developername == 'ADR')
		  en.BDR__c =  ts.createdbyid  ;
		  
		  engagementList.add(en);  
		}
		return engagementList;
		  
	}
	

	
    
}