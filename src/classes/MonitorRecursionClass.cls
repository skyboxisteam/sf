public class MonitorRecursionClass {
	private static Boolean CasePendingCounterMonitor = false;
    
    public static Boolean getCasePendingCounterMonitor() {
        return CasePendingCounterMonitor;
    }

    public static void setCasePendingCounterMonitor(Boolean value) {
        CasePendingCounterMonitor = value;
    }
}