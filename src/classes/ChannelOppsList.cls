public with sharing class ChannelOppsList extends CommonChannelHandler {

	public ChannelOppsList () {
	  	channel = getChannel(ApexPages.currentPage().getParameters().get('id'));
	  	getAllIds();
	}
	
	public ChannelOppsList(String id) {
	  	channel = getChannel(id);
	  	getAllIds();
	}
	
	public Integer year {
		
		get { String y = ApexPages.currentPage().getParameters().get('year'); year = y == null ? 2014 : Integer.valueOf(y); return year; }
		set { year = value; }
	}
	
	public List<Opportunity> getSales() {
		String sortField = 'CloseDate';
		
		List<Opportunity> results = [SELECT id, name, Account.name, Owner.name, amount, StageName, Probability, CloseDate, CreatedDate FROM Opportunity 
					WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and IsWon = true and FISCAL_YEAR(CloseDate) =: year
					order by CloseDate];
			
		return results;
	}

	public List<Opportunity> getNewCustomerPipeline() {
		List<Opportunity> results = [SELECT id, name, Account.name, Owner.name, amount, StageName, Probability, CloseDate, CreatedDate FROM Opportunity 
					WHERE (Accountid in: ids or Partner__c in: ids or Distributor__c in: ids)  and
					(Lead_Source_Type__c = 'partner' or (Lead_Source_Type__c = 'marketing' and Campaign.Partner__c in: ids)) and
					Type = 'Products' and FISCAL_YEAR(CreatedDate) =: year order by CreatedDate ];
			
		return results;
	}
	
	public List<Campaign> getCampaigns() {
		List<Campaign> results = [SELECT id, StartDate, EndDate, Name, HierarchyBudgetedCost, HierarchyNumberOfLeads, HierarchyNumberOfOpportunities, HierarchyAmountAllOpportunities, HierarchyAmountWonOpportunities, Type, Status
			FROM Campaign
			WHERE (Partner__c in: ids) and FISCAL_YEAR(StartDate) =: year order by StartDate ];
			
		return results;
	}
	

}