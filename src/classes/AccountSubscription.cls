public without sharing class AccountSubscription {


Public Account acc {get;set;}
Public id subId {get;set;}
Public boolean clicked  {get;set;}
Public boolean showExpiered {get;set;}
Public List<SB_Subscribed_Asset_Copy__c> SubscriptionsAsset {get;set;}
Public list<id> idList {get;set;}



 public AccountSubscription(ApexPages.StandardController stdCon) {
        if(Test.isRunningTest())
        {
                SB_Subscribed_Asset_Copy__c sua = [select id,SB_Subscription_Copy__c from SB_Subscribed_Asset_Copy__c  where SB_Subscription_Copy__c != null limit 1 ];
                SB_Subscription_Copy__c su = [select id,Account__c from SB_Subscription_Copy__c where id =:sua.SB_Subscription_Copy__c limit 1];
                acc = [select id from account where id =: su.Account__c] ;
        }
        else
     acc =  (Account) stdCon.getRecord();
     acc = [select id,name from account where id =: acc.id];
   
 }


}