/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_taskfeed1_BoardTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_taskfeed1_BoardTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new taskfeed1__Board__c());
    }
}