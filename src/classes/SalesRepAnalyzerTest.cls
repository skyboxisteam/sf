@isTest (SeeAllData=true)
public class SalesRepAnalyzerTest {
    static testMethod void SalesRepAnalyzerTest() {
        SalesRepAnalyzer analyzer = new SalesRepAnalyzer('00532000004uNDL');
        Decimal i = 0;
        
        i += analyzer.getAddOnYield();
        i += analyzer.getNewCustomerYield();
        i += analyzer.getNewCustomerWinLoss();
        i += analyzer.getNewAddOnOppsQA();
        i += analyzer.getNewCustomerOppsQA();
        i += analyzer.getProductOppsQA();
        i += analyzer.getProductsCountOpenPipeline();
        i += analyzer.getValueOpenPipeline();
        i += analyzer.getOpenPipeline();
        i += analyzer.getWonThisYear();
        
        
        analyzer.updateRecord();

        
        System.assert(i >= 0 || i < 0);
    }
}