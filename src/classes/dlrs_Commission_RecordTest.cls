/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Commission_RecordTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Commission_RecordTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Commission_Record__c());
    }
}