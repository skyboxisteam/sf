/****************************************************************************************
    Name              : AccountsWithContactsSameDomainController
    Revision History  :- 
    Created/Modified by         Created/Modified Date     Requested by      Related Task/Issue
    ----------------------------------------------------------------------------------------  
    1. Hernan 		               17/01/2012               		           [SW-4343]    
    ----------------------------------------------------------------------------------------
****************************************************************************************/

public class AccountsWithContactsSameDomainController {
	
	public List<Account> accounts {get; set;}
	public String pageErrorMsg	{get; set;}
	
	public AccountsWithContactsSameDomainController(ApexPages.Standardcontroller controller){
		pageErrorMsg = null;    	        
	    try{
	    	accounts = new List<Account>();
			Set<String> excludedDomains = Excluded_Email_Domains__c.getAll().keySet();
			
			Lead l = [SELECT Id, Email FROM Lead WHERE Id =: controller.getId()];
			
			if(l.Email <> null && l.Email.contains('@')){								
				String currentDomain = l.Email.substring(l.Email.indexOf('@') + 1);
				currentDomain = currentDomain.substring(0, currentDomain.indexOf('.'));
				
				if(!excludedDomains.contains(currentDomain)){
					Set<Id> existingAccountIds = new Set<Id>();	
					
					String fieldsToQuery = '';
					Schema.FieldSet fs = Schema.SObjectType.Account.fieldSets.getMap().get('Accounts_With_Same_Domain_Columns');
					for(Schema.FieldSetMember field : fs.getFields()){
						if(!field.getFieldPath().equalsIgnoreCase('id') && !field.getFieldPath().equalsIgnoreCase('owner.name')){
							fieldsToQuery += ', Account.' + field.getFieldPath();
						}
					}					
					
					String query = 'SELECT Id, AccountId, Account.Id, Account.Owner.Name ' + fieldsToQuery;
					query += ' FROM Contact WHERE Email <> null ';
					query += ' AND Email LIKE \'%@' +  currentDomain + '.%\'';
					System.debug(fieldsToQuery);										
					for(Contact c : Database.query(query)){								
						if(!existingAccountIds.contains(c.AccountId)){
							accounts.add(c.Account);
						}
						existingAccountIds.add(c.AccountId);
					}
				}									
			}
			
			itemsPerPage = 5;
			refreshPages();
		}catch(Exception e){
	        pageErrorMsg = e.getMessage();             
	    }			
	}
	
	
	
	/////// Pagination controlling
	class ItemPage {
        public Boolean first {get; set;}
        public Boolean last {get; set;}
        public List<Account> pageAccounts {get; set;}
        public Integer pageNumber {get; set;}
        public Boolean isActive {get; set;}

        public ItemPage(List<Account> mi, Integer pn)
        {
            pageAccounts = mi;
            pageNumber = pn;
            isActive = false;
            first = false;
            last = false;
        }
    }


	// Paginator variables
	public ItemPage activePage           { get; set; }
    public List<ItemPage> pages 		  { get; set; }          
    public List<ItemPage> activePages    { get; set; }
    public Integer activePagesStartPNumber { get; set; }
    public Integer activePagesEndPNumber   { get; set; }
    public Boolean hasMorePagesAfter       { get; set; }
    public Boolean hasMorePagesBefore      { get; set; }
    public Integer pnum                    { get; set; }
    private static Integer pagesPerSetofActivePages = 50;
    public Integer itemsPerPage          { get; set; }     
	
	
	private void refreshPages(){					
		pages = populatePages(accounts, itemsPerPage);	
		if (pages != null && pages.size() > 0) {
            pnum = 1;
            activePage = pages[0];
            pages[0].isActive = true;
            updateActivePages();
        }else{
        	pnum = null;
        	if(activepage == null){
        		activepage = new ItemPage(new List<Account>(), 5);
        	}
        	activepage.pageAccounts = new List<Account>();
        }
	}
		
	public void nextPage(){		
		if (!activePage.last)
	        pnum = activePage.pageNumber + 1;
	      	updateActivePage();
    }

   public void prevPage(){   		
	      if (!activePage.first)
	        pnum = activePage.pageNumber - 1;
	      	updateActivePage();      
    }

    public List<ItemPage> populatePages(List<Account> items, Integer ItemsPerPage){
        List<ItemPage> newPages = new List<ItemPage>();
        Integer itemsAdded = 0;
        Integer pagenumber = 0;

        // Adds items to itemspages until either all items are added or the page limit is reached
        if(items.size() > 0){        	       
	        while(itemsAdded < items.size()){
	            pagenumber++;
	            List<Account> itemsToAdd = new List<Account>();
	
	            for(Integer i=0; i < ItemsPerPage; i++) {
	                if(itemsAdded == items.size())
	                    break;
	
	                itemsToAdd.add(items[itemsAdded]);
	                itemsAdded++;
	            }
	
	            newPages.add(new ItemPage(itemsToAdd,pagenumber));
	            if(pagenumber == 1) {
	                newPages[0].first = true;
	            }
	        }
	        newPages[pagenumber-1].last=true;
        }               
        return newPages;
    }

    public void updateActivePages(){
        activePages = new List<ItemPage>();
        activePagesStartPNumber = pnum;
        Integer startIndex = pnum - 1;
        Integer endIndex = startIndex + pagesPerSetofActivePages;

        for (Integer i= startIndex; i < pages.size() && i < endIndex; i++)
            activePages.add(pages[i]);

        activePagesEndPNumber = activePages[activePages.size() - 1].pageNumber;

        if (activePagesEndPNumber == pages.size())
            hasMorePagesAfter = false;
        else
            hasMorePagesAfter = true;

        if (activePagesStartPNumber == 1)
            hasMorePagesBefore = false;
        else
            hasMorePagesBefore = true;

        setActivePage();
    }

    public void updateActivePagesBackwards(){
        List<ItemPage> tempActivePages = new List<ItemPage>();
        activePages = new List<ItemPage>();

        activePagesEndPNumber = pnum;
        Integer endIndex = pnum - 1;
        Integer startIndex = endIndex - pagesPerSetofActivePages - 1;

        for (Integer i= endIndex; (i >= 0) && (i > startIndex); i--)
            tempActivePages.add(pages[i]);

        for(Integer i = (tempActivePages.size() - 1); i >= 0; i--) {           
            activePages.add(tempActivePages[i]);
        }

        activePagesStartPNumber = activePages[0].pageNumber;

        if (activePagesEndPNumber == pages.size())
            hasMorePagesAfter = false;
        else
            hasMorePagesAfter = true;

        if (activePagesStartPNumber == 1)
            hasMorePagesBefore = false;
        else
            hasMorePagesBefore = true;

        setActivePage();
    }

    public void updateItemsPerPage(){
        pnum = 1;
        populatePages(accounts, itemsPerPage);
        updateActivePages();
    }

    public void setActivePage(){
        Integer oldPN = activePage.pageNumber;
        activePage = pages[pnum - 1];
        pages[oldPN - 1].isActive = false;
        pages[pnum - 1].isActive = true;
    }

    public void updateActivePage(){
    	try{    		
	        setActivePage();
	        if (pnum > activePagesEndPNumber)
	            updateActivePages();
	        if (pnum < activePagesStartPNumber)
	            updateActivePagesBackwards();	        
        }catch(Exception e){
			pageErrorMsg = e.getMessage();				
		}
    }

	public void goToFirst(){		
        if (pages != null && pages.size() > 0)
            pnum = 1;
        	updateActivePage();        
    }

    public void goToLast(){
        if (pages != null && pages.size() > 0)
            pnum = pages.size();
        updateActivePage();
    }       
	/////// END Pagination controlling
	
	
	@isTest(SeeAllData=true) 
	static void AccountsWithContactsSameDomainController_Test(){		
		Lead l = new Lead(FirstName = 'Test 1', LastName = 'Last', Email = 'test@testmail1.com', Company = 'Company1');
		insert l; 
		
		List<Account> accs2Insert = new List<Account>();
		for(Integer i = 0; i < 15; i++){
			Account acc = new Account(Name = 'Test ' + i, BillingCountryCode = 'US',BillingStateCode = 'TX', Theater__c = 'North America');
			accs2Insert.add(acc);
		}
		insert accs2Insert;
		
		Integer i = 0;
		List<Contact> cons2Insert = new List<Contact>();
		for(Account acc: accs2Insert){
			Contact con = new Contact(FirstName = 'Test', LastName = 'TestLastName', AccountId = acc.Id, Email = 'test' + i + '@testmail1.com');
			cons2Insert.add(con);
			i++;
		}
		insert cons2Insert;
										
		PageReference pageRef = Page.AccountsWithContactsSameDomain;
		pageRef.getParameters().put('id', l.Id);
        Test.setCurrentPageReference(pageRef);
		 			    	          
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(l);
		AccountsWithContactsSameDomainController controller = new AccountsWithContactsSameDomainController(sc);					
				
		controller.pnum = 1;
		controller.nextPage();
		controller.prevPage();
		controller.goToLast();	
		controller.updateActivePages();
		controller.updateActivePagesBackwards();
		controller.updateItemsPerPage();
		controller.goToFirst();			
	} 
	
}