/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata = true)
private class TerritoryManagmentTest {

    static testMethod void TerritoryManagmentTest() {
        ClsObjectCreator cls = new ClsObjectCreator();
           
    
    
      Territory2Model ter2mod = [Select id from Territory2Model where state = 'Active' limit 1];
      system.debug('@@@ter2mod ' + ter2mod);
      Territory2Type ter2type = [select id from Territory2Type limit 1];
      system.debug('@@@ter2type ' + ter2type);

      ClsObjectCreator.createTerritory(ter2mod.Id,ter2type.Id,'TestTerritory1');
      ClsObjectCreator.createTerritory(ter2mod.Id,ter2type.Id,'TestTerritory2');
      list<Territory2> ter =  [select id ,Name,DeveloperName,Territory2ModelId,Territory2TypeId from Territory2 ];

     Account acc = cls.createAccount('TestAcc1');
     Account acc2 = cls.createAccount('TestAcc2');  
     Opportunity opp = cls.createOpportunity(acc);
     Lead ld = cls.createLead();
    ld =  [select id,country,state, Country_and_state_key__c from lead where id =: ld.id];
    Test.startTest();
       Territory_Help__c th =  cls.createTerritoryHelp(acc,'Territory2AssignmentRule',string.valueOf(ter[0].id),'xxx',date.today()-3);
       Territory_Help__c th2 =  cls.createTerritoryHelp(acc,'Territory2Manual',string.valueOf(ter[1].id),'xxx',date.today()-1);
        system.debug(ld.Country_and_state_key__c + ' & ' + th.Country_and_state_key__c )  ;   
       delete th;
       opp.accountid = acc2.id;
       update opp;
       ld.country = 'IL';
       update ld;
   Test.stopTest();

        
        
    }
}