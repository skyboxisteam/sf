/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_TAM_CommentsTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_TAM_CommentsTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new TAM_Comments__c());
    }
}