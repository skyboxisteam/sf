public class BugHandler 
{
    /**
    *@Author Rina Nachbas                           events: after update
    *@Date 31.10.18
    *@Task: SF-733
    *@Description : Integration bugzilla and SF: if Bug_c.Status__c changed & Bug_c.Requirement_c = Support, add comment to related cases & Update Case.For_Support_Review__c = true
    */ 
    public static void createCaseComment(List<Bug__c> bugList, Map<Id, Bug__c> oldBug){
        Set<Id> bugChanged = new Set<Id> ();
        List<Bug_To_Case__c> bug2CaseToAlert;
        //List<CaseComment> newComments = new List<CaseComment> ();
        Map<Id,Case> updateCases = new Map<Id,Case> ();
        String strCommentBody = 'The bug {0} changed the status from {1} to {2}';//'The bug {0} '+ Label.BugRequirementChanged2Support;//

        for(Bug__c b : bugList){
            if(b.Requirement__c == 'Support' && b.Status__c != oldBug.get(b.Id).Status__c){
                bugChanged.add(b.Id);
            }
        }

        //system.debug('@@@createCaseComment: bugChanged: ' + bugChanged);
        bug2CaseToAlert = [select Case__c, Bug__r.Name, Bug__c, Bug__r.Status__c from Bug_To_Case__c where Bug__c in :bugChanged];
        //system.debug('@@@createCaseComment: bug2CaseToAlert: ' + bug2CaseToAlert);

        for(Bug_To_Case__c b2c : bug2CaseToAlert){
            //to avoid 'Duplicate id in list' error on update
            if(!updateCases.keySet().Contains(b2c.Case__c)){
                updateCases.put(b2c.Case__c,(new Case(Id = b2c.Case__c, For_Support_Review__c = true)));
            }
            //newComments.add(new CaseComment(ParentId = b2c.Case__c, IsPublished = false, CommentBody =  String.format(strCommentBody, new String[]{b2c.Bug__r.Name, oldBug.get(b2c.Bug__c).Status__c, b2c.Bug__r.Status__c})));
        }
        //system.debug('@@@createCaseComment: newComments: ' + newComments);

        if(updateCases.size() > 0){
            // DML statement
            Database.SaveResult[] srList = Database.update(updateCases.values(), false);
           // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('@@@createCaseComment: Successfully update Case. Case ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('@@@createCaseComment: The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('CaseComment fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }

        //Comment is created by: Process Builder - Bug to case - Add comment change status
        //if(newComments.size() > 0){
        //    // DML statement
        //    Database.SaveResult[] srList = Database.insert(newComments, false);
        //   // Iterate through each returned result
        //    for (Database.SaveResult sr : srList) {
        //        if (sr.isSuccess()) {
        //            // Operation was successful, so get the ID of the record that was processed
        //            System.debug('@@@createCaseComment: Successfully inserted CaseComment. CaseComment ID: ' + sr.getId());
        //        }
        //        else {
        //            // Operation failed, so get all errors                
        //            for(Database.Error err : sr.getErrors()) {
        //                System.debug('@@@createCaseComment: The following error has occurred.');                    
        //                System.debug(err.getStatusCode() + ': ' + err.getMessage());
        //                System.debug('CaseComment fields that affected this error: ' + err.getFields());
        //            }
        //        }
        //    }
        //}
    }
    
}