/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
//(SeeAllData=true)
private class Ctrl_AssignCasesTest {

    static testMethod void myUnitTest() 
    {
      	ClsObjectCreator cls = new ClsObjectCreator();
       system.debug( '&&&');
      // Id userRoleId = [Select Id,Name,DeveloperName From UserRole where DeveloperName =: 'TLS' limit 1].Id; 
      // ClsObjectCreator.createUserWithRole(userRoleId);
       User userSupport = [select id,UserRoleID,UserRole.developername  from user where UserRole.developername =: 'Support_Engineer' limit 1];
       User userSolutions = [select id,UserRoleID,UserRole.developername  from user where UserRole.developername =: 'Solution_Manager' limit 1];
       User userPS = [select id,UserRoleID,UserRole.developername  from user where UserRole.developername =: 'PSEngineer' and isactive = true limit 1];
    
    
    
       test.startTest(); 
    	Case cas = cls.createCase();
      // Ctrl_AssignCases.run=true; 
        cas.ownerid = userSupport.id; 
        update cas;
        cas.ownerid = userPS.id; 
        update cas;
       
        Ctrl_AssignCases.run=true; 
        cas.assign__c = 'PM';
        update cas;
        
         Ctrl_AssignCases.run=true; 
        cas.assign__c = 'Support';
        update cas;
       
      
        system.debug( '%%%' +cas.ownerid);
     //   cas.assign__c = 'PM';
      //  update cas;
    
    	ApexPages.StandardController sc = new ApexPages.StandardController(cas);
	    Ctrl_AssignCases ctrler = new Ctrl_AssignCases(sc);
	    ctrler.selectedDepartment = 'CurrentUser';
	     system.debug( '%%%' +cas.ownerid);
	    ctrler.executeBtn();
		 test.stopTest(); 
     /*   cas.ownerid = userTLS.id; 
        update cas;
    
        system.debug( '%%%' +cas.ownerid);
  
        cas.assign__c = 'PM';
        update cas;
       
         cas.assign__c = 'Solution';
        update cas;*/
        
    }

    //Rina
    //SF-976 : Rejection field in case level is "mandatory" just when it is being "assign to tier 4"
    static testMethod void assignTier4Test() 
    {
      ClsObjectCreator cls = new ClsObjectCreator();
    
      Case cas = cls.createCase();


      List <Case> caseTest = [select Id, Rejection__c, Status from Case where Id = :cas.Id];
      System.assertEquals(null, caseTest[0].Rejection__c);
      System.assertEquals('New', caseTest[0].Status);
      

      ApexPages.StandardController sc = new ApexPages.StandardController(cas);
      Ctrl_AssignCases ctrler = new Ctrl_AssignCases(sc);
     
     
      test.startTest(); 
      //1. Change fail: Status is 'Tier 4' && Rejection__c is null
        ctrler.selectedDepartment = 'Tier 4';
        ctrler.executeBtn();
     test.stopTest(); 

     //VR Error thrown and status doesn't changed - Status is 'Tier 4' && Rejection__c is null
     caseTest = [select Id, Rejection__c, Status from Case where Id = :cas.Id];
     System.assertEquals('New', caseTest[0].Status);


      //2. Change succeed: Status is 'Tier 3' && Rejection__c is null
      ctrler.selectedDepartment = 'Tier 3';
      ctrler.executeBtn();
      //Status update pass - Status is 'Tier 3' && Rejection__c is null
      caseTest = [select Id, Rejection__c, Status from Case where Id = :cas.Id];
      System.assertEquals('Assigned to Tier 3', caseTest[0].Status);



      //3. Change succeed: Status is 'Tier 4' && Rejection__c is NOT null
      //Case cas2 = cls.createCase();
      cas.Rejection__c = 'Regression';
      //cas.Status = 'Assigned to Tier 4';
      update cas;

      caseTest = [select Id, Rejection__c, Status from Case where Id = :cas.Id];
      System.assertEquals('Regression', caseTest[0].Rejection__c);

      sc = new ApexPages.StandardController(cas);
      ctrler = new Ctrl_AssignCases(sc);
      ctrler.selectedDepartment = 'Tier 4';
      ctrler.executeBtn();

      //Status update pass - Status is 'Assigned to Tier 4' && Rejection__c is NOT null
      caseTest = [select Id, Rejection__c, Status from Case where Id = :cas.Id];
      System.assertEquals('Assigned to Tier 4', caseTest[0].Status);


    }
}