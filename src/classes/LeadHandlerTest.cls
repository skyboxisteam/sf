@isTest
private class LeadHandlerTest {
	
	static ClsObjectCreator objCreator = new ClsObjectCreator();
	// Dummy CRON expression: midnight on March 15.
    // Because this is a test, job executes
    // immediately after Test.stopTest().
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';


	@testSetup static void setupData() 
	{
		Test.startTest();
		User myAdminUser = [select id from user  where profile.Name='System Administrator' and isActive=true limit 1];
		//create Account
		Account acc =  objCreator.createAccount('TestAccount');
        //create Contact
		Contact con = ClsObjectCreator.createContact('Test', acc.Id);
		//create opp
		Opportunity opp =  objCreator.createOpportunity(acc);
		Opportunity opp2 =  objCreator.createOpportunity(acc);
		Test.stopTest();
	}
	
	@isTest static void updateNextExpirationEmailTest() {
		Lead newLead = objCreator.createLead('Deal Registration', 'Magentrix Partner Portal');
		List<Lead> leadTest = [select ID, Magentrix_Partner_Expiration_Date__c, Next_Expiration_Email__c from Lead];
		System.assertEquals('Next: 7 Days Expiration',leadTest[0].Next_Expiration_Email__c);
		
		newLead.Magentrix_Partner_Expiration_Date__c = Date.today().addDays(8);
		update newLead;

		Test.startTest();
			newLead.Magentrix_Partner_Expiration_Date__c = Date.today().addDays(4);
			update newLead;
		Test.stopTest();
		leadTest = [select ID, Magentrix_Partner_Expiration_Date__c, Next_Expiration_Email__c from Lead];
		System.assertEquals('Next: Expiration',leadTest[0].Next_Expiration_Email__c);


		newLead.Magentrix_Partner_Expiration_Date__c = Date.today().addDays(8);
		update newLead;

		leadTest = [select ID, Magentrix_Partner_Expiration_Date__c, Next_Expiration_Email__c from Lead];
		System.assertEquals('Next: 7 Days Expiration',leadTest[0].Next_Expiration_Email__c);
		


	}
	//Test OpportunityHandler.updateNextExpirationEmail
	@isTest static void updateOppNextExpirationEmailTest() {
		List<Opportunity> oppTest = [select ID, MagentrixOne__ExclusivityExpirationDate_mgtrx__c, Next_Expiration_Email__c from Opportunity];
		//Field default value is "Next: 7 Days Expiration"
		System.assertEquals('Next: 7 Days Expiration',oppTest[0].Next_Expiration_Email__c);
		System.assertEquals('Next: 7 Days Expiration',oppTest[1].Next_Expiration_Email__c);
		
		oppTest[0].MagentrixOne__ExclusivityExpirationDate_mgtrx__c = Date.today().addDays(8);
		update oppTest[0];

		Test.startTest();
			oppTest[0].MagentrixOne__ExclusivityExpirationDate_mgtrx__c = Date.today().addDays(4);
			update oppTest[0];
		Test.stopTest();
		oppTest = [select ID, MagentrixOne__ExclusivityExpirationDate_mgtrx__c, Next_Expiration_Email__c from Opportunity];
		System.assertEquals('Next: Expiration',oppTest[0].Next_Expiration_Email__c);


		oppTest[0].MagentrixOne__ExclusivityExpirationDate_mgtrx__c = Date.today().addDays(8);
		update oppTest[0];

		oppTest = [select ID, MagentrixOne__ExclusivityExpirationDate_mgtrx__c, Next_Expiration_Email__c from Opportunity];
		System.assertEquals('Next: 7 Days Expiration',oppTest[0].Next_Expiration_Email__c);
		


	}

	@isTest static void sendAlertInsertTest() {
		Test.startTest();
			objCreator.createLead('Deal Registration', 'Magentrix Partner Portal');
		Test.stopTest();
		List<Lead> leadTest = [select ID from Lead];
		System.assertEquals(1,leadTest.size());
	}
	
	//Test Update status to Accepted,Rejected,Convert
	@isTest static void sendAlertUpdateTest() {
        Contact con = [select Id from Contact limit 1];
        Account acc = [select Id from Account limit 1];
		Lead newLead = objCreator.createLead('Deal Registration', 'Magentrix Partner Portal');
		newLead.Status = 'Accepted';
        newLead.Partner_ContactNameLU__c = con.Id;
        newLead.Name__c = acc.Id;
		update newLead;
		newLead.Status = 'Rejected';
		newLead.Deal_Reg_Rejection_Reason__c = 'Rejected';
		update newLead;
		

		Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(newLead.id);

        test.startTest();

        LeadStatus convertStatus = [Select Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        //lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setConvertedStatus('Accepted');//exist in lead status

        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());

        test.stopTest();
	}

	//Test Lead TimeBase
	@isTest static void sendAlertToLeadOnTimeBaseTest() {


		Lead leadToExpiry = objCreator.createLead('Deal Registration', 'Magentrix Partner Portal');
		leadToExpiry.Magentrix_Partner_Expiration_Date__c = Date.today().addDays(7);
		update leadToExpiry;

		Lead leadExpired = objCreator.createLead('Deal Registration', 'Magentrix Partner Portal');
		leadToExpiry.Magentrix_Partner_Expiration_Date__c = Date.today();
		update leadExpired;

		Test.startTest();
        // Schedule the test job
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new LeadJobSchedulable());         
        
        // Stopping the test will run the job synchronously
        Test.stopTest();
    }

    //Test Opp, lead time base
	@isTest static void sendAlertOnTimeBaseTest() {
		Contact con = [select Id from Contact limit 1];
        Account acc = [select Id from Account limit 1];
        
		Lead leadAcceptedExpiredToday = objCreator.createLead('Deal Registration', 'Magentrix Partner Portal');
		leadAcceptedExpiredToday.Deal_Reg_Accept_Reject_Date__c = Date.today();
		leadAcceptedExpiredToday.Status = 'Accepted';
        leadAcceptedExpiredToday.Partner_ContactNameLU__c = con.Id;
        leadAcceptedExpiredToday.Name__c = acc.Id;
		update leadAcceptedExpiredToday;

		Lead leadAcceptedExpired = objCreator.createLead('Deal Registration', 'Magentrix Partner Portal');
		leadAcceptedExpired.Deal_Reg_Accept_Reject_Date__c = Date.today().addDays(-14);
		leadAcceptedExpired.Status = 'Accepted';
        leadAcceptedExpired.Partner_ContactNameLU__c = con.Id;
        leadAcceptedExpired.Name__c = acc.Id;

		update leadAcceptedExpired;



		List<Opportunity> oppList = [select Id from Opportunity ];
		oppList[0].MagentrixOne__ExclusivityExpirationDate_mgtrx__c = Date.today().addDays(7);
		oppList[1].MagentrixOne__ExclusivityExpirationDate_mgtrx__c = Date.today();
		
		Test.startTest();
		update oppList;

        // Schedule the test job
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new LeadJobSchedulable());         
        
        // Stopping the test will run the job synchronously
        Test.stopTest();
    }
}