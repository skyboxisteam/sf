/*
AUTHOR : Dmitry Rivlin 
EMAIL : wildervit@gmail.com
TARGET: implement a batch and scheduler for prepare PS Engineers
*/

global with sharing class SCH_collectAccountTeamChanges implements Schedulable,Database.Batchable<sObject>,Database.stateful {
    
    global SCH_collectAccountTeamChanges() {
    }

    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String SQL = 'SELECT Id, PS_Engineer__c, (SELECT ID, UserId, User.Name FROM AccountTeamMembers WHERE TeamMemberRole=\'PS Engineer\') FROM Account WHERE Id IN (SELECT AccountId from AccountTeamMember WHERE TeamMemberRole=\'PS Engineer\')';
        return Database.getQueryLocator(SQL);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        for (Account Item : (Account[]) scope) {
            Item.PS_Engineer__c = '';
            for (AccountTeamMember aItem : Item.AccountTeamMembers) {
                Item.PS_Engineer__c += (Item.PS_Engineer__c=='' ? '' :', ') + aItem.User.Name;
            }
            system.debug(LoggingLevel.ERROR,'UPDATE Account ' + Item.Id + ' : ' + Item.PS_Engineer__c);
        }
        Database.SaveResult[] results = Database.Update(scope, false);
    }
    
    global void finish(Database.BatchableContext BC) {
    }

    /* SCHEDULER PART */
    global void execute(SchedulableContext sc) {
        //system.abortJob(sc.getTriggerID());
        database.executeBatch(new SCH_collectAccountTeamChanges(), 100000);
    }

}