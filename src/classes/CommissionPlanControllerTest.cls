/*
	CommissionPlanControllerTest - test for Commission plan & Booking schedule proccess - CM-4
	Author: Rina Nachbas
	Date: 27.6.18
*/
@isTest
public class CommissionPlanControllerTest {
	
	static ClsObjectCreator objCreator = new ClsObjectCreator();

	@testSetup static void setupData() {
		
		User myAdminUser = [select id from user  where profile.Name='System Administrator' and isActive=true limit 1];
		
		//create Account
		Account acc =  objCreator.createAccount('TestAccount');
		//create opp
		Opportunity opp =  objCreator.createOpportunity(acc);
		//create Contact
		Contact con = ClsObjectCreator.createContact('Test', acc.Id);
		//create qoute
		SBQQ__Quote__c qoute = objCreator.createQuote(con, 'Draft');
		qoute.SBQQ__Opportunity2__c = opp.Id;
		update qoute;
Test.startTest();
		opp.SBQQ__PrimaryQuote__c = qoute.Id;
		opp.Probability = 96;
		update opp;


		//create cp
		Commission_Plan__c cp = new Commission_Plan__c();
		cp.Sales_User__c = myAdminUser.Id;
		cp.Name = 'Rinas Commision Plan 2018';
		insert cp;

		//create Commission Rule
		Commission_Rule__c annualAccelerator = objCreator.createCommissionRule(Label.AnnualAccelerator, '{ "Annual Quota" : 2165, "On Target Commission" : 0.5 ,"Factor" : 1.3,"Annual Quota Override":0 , "Base Rate Override":0}', Label.Commission_Plan);
		Commission_Rule__c annualPIOAccelerator = objCreator.createCommissionRule(Label.AnnualPIOAccelerator, '{ "Annual Quota" : 2165, "On Target Commission" : 0.5 ,"Factor" : 1.3,"Annual Quota Override":0 , "Base Rate Override":0}');
		Commission_Rule__c h1Accelerator = objCreator.createCommissionRule(Label.H1Accelerator, '{ "Threshold" : 0.4, "Factor" : 1.5 , "H1 Quota Override": 0 , "H1 Attainment Override" :0 }');
		Commission_Rule__c h1PIOAccelerator = objCreator.createCommissionRule(Label.H1PIOAccelerator, '{ "Threshold" : 0.4, "Factor" : 1.5 , "H1 Quota Override": 0 , "H1 Attainment Override" :0 }');
		Commission_Rule__c superAccelerator = objCreator.createCommissionRule(Label.SuperAccelerator, '{"Threshold":0,"Factor":0 }');
		Commission_Rule__c multiYearAccelerator = objCreator.createCommissionRule(Label.MultiYear, '{ "CommissionFactor" : 0, "QuotaFactor" : 0.5 }', Label.Commission_Schedule);


		//create opp line
		objCreator.createOppLineItem(opp.Id, 'Products', 'Perpetual');
		objCreator.createOppLineItem(opp.Id, 'Products', 'Subscription');
		objCreator.createOppLineItem(opp.Id, 'Support, maintenance and content', 'Subscription');
//		Test.startTest();
		objCreator.createOppLineItem(opp.Id, 'Professional Services and Training', 'Subscription');
		objCreator.createOppLineItem(opp.Id, 'Professional Services and Training', 'PS');
		
		//to avoid too many soql in prod on createCommissionRecord step
		//objCreator.createOppLineItem(opp.Id, 'Professional Services and Training', 'PS','On Delivery');
		//objCreator.createOppLineItem(opp.Id, 'Professional Services and Training', 'PS','Prepaid');
		//objCreator.createOppLineItem(opp.Id, 'Professional Services and Training', 'PS','Prepaid', 'Discount');


		//create Commission Rule Assi
		List<Commission_Rule_Assignment__c>  craList = new List<Commission_Rule_Assignment__c>();
		craList.add(objCreator.createCommissionRuleAssignment(Label.AnnualAccelerator, annualAccelerator.Id, cp.Id, false));
		craList.add(objCreator.createCommissionRuleAssignment(Label.AnnualPIOAccelerator, annualPIOAccelerator.Id, cp.Id, false));
		craList.add(objCreator.createCommissionRuleAssignment(Label.H1Accelerator, h1Accelerator.Id, cp.Id, false));
		craList.add(objCreator.createCommissionRuleAssignment(Label.H1PIOAccelerator, h1PIOAccelerator.Id, cp.Id, false));
		craList.add(objCreator.createCommissionRuleAssignment(Label.Commission_Schedule, multiYearAccelerator.Id, cp.Id, false));
		insert craList;
		Test.stopTest();//to decrease too many soql


		//create Commission Record
		Commission_Record__c cr = objCreator.createCommissionRecord(opp.Id, cp.Id, Label.Regular);
		Commission_Record__c crPIO = objCreator.createCommissionRecord(opp.Id, cp.Id, Label.PIO);

	}

	static testMethod void testCtor()   {


		//User myAdminUser = [select id from user  where profile.Name='System Administrator' and isActive=true limit 1];
		Opportunity opp = [select id from Opportunity Limit 1];
		
		Test.startTest();
		Commission_Plan__c cp = [select id from Commission_Plan__c where Name = 'Rinas Commision Plan 2018' limit 1];
		
		Test.stopTest();//to decrease too many soql

		//create Commission Record
		//Commission_Record__c cr = objCreator.createCommissionRecord(opp.Id, cp.Id, Label.Regular);
		//Commission_Record__c crPIO = objCreator.createCommissionRecord(opp.Id, cp.Id, Label.PIO);
		

        //Test CommissionRecalculateJob schedule job
        CommissionRecalculateJob c = new CommissionRecalculateJob();
        c.execute(null);
		//create StandardController
		ApexPages.StandardController sc = new ApexPages.StandardController(cp);
		CommissionPlanController cpCtrl = new CommissionPlanController(sc);
		String commissionPlan 			= cpCtrl.serializedCommissionPlan;
		String sCommissionScheduleList 	= cpCtrl.serializedCommissionScheduleList;
		String commissionRuleAssList  	= cpCtrl.serializedCommissionRuleAssList;
		String acceleratorMap 			= cpCtrl.serializedAcceleratorMap;
		String recurringIncentiveMap  	= cpCtrl.serializedRecurringIncentiveMap;
		String valuesMap  				= cpCtrl.serializedValuesMap;

		
	}
}