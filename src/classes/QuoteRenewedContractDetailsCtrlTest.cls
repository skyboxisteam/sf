@isTest
private class QuoteRenewedContractDetailsCtrlTest {
  private static string renewal = 'Renewal';
	
	@testSetup static void setupData() {
       
       ClsObjectCreator cls = new ClsObjectCreator();
       Account a = cls.CreateAccount('Test');
       opportunity opp = cls.createOpportunity(a);
       SBQQ__Quote__c q = cls.createQuote(ClsObjectCreator.createContact('Test',a.id),'Draft');
       Contract c = cls.createContract(a,opp,q);
       //c.SBQQ__RenewalOpportunity__c = opp.Id;
       //update c;
       SBQQ__Subscription__c sc= cls.createSubscription(a,c,cls.createProduct('name ')); 
       SBQQ__Subscription__c sc2= cls.createSubscription(a,c,cls.createProduct('name2')); 
       Asset ass = cls.createAsset('test','test',a);
       SBQQ__SubscribedAsset__c sa = cls.createSubscribedAsset(sc,ass);

    }

	@isTest static void ctrlTest() {

       Account  a = [select Id from Account Limit 1];
       Contact con = [select Id from Contact Limit 1];
       Opportunity opp = [select Id from Opportunity Limit 1];
		SBQQ__Quote__c q = [select Id, SBQQ__Opportunity2__c from SBQQ__Quote__c Limit 1];

		//SBQQ__Quote__c qu = new SBQQ__Quote__c(SBQQ__SubscriptionTerm__c = 13);
      	//qu.SBQQ__Status__c = 'Draft';
      	//qu.SBQQ__Account__c = a.Id;
      	//qu.WDSB_CustomerContact__c = con.id;
      	//qu.Pending_approval_timestamp__c = system.now() - 8;
      	//qu.SBQQ__Type__c = renewal;
      	//qu.SBQQ__Opportunity2__c = opp.Id;
      	//insert qu;

      	Test.startTest();
		ApexPages.StandardController sc = new ApexPages.StandardController(q);
		QuoteRenewedContractDetailsCtrl ctrl = new QuoteRenewedContractDetailsCtrl(sc);
		Test.stopTest();
	}
	
	
}