@isTest (SeeAllData=true)
public class ChannelAnalyzerTest {
    static testMethod void ChannelAnalyzerTest() {
        ChannelAnalyzer analyzer = new ChannelAnalyzer('00130000003EHnq');
        Integer pipe = analyzer.getOpenPipeline();
        pipe = analyzer.getOpenPipelineThisQuarter();
        pipe = analyzer.getWonThisYear();
        
        pipe = 0;
        
        List<AggregateResult> l = analyzer.getSalesByYear();
        l = analyzer.getNewCustomerPipelineByYear();
        analyzer.getCampaignsByYear();
        
        Date d = analyzer.getLatestCampaignDate();
        d = analyzer.getLatestPipelineProgressDate();
        d = analyzer.getLatestWinDate();
        d = analyzer.getLatestPipelineAdditionDate();
        d = analyzer.getLatestNewCustomerPipelineOriginatedDate();
        
        analyzer.getEngagementScore();
        analyzer.getEngagementScore3M();
        analyzer.getHistoricEngagementScore();
        analyzer.getWon365D();
        
        analyzer.getSubChannelsNames();
        
        System.assert(pipe >= 0 || pipe < 0);
    }
}