global without sharing class CommissionRecalculateJob  implements Schedulable,  Database.Batchable<sobject> , Database.Stateful {
  
    
global void execute(SchedulableContext sc)
    { 
       Database.executeBatch(this,50); 
    }
    global Database.Querylocator start (Database.BatchableContext BC)
    {
     return Database.getQueryLocator( [SELECT id,
                                              Commission_Amount__c,
                                              Booking_Schedule__c,
                                              Booking_Schedule__r.Type__c ,
                                              Opportunity__c,
                                              Opportunity__r.SBQQ__PrimaryQuote__c,
                                              Commission_Plan__c,
                                              isMulti__c
                                       FROM Commission_Schedule__c 
                                       WHERE Is_Advancable__c = true and 
                                             Data_Conversion__c = false and 
                                             Collection_date_passed__c = true and 
                                             Opportunity_Close_Date__c = LAST_N_YEARS:3 and
                                             Booking_Schedule__r.Fully_Collected__c = false and 
                                             Booking_Schedule__r.Commissinable__c = true ]);
    }
 
  global void execute (Database.BatchableContext BC, List<sobject> scope)
    {
      list<Commission_Schedule__c> scheduleList = new list<Commission_Schedule__c>((list<Commission_Schedule__c>)scope); 
      if(!scheduleList.IsEmpty())
       {  
          CommissionScheduleCalc csc = new CommissionScheduleCalc(scheduleList); // Create the instance of Commission schedule calculation
          map<id,Booking_Schedule__c> bookingMap = new map<id,Booking_Schedule__c> (csc.getBookingSchedules()); //Map to be able to pass BS from id
          list<Commission_Schedule__c> scheduleUpdateList = new list<Commission_Schedule__c>();          
          csc.createRuleMap(scheduleList); // Create the commission rule map for the relevant commission schedule
          
          for(Commission_Schedule__c cs : scheduleList)
          {
            system.debug(' cs ' + cs);
             Booking_Schedule__c bs = bookingMap.get(cs.Booking_Schedule__c);
             Boolean isAdvanceable = csc.getIsCommissinable(cs.Commission_Plan__c,bs ); // check if the collection rules apply or not
             system.debug(' isAdvanceable ' + isAdvanceable);
             if(!isAdvanceable)
               {
                 cs.Commission_Amount__c = csc.getCommissionFactoredAmount(cs,bs,bookingMap.Values(),cs.isMulti__c, csc.getAdvancable(bs, isAdvanceable)); // Re-Calcualte commission amount
                 cs.Is_Advancable__c = false;
                 scheduleUpdateList.add(cs);
               }
          }
 
           system.debug(' scheduleUpdateList ' + scheduleUpdateList);
          if(!scheduleUpdateList.isEmpty()) // Update the commission schedule and the trigger of commission schedule will re-calc the comission plans
          {
            update scheduleUpdateList;
    
          }
         
       }
    //  system.debug(bookingRecalc);
  }

  global void finish(Database.BatchableContext BC)
   {
        
   }
}