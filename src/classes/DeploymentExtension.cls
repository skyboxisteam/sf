public with sharing class DeploymentExtension {
    public List<Deployment__c> deploymentList {get;set;}
    public DeploymentExtension(ApexPages.StandardController controller) {    
        List<String> fields = new List<String>();
        fields.add('accountid');
        String accnt = '';
        if(!test.isRunningTest())
             controller.addFields(fields);
        
        Case c = (Case)controller.getRecord();
        if(!test.isRunningTest())
            accnt = c.AccountId;
        deploymentList = [select id, name, deployment_type__c, phase__c, project_name__c, PS_Owner__r.name from deployment__c where account__c = :accnt];
    }
}