@isTest
private class CaseControllerTest {
    
    @isTest static void saveCaseTest() {
        ClsObjectCreator obCr = new ClsObjectCreator();
        Case c = obCr.createCase();
        System.assertEquals(c.Id, CaseController.saveCase(c));
    }
    
    @isTest static void getselectOptionsTest() {
        ClsObjectCreator obCr = new ClsObjectCreator();
        Case c = obCr.createCase();
       
        Schema.DescribeFieldResult fieldResult = Case.Module__c.getDescribe();
        List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
        List <String> allOpts = new list <String> ();
        for (Schema.PicklistEntry a: values) {
           allOpts.add(a.getValue());
        }
        System.assertEquals(allOpts, CaseController.getselectOptions(c, 'Module__c',c.recordtypeid));
    }
    
}