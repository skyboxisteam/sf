public with sharing class PartnerOpps {
	public List<Opportunity> opportunities;

	private ApexPages.StandardController ctrl;

	public PartnerOpps(ApexPages.StandardController ctrlParam){
	  ctrl = ctrlParam;
	}
	
	public PartnerOpps() {
		
	}
	
	public List<AggregateResult> getAllPipeline() {
		return [SELECT FISCAL_YEAR(CreatedDate) year, count(Id) opportunities, sum(Amount) amount FROM Opportunity 
					WHERE Partner__c != null group by FISCAL_YEAR(CreatedDate) order by FISCAL_YEAR(CreatedDate)];
	}

	public List<AggregateResult> getPipelineCreatingChannels() {
		return [SELECT FISCAL_YEAR(CreatedDate) year, count_distinct(Partner__c) channels, count(Id) opportunities, sum(Amount) amount FROM Opportunity 
					WHERE Partner__c != null and Lead_Source_Type__c = 'partner' and CreatedDate >= LAST_N_FISCAL_YEARS:5  group by FISCAL_YEAR(CreatedDate) 
					order by FISCAL_YEAR(CreatedDate) ];
	}

	public List<AggregateResult> getTransactingChannels() {
		List<AggregateResult> results = [SELECT FISCAL_YEAR(CloseDate) year, count_distinct(Partner__c) channels, sum(Amount) amount FROM Opportunity 
					WHERE Partner__c != null and IsWon = true and CloseDate >= LAST_N_FISCAL_YEARS:5 group by FISCAL_YEAR(CloseDate) order by FISCAL_YEAR(CloseDate) ];
			
		return results;
	}

/*
	public List<AggregateResult> getTransactingChannelsByTheater() {
		List<AggregateResult> results = [SELECT FISCAL_YEAR(CloseDate) year, Theater__c theater, count_distinct(Partner__c) channels, sum(Amount) amount FROM Opportunity 
					WHERE Partner__c != null and IsWon = true group by FISCAL_YEAR(CloseDate), Theater__c order by FISCAL_YEAR(CloseDate) ];
			
		return results;
	}
*/

}