/**
*@Author Rina Nachbas                   
*@Date 12/19                      
*@Task: SF-1072
*@Description : controller for QuoteRenewedContractDetails VF (UI of Renewed contract data in the renewal quote)
*/  
public with sharing class QuoteRenewedContractDetailsCtrl {

	private SBQQ__Quote__c               myQuote;
    public List<wrapSub>                 wrapSubList  = new  List<wrapSub>();

    // The extension constructor initializes the private member
    // variable myQuote by using the getRecord method from the standard
    // controller.
    public QuoteRenewedContractDetailsCtrl(ApexPages.StandardController stdController) {
        this.myQuote = (SBQQ__Quote__c)stdController.getRecord();
        this.myQuote =[select Id, SBQQ__Opportunity2__c from SBQQ__Quote__c where Id = :this.myQuote.Id];

        getRenewedContractData();
    }

    public String serializedSubscrpList {get {
                                        return JSON.serialize(this.wrapSubList);
                                    } private set;}

    public void getRenewedContractData() {

        //system.debug('this.myQuote.SBQQ__Opportunity2__c: ' + this.myQuote.SBQQ__Opportunity2__c);
        //get all contracts related to current quote's opp
       List<Contract> contractList = [SELECT Id, ContractNumber, Contract_Value__c 
                                       FROM Contract 
                                       where SBQQ__RenewalOpportunity__c = :this.myQuote.SBQQ__Opportunity2__c ];
       //system.debug('contractList: ' + contractList);                                
       Integer term = 0;
       Decimal effectiveDiscount = 0;
       Decimal proratedPrice = 0;

        //get renewed subscriptions & contract data 
        for(SBQQ__Subscription__c sub : [SELECT Id, Name, SBQQ__Contract__c,SBQQ__ContractNumber__c,SBQQ__Contract__r.Contract_Value__c, 
                                            Original_Renewal_Price__c, SBQQ__ListPrice__c, SBQQ__ProductName__c, SBQQ__StartDate__c, SBQQ__EndDate__c
                                            ,SBQQ__ProrateMultiplier__c,WDSB_Distributor_Discount__c, WDSB_Partner_Discount_SB__c, SBQQ__Discount__c
                                            FROM SBQQ__Subscription__c 
                                            where SBQQ__Contract__c in :contractList]){
            
            term = Integer.valueOf(sub.SBQQ__StartDate__c.monthsBetween(sub.SBQQ__EndDate__c+1)); 
            effectiveDiscount = getFirstNotEmptyValue(new List<Decimal>{ sub.WDSB_Distributor_Discount__c, sub.WDSB_Partner_Discount_SB__c, sub.SBQQ__Discount__c});
            proratedPrice = ifnull(sub.SBQQ__ProrateMultiplier__c) == 0 ?  0: ifnull(sub.Original_Renewal_Price__c)/sub.SBQQ__ProrateMultiplier__c;//SB Net Price/prorate multiplier

            wrapSubList.add(new wrapSub(sub.Id, sub.Name,sub.SBQQ__ProductName__c,term, sub.Original_Renewal_Price__c, proratedPrice, effectiveDiscount,sub.SBQQ__Contract__c, sub.SBQQ__ContractNumber__c, sub.SBQQ__Contract__r.Contract_Value__c));
        }
    }

    //return the first value in the list which is not null/zero, if all are zero returns zero
    private Decimal getFirstNotEmptyValue(List<Decimal> values){
        for(Decimal val : values){
            if(ifnull(val) != 0)
                return val;
        }
        return 0;
    }
    private Decimal ifnull(Decimal num ) {
        if (num == null) num = 0;
            return num;
    } 

    public class wrapSub {
        
        public id       subId {get; set;}
        public string   name {get; set;}                //subscription number=name
        public string   product {get; set;}     
        public Integer  term {get; set;}                //subscription term in month
        public Decimal  netPrice {get; set;}            //price of all term period include discounts
        public Decimal  proratedPrice {get; set;}       //price of one year period include discounts
        public Decimal  effectiveDiscount {get; set;} 
        public string   contractId {get; set;}   
        public string   contractNumber {get; set;} 
        public Decimal  contractNetPrice {get; set;} 
 
      public wrapSub (id subIdParam,string nameParam, string productParam, Integer termParam, Decimal netPriceParam  ,Decimal proratedPriceParam, Decimal effectiveDiscountParam, string contractIdParam, string contractNumberParam, Decimal contractNetParam)
          {
            subId = subIdParam;
            name = nameParam ;
            product = productParam;
            term = termParam;
            netPrice = netPriceParam;
            proratedPrice = proratedPriceParam; 
            effectiveDiscount = effectiveDiscountParam;
            contractId = contractIdParam;
            contractNumber = contractNumberParam;
            contractNetPrice = contractNetParam;
          }
     
        }
}