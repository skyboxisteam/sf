public with sharing class CaseHandler {
	public CaseHandler() {
		
	}

	//Author: Rina Nachbas		Date:  16.8.18
	//Task:   SF-847			Event: after insert
	//Desc:   Update last creared deployment in case
	public static void updateCase(List<Case> newCaseList, map<id,Case> mapOldCase){
		List<Case> caseToUpdate = new List<Case>();
		Set<Id> accountSet = new Set<Id>();
		for(Case cs : newCaseList)
 		{
 			accountSet.add(cs.AccountId);
 			//system.debug('@@@updateCase: cs.' + cs.AccountId);
 		}
 		//get relevant Deployments by 'Has_active_deployment__c' & Number_of_active_PS_activities__c formulas
 		List<Deployment__c> DeploymentList = [select Id, Account__c from Deployment__c 
 												Where Account__c in :accountSet and
 													  Deployment_Type__c in ('Deployment', 'Project', 'Deployment by partner') and
 													  Phase__c not in ('Production', 'Stalled', 'Unknown', 'Stalled', 'Not in use', 'No PS - Unknown')
 													  order by createdDate desc];
        Map<Id, Id> accDep = new Map<Id, ID>();
 	    for(Deployment__c dep : DeploymentList)
 		{
 			//insert only first Deployment__c per Account, to get the last created deployment, as it is orderd by descending by createdDate
 			if(accDep.get(dep.Account__c) == null){ 
				accDep.put(dep.Account__c, dep.Id);
 			}
 		}
		for(Case cs : newCaseList)
 		{
 			caseToUpdate.add(new Case(Id=cs.Id, Deployment__c = accDep.get(cs.AccountId)));
 		}

 		if(caseToUpdate.size() > 0){
 			update caseToUpdate;
 		}
	}
}