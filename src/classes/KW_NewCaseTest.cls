@isTest
public class KW_NewCaseTest {

    @isTest(SeeAllData=true)
    static void RunTests(){
		KiteWorks_Integration__c KW = KiteWorks_Integration__c.getValues('TestCloud');
        Test.startTest();
        
        // Test GetKWFileRequestLink
        Test.setMock(HttpCalloutMock.class, new KW_NewCaseTest_CallOutMock());
        
        String actualValueGetKWFileRequestLink = KW_NewCase.GetKWFileRequestLink('2', KW);
        String expectedValueGetKWFileRequestLink = 'http://bla.com/asd123#2';
        System.assertEquals(actualValueGetKWFileRequestLink, expectedValueGetKWFileRequestLink);
        
        
        // Test GetKWFileRequestID
        String actualValueGetKWFileRequestID = KW_NewCase.GetKWFileRequestID('2', KW);
        String expectedValueGetKWFileRequestID = '123';
        System.assertEquals(actualValueGetKWFileRequestID, expectedValueGetKWFileRequestID);
        
        
        // Test CreateKWFileRequest
        Integer actualValueCreateKWFileRequest = KW_NewCase.CreateKWFileRequest('2', KW);
        Integer expectedValueCreateKWFileRequest = 200;
        System.assertEquals(actualValueCreateKWFileRequest, expectedValueCreateKWFileRequest);

        Test.setMock(HttpCalloutMock.class, new KW_NewCaseTest_CallOutMock201());        
        
        // Test MakeCallOutCreateCurrentFolder
        String actualValueMakeCallOutCreateCurrentFolder = KW_NewCase.MakeCallOutCreateCurrentFolder('00053555', KW);
        String expectedValueMakeCallOutCreateCurrentFolder = '28362';
        System.assertEquals(actualValueMakeCallOutCreateCurrentFolder, expectedValueMakeCallOutCreateCurrentFolder);
        

        // Test FolderCreation
        KW_NewCase.FolderCreation('00053555');
        //Integer expectedValueFolderCreation = 201;
        //System.assertEquals(actualValueFolderCreation, expectedValueFolderCreation);
        

		Test.stopTest();
    }
    
}