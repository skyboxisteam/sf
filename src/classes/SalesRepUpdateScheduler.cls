global class SalesRepUpdateScheduler implements Schedulable {
   private salesRepUpdateBatch batch = null;
   
   global SalesRepUpdateScheduler(String query) {
       batch = new SalesRepUpdateBatch(query);
   }
   
   global SalesRepUpdateScheduler() {
       batch = new SalesRepUpdateBatch();
   }

   global void execute(SchedulableContext ctx) {
      batch.runBatch();
      System.debug('running batch');
//      Database.executeBatch(batch);
   }   
}