public without sharing class CommissionPlanCalc 
{
  
  private list<Commission_Schedule__c> listCommissionSchedule = new list<Commission_Schedule__c>();
  private list<id> listComPlanId= new list<id>();
  public map<string,Commission_Rule_Assignment__c> mapComRuleAss = new map<string,Commission_Rule_Assignment__c>();
  private map<id,Commission_Plan__c> mapCommissionPlan = new map<id,Commission_Plan__c>();
  private set<string> bookingType =  new set<string>();  
  private list<string> comissionType =  new list<string> { Label.Commission,Label.Quota }; 
 

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------constractor------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  public Label.CommissionPlanCalc()
  {
 
  }
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------Constractor Commission Rule Assignment----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
  public CommissionPlanCalc(list<Commission_Rule_Assignment__c> listCRA)
  {
    for(Commission_Rule_Assignment__c cra : listCRA)
      {
        mapComRuleAss.put(cra.commission_plan__c+cra.Type__c,cra);
        listComPlanId.add(cra.Commission_Plan__c);
      }
   createMapCommissionPlan();
   getBookingType();
  }
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------Constractor Commission Schedule----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 
  public CommissionPlanCalc(list<Commission_Schedule__c> csCont)
  {
    list<Commission_Plan__c> cpCont = new list<Commission_Plan__c>();
    for(Commission_Schedule__c cs : csCont)
      {
        if(cs.Data_conversion__c)
          continue;
        listComPlanId.add(cs.Commission_Plan__c);
      }
    createMapCommissionPlan();
    createMapComRuleAss(listComPlanId);
    getBookingType();
  }

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------Constractor Commission Plan----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

  public CommissionPlanCalc(list<Commission_Plan__c> cpCont)
  {
 
    for(Commission_Plan__c cp : cpCont)
      {
       mapCommissionPlan.put(cp.id,cp);
       listComPlanId.add(cp.id);
      }
    listCommissionSchedule = getListCS();
    createMapComRuleAss(listComPlanId);
    getBookingType();
  }
  
  
   public CommissionPlanCalc(list<id> cpIdList)
  {
    listComPlanId = cpIdList;
    createMapCommissionPlan();
    listCommissionSchedule = getListCS();
    createMapComRuleAss(listComPlanId);
    getBookingType();
  }

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getBookingType----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

 private void getBookingType()
  { 
     Schema.DescribeFieldResult fieldResult =  Booking_Schedule__c.Type__c.getDescribe();
     for( Schema.PicklistEntry f : fieldResult.getPicklistValues())
      {
           bookingType.add(f.getValue());
      } 
 }   
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------createMapComRuleAss----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

private void createMapComRuleAss(list<id> listComPlanId)
  {
     for(Commission_Rule_Assignment__c cra :  CommissionMethods.getCommissionRule(listComPlanId))
       mapComRuleAss.put(cra.Commission_Plan__c + cra.Type__c,cra);
  }
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------createMapCommissionPlan----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
  private void createMapCommissionPlan()
   {
     listCommissionSchedule = getListCS();
     list<Commission_Plan__c> cpCont = getComPlanList();    
     for(Commission_Plan__c cp : cpCont)
       {
        cp = CommissionMethods.avoidNulls(cp);
        mapCommissionPlan.put(cp.id,cp);
       }
    
   }
   
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------calcAllComponents------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
   public void calcAllComponents()
    {   
     for(Commission_Plan__c cp :mapCommissionPlan.values())
       {
        cp = zeroAll(cp);
        cp = updateLegacyFields(cp);
        cp = calcSumSchedule(cp);
       // cp = RecurringIncetive(cp);
        cp = addFlat(cp);
        cp = calcUpToRate(cp);
        cp = calcUpToRatePIO(cp);
        cp = upToQuota(cp);
        cp = upToQuotaPIO(cp);
        cp = calcBeyondRate(cp);
        cp = calcBeyondRatePIO(cp);
        for(string s : comissionType)
          {
           cp = beyondQuota(cp,s);
          }
        cp = beyondQuotaPIO(cp);
        cp = getAccAchievements(cp);
        mapCommissionPlan.put(cp.id,cp);
     
       }
       
       update mapCommissionPlan.values();
        
    }

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------calcAllComponents------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
   public void calcAllComponentsNoLegacy()
    {   
     for(Commission_Plan__c cp :mapCommissionPlan.values())
       {
        cp = zeroAll(cp);
        cp = calcSumSchedule(cp);
       // cp = RecurringIncetive(cp);
        cp = addFlat(cp);
        cp = calcUpToRate(cp);
        cp = calcUpToRatePIO(cp);
        cp = upToQuota(cp);
        cp = upToQuotaPIO(cp);
        cp = calcBeyondRate(cp);
        cp = calcBeyondRatePIO(cp);
        for(string s : comissionType)
          {
           cp = beyondQuota(cp,s);
          }
        cp = beyondQuotaPIO(cp);
        cp = getAccAchievements(cp);
        
        mapCommissionPlan.put(cp.id,cp);
       }
       
       update mapCommissionPlan.values();
        
    }

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------zeroAll--------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------- 
 
  
 private Commission_Plan__c zeroAll(Commission_Plan__c cp)
 {
    cp.Sum_commission_amount__c = 0;
    cp.Sum_quota_amount__c = 0;
    cp.Flat_commission_amount__c = 0;
    cp.Flat_quota_amount__c = 0;
    cp.Sum_PIO__c=0;
    return cp;
    
 }      
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------calcSumSchedule----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
  public Commission_Plan__c calcSumSchedule(Commission_Plan__c cp)
   {
    decimal relevantAmount ;
    set<id> oppSet = new  set<id>();
    for(Commission_Schedule__c cs : listCommissionSchedule)
      {
       if(cp.id == cs.commission_plan__c && cs.Commissinable__c &&  cs.Commission_Record__r.Type__c == Label.Regular )//&& bookingType.contains(cs.Type__c) )      
         {
          // system.debug('Commission Amount ' + cs.Commission_Amount__c);
           if(cs.Commissinable__c)
           {
             cp.Sum_commission_amount__c += cs.isMulti__c ? 0 : ifnull(cs.Commission_Amount__c);
             cp.Sum_quota_amount__c += cs.isMulti__c ? 0 : ifnull(cs.Quota_Amount__c);
            }
         }
         
         if(cp.id == cs.Commission_plan__c &&  cs.Commission_Record__r.Type__c == Label.PIO )//&& bookingType.contains(cs.Type__c) )      
         {
           if(!oppSet.contains(cs.Opportunity__c))
             cp.Sum_PIO__c ++;
           
           oppSet.add(cs.Opportunity__c);
         }
        }
        return cp;
   }

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------calcUpToRatePIO----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
   public Commission_Plan__c calcUpToRatePIO(Commission_Plan__c cp)
  {
      if(CommissionMethods.checkRuleExist(cp.id,Label.AnnualPIOAccelerator,mapComRuleAss))
        if(CommissionMethods.checkRuleExist(cp.id,Label.H1PIOAccelerator,mapComRuleAss) && h1AttainmentPIO(cp,Label.H1PIOAccelerator,Label.AnnualPIOAccelerator) )
           cp.Up_to_quota_rate_PIO__c = getBaseRate(cp,Label.AnnualPIOAccelerator)*CommissionMethods.getRuleValue(cp.id,Label.H1PIOAccelerator,Label.Factor,mapComRuleAss);
        else
           cp.Up_to_quota_rate_PIO__c = getBaseRate(cp,Label.AnnualPIOAccelerator);
     
      return cp;
                
  }
  
  
  public Commission_Plan__c calcUpToRate(Commission_Plan__c cp)
  {
       
        if(CommissionMethods.checkRuleExist(cp.id,Label.H1Accelerator,mapComRuleAss) && h1Attainment(cp,Label.H1Accelerator, Label.AnnualAccelerator) )
            cp.Up_to_quota_rate__c = getBaseRate(cp,Label.AnnualAccelerator) *CommissionMethods.getRuleValue(cp.id,Label.H1Accelerator,Label.Factor,mapComRuleAss);
        else
           cp.Up_to_quota_rate__c = getBaseRate(cp,Label.AnnualAccelerator);
    
        return cp;
                
  }

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getBaseRate----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
  public  decimal getBaseRate(Commission_Plan__c cp,string annualType)
  {
    if(annualRelevantQuota(cp,annualType) == 0)
      return 0;
    else
      return CommissionMethods.getRuleValue(cp.id,annualType,Label.BaseRateOverride,mapComRuleAss) > 0?CommissionMethods.getRuleValue(cp.id,annualType,Label.BaseRateOverride,mapComRuleAss):
                                    CommissionMethods.getRuleValue(cp.id,annualType,Label.OnTargetCommission,mapComRuleAss)/annualRelevantQuota(cp,annualType);
  }

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------annualRelevantQuota----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
   public decimal annualRelevantQuota(Commission_Plan__c cp, string annualType)
  {
     if(CommissionMethods.checkRuleExist(cp.id,annualType,mapComRuleAss))
       return CommissionMethods.getRuleValue(cp.id,annualType,Label.AnnualQuotaOverride,mapComRuleAss) > 0 ? CommissionMethods.getRuleValue(cp.id,annualType,Label.AnnualQuotaOverride,mapComRuleAss) :CommissionMethods.getRuleValue(cp.id,annualType,Label.AnnualQuota,mapComRuleAss) ; 
     else
       return cp.Adjusted_Quota__c;
  }


//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------h1AttainmentPIO----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
  public boolean h1AttainmentPIO(Commission_Plan__c cp, string h1Type, string annualType)
  {
  
    return  CommissionMethods.acceleratorAmount(listCommissionSchedule,cp,h1Type,true,mapComRuleAss) >= h1RelevantQuota(cp,h1Type,annualType)  || 
                                              CommissionMethods.getRuleValue(cp.id,h1Type,Label.H1AttainmentOverride,mapComRuleAss)==1;
  }
  
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------h1Attainment----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
  public boolean h1Attainment(Commission_Plan__c cp, string h1Type, string annualType)
  {
   //  system.debug('This is the amount '+ CommissionMethods.acceleratorAmount(listCommissionSchedule,cp,h1Type,true,mapComRuleAss) );
    return CommissionMethods.acceleratorAmount(listCommissionSchedule,cp,h1Type,true,mapComRuleAss) >= h1RelevantQuota(cp,h1Type,annualType) || 
                                          CommissionMethods.getRuleValue(cp.id,h1Type,Label.H1AttainmentOverride,mapComRuleAss)==1;
  }
  
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------calcUpToRate----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

  public decimal h1RelevantQuota(Commission_Plan__c cp, string h1Type,string annualType)
  {
    
     return CommissionMethods.getRuleValue(cp.id,h1Type,Label.H1QuotaOverride,mapComRuleAss) > 0 ? CommissionMethods.getRuleValue(cp.id,h1Type,Label.H1QuotaOverride,mapComRuleAss) :annualRelevantQuota(cp,annualType)* CommissionMethods.getRuleValue(cp.id,h1Type,Label.Threshold,mapComRuleAss)  ; 
  }
  
 
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------calcBeyondRate----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
  public Commission_Plan__c calcBeyondRate(Commission_Plan__c cp)
  {
      
        cp.Beyond_quota_rate__c = CommissionMethods.getRuleValue(cp.id,Label.AnnualAccelerator,Label.Factor,mapComRuleAss) * ifnull(cp.Up_to_quota_rate__c);
        return cp;      
  }
  
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------calcBeyondRatePIO----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
  public Commission_Plan__c calcBeyondRatePIO(Commission_Plan__c cp)
  {
        cp.Beyond_quota_rate_PIO__c = CommissionMethods.getRuleValue(cp.id,Label.AnnualPIOAccelerator,Label.Factor,mapComRuleAss) * ifnull(cp.Up_to_quota_rate_PIO__c);
        return cp;      
  } 
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------upToQuota----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 public Commission_Plan__c upToQuotaPIO(Commission_Plan__c cp)
 {
   cp.Up_to_quota_amount_PIO__c = getUpToAmountPIO(cp,cp.Sum_PIO__c) ;  
   return cp;
    
 }
 
  
 public Commission_Plan__c upToQuota(Commission_Plan__c cp)
 {
   cp.Up_to_quota_amount__c = getUpToAmount(cp,cp.Sum_quota_amount__c);//+ cp.Recurring_Incentive__c) ;
   cp.Up_to_commission_amount__c = getUpToAmount(cp,cp.Sum_commission_amount__c); //+ cp.Advanceable_Recurring_Incentive__c) ;   
   
   return cp;    
 }
 
 //------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getUpToAmount----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  public decimal getUpToAmountPIO(Commission_Plan__c cp, decimal amount)
  {
    return math.min(ifnull(annualRelevantQuota(cp,Label.AnnualPIOAccelerator)),ifnull(amount) ) * ifnull(cp.Up_to_quota_rate_PIO__c);
  }
 
 
 public decimal getUpToAmount(Commission_Plan__c cp, decimal amount)
  {
    return math.max(0,math.min(annualRelevantQuota(cp,Label.AnnualAccelerator) ,ifnull(amount)+cp.Predecessor_Plan_Sales__c ) - cp.Predecessor_Plan_Sales__c) *cp.Up_to_quota_rate__c;
  }
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------beyondQuotaPIO----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 
 public Commission_Plan__c beyondQuotaPIO(Commission_Plan__c cp)
 {
    cp.Beyond_quota_amount_PIO__c = getBeyondAmountPIO(cp,cp.Sum_PIO__c);
    return cp;
 }
 
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------beyondQuota----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

 public Commission_Plan__c beyondQuota(Commission_Plan__c cp,string s)
 {
     boolean superAcc ;
     decimal  total,superAmount,minTotal,superAddition,relSum;
     
       superAcc = CommissionMethods.checkRuleExist(cp.id,Label.SuperAccelerator,mapComRuleAss);
       //relSum = s == Label.quota? cp.Sum_Quota_amount__c +  cp.Recurring_Incentive__c :  s == Label.commission?cp.Sum_commission_amount__c +cp.Advanceable_Recurring_Incentive__c :0;
       relSum = s == Label.quota? cp.Sum_Quota_amount__c  :  s == Label.commission?cp.Sum_commission_amount__c  :0;
       total = relSum+cp.Predecessor_Plan_Sales__c;
       superAmount = superAcc?CommissionMethods.getRuleValue(cp.id,Label.SuperAccelerator,Label.Threshold,mapComRuleAss):0; //(annualRelevantQuota(cp,Label.AnnualAccelerator) == null? 0 :annualRelevantQuota(cp,Label.AnnualAccelerator)) + CommissionMethods.getRuleValue(cp.id,Label.SuperAccelerator,Label.Threshold,mapComRuleAss);       
       minTotal = superAcc ? math.min(total,superAmount):total;
       superAddition = superAcc?superAccAddition(superAmount,total,cp):0;
       if(s == Label.Quota)
         cp.Beyond_quota_amount__c = getBeyondAmount(cp,total,superAmount) ;
      if(s == Label.Commission)
         cp.Beyond_commission_amount__c = getBeyondAmount(cp,total,superAmount);
         
          
     // system.debug('superAddition ' + superAddition);
    return cp;
 }
 
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getBeyondAmountPIO--------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 
  public decimal getBeyondAmountPIO(Commission_Plan__c cp, decimal minTotal)
  {
    return (math.max(0,minTotal - annualRelevantQuota(cp,Label.AnnualPIOAccelerator)))*cp.Beyond_quota_Rate_PIO__c;
  }
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getBeyondAmount--------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 
  public decimal getBeyondAmount(Commission_Plan__c cp, decimal minTotal, decimal superAmount)
  {
  //if(cp.id == 'a2Y0e000000jRgQEAU')
   
    return (math.max(0,math.min(minTotal,superAmount>0?superAmount:minTotal) - math.max(cp.Predecessor_Plan_Sales__c,annualRelevantQuota(cp,Label.AnnualAccelerator))))*cp.Beyond_quota_Rate__c;
  }
  
 //-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------superAccAddition----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 
 public decimal superAccAddition(decimal superAmount, decimal total,  Commission_Plan__c cp)
  {
    // system.debug('tota; ' + total+ ' base ' + math.max(ifnull(cp.Predecessor_Plan_Sales__c),ifnull(superAmount)) +' '+ superAmount );
  
    //return math.max(0,total-math.max(ifnull(cp.Predecessor_Plan_Sales__c),ifnull(superAmount)))*CommissionMethods.getRuleValue(cp.id,Label.SuperAccelerator,Label.Factor,mapComRuleAss)*ifnull(cp.Up_to_quota_rate__c);
       return math.max(0,total-superAmount);//*CommissionMethods.getRuleValue(cp.id,Label.SuperAccelerator,Label.Factor,mapComRuleAss)*ifnull(cp.Up_to_quota_rate__c);
 
  }
  
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------addFlat----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
    
  public Commission_Plan__c addFlat(Commission_Plan__c cp)
   {
    for(Commission_Schedule__c cs : listCommissionSchedule)
     if(cs.isMulti__c && cp.id == cs.Commission_Plan__c)
       {
        cp.Flat_commission_amount__c += cs.Commission_amount__c;
        cp.Flat_quota_amount__c += cs.Quota_Amount__c;
       }
         
       return cp;   
   }


//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------calcMultiFlat----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  /* public Boolean isMulti(Commission_Plan__c cp, Commission_Schedule__c cs ,List<Commission_Schedule__c> csList)
   {
        return cp.id == cs.commission_plan__c &&
               cs.Commissinable__c &&  
               cs.Commission_Record__r.Type__c == Label.Regular &&
               CommissionMethods.isMulti(cp.id,cs,csList,mapComRuleAss);    

   }*/

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------calcRecurringFlat----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
/* -- Recurring is being caluclated in CommissionScheduleCalc in the quoa and commission level   
 public Commission_Plan__c calcRecurringFlat(Commission_Plan__c cp) //not relevant
   {
     map<string,decimal> result = new  map<string,decimal>();
     result = CommissionMethods.getRecurringIncentiveMap(listCommissionSchedule,cp);
  
     
     cp.Flat_commission_amount__c += result.get(Label.AdvanceRecurringIncentive); 
     cp.Flat_quota_amount__c += result.get(Label.RecurringIncentiveType) ; 
     
     return cp;
   }*/

public Commission_Plan__c getAccAchievements(Commission_Plan__c cp)
{
  cp = annualAchievements(cp);
  cp = annualPIOAchievements(cp);
  cp = h1Achievements(cp);
  cp = h1PIOAchievements(cp);

  return cp;
}

public Commission_Plan__c annualAchievements(Commission_Plan__c cp)
{
  cp.Annual_Accelerator_Attainment__c = annualPerAttainment( cp, Label.AnnualAccelerator);
  return cp;
}

public Commission_Plan__c annualPIOAchievements(Commission_Plan__c cp)
{
  cp.Annual_PIO_Accelerator_Attainment__c =  annualPerAttainment( cp, Label.AnnualPIOAccelerator);
  return cp;
}

public Commission_Plan__c h1Achievements(Commission_Plan__c cp)
{
  cp.H1_Attainment__c =  h1PerAttainment( cp, Label.H1Accelerator,Label.AnnualAccelerator);
  return cp;
}

public Commission_Plan__c h1PIOAchievements(Commission_Plan__c cp)
{
  cp.H1_PIO_Attainment__c =  h1PerAttainment( cp, Label.H1PIOAccelerator, Label.AnnualPIOAccelerator);
  return cp;
}

public decimal annualPerAttainment(Commission_Plan__c cp, string annualType)
{
   // decimal predecessor = annualType == Label.AnnualAccelerator ? ifnull(cp.Predecessor_Plan_Sales__c) :0;
    return 100*(annualRelevantQuota(cp,annualType) == null || annualRelevantQuota(cp,annualType) == 0 ? 0 : 
                CommissionMethods.acceleratorAmount(listCommissionSchedule,cp,annualType,false,mapComRuleAss)/annualRelevantQuota(cp,annualType));
}

public decimal h1PerAttainment(Commission_Plan__c cp, string h1Type, string annualType)
  {
    // system.debug(' Annuaal ' + CommissionMethods.acceleratorAmount(listCommissionSchedule,cp,h1Type,true,mapComRuleAss));
    return  100*(h1RelevantQuota(cp,h1Type,annualType) == null ||  h1RelevantQuota(cp,h1Type,annualType) == 0 ? 0 : 
                                              CommissionMethods.getRuleValue(cp.id,h1Type,Label.H1AttainmentOverride,mapComRuleAss)==1 ? 1 :
                                              (CommissionMethods.acceleratorAmount(listCommissionSchedule,cp,h1Type,true,mapComRuleAss) )/ h1RelevantQuota(cp,h1Type,annualType)) ;
  }

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------updateLegacyFields--------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------- 


public Commission_Plan__c updateLegacyFields(Commission_Plan__c cp)
{
  
  cp.Annual_Quota__c = CommissionMethods.getRuleValue(cp.id,Label.AnnualAccelerator,Label.AnnualQuota,mapComRuleAss) == 0 ? null :CommissionMethods.getRuleValue(cp.id,Label.AnnualAccelerator,Label.AnnualQuota,mapComRuleAss);
  cp.On_target_commission__c = CommissionMethods.getRuleValue(cp.id,Label.AnnualAccelerator,Label.OnTargetCommission,mapComRuleAss) == 0 ? null :CommissionMethods.getRuleValue(cp.id,Label.AnnualAccelerator,Label.OnTargetCommission,mapComRuleAss);
  cp.Base_Rate_Override__c = CommissionMethods.getRuleValue(cp.id,Label.AnnualAccelerator,Label.BaseRateOverride,mapComRuleAss) == 0 ? null :CommissionMethods.getRuleValue(cp.id,Label.AnnualAccelerator,Label.BaseRateOverride,mapComRuleAss) *100;
  cp.H1_Target_Attainment_Override__c = CommissionMethods.getRuleValue(cp.id,Label.H1Accelerator,Label.H1AttainmentOverride,mapComRuleAss) == 0 ? false :true;
  cp.Support_Multi_Year_Factor_Commission__c =!CommissionMethods.checkRuleExist(cp.id,Label.MultiYear, mapComRuleAss ) ? null :CommissionMethods.getRuleValue(cp.id,Label.MultiYear,Label.CommissionFactor,mapComRuleAss)*100;
  cp.Support_Multi_Year_Factor_Quota__c = !CommissionMethods.checkRuleExist(cp.id,Label.MultiYear, mapComRuleAss ) ? null :CommissionMethods.getRuleValue(cp.id,Label.MultiYear,Label.QuotaFactor,mapComRuleAss)*100;
 // system.debug(' The rile ' +CommissionMethods.getRuleValue(cp.id,Label.MultiYear,Label.CommissionFactor,mapComRuleAss) + ' ' + CommissionMethods.getRuleValue(cp.id,Label.MultiYear,Label.QuotaFactor,mapComRuleAss) );
  cp.Override_Quote_rate_calculation__c = CommissionMethods.getRuleValue(cp.id,Label.AnnualAccelerator,Label.AnnualQuotaOverride,mapComRuleAss) == 0 ? null :CommissionMethods.getRuleValue(cp.id,Label.AnnualAccelerator,Label.AnnualQuotaOverride,mapComRuleAss);
  cp.H1_Threshold__c = CommissionMethods.getRuleValue(cp.id,Label.H1Accelerator,Label.Threshold,mapComRuleAss) == 0 ? null :100*CommissionMethods.getRuleValue(cp.id,Label.H1Accelerator,Label.Threshold,mapComRuleAss);
  cp.H1_Factor__c = CommissionMethods.getRuleValue(cp.id,Label.H1Accelerator,Label.Factor,mapComRuleAss) == 0 ? null :CommissionMethods.getRuleValue(cp.id,Label.H1Accelerator,Label.Factor,mapComRuleAss);
  cp.H1_Quota_Override__c = CommissionMethods.getRuleValue(cp.id,Label.H1Accelerator,Label.H1QuotaOverride,mapComRuleAss) == 0 ? null :CommissionMethods.getRuleValue(cp.id,Label.H1QuotaOverride,Label.Factor,mapComRuleAss);
  cp.Annual_Acc_Factor__c = CommissionMethods.getRuleValue(cp.id,Label.AnnualAccelerator,Label.Factor,mapComRuleAss) == 0 ? null :CommissionMethods.getRuleValue(cp.id,Label.AnnualAccelerator,Label.Factor,mapComRuleAss);
  cp.Super_Acc__c = CommissionMethods.checkRuleExist(cp.id,Label.SuperAccelerator, mapComRuleAss );
  cp.Super_Acc_Threshold__c = CommissionMethods.getRuleValue(cp.id,Label.SuperAccelerator,Label.Threshold,mapComRuleAss) == 0 ? null :CommissionMethods.getRuleValue(cp.id,Label.SuperAccelerator,Label.Threshold,mapComRuleAss);
  cp.Super_Acc_Factor__c = CommissionMethods.getRuleValue(cp.id,Label.SuperAccelerator,Label.Factor,mapComRuleAss) == 0 ? null :CommissionMethods.getRuleValue(cp.id,Label.SuperAccelerator,Label.Factor,mapComRuleAss);
  cp.H1_Acc__c = CommissionMethods.checkRuleExist(cp.id,Label.H1Accelerator, mapComRuleAss );
  cp.Max_Collection_Days__c = CommissionMethods.getRuleValue(cp.id,Label.CollectionDaysLimit,Label.DaysLimit,mapComRuleAss)  == 0 ? 0 :CommissionMethods.getRuleValue(cp.id,Label.CollectionDaysLimit,Label.DaysLimit,mapComRuleAss) ;
  return cp;
}

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getListCS--------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------- 
 

  public list<Commission_Schedule__c> getListCS()
  {
    return  [select id, 
                    Name,
                    Commission_Plan__c,
                    Commissinable__c,
                    Credit__c,
                    Type__c,
                    Recurring_incentive__c,
                    Data_conversion__c,
                    Advanceable_Recurring_incetive__c,
                    Advanceable_Amount__c, 
                    Opportunity__c,
                    Is_Advancable__c,
                    H1_Deal__c ,
                    Opportunity_Amount__c ,
                    Opportunity_Close_Date__c,
                    Booking_Date__c, 
                    Booking_Schedule__r.Type_Family__c,
                    Booking_Schedule__r.Amount__c,
                    Booking_Schedule__r.Type__c ,
                    Booking_Schedule__r.Collected_Amount__c,
                    Booking_Schedule__r.Support_Renewal_Amount__c ,
                    Billing_Credit_Amount__c, 
                    Commission_Record__c,
                    Commission_Record__r.Type__c, 
                    Commission_Record_Type__c,              
                    Order_Date__c,
                    Quota_Amount__c,
                    Commission_Amount__c,
                    PIO__c ,
                    IsMulti__c,
                    Opportunity__r.Name,
                    Opportunity__r.SBQQ__PrimaryQuote__r.Recurring_incentive__c,
                    Opportunity__r.No_commission_until_collection__c,
                    Opportunity__r.PO_date__c, 
                    Opportunity__r.Createddate, 
                    Opportunity__r.CloseDate,
                    Opportunity__r.Amount,
                    Opportunity__r.Collection_Risk__c,
                    opportunity__r.Quota_Credit_Override__c,
                    Opportunity__r.daysToClose__c,
                    Opportunity__r.StageName,
                    Opportunity__r.Is_renewal_for_commission_calculation__c,
                    opportunity__r.SBQQ__PrimaryQuote__r.SBQQ__SubscriptionTerm__c,
                    opportunity__r.SBQQ__PrimaryQuote__r.SBQQ__EndDate__c,
                    opportunity__r.SBQQ__PrimaryQuote__r.SBQQ__StartDate__c
              from Commission_Schedule__c
              where Commission_Plan__c in:listComPlanId];
  }
  
  
  
  public list<Commission_Plan__c> getComPlanList()
  {
     return [select id,
                    Predecessor_Plan_Sales__c,
                    Adjusted_Quota__c,
                    Recurring_Incentive__c,
                    H1_Threshold__c,
                    H1_Target_Attainment_Override__c,
                    H1_Acc__c,
                    Up_to_quota_rate__c,
                    Base_Rate__c,
                    H1_Factor__c,
                    Beyond_quota_rate__c,
                    Annual_Acc_Factor__c,
                    Super_Acc_Threshold__c,
                    Super_Acc_Factor__c,
                    Flat_commission_amount__c,
                    Flat_quota_amount__c,
                    Sum_commission_amount__c,
                    Sum_quota_amount__c,
                    Sum_PIO__c,
                    Up_to_quota_rate_PIO__c,
                    Up_to_quota_amount_PIO__c,
                    Beyond_quota_rate_PIO__c,
                    Beyond_quota_amount_PIO__c,
                    Annual_Accelerator_Attainment__c,
                    Annual_PIO_Accelerator_Attainment__c,
                    H1_Attainment__c,
                    H1_PIO_Attainment__c
              from Commission_Plan__c 
              where id in:listComPlanId];
  }

  public static Decimal ifnull(Decimal num ) {
    if (num == null) num = 0;
    return num;
  }  
}