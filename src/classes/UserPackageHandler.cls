public with sharing class UserPackageHandler {
    
    public void InactiveUserPackageRemove(list<User> userList, map<id,User> oldMap)
    {
      list<User> inactiveUsers = new list<User>();
      for(User us : userList)
      {
        if(!us.isActive && oldMap.get(us.id).isActive )
         {
       	   inactiveUsers.add(us);
         } 
      }
      try
      {
        delete [select id,PackageLicenseid,Userid from UserPackageLicense where userId IN :inactiveUsers];
      }
      catch(exception ex)
      {
      	system.debug(ex.getMessage());
      }
    }   
}