public with sharing class QuoteHandler {

  

  //Author: Guy Grinberg       Date:22.8.18      Event: before
  //Task:   CPQ-173 & CPQ-174:WF on Quote when QuotedFor="Reseller" and Distributed is not empty to empty the Distributed
  public static void manageDistResller(List<SBQQ__Quote__c> quotesList)
  {
    
     for(SBQQ__Quote__c q : quotesList)
       {
        if(q.WDSB_Paying_Entity__c == 'Reseller' )//&& clearField)
            q.SBQQ__Distributor__c = null;
     
       if(q.WDSB_Paying_Entity__c == 'End_Customer') //&& clearField)   
       {
        q.SBQQ__Partner__c = null;
        q.SBQQ__Distributor__c = null; //Modifier: David Mizrahi Date 16.01.19 change: Clear distributor as well in case of end customer
       }
            
        

      }
  }
}
  /*
//------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------
  // Run process of creating Budetary PS Estimation for each quote (SF-1018)
  public static void createPSBudetary(list<SBQQ__Quote__c> quotesList)
  {
      List<PS_Estimation__c> newPSBugetary = new List<PS_Estimation__c>();
      Map<id,SBQQ__Quote__c> quotesMap = new Map<id,SBQQ__Quote__c>([Select id,Name, SBQQ__Opportunity2__c , WD_SB_Num_FA_Licenses__c,WD_SB_Num_NA_Licenses__c,WD_SB_Num_of_VC_Licenses__c,WD_SB_Num_CM_Licenses__c, Budgetary_Estimation__c from SBQQ__Quote__c where id in:quotesList]);
      Id budgetryID = [Select id from RecordType where SobjectType = 'PS_Estimation__c' and developerName =  'Budgetary' limit 1].id;
      for(SBQQ__Quote__c q : quotesMap.values())
         newPSBugetary.add(newPS(q,budgetryID));

      Database.SaveResult[] srList = Database.insert(newPSBugetary, false);

      for(PS_Estimation__c ps : newPSBugetary)
        if(quotesMap.containsKey(ps.Quote__c))
        {
           quotesMap.get(ps.Quote__c).Budgetary_Estimation__c = ps.id;
           
         }
      update quotesMap.values();
  }

//------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------
  
  //Create new Budgetary PS Estimation (SF-1018)
  private static PS_Estimation__c newPS(SBQQ__Quote__c q, Id budgetryID)
  {
    return new PS_Estimation__c(Name = q.Name + ' Budgetary',
                                FA_licenses__c = q.WD_SB_Num_FA_Licenses__c,
                                NA_Licenses__c = q.WD_SB_Num_NA_Licenses__c,
                                VC_Licenses__c = q.WD_SB_Num_of_VC_Licenses__c,
                                Is_CM__c =  q.WD_SB_Num_CM_Licenses__c > 0 ? true : false,
                                Opportunity__c = q.SBQQ__Opportunity2__c,
                                Budgetary_Ball_Park_Quote_Request__c = true,
                                Get_Estimation__c = true,
                                Quote__c = q.id,
                                RecordTypeId = budgetryID );
  }

//------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------
  //Run the process of either create or udpdate PS Estimation budgetary (SF-1018)
  public static void GeneratePSEstimation(list<SBQQ__Quote__c> quotesList, Map<id, SBQQ__Quote__c> oldQuote)
  {
     list<Id> PSIdList = new list<Id>();
     list<SBQQ__Quote__c> quotesPSCreate = new list<SBQQ__Quote__c>();

     for(SBQQ__Quote__c qu :quotesList)
     {
        if(isRelevantQuoteToCreate(qu))
           quotesPSCreate.add(qu);
        else if(oldQuote != null &&  isRelevantQuoteToUpdate(qu,oldQuote.get(qu.id)))
            PSIdList.add(qu.Budgetary_Estimation__c);
     }
     
     if(!quotesPSCreate.isEmpty())
        createPSBudetary(quotesPSCreate);

     if(!PSIdList.isEmpty())
        updatePSEstimation(PSIdList);
   
  }

//------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------
//Update an existing PS of the quote (SF-1018)
private static void updatePSEstimation( list<Id> PSIdList) 
{
   list<PS_Estimation__c> PSList = new list<PS_Estimation__c>();
   for(PS_Estimation__c ps : getEstimations(PSIdList) )
         {
           ps.Get_Estimation__c = true;
           ps.FA_licenses__c = ps.Quote__r.WD_SB_Num_FA_Licenses__c;
           ps.NA_Licenses__c = ps.Quote__r.WD_SB_Num_NA_Licenses__c;
           ps.VC_Licenses__c = ps.Quote__r.WD_SB_Num_of_VC_Licenses__c;
           ps.Is_CM__c = ps.Quote__r.WD_SB_Num_CM_Licenses__c > 0 ? true : false;
           ps.Difficulty__c = 'no_poc';
           ps.Server_Type__c = 'virtual';

           PSList.add(ps);
         }
  
     update PSList;
   }
//------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------

//get the esitimations list 
private static list<PS_Estimation__c> getEstimations( list<Id> PSIdList)
{
  return [Select id , 
                 Get_Estimation__c,
                 FA_licenses__c,
                 NA_Licenses__c,
                 VC_Licenses__c,
                 Is_CM__c,
                 Quote__r.WD_SB_Num_FA_Licenses__c,
                 Quote__r.WD_SB_Num_NA_Licenses__c,
                 Quote__r.WD_SB_Num_of_VC_Licenses__c,
                 Quote__r.WD_SB_Num_CM_Licenses__c,
                 Difficulty__c,
                 Server_Type__c
           From PS_Estimation__c 
           Where id in : PSIdList];
}
//------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------

 
//Check if need to update the PS Estimation (SF-1018)
private static Boolean isRelevantQuoteToUpdate(SBQQ__Quote__c qu,SBQQ__Quote__c oldQu )
{ 
   return qu.Budgetary_Estimation__c != null
          || qu.WD_SB_Num_FA_Licenses__c != oldQu.WD_SB_Num_FA_Licenses__c 
          || qu.WD_SB_Num_NA_Licenses__c != oldQu.WD_SB_Num_NA_Licenses__c 
          || qu.WD_SB_Num_of_VC_Licenses__c != oldQu.WD_SB_Num_of_VC_Licenses__c ;
}
//------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------

//Check if need to create the PS Estimation (SF-1018)
private static Boolean isRelevantQuoteToCreate(SBQQ__Quote__c qu )
{ 
   set<string> typeForPSBudgetary = new set<string>{'Quote', 'Amendment'};
   return  typeForPSBudgetary.contains(qu.SBQQ__Type__c)
          && qu.Budgetary_Estimation__c == null  
          && (qu.WD_SB_Num_FA_Licenses__c != null
          || qu.WD_SB_Num_NA_Licenses__c != null
          || qu.WD_SB_Num_of_VC_Licenses__c != null ) ;
}

*/