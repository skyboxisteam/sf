public without sharing class TerritoryHelpHandler
{
	public map<id,Territory_Help__c> MapAccountToTerritory = new map<id,Territory_Help__c>();
    public map<string,string> MapTerritoryPerCountryState = new map<string,string>();
    private final string autoAssociation = 'Territory2AssignmentRule';

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------Constractor for full process-------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//


  public TerritoryHelpHandler (list<Territory_Help__c> territoryList)
   {
   	MapAccountToTerritory = createMap(territoryList);
   	TerritoryFullHandler();
   }

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------Constractor for specific object----------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
 
  public TerritoryHelpHandler (list<sobject> sobjectList, list<string> conditionList)
  {
  	 string getObject = sobjectList[0].getSObjectType().getDescribe().getName();
  /*	 string leadCondition = 'Cause_of_association__c =\'Territory2AssignmentRule\' and Country_and_state_key__c in:'+conditionList;
  	 string OpportunityCondition  ='Account__r.id in:'+ conditionList;
  	 
  	 string condition = getObject == 'Lead' ? leadCondition:
  	                    getObject == 'opportunity' ? OpportunityCondition:'';*/
  	  
  	  MapAccountToTerritory = createMap(getTerritoryList(getObject, conditionList)); 
  	  system.debug('$$$'+MapAccountToTerritory);
  //	  if(!MapAccountToTerritory.isEmpty())
  	      updateTerritory(sobjectList);
  	
  }

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------TerritoryFullHandler---------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  
   private void TerritoryFullHandler()
    {
       list<sobject> sobjectList = new list<sobject>();
    
       list<account> relevantAccounts = [select id,territory__c from account where id IN : MapAccountToTerritory.keyset()];
       sobjectList.addAll((List<sObject>)relevantAccounts);
       system.debug('DDD'+ 	sobjectList);
    	
       list<Opportunity> relevantOppotunities = [select id,AccountId, Territory2Id from opportunity where AccountId IN : MapAccountToTerritory.keyset()  ];    	 
       sobjectList.addAll((List<sObject>)relevantOppotunities);	
      
       list<Lead> relevantLeads = [select id,Country_and_state_key__c,territory__c From Lead Where Country_and_state_key__c IN : MapTerritoryPerCountryState.keyset() ];     
       sobjectList.addAll((List<sObject>)relevantLeads);
        
       updateTerritory(sobjectList);
    	
    }


//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------updateTerritory---------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  
 
private void updateTerritory(list<sobject> sobjectList)
 {
     set<sobject> resultSet = new set<sobject>();
 	 set<Lead> resultSetLead = new set<Lead>();
 	 set<Opportunity> resultSetOpportunity = new set<Opportunity>();
 	 set<Account> resultSetAccount = new set<Account>();
 	 list<sobject> result = new list<sobject>();
 	 list<Lead> resultLead = new list<Lead>();
 	 list<Opportunity> resultOpportunity = new list<Opportunity>();
 	 list<Account> resultAccount = new list<Account>();
 	 string getObject;
 	 sobject newObj; 
 	  
 	 for(sobject s :sobjectList)
 	 {
 	    getObject = s.getSObjectType().getDescribe().getName();
 	       
 	    if(getObject == 'Account')
 	     {
 	       Account acc = (Account)s;
 	    //   newObj = new Account(id = acc.id , territory__c = MapAccountToTerritory.get(acc.id).Territory_Name__c);
 	         Account acc2 = new Account(id = acc.id , territory__c = MapAccountToTerritory.get(acc.id) <>null?MapAccountToTerritory.get(acc.id).Territory_Name__c:Null);	       
 	         resultSetAccount.add(acc2);
 	      //   system.debug('DDDd'+ 	acc2);
 	     }
 	    if( getObject == 'opportunity')
 	       {
 	          Opportunity opp = (Opportunity)s;
 	      //  newObj = new Opportunity(id = opp.id,Territory2Id = MapAccountToTerritory.get(opp.AccountId) <>null?MapAccountToTerritory.get(opp.AccountId).Territory_ID__c:null );
 	         if(MapAccountToTerritory.get(opp.AccountId) == null || opp.Territory2Id <> MapAccountToTerritory.get(opp.AccountId).Territory_ID__c)
 	          {
 	          Opportunity opp2 = new Opportunity(id = opp.id,Territory2Id = MapAccountToTerritory.get(opp.AccountId) <>null?MapAccountToTerritory.get(opp.AccountId).Territory_ID__c:null );
 	          resultSetOpportunity.add(opp2);
 	          }
 	      //    system.debug('$$$' + opp2);
 	        
 	       }                     
 	    if(getObject == 'Lead')
 	      {
 	        lead ld = (Lead)s;
 	      //  newObj = new lead(id = ld.id , territory__c = MapTerritoryPerCountryState.get(ld.Country_and_state_key__c));
 	        system.debug('ld.territory__c' + ld.territory__c + 'MapValue' +  MapTerritoryPerCountryState.get(ld.Country_and_state_key__c));
 	        if(ld.territory__c <> MapTerritoryPerCountryState.get(ld.Country_and_state_key__c))
 	         {
 	          Lead ld2 = new lead(id = ld.id , territory__c = MapTerritoryPerCountryState.get(ld.Country_and_state_key__c));	        
 	          resultSetLead.add(ld2);
 	         }
 	      }             
	     
 	//    if(newObj != null)
 	  //    resultSet.add(newObj);  
 	      
 	   
 	 }
      resultLead.addall(resultSetLead);
      resultOpportunity.addall(resultSetOpportunity);
      resultAccount.addall(resultSetAccount);
     
       try
 	      {
      
      if(!resultLead.isempty())
         database.update( resultLead,false);
      if(!resultOpportunity.isempty())
         database.update(resultOpportunity,false);
      if(!resultAccount.isempty())
         database.update(resultAccount,false);
   	  }
 	   catch(exception ex)
 	      {
 		   system.debug('Exception ' +ex);
 	      }
    

 /*   result.addall(resultSet);
   
 	
 	if(!result.isempty())
 	  {
 	    result.sort(); 	
 	   try
 	      {
           database.update(result,false);
     	  }
 	   catch(exception ex)
 	      {
 		   system.debug('Exception ' +ex);
 	      }
       }*/
 }

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------createMap -------------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

    private map<id,Territory_Help__c> createMap (list<Territory_Help__c> territoryAlterList )
     {
      for(Territory_Help__c th : territoryAlterList)
      {
      if(MapAccountToTerritory.get(th.account__c) ==  null 
         || (MapAccountToTerritory.get(th.account__c) !=null 
         && MapAccountToTerritory.get(th.account__c).Update_Association__c <th.Update_Association__c))
  	       {
  	         MapAccountToTerritory.put(th.account__c, th);
  	         MapTerritoryPerCountryState.put(th.Country_and_state_key__c,th.Territory_Name__c) ;
  	       }
        }
         system.debug('$$$'+MapAccountToTerritory);
       return MapAccountToTerritory;
     }
     
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------getTerritoryList ------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

private list<Territory_Help__c> getTerritoryList(string objectType, list<string> conditionList)
  {
    list<Territory_Help__c> territorylist = new  list<Territory_Help__c>();
    
     string query = 'Select ID,name,Account_territory_Association_id__c,Update_Association__c,Cause_of_association__c , Account__c,Territory_ID__c,Country_and_state_key__c,Territory_Name__c,Country__c, State__c From Territory_Help__c ';
     string leadCondition = 'where Cause_of_association__c =\'Territory2AssignmentRule\' and Country_and_state_key__c in:conditionList';
  	 string OpportunityCondition  ='where Account__r.id in: conditionList and Model_status__c = \'Active\'';
  	 
  	query +=  objectType == 'Lead' ? leadCondition:
  	          objectType == 'opportunity' ? OpportunityCondition:'';
  	         
     System.debug('@@@'+query);
     System.debug('###'+conditionList);
     territorylist.addall((list<Territory_Help__c>)database.query(query));
     System.debug('&&&'+territorylist);
  
    return territorylist;
  }
 
/*private list<Territory_Help__c> getTerritoryList2(string condition)
  {
  	list<Territory_Help__c> territorylist = new  list<Territory_Help__c>();
    string query = 'Select ID,name,Account_territory_Association_id__c,Update_Association__c,Cause_of_association__c , Account__c,Territory_ID__c,Country_and_state_key__c,Territory_Name__c,Country__c, State__c From Territory_Help__c where '+condition ;
   // query+=condition;   
     System.debug('@@@'+query);
     territorylist.addall((list<Territory_Help__c>)database.query(query));
     System.debug('&&&'+territorylist);
  
    return territorylist;
  
  }*/
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
   
}