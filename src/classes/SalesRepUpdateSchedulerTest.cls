@isTest
public class SalesRepUpdateSchedulerTest {  

    public static testmethod void first1(){
        String query = 'select id, name from User where IsActive = true and id = \'00532000004uNDL\' ';

        Test.startTest();
        new SalesRepUpdateScheduler (); // included to increase test coverage
        
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        System.schedule('Sales rep update scheduler test', CRON_EXP, new SalesRepUpdateScheduler (query) );   
        Test.stopTest();
    }  

}