abstract public with sharing class CommonChannelHandler {
    public Account  channel;
    public Set<Account> subChannels = new Set<Account>();
    
    public Set<String> ids = new Set<String>();
    
    public Account getChannel(String id) {
        List<Account> accounts = [SELECT Id, Name FROM Account WHERE Id =: id];
        if (accounts != null) {
            Account channel = accounts[0];
            return channel;
        }
        else
            return null;
    }
    
    public String channelName {
        get { return channel.name; }
        set {}
    }
    
    public String channelId {
        get { return channel.id; }
        set {}
    }
    
    public Set<String> getAllIds() {
        ids.add(channel.id);
        
        for (Integer i = 0 ; i < 3 ; i++) {
            List<Account> accounts = [SELECT Id, Name FROM Account WHERE ParentId in :ids];
            for (Account a : accounts) {
                ids.add(a.Id);
                subChannels.add(a);
            }
        }
        
        return ids;
    }
    
    public List<Account> getSubChannels() {
        List<Account> subs = new List<Account>();
        subs.addAll(subChannels);
        
        return subs;
    }
    
    public String getSubChannelsNames() {
        String s = '';
        
        List<Account> subs = getSubChannels();
        
        for (Integer i = 0 ; i < subs.size(); i++) {
            s += subs[i].name + ' ';
        }
        
        return s;
     
    }
    
}