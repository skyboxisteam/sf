/**
    *@Author Rina Nachbas                   
    *@Date 06/18                       *@Task: Booking schedule - Commission Management/commissionPlan-4
    *@Description : controller for Commission Plan VF
    */  
public with sharing class CommissionPlanController {

	public Commission_Plan__c                   commissionPlan           {get;set;}
    private List<Commission_Schedule__c>        commissionScheduleList   {get;set;}
    private List<Commission_Rule_Assignment__c> commissionRuleAssList    {get;set;}
    private Map<String, Accelerator>            acceleratorMap           {get;set;}
    private Map<Id, Decimal>                    recurringIncentiveMap    {get;set;}
    private Map<Id, Boolean>                    isAdvanceableMap         {get;set;}
    private Map<String, Decimal>                valuesMap                {get;set;}
    public double                               baseRate                 {get;set;}
    public double                               baseRatePIO              {get;set;}
  //public double                               cpOppsAmnt               {get;set;}
    public decimal                              recurringIncentive       {get;set;}
    public decimal                              advanceRecurringIncentive{get;set;}
    public decimal                              annualQuota              {get;set;}
    public decimal                              H1Factor                 {get;set;}
  //public decimal                              annualFactor             {get;set;}
    public decimal                              superFactor              {get;set;}
    public Map<Id, Decimal>                     multiYearFactor          {get;set;}
  //public Map<Id, Boolean>                     isMultiYearMap           {get;set;}
    public decimal                              superAddition            {get;set;}

    

    public String serializedCommissionPlan {get { 
                                        return JSON.serialize(this.commissionPlan);
                                    } private set;}

    public String serializedCommissionScheduleList {get {
                                        return JSON.serialize(this.commissionScheduleList);
                                    } private set;}
    //public String serializedCPOppsAmnt{get {
    //                                    return JSON.serialize(this.cpOppsAmnt);
    //                                } private set;}
    public String serializedCommissionRuleAssList {get {
                                        return JSON.serialize(this.commissionRuleAssList);
                                    } private set;}
    public String serializedAcceleratorMap {get {
                                        return JSON.serialize(this.acceleratorMap);
                                    } private set;}
    public String serializedRecurringIncentiveMap {get {
                                        return JSON.serialize(this.recurringIncentiveMap);
                                    } private set;}
    public String serializedIsAdvanceableMap {get {
                                        return JSON.serialize(this.isAdvanceableMap);
                                    } private set;}
    public String serializedValuesMap {get {
                                        return JSON.serialize(this.valuesMap);
                                    } private set;}
    public String serializedMultiYearFactorMap {get {
                                        return JSON.serialize(this.multiYearFactor);
                                    } private set;}
    //public String serializedIsMultiYearMap {get {
    //                                    return JSON.serialize(this.isMultiYearMap);
    //                                } private set;}
    
    // The extension constructor initializes the private member variable commissionPlan by using the getRecord method from the standard controller.
    public CommissionPlanController(ApexPages.StandardController stdController) {
        this.commissionPlan = (Commission_Plan__c)stdController.getRecord();

        CommissionPlanCalc cpCalc = new CommissionPlanCalc(new List<Id> {this.commissionPlan.Id});
        
        this.commissionPlan = [SELECT Id, Name , Up_to_commission_amount__c, Beyond_commission_amount__c,Flat_commission_amount__c,
                                Up_to_quota_amount_PIO__c, Beyond_quota_amount_PIO__c, Commission_Paid__c,
                                Up_to_quota_rate__c, Beyond_quota_rate__c, 
                                Up_to_quota_rate_PIO__c, Beyond_quota_rate_PIO__c,
                                Up_to_quota_amount__c, Beyond_quota_amount__c,
                                Sum_PIO__c, Adjusted_Quota__c, Sum_commission_amount__c, Sum_quota_amount__c, Recurring_Incentive__c,
                                Predecessor_Plan_Sales__c

                               FROM Commission_Plan__c 
                               Where Id=:commissionPlan.Id limit 1];
        //System.debug('commissionPlan: ' + commissionPlan);

        this.commissionScheduleList = cpCalc.getListCS();
                                    //CM-84 Use one place to retrive cs data
                                    /*[SELECT Id, Commission_Plan__c ,Name, Opportunity__r.Name,
                                    Opportunity_Close_Date__c, Opportunity_Amount__c,
                                    Credit__c, Is_Advancable__c, H1_Deal__c, Type__c,
                                    Booking_Date__c, Quota_Amount__c, Commission_Amount__c,
                                    Booking_Schedule__r.Collected_Amount__c, Commissinable__c,
                                    Opportunity__r.SBQQ__PrimaryQuote__r.Recurring_incentive__c,
                                    Advanceable_Amount__c, Commission_Record_Type__c, Commission_Record__c,
                                    Booking_Schedule__r.Amount__c, Booking_Schedule__r.Type_Family__c, Billing_Credit_Amount__c,
                                    Commission_Schedule__c.Commission_Record__r.Type__c, Opportunity__c,
                                    Opportunity__r.daysToClose__c, Opportunity__r.CreatedDate, 
                                    Opportunity__r.StageName, Opportunity__r.Amount, Opportunity__r.Collection_Risk__c,
                                    Opportunity__r.No_commission_until_collection__c, Opportunity__r.Closedate,
                                    Opportunity__r.Is_renewal_for_commission_calculation__c, Opportunity__r.PO_date__c
                                    FROM Commission_Schedule__c
                                    WHERE Commission_Plan__c =:commissionPlan.Id];*/


        this.commissionRuleAssList = [SELECT Id, Commission_Plan__c ,Name, Rule__c, Type__c, Commission_Rule__r.Attainable__c
                                    FROM Commission_Rule_Assignment__c
                                    WHERE Commission_Plan__c =:commissionPlan.Id
                                    Order by CR_Order__c ];

        //13.   Sum all opportunities that are linked to the commission plan
        //this.cpOppsAmnt =           (Double)[SELECT Sum(Opportunity_Amount__c) sumOppAmnt
        //                                    FROM Commission_Record__c
        //                                    WHERE Commission_Plan__c =:commissionPlan.Id][0].get('sumOppAmnt');

        
        
    }
    public void init(){

        CommissionPlanCalc cpCalc = new CommissionPlanCalc(new List<Id> {this.commissionPlan.Id});
        this.baseRate = cpCalc.getBaseRate(this.commissionPlan, Label.AnnualAccelerator);//'Annual Accelerator'
        this.baseRatePIO = cpCalc.getBaseRate(this.commissionPlan,Label.AnnualPIOAccelerator);//'Annual PIO Accelerator'

        //53.   CommissionMethods.getRecurringIncentiveMap(list commission schedule, CommissionPlan).
        this.recurringIncentive = CommissionMethods.getRecurringIncentiveMap(this.commissionScheduleList,this.commissionPlan).get('RecurringIncentive');
        this.advanceRecurringIncentive = CommissionMethods.getRecurringIncentiveMap(this.commissionScheduleList,this.commissionPlan).get('AdvanceRecurringIncentive');

        //set recurringIncentiveMap per Opportunity && per Commission_Schedule
        getRecurringIncentiveMap();

        //set IsAdvanceableMap per Opportunity
        getIsAdvanceableMap(cpCalc);

        //set isMultiYearMap per CS
        //getIsMultiYearMap(cpCalc);

        //fields for Accelarator Table && PIO Commission Rates section
        getCRAvalues(cpCalc);
    }

    private void getCRAvalues(CommissionPlanCalc cpCalc){
        this.annualQuota = cpCalc.annualRelevantQuota(this.commissionPlan, Label.AnnualAccelerator);//'Annual Accelerator'
        this.H1Factor = CommissionMethods.getRuleValue(this.commissionPlan.Id, Label.H1Accelerator, 'Factor', cpCalc.mapComRuleAss);
        //this.annualFactor = CommissionMethods.getRuleValue(this.commissionPlan.Id, Label.AnnualAccelerator, 'Factor', cpCalc.mapComRuleAss);//Comment by Rinan 04/12/18 Not In Use
        this.superFactor = CommissionMethods.getRuleValue(this.commissionPlan.Id, Label.SuperAccelerator, 'Factor', cpCalc.mapComRuleAss);
        //this.multiYearFactor = CommissionMethods.getRuleValue(this.commissionPlan.Id,Label.MultiYear,'CommissionFactor',cpCalc.mapComRuleAss);//CM-55: changed to factor per CS
        this.superAddition = cpCalc.superAccAddition(CommissionMethods.getRuleValue(this.commissionPlan.id,Label.SuperAccelerator,Label.Threshold,cpCalc.mapComRuleAss),ifnull(this.commissionPlan.Predecessor_Plan_Sales__c) + ifnull(this.commissionPlan.Sum_commission_amount__c),this.commissionPlan);
        
        decimal annualPIOQuota = cpCalc.annualRelevantQuota(this.commissionPlan, Label.AnnualPIOAccelerator);
        decimal targetH1 = cpCalc.h1RelevantQuota(this.commissionPlan, Label.H1Accelerator, Label.AnnualAccelerator);
        decimal currentH1 = CommissionMethods.acceleratorAmount(this.commissionScheduleList, this.commissionPlan,Label.H1Accelerator, true, cpCalc.mapComRuleAss);
        decimal targetH1PIO = cpCalc.h1RelevantQuota(this.commissionPlan, Label.H1PIOAccelerator, Label.AnnualPIOAccelerator);
        decimal currentH1PIO = CommissionMethods.acceleratorAmount(this.commissionScheduleList, this.commissionPlan,Label.H1PIOAccelerator, true, cpCalc.mapComRuleAss);
        decimal currentPIO = CommissionMethods.acceleratorAmount(this.commissionScheduleList,this.commissionPlan, Label.AnnualPIOAccelerator, false, cpCalc.mapComRuleAss);
        decimal currentAnnual = CommissionMethods.acceleratorAmount(this.commissionScheduleList,this.commissionPlan, Label.AnnualAccelerator,false, cpCalc.mapComRuleAss);
  
        this.valuesMap  = new Map<String, Decimal>();
        this.valuesMap.put( Label.H1Accelerator, this.H1Factor);// H1 Factor
        this.valuesMap.put(Label.AnnualPIOAccelerator, annualPIOQuota);//PIO Annual Quota
        this.valuesMap.put(Label.H1PIOAccelerator, CommissionMethods.getRuleValue(this.commissionPlan.Id, Label.H1PIOAccelerator, 'Factor', cpCalc.mapComRuleAss));//PIO H1 Factor
        this.valuesMap.put(Label.AnnualPIOAccelerator+Label.Factor, CommissionMethods.getRuleValue(this.commissionPlan.Id, Label.AnnualPIOAccelerator, 'Factor', cpCalc.mapComRuleAss));//Annual PIO Factor
        this.valuesMap.put(Label.OnTargetCommission, CommissionMethods.getRuleValue(this.commissionPlan.Id, Label.AnnualPIOAccelerator, Label.OnTargetCommission, cpCalc.mapComRuleAss));//Annual PIO Accelerator -  OnTargetCommission

        //Accelerators
        acceleratorMap = new Map<String, Accelerator>();
        acceleratorMap.put(Label.AnnualAccelerator, new Accelerator(annualQuota>0 ? currentAnnual/annualQuota : 0, currentAnnual, annualQuota));//39
        acceleratorMap.put(Label.H1Accelerator, new Accelerator(targetH1>0 ? currentH1/targetH1 : 0, currentH1, targetH1));//41
        acceleratorMap.put(Label.AnnualPIOAccelerator, new Accelerator(annualPIOQuota>0 ? currentPIO/annualPIOQuota : 0, currentPIO, annualPIOQuota));//45
        acceleratorMap.put(Label.H1PIOAccelerator, new Accelerator(targetH1PIO>0 ? currentH1PIO/targetH1PIO : 0, currentH1PIO, targetH1PIO));//47


        //CM-55 calc multiYearFactor per CS
        this.multiYearFactor = new Map<Id, Decimal>();
        for(Commission_Schedule__c cs : this.commissionScheduleList){
            this.multiYearFactor.put(cs.Id, CommissionMethods.getFactor(this.commissionPlan.Id, cs, commissionScheduleList, cpCalc.mapComRuleAss));
        }
    }
    private void getRecurringIncentiveMap(){
        //RecurringIncentive per Opportunity
        this.recurringIncentiveMap = new Map<Id, Decimal>();
        Map<Id, List<Commission_Schedule__c>> opps = new Map<Id, List<Commission_Schedule__c> >();
        for(Commission_Schedule__c cs : this.commissionScheduleList){
            if(opps.get(cs.Opportunity__c) == null ){
                opps.put(cs.Opportunity__c, new List<Commission_Schedule__c>() );
            }
            opps.get(cs.Opportunity__c).add(cs);
            //opps.put(cs.Opportunity__c, opps.get(cs.Opportunity__c));
        }
        for( Id oppId : opps.keySet()){
            List<Commission_Schedule__c> csList = opps.get(oppId);
            this.recurringIncentiveMap.put(oppId, CommissionMethods.getRecurringIncentiveMap(csList,this.commissionPlan).get('RecurringIncentive'));
        }
        //RecurringIncentive per Commission_Schedule
        for( Commission_Schedule__c cs : commissionScheduleList){
            this.recurringIncentiveMap.put(cs.Id, CommissionMethods.getRecurringIncentiveMap(new List<Commission_Schedule__c>{cs},this.commissionPlan).get('RecurringIncentive'));
        }
    }
    //private decimal recurringIncentive(String oppId) {
    //
    //    List<Commission_Schedule__c> csList = new List<Commission_Schedule__c>();
    //    for(Commission_Schedule__c cs : this.commissionScheduleList){
    //        if(cs.Opportunity__c == oppId){
    //            csList.add(cs);
    //        }
    //    }
    //    return CommissionMethods.getRecurringIncentiveMap(csList,this.commissionPlan).get('RecurringIncentive');
    //}

    private void getIsAdvanceableMap(CommissionPlanCalc cpCalc)
    {
        //Commissionable per Opportunity
        this.isAdvanceableMap = new Map<Id, boolean>();
        Map<Id, Opportunity> opps = new Map<Id, Opportunity>();
        for(Commission_Schedule__c cs : this.commissionScheduleList){
            opps.put(cs.Opportunity__c, cs.Opportunity__r );
        }
        for( Id oppId : opps.keySet()){
            this.isAdvanceableMap.put(oppId, this.getIsAdvanceable(this.commissionPlan.Id, opps.get(oppId), cpCalc));
        }
    }
    private boolean getIsAdvanceable(Id planId, Opportunity opp, CommissionPlanCalc cpCalc )
    {
     return (((opp.StageName == 'Closed /Won' || opp.StageName == 'Ready to book') && opp.Amount <= CommissionMethods.getRuleValue(planId,Label.MaxAmountRuleType,Label.MaxAmountRuleAttribute,cpCalc.mapComRuleAss)) &&
        !(opp.Closedate.daysBetween(system.today()) > CommissionMethods.getRuleValue(planId,Label.CollectionDaysLimit,Label.DaysLimit,cpCalc.mapComRuleAss) ||
              opp.Collection_Risk__c || opp.No_commission_until_collection__c));
    } 

    //private void getIsMultiYearMap(CommissionPlanCalc cpCalc){
    //    //CM-84 calc isMultiYear per CS
    //    this.isMultiYearMap = new Map<Id, Boolean>();
    //    for(Commission_Schedule__c cs : this.commissionScheduleList){
    //        this.isMultiYearMap.put(cs.Id, cs.isMulti__c);
    //    }
    //}

    //return 0 if input is null
    public Decimal ifnull(Decimal num ) {
        if (num == null) num = 0;
        return num;
    }

    class Accelerator{
        public Decimal attainment;
        public Decimal current;
        public Decimal target;
        public Accelerator(Decimal attainment, Decimal current, Decimal target){
            this.attainment = attainment;
            this.current = current;
            this.target = target;

        }
    }
}