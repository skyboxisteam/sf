global class SalesRepUpdateBatch implements Database.Batchable<sObject> {
    global final String defaultQuery = 'select id, name from User where IsActive = true and ProfileId = \'00e30000000hLMD\'';
    String query;

    global SalesRepUpdateBatch() {
        query = defaultQuery;
    }
    
    global SalesRepUpdateBatch(String q) {
        query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
    /*
        VR_Batch_Exclusion__c vr = VR_Batch_Exclusion__c.getOrgDefaults();
        vr.is_Batch__c = true;
        update vr;
     */    
        for (sObject s : scope) {
            
            User a = (User)s;
            System.debug('Analyzing sales rep: ' + a.name);
            SalesRepAnalyzer analyzer = new SalesRepAnalyzer(a.id);
            if (analyzer.salesRep == null)
                continue;
            analyzer.updateRecord();
        }
/*
        vr.is_Batch__c = false;
        update vr;
  */
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
    global void runBatch() {
//      UpdateBatch batch = new SalesRepUpdateBatch();
          System.debug('Starting batch: analyzing sales reps');
          Database.executeBatch(this, 10);
    }
}