@isTest
private class Test_SiteLoginControllers {
	
	@isTest 
	static void test_SiteLoginControlller() {
		 // Instantiate a new controller with all parameters in the page
        SiteLoginController controller = new SiteLoginController ();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
        controller.firstname= 'ravid test';
        controller.email = 'email@test.com';
        controller.lastname= 'ravid test';
        controller.Phone = '123123123';
        controller.country = 'TestCountry';
        controller.city = 'TestCity';
        controller.street = 'TestStreet, 4';
        controller.postalCode = 'TestCode';
	    controller.register();
	}
	
	@isTest 
	static void test_SiteLoginControlllerPortal() {
        SiteLoginControllerPortal controller = new SiteLoginControllerPortal();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
        controller.firstname= 'ravid test';
        controller.email = 'email@test.com';
        controller.lastname= 'ravid test';
        controller.Phone = '123123123';
        controller.country = 'TestCountry';
        controller.city = 'TestCity';
        controller.street = 'TestStreet, 4';
        controller.postalCode = 'TestCode';
	    controller.register();
	}

}