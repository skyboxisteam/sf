public with sharing class OpportunityHandler {
  
  
    
  public static void ignoreValidations(list<opportunity> newList, map<id,opportunity> oldMap)
  {
     list<opportunity> result = new list<opportunity>();
     for(Opportunity opp : getOpp(newList))
      {
         //system.debug('checkEvent ' + checkEvent(opp,oldMap.get(opp.id))+ ' Override_validation_Rules__c ' + opp.Override_validation_Rules__c);
        if(!checkEvent(opp,oldMap.get(opp.id)) && opp.Override_validation_Rules__c == true)
            {
              try                
                 {
                  opp.Override_validation_Rules__c= false;
                  result.add(opp);
                 }
             catch(DMLException e) 
                {
             
                }             
           }
       if(checkEvent(opp,oldMap.get(opp.id)))   
         {
           opp.Override_validation_Rules__c= true;
           result.add(opp);
         }
      }
    update result;

  }
    
  
  public static boolean checkEvent(Opportunity newOpp, Opportunity oldOpp)  
  {
   return  newOpp.SBQQ__PrimaryQuote__c != oldOpp.SBQQ__PrimaryQuote__c ||
           newOpp.SBQQ__AmendedContract__c != oldOpp.SBQQ__AmendedContract__c ||
           newOpp.SBQQ__RenewedContract__c!= oldOpp.SBQQ__RenewedContract__c ||
           newOpp.of_MBO_Commission_Record__c!= oldOpp.of_MBO_Commission_Record__c;     
  }
 
 public static list<opportunity> getOpp(list<opportunity> oppList)
 {
   return [Select id,
                  SBQQ__PrimaryQuote__c,
                  SBQQ__AmendedContract__c,
                  SBQQ__RenewedContract__c,
                  WDSB_Appliance_Approved_discount_level__c,
                  WDSB_PS_Approved_discount_level__c,
                  WDSB_Software_Approved_discount_level__c,
                  WDSB_Support_Approved_discount_level__c,
                  of_MBO_Commission_Record__c,
                  WDSB_Training_Approved_discount_level__c,
                  Override_validation_Rules__c ,
                  WDSB_Last_Approved_Quote_Net_Amount__c,
                  Quote_Renewal_Approved__c
           From Opportunity
           WHere id in : oppList];


 }


  //RinaN 10.7.18 CPQ-127
  //CPQ-127:Remove CPQ data(Renewed contract, Amended contract, Contracted, PrimaryQuote) upon clone opportunity
  public static void clearOpportunityFildsOnClone(List<opportunity> newList){
    for (Opportunity o : newList) {
      //system.debug('@@@clearOpportunityFildsOnClone:o ' + o);
        if (o.isClone()) {
          //system.debug('@@@isClone: ' + o.isClone());
            o.SBQQ__Contracted__c = false;
            o.SBQQ__RenewedContract__c = null;
            o.SBQQ__AmendedContract__c = null;
            o.SBQQ__PrimaryQuote__c = null;
        }
    }
  }

  //RinaN 10.7.18 SF-762
  //update Opp by contact values, OpportunityContactRole object is created because 'ConId' param is passes to url for new Opp creation 
  public static void updateOppByOpportunityContactRole(List<opportunity> newList){
    List<OpportunityContactRole> oppContRoleList = [Select id, IsPrimary, OpportunityId , ContactId, 
                                                    Contact.Lead_Source_Detail__c, Contact.Current_Lead_Source_Detail__c, Contact.LeadSource,
                                                    Contact.Deal_Registration_Date__c, Contact.Deal_Registration_Rejection__c, 
                                                    Contact.Deal_Registration_Rejection_Details__c, Contact.Deal_Registration_Rejection_Reson__c, Contact.Deal_Reg_Status__c
                                                    from OpportunityContactRole 
                                                    Where OpportunityId IN :newList];
    //system.debug('@@oppContRoleList: ' + oppContRoleList);

    Map<Id,OpportunityContactRole> oppConRoleMap = new Map<Id,OpportunityContactRole>();
    for(OpportunityContactRole oppConRole : oppContRoleList){
      oppConRoleMap.put(oppConRole.OpportunityId, oppConRole);
    }

    //retrive opp data for update values only if empty, don't update opp if it eas created by lead convertion.
    List<opportunity> oppToUpdate = [select Id, Main_Contact__c, Lead_Source_Detail__c, Current_Lead_Source_Detail__c
                                     from opportunity where Id IN :newList and Lead_Created_Date__c = null]; 
    //system.debug('@@oppToUpdate: ' + oppToUpdate);
    
    /*NEED - because opp can be createn from else then under contact. when we use standard button of new opp - creates automaticlly the contactRole*/
     for(Opportunity opp : oppToUpdate){
      //when created by mobile/independent opp without 'ConId' - need to create OpportunityContactRole
      if(oppConRoleMap.get(opp.Id) == null && opp.Main_Contact__c != null){
        OpportunityContactRole newOppConRol = new OpportunityContactRole(OpportunityId=opp.Id, ContactId=opp.Main_Contact__c, IsPrimary=true);
        insert newOppConRol;
        oppConRoleMap.put(newOppConRol.OpportunityId, newOppConRol);
        //system.debug('@@add newOppConRol: ' + newOppConRol);
      }
    }
      //system.debug('@@oppConRoleMap: ' + oppConRoleMap);

    //retrive Contact data of the new created 'OpportunityContactRole'
    List<OpportunityContactRole> oppContRoleList2 = [Select id, IsPrimary, OpportunityId , ContactId, 
                                                    Contact.Lead_Source_Detail__c, Contact.Current_Lead_Source_Detail__c, Contact.LeadSource,
                                                    Contact.Deal_Registration_Date__c, Contact.Deal_Registration_Rejection__c, 
                                                    Contact.Deal_Registration_Rejection_Details__c, Contact.Deal_Registration_Rejection_Reson__c, Contact.Deal_Reg_Status__c
                                                    from OpportunityContactRole 
                                                    Where OpportunityId IN :oppConRoleMap.keyset()];
    
    Map<Id,OpportunityContactRole> oppConRoleMap2 = new Map<Id,OpportunityContactRole>();
    for(OpportunityContactRole oppConRole : oppContRoleList2){
      oppConRoleMap2.put(oppConRole.OpportunityId, oppConRole);
    }
    system.debug('@@oppConRoleMap2: ' + oppConRoleMap2);
    
    for(Opportunity opp : oppToUpdate){
      if(oppConRoleMap2.get(opp.Id) != null && oppConRoleMap2.get(opp.Id).IsPrimary == true){
          updateFieldIfEmpty(opp, 'Main_Contact__c', oppConRoleMap2.get(opp.Id).ContactId);//opp.Main_Contact__c = oppConRoleMap2.get(opp.Id).ContactId);//when created by desktop and 'ConId' parameter created the OpportunityContactRole
          opp.LeadSource = oppConRoleMap2.get(opp.Id).Contact.LeadSource; //updateFieldIfEmpty(opp, 'LeadSource', oppConRoleMap2.get(opp.Id).Contact.LeadSource);
          updateFieldIfEmpty(opp, 'Lead_Source_Detail__c', oppConRoleMap2.get(opp.Id).Contact.Lead_Source_Detail__c);
          updateFieldIfEmpty(opp, 'Current_Lead_Source_Detail__c', oppConRoleMap2.get(opp.Id).Contact.Current_Lead_Source_Detail__c);
          
      }
        
    }
     //to show only validation error message, when custom validation is comming up
    try{
      if(oppToUpdate.size() > 0)
        update oppToUpdate;

    } catch(System.DmlException e) {
        for (SObject opp : trigger.new) {
          opp.addError(e.getDmlMessage(0));
      }
    }

  }

  public static void updateFieldIfEmpty(sObject obj, String fieldName, String value){
    //system.debug('@@updateFieldIfEmpty: ' + fieldName + ': ' + (string)obj.get(fieldName));
      obj.put(fieldName, String.isNotEmpty((string)obj.get(fieldName)) ?  (String)obj.get(fieldName) : value);
  }


  /**
    *@Author Rina Nachbas                           events: befor update
    *@Date 1.11.18
    *@Task: CM-64
    *@Description : If a user tries to change the probability (from higher than 95 to lower than 95) 
    *               and the opportunity has commission record which its type is different than PIO the user will get an error
    */ 
  
   public static void probabilityValidation(list<opportunity> newList, map<id,opportunity> oldMap)
  {
    List<Opportunity> oppList = new List<Opportunity>();
    Set<Id> oppWithCR         = new Set<Id> ();

     for(Opportunity opp : newList)
     {
        if(opp.Probability < 95 && oldMap.get(opp.id).Probability >= 95)
        {
          oppList.add(opp);
        }
      }

     List<Commission_Record__c> crList = [select Id, Opportunity__c from Commission_Record__c where Type__c != 'PIO' AND Opportunity__c in :oppList];
     for(Commission_Record__c cr : crList)
        oppWithCR.add(cr.Opportunity__c);
     for(Opportunity opp : oppList)
     {
        if(oppWithCR.contains(opp.Id))
          opp.addError(Label.OpportunityProbabilityError);
     }
  }

  /**
    *@Author Rina Nachbas                           events: befor update, after insert
    *@Date 26.12.18
    *@Task: SF-980
    *@Description : there must be a 'Pilot Scope' related to opp in order for the opp to be moved to any status expect 'Projected' or 'No Pilot Needed'.
    */ 
  
   public static void pilotValidation(list<opportunity> newList, map<id,opportunity> oldMap)
  {
    List<Opportunity> oppList     = new List<Opportunity>();
    Set<Id> oppWithPS             = new Set<Id> ();
    Set<String> validScopeStatus  = new Set<String> {'', 'No pilot needed', 'Projected'};
    List<Pilot_Scope__c> psList   = new List<Pilot_Scope__c>();
    Boolean isInsert = oldMap == null ? true : false;

    //system.debug('@@@pilotValidation: isInsert: ' + isInsert);
     for(Opportunity opp : newList)
     {
        //system.debug('@@@pilotValidation: opp.Pilot_Status__c: ' + opp.Pilot_Status__c);
        //The pilot status changed and is of type that need validation
        if(String.isNotBlank(opp.Pilot_Status__c) && !validScopeStatus.contains(opp.Pilot_Status__c) && ( isInsert || opp.Pilot_Status__c != oldMap.get(opp.id).Pilot_Status__c ))
        {
          oppList.add(opp);
        }
     }

     //Retrive Pilot_Scope__c related to the Opp
     if(oppList.size() > 0){
         psList = [select Id, Opportunity__c from Pilot_Scope__c where Opportunity__c in :oppList];
     }
     for(Pilot_Scope__c ps : psList)
        oppWithPS.add(ps.Opportunity__c);
     for(Opportunity opp : oppList)
     {
        //if the opp has no related Pilot_Scope__c raise error 
        if(!oppWithPS.contains(opp.Id))
          opp.addError(Label.OpportunityMissingPilotError);
     }
  }

//Test in LeadHandlerTest
  public static void fieldUpdates(List<Opportunity> newOppList, Map<Id, Opportunity> oldOppList)
  {
    for( Opportunity Opp : newOppList)
    {
      //MP-63
      updateNextExpirationEmail(Opp, oldOppList);
    }
  
  }
  //Author: Rina Nachbas      Date:  07.11.18
  //Task:   MP-63             Event: befor update
  //Desc:   If the new date is bigger by 7 days from previous value change "Next Expiration Email" to "Next: 7 Days Expiration"
  //    If the new date is smaller than 7 days from previous value change "Next Expiration Email" to "Next: Expiration"
  private static void updateNextExpirationEmail(Opportunity opp, Map<Id, Opportunity> oldOppList)
  {
      if(Opp.MagentrixOne__ExclusivityExpirationDate_mgtrx__c >= System.today() && Opp.MagentrixOne__ExclusivityExpirationDate_mgtrx__c != oldOppList.get(Opp.Id).MagentrixOne__ExclusivityExpirationDate_mgtrx__c)
      {
        if(Opp.MagentrixOne__ExclusivityExpirationDate_mgtrx__c >= System.today().addDays(7) && Opp.Next_Expiration_Email__c != 'Next: 7 Days Expiration'){
          Opp.Next_Expiration_Email__c = 'Next: 7 Days Expiration';
        }
        else if(Opp.MagentrixOne__ExclusivityExpirationDate_mgtrx__c < System.today().addDays(7) && Opp.Next_Expiration_Email__c != 'Next: Expiration'){
          Opp.Next_Expiration_Email__c = 'Next: Expiration';
        }
      }
  }

/**
    *@Author Rina Nachbas                           events: After Update, After Inset
    *@Date 12.12.18
    *@Task: SF-1021
    *@Description : Opportunity cannot be moved to ready to book stage without signed SOW attached
    */
  /* public static void validateSOWExistsOnReadyToBook(List<Opportunity> newList, Map<Id, Opportunity> oldMap)
  {
    List<Opportunity> oppList = new List<Opportunity>();
    Set<Id> oppWithoutSOW     = new Set<Id> ();
    String readyToBook = 'Ready to book';
    String closeWon = 'Closed /Won';
    Set<String> sowRequiredStage = new Set<String>{readyToBook, closeWon};

     for(Opportunity opp : newList)
     {
        //is new readyToBook opp, or changed to ReadyToBook
        if(sowRequiredStage.contains(opp.StageName) && (oldMap == null || oldMap.get(opp.id).StageName != opp.StageName))
        {
          oppList.add(opp);
        }
      }

      //Check if the opp has SOW object
     List<SOW__c> sowList = [select Id, Opportunity__c from SOW__c where Opportunity__c in :oppList];
     for(SOW__c sow : sowList)
        oppWithoutSOW.add(sow.Opportunity__c);
     for(Opportunity opp : oppList)
     {
        //if opp has no SOW object we will rase error
        if(!oppWithoutSOW.contains(opp.Id))
          opp.addError(Label.OpportunitySowMissingError);
     }
  }*/
}