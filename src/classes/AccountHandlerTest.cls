@isTest
private class AccountHandlerTest {

    @testSetup static void setupData() {
       
       ClsObjectCreator cls = new ClsObjectCreator();

       //create Account
        Account acc =  cls.createAccount('PartnerAccount');
        acc.Type = 'Partner';
        acc.Partner_Status__c  = 'Active';
        update acc;
        
        //create Contact
        Contact con = ClsObjectCreator.createContact('Test', acc.Id);
        con.MagentrixOne__CommunityUser_mgtrx__c = true;
        con.FirstName = 'madatory for Magentrix';
        con.Email = 'madatory@forMagentrix.com';
        update con;
   }
    
    @isTest static void deactivatePartnerUsersTest() {
        
        //SetUp method

        //check that con.MagentrixOne__CommunityUser_mgtrx__c before test is false
        Account  acc = [select Type, Partner_Status__c from Account];
        System.assertEquals('Partner', acc.Type);
        System.assertEquals('Active', acc.Partner_Status__c);
       
       //check that contact.MagentrixOne__CommunityUser_mgtrx__c is true
      Contact  con = [select MagentrixOne__CommunityUser_mgtrx__c from Contact];
      System.assertEquals(true, con.MagentrixOne__CommunityUser_mgtrx__c);


       User u = [select id from user  where profile.Name='System Administrator' and isActive=true limit 1];
      
      Test.startTest();
        System.runAs(u) {

           acc.Partner_Status__c = 'Inactive';
           update acc;
       }
      Test.stopTest();
      
      //check contacts (acccount's related Magentrix users) was deactivated
      Contact  conTest = [select MagentrixOne__CommunityUser_mgtrx__c from Contact];
      System.assertEquals(false, conTest.MagentrixOne__CommunityUser_mgtrx__c);
    }
    
    @isTest static void test_method_two() {
        // Implement test code
    }
    
}