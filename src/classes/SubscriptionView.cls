public with sharing class SubscriptionView 
{
/*public List<wrapSub> wrapSubList = new List<wrapSub> ();
public List<wrapSubAsset> wrapSubListAsset = new List<wrapSubAsset> ();
public list<id> idList = new list<id> ();
    */
    
public List<wrapSub> wrapSubList  = new  List<wrapSub>();
public List<wrapSubAsset> wrapSubListAsset  {get;set;}
public map<string,WrapTotal> totalMap;
public map<string,WrapTotal> totalShowExpMap;
public list<id> idList {get;set;}
public id relevantId ;
public Id accountId ;
public string accountName ;
public boolean showExp;
public boolean clicked {get;set;}
public boolean selected {get;set;}
public boolean allowShowExp {get;set;}
public boolean allowSelectSubscription {get;set;}


public String serializedWrapSubList {get {
                                        return JSON.serialize(this.wrapSubList);
                                    } private set;}
public String serializedWrapSubListAsset {get {
                                        return JSON.serialize(this.wrapSubListAsset);
                                    } private set;}
public String serializedTotalMap {get {
                                        return JSON.serialize(this.totalMap);
                                    } private set;}

public String serializedTotalShowExpMap {get {
                                        return JSON.serialize(this.totalShowExpMap);
                                    } private set;}
public Boolean init{
    get{
        init();
        return false;
    }
}

public void init(){
  //put all your initlization logic here.
  //your assigned properties will be set when this executes
  system.debug('### SubscriptionView: theAccountId:' + accountId);
  getwrapSubList();
  processSelected();
  totalMap = getTotalMap(date.today());//without expired subscription
  totalShowExpMap = getTotalMap(system.today().addYears(-1000)); //to get also expired subscription
}

public void setrelevantId (Id argId)
{
   relevantId = argId;    
} 


public Id getrelevantId ()
{
   return relevantId;   
} 

public void setaccountId (id theAccountId)
{
   accountId = theAccountId;    
} 


public Id getaccountId ()
{
   return accountId;   
} 

public void setaccountName (string theAccountName)
{
   accountName = theAccountName;    
} 


public string getaccountName ()
{
   return accountName;   
} 

public PageReference setShowExpire()
{
 showExp = showExp == null || showExp == false? true:false;
 //getSubscrition(showExpiered );
 return null;
}




public List<wrapSub> getwrapSubList()
{
    wrapSubList.clear();
    showExp = showExp == null ? false: showExp  ;
    system.debug('Relevant' + relevantId);
    for(SBQQ__Subscription__c sc: [select id,name,WDSB_ProductCode__c,SBQQ__ProductName__c,SBQQ__ContractNumber__c,SBQQ__Contract__r.PO__c,SBQQ__Quantity__c ,SBQQ__StartDate__c, SBQQ__EndDate__c, WDSB_Original_Date__c, SB_Appliance_EOL_Date__c from SBQQ__Subscription__c where SBQQ__Contract__c = :relevantId or SBQQ__Account__c = : relevantId ])
      //if(showExp|| (!showExp && system.today() < sc.SBQQ__EndDate__c) || !allowShowExp ) //comment by RinaN 21.8.18 filter will be in angular code
           wrapSubList.add(new wrapSub(sc.id, sc.name,sc.WDSB_ProductCode__c, sc.SBQQ__ProductName__c, sc.SBQQ__ContractNumber__c,sc.SBQQ__Contract__r.PO__c,sc.SBQQ__Quantity__c, sc.SBQQ__StartDate__c, sc.SBQQ__EndDate__c, sc.WDSB_Original_Date__c, sc.SB_Appliance_EOL_Date__c, selected) );    
     //if(!allowShowExp)//comment by RinaN 21.8.18 filter will be in angular code
       processSelected();
           
     return wrapSubList;
} 

 public  void processSelected() 
  {
    idList  = new list<Id>();
    wrapSubListAsset = new list<wrapSubAsset>();
    for(wrapSub wrapObj : wrapSubList) 
        //if(wrapObj.selected == true || !allowShowExp) //comment by RinaN 21.8.18 filter will be in angular code       
           idList.add(wrapObj.subId); 
                         
   for(SBQQ__SubscribedAsset__c sca: [select id,name,SBQQ__Active__c,SBQQ__Asset__r.name ,SBQQ__Asset__r.Product2.WDSB_Sub_Family__c, SBQQ__Asset__r.Quantity_Attribute__c ,SBQQ__Asset__r.SerialNumber, SBQQ__Asset__r.Status, SBQQ__Asset__r.Asset__c, WDSB_Asset_Quantity__c,SBQQ__Subscription__r.Name, createddate, SBQQ__UsageEndDate__c from SBQQ__SubscribedAsset__c where SBQQ__Subscription__c in :idList  ])
         wrapSubListAsset.add(new wrapSubAsset(sca.name, sca.SBQQ__Active__c, sca.SBQQ__Asset__r.name, sca.SBQQ__Asset__r.Asset__c, sca.SBQQ__Asset__r.SerialNumber, sca.WDSB_Asset_Quantity__c,sca.SBQQ__Subscription__r.Name, sca.createddate, sca.SBQQ__UsageEndDate__c, sca.SBQQ__Asset__r.Product2.WDSB_Sub_Family__c, sca.SBQQ__Asset__r.Quantity_Attribute__c, sca.SBQQ__Asset__r.Status ) );
   
     clicked = true;
  //  return wrapSubListAsset;
    
  }
  
  //return Map between  products subFamily and the total counts of each subFamily divide by subscription/asset of the account/contract
  //param: dateParam the date to compare if subscription is not expired
  public map<string,WrapTotal> getTotalMap(Date dateParam)
  {
    system.debug('@@@getTotalMap: dateParam: ' + dateParam);
    List<String> familyTypes = new List<String> {'Software License', 'Appliances'};
    map<string,WrapTotal> totalMap = new map<string,WrapTotal> ();
    map<Id, SBQQ__SubscribedAsset__c> distinctAssetsFormSubscribedAsset = new map<Id, SBQQ__SubscribedAsset__c>();
    //get perpetual assets of account
    //for(Asset ass: [select Product2.WDSB_Sub_Family__c, Quantity_Attribute__c from Asset where accountId = :relevantId and Product2id != null and Product2.Family in :familyTypes  ] ) 
    //    addTotalMap(totalMap, ass.Product2.WDSB_Sub_Family__c,ass.Quantity_Attribute__c, 'perpetual'); 

    //get Subscriptions
    for(SBQQ__Subscription__c  sc : [select Subscription_Quantity_Formula__c, SBQQ__Product__r.WDSB_Sub_Family__c 
                                      from SBQQ__Subscription__c 
                                      where  (SBQQ__Account__r.id = :relevantId or SBQQ__Contract__c = : relevantId  )and SBQQ__Product__c !=null and SBQQ__Product__r.Family in :familyTypes and SBQQ__EndDate__c > Today]) // and SBQQ__EndDate__c > :dateParam
        
        addTotalMap(totalMap, sc.SBQQ__Product__r.WDSB_Sub_Family__c, sc.Subscription_Quantity_Formula__c, 'subscription');
    
    //get perpetual assets of contract or account - distinct the asset from SubscribedAsset, because multiple subAssets get point on same asset
    for( SBQQ__SubscribedAsset__c sca : [select SBQQ__Asset__r.Quantity_Attribute__c , SBQQ__Asset__r.Product2.WDSB_Sub_Family__c, SBQQ__Asset__c 
                                        from SBQQ__SubscribedAsset__c  
                                        where SBQQ__Asset__r.Product2id !=null and SBQQ__Asset__r.Product2.Family in :familyTypes and SBQQ__Subscription__r.SBQQ__EndDate__c > :dateParam and
                                              (SBQQ__UsageEndDate__c >= Today or SBQQ__UsageEndDate__c = null) and (SBQQ__Subscription__r.SBQQ__Contract__c = : relevantId or SBQQ__Subscription__r.SBQQ__Account__c = :relevantId) ])
    {
         
            distinctAssetsFormSubscribedAsset.put(sca.SBQQ__Asset__c, sca);
    }
    for( SBQQ__SubscribedAsset__c sca : distinctAssetsFormSubscribedAsset.Values())
         addTotalMap(totalMap, sca.SBQQ__Asset__r.Product2.WDSB_Sub_Family__c, sca.SBQQ__Asset__r.Quantity_Attribute__c, 'perpetual'); 
         
    return totalMap;
  }
    
  
 private void addTotalMap(map<string,WrapTotal> totalMap, string family, decimal amount, string amountType)
 {
  if(amountType == 'perpetual')
    totalMap.put(family,totalMap.containsKey(family)? totalMap.get(family).addAmount(amount, 'perpetual') : new WrapTotal(amount, amount, 0) );
  else if(amountType == 'subscription')
    totalMap.put(family,totalMap.containsKey(family)? totalMap.get(family).addAmount(amount, 'subscription') : new WrapTotal(amount, 0, amount) );
 }
 public class WrapTotal {
        
        public decimal totalAmount {get; set;}
        public decimal perpetualAmount {get; set;}
        public decimal subscriptionAmount {get; set;}  
 
      public WrapTotal (decimal totalAmountParam,decimal perpetualAmountParam , decimal subscriptionAmountParam )
      {
        totalAmount = totalAmountParam;
        perpetualAmount = perpetualAmountParam ;
        subscriptionAmount = subscriptionAmountParam; 
      }

      public WrapTotal addAmount(decimal amount, string amountType){
        totalAmount += amount;
        
        if(amountType == 'perpetual')
          perpetualAmount += amount;
        else if(amountType == 'subscription')
          subscriptionAmount += amount;

          return this;
      }
     
 }

 public class wrapSub {
        
        public id       subId {get; set;}
        public string   name {get; set;}
        public string   productCode {get; set;}
        public string   productName {get; set;}
        public string   contractNumber {get; set;}
        public string   contractPO {get; set;}
        public decimal  quantity {get; set;}
        public date     startDate {get; set;}
        public date     endDate {get; set;}
        public date     originalDate {get; set;}
        public date     eolDate {get; set;}
        public boolean  selected {get; set;}     
 
      public wrapSub (id subIdParam,string nameParam , string productCoedParam , string productNameParam, string  contractNumberParam, string contractPOParamp, decimal quantityParam  ,date startDateParam, date EndDateParam, date originalDateParam, date eolDateParam, boolean selectParam  )
          {
            subId = subIdParam;
            name = nameParam ;
            productCode = productCoedParam;
            productName = productNameParam;
            contractNumber = contractNumberParam;
            contractPO = contractPOParamp;
            quantity = quantityParam; 
            startDate = startDateParam;
            EndDate = EndDateParam;
            originalDate = originalDateParam;
            eolDate = eolDateParam;
            selected = selectParam;
          }
     
        }
  public class wrapSubAsset {
        
        public string name {get; set;}
        public boolean Active {get; set;}
        public string SerialNumber {get; set;}
        public string assetName {get; set;}
        public string assetNumber {get; set;}
        public decimal assetQuantity {get; set;}
        public string assetStatus {get; set;}
        public string subNumber {get; set;}
        public datetime createddate {get; set;}
        public date usageEndDate {get; set;}
        public string subFamily {get; set;}
        public decimal userQuantity {get; set;}
          
      public wrapSubAsset (string nameParam, boolean activeParam, string assetNameParam, string assetNumberParam, string serialNumberParam, decimal assetQuantityParam,string subNumberParam ,datetime createddateParam ,date usageEndDateParam, string subFamilyParam, decimal userQuantityParam, string assetStatusParam )
         {
            name = nameParam ;
            Active = activeParam;
            assetName = assetNameParam;
            assetNumber = assetNumberParam;
            SerialNumber = serialNumberParam;
            assetQuantity = assetQuantityParam;
            assetStatus = assetStatusParam;
            subNumber = subNumberParam; 
            createddate = createddateParam;
            usageEndDate = usageEndDateParam;
            subFamily = subFamilyParam;
            userQuantity = userQuantityParam;
            
         }
        }
}