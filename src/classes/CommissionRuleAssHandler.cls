public without sharing class CommissionRuleAssHandler {
  
  
private map<id,Commission_Rule__c> mapRule = new map<id,Commission_Rule__c>();
private list<Commission_Rule_Assignment__c> listRuleAss = new list<Commission_Rule_Assignment__c>();
private list<id> listComPlan = new list<id>();
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------ruleAssCreated---------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 public CommissionRuleAssHandler()
 {
 	
 }
 
 public CommissionRuleAssHandler(list<Commission_Rule_Assignment__c> listCRA)
 {
    list<id> listRuleId = new list<id>();
    
    listRuleAss = listCRA;
    for(Commission_Rule_Assignment__c cra : listRuleAss  )
       {
        if(!cra.Data_Conversion__c)
         {
          listRuleId.add(cra.Commission_Rule__c);
          listComPlan.add(cra.Commission_Plan__c);
        }
       }
     for(Commission_Rule__c cr : getRule(listRuleId))
        mapRule.put(cr.id,cr);
     
     
     system.debug(mapRule);
 }
 
 public void updateBeforeInsert()
 {  
    
    for(Commission_Rule_Assignment__c cra : listRuleAss)
    {
        if( !cra.Data_Conversion__c)
        {
         cra.Rule__c = mapRule.get(cra.Commission_Rule__c).Rule__c;
         cra.Type__c = mapRule.get(cra.Commission_Rule__c).Type__c;     
        }
    }
   
        
 } 
  
 public void calculateComPlan()
 {
    map<id,id> mapComPlanLevel = new map<id,id>();
    map<id,id> mapComScheduleLevel = new map<id,id>();
   
    for(Commission_Rule_Assignment__c cra : listRuleAss)
     {
       if(cra.Data_Conversion__c)
         continue;
       if(mapRule.get(cra.Commission_Rule__c).Rule_Level__c == Label.Commission_Schedule)
        {
          mapComScheduleLevel.put(cra.id,cra.Commission_Plan__c);
          mapComPlanLevel.remove(cra.id);
        }
       if(mapRule.get(cra.Commission_Rule__c).Rule_Level__c == Label.Commission_Plan)
          mapComPlanLevel.put(cra.id,cra.Commission_Plan__c);      
     }
      system.debug('mapComPlanLevel ' + mapComPlanLevel + 'mapComScheduleLevel ' +mapComScheduleLevel);
    if(!mapComScheduleLevel.isEmpty()) 
     {
      BookingScheduleCommission bsc = new BookingScheduleCommission();
      bsc.createFromCommissionPlan(mapComScheduleLevel.values());
     } 
    
    if(!mapComPlanLevel.isEmpty() ) 
      {
        CommissionPlanCalc cpc = new CommissionPlanCalc(mapComPlanLevel.values());
        cpc.calcAllComponents();
      }
      
 }
 
 public void reCalculateComPlan()
{
	CommissionPlanCalc cpc = new CommissionPlanCalc(listComPlan);
    cpc.calcAllComponents();
}
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getRule----------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 
 private list<Commission_Rule_Assignment__c> getExpendRuleAssList(list<Commission_Rule_Assignment__c> listType)
 {
    return [SELECT id,
                   type__c,
                   Rule__c,
                   Commission_Plan__c,
                   Commission_Rule__r.type__c,
                   Commission_Rule__r.Rule__c,
                   Commission_Rule__r.Rule_Level__c,
                   Data_Conversion__c
             FROM Commission_Rule_Assignment__c
             WHERE id in:listType];
 }
 
  private list<Commission_Rule__c> getRule(list<id> listId)
 {
    return [SELECT id,
                   type__c,
                   Rule__c,
                   Rule_Level__c
             FROM Commission_Rule__c
             WHERE id in:listId];
 }
  
}