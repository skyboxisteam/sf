/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class UserPackageHandlerTest {

    static testMethod void myUnitTest() {
        list<id> userIdList = new list<id>();
        list<UserPackageLicense> packageUserList = [select id,PackageLicenseid,Userid from UserPackageLicense];     
        for(UserPackageLicense upl : packageUserList)
        {
        	userIdList.add(upl.UserId);
        }
        
        List<User> us = [select id from User where id =: userIdList AND isActive = true]; // limit 1
        //27/01/2019 Addes try catch by Rina Nachbas because the select catched user that is being in use and cannot be deactivated.
       for(Integer i = 0; i< us.size(); i++)
       {
               try
               {
                   us[i].Isactive = false;
                   update us[i];
                   break;
               }
               catch(exception ex)
               {
                 system.debug(ex.getMessage());
               }  
        }
        
        
        
        
    }
}