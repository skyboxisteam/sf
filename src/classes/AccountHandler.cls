public with sharing class AccountHandler {
    public AccountHandler() {
        
    }

    
    //Author: Rina Nachbas      Date:  14.10.18
    //Task:   MP-10             Event: after Update
    //Desc:   Deactivate Partner Users
    public static void deactivatePartnerUsers(List<Account> newAccountList, map<id,Account> mapOldAccount){
        List<Contact> conToUpdate = new List<Contact>();
        Set<Id> accountSet = new Set<Id>();
        system.debug('@@@deactivatePartnerUsers: newAccountList: ' + newAccountList);
        system.debug('@@@deactivatePartnerUsers: mapOldAccount: ' + mapOldAccount);
        for(Account acc : newAccountList)
        {
            if(acc.Type == 'Partner' && acc.Partner_Status__c  != 'Active' && ((Account)mapOldAccount.get(acc.Id)).Partner_Status__c  == 'Active'){
                accountSet.add(acc.Id);
            }
        }
        system.debug('@@@deactivatePartnerUsers: accountSet: ' + accountSet);

        //get Account related Contacts
        List<Contact> accContacts = [select Id, AccountId, MagentrixOne__CommunityUser_mgtrx__c 
        							FROM Contact 
                                    Where AccountId in :accountSet ];
        //Deactivate the contacts related to the Partner Account which have been deactivated
        for(Contact con : accContacts)
        {
            conToUpdate.add(new Contact(Id=con.Id, MagentrixOne__CommunityUser_mgtrx__c = false));
        }

        if(conToUpdate.size() > 0){
            update conToUpdate;
        }

    }

}