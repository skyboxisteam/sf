global  with sharing class TerritoryLegacyUpdateBatch implements Database.Batchable<sobject> , Database.Stateful,Schedulable
{
  global void execute(SchedulableContext sc)
    { 
       Database.executeBatch(this,50); 
    }
 
  global Database.Querylocator start (Database.BatchableContext BC)
    {
       return Database.getQueryLocator([Select Id,Name,Territory__c From Account]);
    }
 
  global void execute (Database.BatchableContext BC, List<sobject> scope)
    {
      try
       {
//-------------------------------------------------------------------------------------//
//-------------------------Define parameters-------------------------------------------//
//-------------------------------------------------------------------------------------//

         list<Account> accounts = (list<Account>) scope;
         Map<Id,Account> StoreAccounts = new Map<id,Account>(); 
         list<ObjectTerritory2Association> Account2Territory = [Select ID,ObjectId,Territory2Id,Territory2.Name From ObjectTerritory2Association where ObjectId in : accounts
                                                                ORDER BY LastModifiedDate];
         Map<Id,Account> RelevantAccount = new Map<id,Account> ();
         for(account acc : accounts)
             RelevantAccount.put(acc.id,acc);
         
//-------------------------------------------------------------------------------------//
//-------------------------Compare Terrirtories----------------------------------------//
//-------------------------------------------------------------------------------------//
         
         if(!RelevantAccount.isempty() && !Account2Territory.isEmpty())
           for(ObjectTerritory2Association TMP : Account2Territory)
               {
                     
                   StoreAccounts.remove(TMP.ObjectId);//Remove accounts that have newer Territory
                   if(TMP.Territory2.Name <> RelevantAccount.get(TMP.ObjectId).Territory__c)
                     { 
                       Account acc = new Account();  
                       acc.id = TMP.ObjectId;
                       acc.Territory__c = TMP.Territory2.Name;                    
                       StoreAccounts.put(acc.id,acc);
                     }
                     
               
                  }  
                    
//-------------------------------------------------------------------------------------//
//-------------------------Update result-----------------------------------------------//
//-------------------------------------------------------------------------------------//                                      
             Database.update(StoreAccounts.values(),false);
         
         
        
       }
         catch(Exception ex) 
       {
         system.debug('@@@Exception ' + ex.getMessage());    
         throw(ex);
       }
   }
 
  global void finish(Database.BatchableContext BC)
   {
        
   }
}