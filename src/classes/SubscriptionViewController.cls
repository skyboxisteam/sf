public with sharing class SubscriptionViewController {
 
 /*public List<wrapSub> wrapSubList = new List<wrapSub> ();
public List<wrapSubAsset> wrapSubListAsset = new List<wrapSubAsset> ();
public list<id> idList = new list<id> ();
    */
    
public List<wrapSub> wrapSubList  = new  List<wrapSub>();
public List<wrapSubAsset> wrapSubListAsset  {get;set;}
public map<string,decimal> totalMap;
public list<id> idList {get;set;}
public id relevantId ;
public Id accountId ;
public string accountName ;
public boolean showExp;
public boolean clicked {get;set;}
public boolean selected {get;set;}
public boolean allowShowExp {get;set;}
public boolean allowSelectSubscription {get;set;}

public void setrelevantId (Id argId)
{
   relevantId = argId;    
} 


public Id getrelevantId ()
{
   return relevantId;   
} 

public void setaccountId (id theAccountId)
{
   accountId = theAccountId;    
} 


public Id getaccountId ()
{
   return accountId;   
} 

public void setaccountName (string theAccountName)
{
   accountName = theAccountName;    
} 


public string getaccountName ()
{
   return accountName;   
} 

public PageReference setShowExpire()
{
 showExp = showExp == null || showExp == false? true:false;
 //getSubscrition(showExpiered );
 return null;
}




public List<wrapSub> getwrapSubList()
{
    wrapSubList.clear();
    showExp = showExp == null ? false: showExp  ;
    system.debug('Relevant' + relevantId);
    for(SBQQ__Subscription__c sc: [select id,name,WDSB_ProductCode__c,SBQQ__ProductName__c,SBQQ__Quantity__c ,SBQQ__ContractNumber__c,SBQQ__StartDate__c, SBQQ__EndDate__c from SBQQ__Subscription__c where SBQQ__Contract__c = :relevantId or SBQQ__Account__c = : relevantId ])
      if(showExp|| (!showExp && system.today() < sc.SBQQ__EndDate__c) || !allowShowExp )
           wrapSubList.add(new wrapSub(sc.id, sc.name,sc.WDSB_ProductCode__c, sc.SBQQ__ProductName__c, sc.SBQQ__ContractNumber__c,sc.SBQQ__Quantity__c, sc.SBQQ__StartDate__c, sc.SBQQ__EndDate__c, selected) );    
     if(!allowShowExp)
       processSelected();
           
     return wrapSubList;
} 

 public  void processSelected() 
  {
    idList  = new list<Id>();
    wrapSubListAsset = new list<wrapSubAsset>();
    for(wrapSub wrapObj : wrapSubList) 
        if(wrapObj.selected == true || !allowShowExp)        
           idList.add(wrapObj.subId); 
                         
   for(SBQQ__SubscribedAsset__c sca: [select id,name,SBQQ__Active__c,SBQQ__Asset__r.name ,SBQQ__Asset__r.Product2.WDSB_Sub_Family__c, SBQQ__Asset__r.Quantity_Attribute__c ,SBQQ__Asset__r.SerialNumber,WDSB_Asset_Quantity__c,SBQQ__Subscription__r.Name, createddate from SBQQ__SubscribedAsset__c where SBQQ__Subscription__c in :idList  ])
         wrapSubListAsset.add(new wrapSubAsset(sca.name,sca.SBQQ__Active__c, sca.SBQQ__Asset__r.name, sca.SBQQ__Asset__r.SerialNumber, sca.WDSB_Asset_Quantity__c,sca.SBQQ__Subscription__r.Name, sca.createddate, sca.SBQQ__Asset__r.Product2.WDSB_Sub_Family__c, sca.SBQQ__Asset__r.Quantity_Attribute__c ) );
   
     clicked = true;
  //  return wrapSubListAsset;
    
  }
  
  public map<string,decimal> gettotalMap()
  {
    system.debug('FFF');
    totalMap = new map<string,decimal> ();
    for(Asset ass: [select Product2.WDSB_Sub_Family__c, Quantity_Attribute__c from Asset where accountId = :relevantId and Product2id != null and Product2.Family = 'Software License' ] ) 
        addTotalMap(ass.Product2.WDSB_Sub_Family__c,ass.Quantity_Attribute__c ); 
    
    for(SBQQ__Subscription__c  sc : [select Subscription_Quantity_Formula__c, SBQQ__Product__r.WDSB_Sub_Family__c from SBQQ__Subscription__c where  (SBQQ__Account__r.id = :relevantId or SBQQ__Contract__c = : relevantId  )and SBQQ__Product__c !=null and SBQQ__Product__r.Family = 'Software License'  and SBQQ__EndDate__c > Today])
        addTotalMap(sc.SBQQ__Product__r.WDSB_Sub_Family__c, sc.Subscription_Quantity_Formula__c);
    
    for( SBQQ__SubscribedAsset__c sca : [select SBQQ__Asset__r.Quantity_Attribute__c , SBQQ__Asset__r.Product2.WDSB_Sub_Family__c from SBQQ__SubscribedAsset__c  where SBQQ__Asset__r.Product2id !=null and SBQQ__Asset__r.Product2.Family = 'Software License' and SBQQ__Subscription__r.SBQQ__Contract__c = : relevantId])
         addTotalMap(sca.SBQQ__Asset__r.Product2.WDSB_Sub_Family__c, sca.SBQQ__Asset__r.Quantity_Attribute__c);
         
    return totalMap;
  }
    
  
 private void addTotalMap(string family, decimal amount)
 {
    totalMap.put(family,totalMap.containsKey(family)? totalMap.get(family) + amount : amount );
 }

 public class wrapSub {
        
        public id subId {get; set;}
        public string name {get; set;}
        public string productCode {get; set;}
        public string productName {get; set;}
        public string contractNumber {get; set;}
        public decimal quantity {get; set;}
        public date startDate {get; set;}
        public date EndDate {get; set;}
        public boolean selected {get; set;}     
 
      public wrapSub (id subIdParam,string nameParam , string productCoedParam , string productNameParam, string  contractNumberParam, decimal quantityParam  ,date startDateParam, date EndDateParam, boolean selectParam  )
          {
            subId = subIdParam;
            name = nameParam ;
            productCode = productCoedParam;
            productName = productNameParam;
            contractNumber = contractNumberParam;
            quantity = quantityParam; 
            startDate = startDateParam;
            EndDate = EndDateParam;
            selected = selectParam;
          }
     
        }
  public class wrapSubAsset {
        
        public string name {get; set;}
        public boolean Active {get; set;}
        public string SerialNumber {get; set;}
        public string assetName {get; set;}
        public decimal assetQuantity {get; set;}
        public string subNumber {get; set;}
        public datetime createddate {get; set;}
        public string subFamily {get; set;}
        public decimal userQuantity {get; set;}
          
      public wrapSubAsset (string nameParam , boolean activeParam , string assetNameParam , string serialNumberParam, decimal assetQuantityParam,string subNumberParam ,datetime createddateParam , string subFamilyParam, decimal userQuantityParam )
         {
            name = nameParam ;
            Active = activeParam;
            assetName = assetNameParam;
            SerialNumber = serialNumberParam;
            assetQuantity = assetQuantityParam;
            subNumber = subNumberParam; 
            createddate = createddateParam;
            subFamily = subFamilyParam;
            userQuantity = userQuantityParam;
            
         }
        }
    
}