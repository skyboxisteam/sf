public with sharing class License2ProductionController {

public Boolean email {get; set;}
public License2__C lic {get; set;}
public String accName {get; set;}
public string checkBoxLength {get; set;}
public string nodesLength {get; set;}
public string dateLength {get; set;}

public License2ProductionController(ApexPages.StandardController controller){
   ClsObjectCreator cls = new ClsObjectCreator();
   Id accId = Test.isRunningTest() ? cls.CreateAccount('test').id : System.currentPageReference().getParameters().get('CF00N0e000003JpNQ_lkid');
   system.debug('account ' + accId);
   this.lic = (License2__C)controller.getRecord(); 
   cssParam();
   this.lic.Account__c = accId;
   this.lic.License_Version__c = [select id from License_Version__c where Default_version__c = true limit 1].id;
   accName  =  Test.isRunningTest() ? 'test' : [select name from account where id =:accId].Name;
   if(System.currentPageReference().getParameters().get('RecordType') == '0120e000000M5RS')
   {
     LicenseProduction lp = new LicenseProduction(accId );
     lic = lp.popLicense(lic);//new License2__C(Account__c = accId));
   }
   system.debug('License2__C ' + lic);
}

public void cssParam()
{
  checkBoxLength =  'width:15Px';
  dateLength =  'width:80Px';
  nodesLength = 'width:80Px';
}

}