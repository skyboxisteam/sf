public with sharing class QuoteDeleteHandler {
	

	public static void validateDelete(list<SBQQ__Quote__c> quoteList)
	{
		for(SBQQ__Quote__c qu : getQuoteList(quoteList))
		{    
		  if(qu.SBQQ__Primary__c && (qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__c == null || 
		                             qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.SBQQ__RenewalQuoted__c))
	  	    qu.addError('You cannot delete a primary quote');
	  	}
	}

	public static list<SBQQ__Quote__c> getQuoteList(list<SBQQ__Quote__c> quoteList)
	{
       return [Select SBQQ__Primary__c,
                      SBQQ__Opportunity2__r.SBQQ__RenewedContract__c,
                      SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.SBQQ__RenewalQuoted__c
		       From SBQQ__Quote__c
		       Where id in : quoteList ];
	}
}