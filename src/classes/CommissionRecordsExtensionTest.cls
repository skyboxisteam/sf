@isTest
public class CommissionRecordsExtensionTest {
    
    @isTest
    public static void testCreation(){
        ClsObjectCreator cls = new ClsObjectCreator();
        
        Account acc = cls.createAccount('test');
        
        Profile profile = [select id from Profile where name = 'Skybox Sales User' limit 1];
        
        User user = new User(Username = '123@s.com', LastName = '1113', Email = '12@d.com', Alias = '11', CommunityNickname = '1117', TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                             ProfileId = profile.id, LanguageLocaleKey = 'en_US', Theater__c = '11');
        insert user;
        
        Commission_Plan__c comissionPlan = new Commission_Plan__c(Name = '1234',Sales_User__c = user.id, Fiscal_Year__c = '2016', Annual_Quota__c = 22,Base_Salary_Currency__c='USD');
        insert comissionPlan;
        
        Opportunity opportunity = new Opportunity(Name = '123', StageName = '11111', CloseDate = System.today(), AccountId = acc.id);
        insert opportunity;
        
        Commission_Team__c ct = new Commission_Team__c(name = 'Test commission team', theater__c = 'EMEA');
        insert ct;
        
        Commission_Team_Member__c ctm = new  Commission_Team_Member__c (Member__c = user.id, Commission_Team__c = ct.id);
        insert ctm;
        
        ApexPages.StandardController sc = new ApexPages.standardController(opportunity);
        CommissionRecordsExtension comissionRecord = new CommissionRecordsExtension(sc);
        System.assertEquals(comissionRecord.selYear, '2016');
        
        comissionRecord.selYear = '2016';
        comissionRecord.userCommPlan.put(user,'1234' );
        CommissionRecordsExtension.UserWrapper userWrapper = new CommissionRecordsExtension.UserWrapper(user, new List<SelectOption>());
        CommissionRecordsExtension.UserWrapper userWrapper2 = new CommissionRecordsExtension.UserWrapper(user, new List<SelectOption>(), '111');
        CommissionRecordsExtension.UserWrapper userWrapper3 = new CommissionRecordsExtension.UserWrapper(user, new List<SelectOption>(), '111', 45);
        
        comissionRecord.setTheater('11');
        System.assertEquals(comissionRecord.theater, '11');
        
        comissionRecord.getSelectedYear();
        
        comissionRecord.setSelectedYear('2016');
        System.assertEquals(comissionRecord.selYear, '2016');
        
        comissionRecord.selCommissionTeam = ct.id;
       
        
        comissionRecord.getCommissionTeam();
        comissionRecord.searchUsers();
        comissionRecord.getTheater();
        
        comissionRecord.selCommissionTeam = 'All';
        comissionRecord.theater = 'EMEA';
         
        comissionRecord.getCommissionTeam();
        comissionRecord.searchUsers();
        
         comissionRecord.theater = 'All';
         comissionRecord.getCommissionTeam();
          comissionRecord.searchUsers();
        
        comissionRecord.back();
        System.assertEquals(comissionRecord.selectAllObject, false);
        
        comissionRecord.backToSearch();
        System.assertEquals(comissionRecord.userTable, false);
        
        comissionRecord.searchUsers();
       // System.assertEquals(comissionRecord.userTable, true);
        
        comissionRecord.getSelectedUser();
        System.assertEquals(comissionRecord.noUserSelected, true);
        
        System.assert(comissionRecord.saveI() != null);
        
        
        comissionRecord.first();
        comissionRecord.last();
        comissionRecord.previous();
        comissionRecord.next();
        System.assert(comissionRecord.getTheater() != null);
        System.assert(comissionRecord.getYear() != null);
        System.assert(comissionRecord.cancelOpp() != null);
        
    }
}