/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CertificationHandlerTest {

    static testMethod void myUnitTest() {
        
      ClsObjectCreator cls =  new ClsObjectCreator();
      Certification_type__c ct = new Certification_type__c(name = 'test');
      insert ct;
      
      Certification_type__c ct2 = new Certification_type__c(name = 'test2');
      insert ct2;
      
      Campaign cp = new Campaign( name = 'test',Certification_type__c = ct.id , EndDate = system.today().addDays(5));
      insert cp;  
      
               
      Contact co = ClsObjectCreator.createContact('test', cls.createAccount('test').id);
      
      CampaignMember cpm  = new CampaignMember(CampaignId = cp.id , Contactid = co.id, status = 'Attended');
      insert cpm;
      
      cpm.status= 'Attended';
       update cpm;
      
      cp.Certification_type__c  = ct2.id;
      update cp;
    }
}