/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Bug_To_CaseTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Bug_To_CaseTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Bug_To_Case__c());
    }
}