public class KW_CaseFileListContoller {
    
    private final Case myCase;
    static Map<String, String> files = new Map<String, String>();
    
    @AuraEnabled
    public static Map<String, String> MakeCallOut(Id caseId){
        Map<String, String> currFiles = new Map<String, String>();
        
        KiteWorks_Integration__c KW = KiteWorks_Integration__c.getValues('TestCloud');
        String FolderID = '-1';
        
        Case CaseToUpdate = [select KW_FolderID__c, CaseNumber from case where Id = :caseId];
        String CaseNum = CaseToUpdate.CaseNumber;
        System.debug('KW list currFiles started for case :' + CaseNum);
        FolderID = CaseToUpdate.KW_FolderID__c;
        
        System.debug('KW list currFiles - get files from folder ID - started for case :' + CaseNum + ' with FolderID'+FolderID);
        Http http2 = new Http();
        HttpRequest request2 = new HttpRequest();
        
        request2.setHeader('Accept', 'application/json');
        request2.setHeader('Authorization', 'Bearer '+ KW.Token__c);
        request2.setHeader('X-Accellion-Version' , KW.API_Version__c);
        request2.setMethod('GET');
        request2.setTimeout(120000);
        
        request2.setEndpoint(KW.Host__c+'rest/folders/'+FolderID+'/files?name%3Acontains='+KW.UserID__c);

        HttpResponse response2 = http2.send(request2);
        
        // Debug status
        System.debug( 'KW GetFolderFiles exit code For Folder ID ' + FolderID + ':' + response2.getStatusCode() + response2.getStatus() );
        
        // If success - parsing files from JSON and get the list of files
        if (response2.getStatusCode() == 200) {
            // Deserialize the JSON string into collections of primitive data types.
            System.debug(response2.getBody());
            JSONParser parser = JSON.createParser(response2.getBody());
            Integer Flag=0;
            String FileName = '';
            String FileCreationTime = '';
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                    String fieldName = parser.getText();
                    System.debug('fieldName: ' + fieldName);
                    parser.nextToken();
                    if(fieldName == 'name'){
                        System.debug ( parser.getText() );
                        FileName = parser.getText();
                        FileName = FileName.remove('(kw_sf_apiuser@skyboxsecurity.com)');
                        Flag = Flag + 1;
                        if (Flag == 2 ) {
                            currFiles.put(FileName, FileCreationTime);
                            Flag = 0;
                        }
                    }
                    if(fieldName == 'created'){
                        System.debug ( parser.getText() );
                        FileCreationTime = parser.getText();
                        FileCreationTime = FileCreationTime.removeEnd('+0000');
                        FileCreationTime = FileCreationTime.replace('T', ' ');
                        Flag = Flag + 1;
                        if (Flag == 2 ) {
                            currFiles.put(FileName, FileCreationTime);
                            Flag = 0;
                        }
                    }
                }
            }
        }
        else{
        	System.debug('KW list files - get files from folder ID - FAILED for case :' + CaseNum + ' with FolderID'+FolderID);
        }
        return currFiles;
    }
    
    public KW_CaseFileListContoller (ApexPages.StandardController stdController){
        
        this.myCase = (Case)stdController.getRecord();
        System.debug('KW view files for case:' + myCase.CaseNumber);
        files = MakeCallOut (myCase.Id);
	}
    
	public Map<String, String> getFiles() {
        return files;
    }
}