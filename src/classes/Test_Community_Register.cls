@isTest
private class Test_Community_Register 
{
	static testmethod void community_Register_Test()
	{
        // Instantiate a new controller with all parameters in the page
        SiteLoginController controller = new SiteLoginController ();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
        controller.firstname= 'FirstNameTest';
        controller.email = 'email@test.com';
        controller.lastname= 'LastNameTest';
        controller.Phone = '123123123';
        controller.country = 'TestCountry';
        controller.city = 'TestCity';
        controller.street = 'TestStreet, 4';
        controller.postalCode = 'TestCode';
  	    controller.login();
  	    controller.register();
	}

}