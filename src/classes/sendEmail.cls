public class SendEmail {
    public String regCode { get; set; }
    public String hwId{ get; set; }
    public String firstname{ get; set; }
    public String lastname{ get; set; }
    public String email{ get; set; }
    public String whatId{ get; set; }
    public String templateName{ get; set; }
    public List<String> recipients{ get; set; }
    

  	public void sethwId(string Value)
	{
		hwId = Value;
	}

	public void setregCode(string Value)
	{
		regCode = Value;
	}

  
    public PageReference send() 
    {
    	
	    if (!ishwIdValid())
		{

		//what to show when the data is not valid:

		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please check your HardwareID.'));

		//textAreaStyle = 'border-size:2px; border-color:red;border-style:solid;';

		return null;

		}
		if (!isregCodeValid())
		{

		//what to show when the data is not valid:

		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please check your activation code. Activation code is 13 charcters long and it is case sensative.'));

		//textAreaStyle = 'border-size:2px; border-color:red;border-style:solid;';

		return null;

		}
	    sObject[] contract=[select Id,AccountId,EndDate,CertiFire_Nodes__c,Licensed_Hardware_IDs__c from Contract where Activation_Key__c=:regCode];
		String contactId=[select ContactId from User where id=:UserInfo.getUserId()].ContactId;
		String accountId=[select Contact.AccountId from Contact where id=: contactId].AccountId;
		if (contract.size()==0)
		{
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'The Activtion Code you entered is not valid to your organization. Please check your Activation code.'));
			return null;
		}
		if (contract[0].get('Licensed_Hardware_IDs__c')!=null)
		{
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'This license was already activated. Please contact support@certifire.net.'));
			return null;
		}
		if (contract[0].get('AccountId') != accountId)
		{
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'The Activtion Code you entered is invalid to your organization. Please check your Activation code.'));
			return null;	
		}
	  
	    // Define the email
	    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
	    String[] toAddresses=new String[] {'ravid@skyboxsecurity.com'};
	        // Sets the paramaters of the email
	    //String body='Registration Code: '+regCode+'\n' +'Hardware ID: ' + hwId +'\n' + 'User ID:'+UserInfo.getUserId();
	    String body='end date '+contract[0].get('EndDate')+'\n' +'Nodes: ' + contract[0].get('CertiFire_Nodes__c') +'\n'+'hardware id '+hwId+'\n'+'contact: '+ contactId +firstname + lastname + email;
	    email.setSubject( 'Certifire License Activation');
	    email.setToAddresses( toAddresses);
	    email.setPlainTextBody( body );
	    // Sends the email
	    Messaging.SendEmailResult [] r =
	    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
	    PageReference secondPage = Page.licenseConfirmation;
		secondPage.setRedirect(true);
	    return secondPage;
    }
    private Boolean ishwIdValid()
	{
		if (hwId.length() != 27)
			return false; 
		else 
			return true;
	}
	private Boolean isregCodeValid()
	{
		if (regCode.length() != 13)
			return false; 
		else 
			return true;
	}

    
    static testMethod void myTouchEmailTest() 
    {
    	PageReference secondPage = Page.licenseConfirmation;
    	secondPage.setRedirect(true);
 		sendEmail controller = new sendEmail();
 		
 	}

	//Author: Rina Nachbas       Date:31.10.18      
    //Task:   MP-34:
    //email.sendEmailToTemplate('Deal Registration Alerts','0016000000M3Rh0','0050e0000069nqw', new List<String>{'rina.nachbas@skyboxsecurity.com'} );
 	public void sendEmailToTemplate(String templateName, Id whatId, Id targetObjId, Set<String> recipients)
 	{
 		List<String> recipientsList = new List<String>();
 		recipientsList.addAll(recipients);

 		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
		List<EmailTemplate> et = [Select id from EmailTemplate where Name= :templateName]; 
		mail.setTemplateId(et[0].id); 
		mail.setTargetObjectId(targetObjId); //to which the email will be sent
		mail.setWhatId(whatId);//ensure that merge fields in the template contain the correct data (posible to assiign only if use VF Template, Or classic template but the targetId is contact only!)
		mail.setToAddresses(recipientsList);
		// Specify the address used when the recipients reply to the email.  
		mail.setReplyTo('partners@skyboxsecurity.com');
		// Specify the name used as the display name. 
		mail.setSenderDisplayName('Skybox Security');
		mail.setUseSignature(false); 
		mail.setBccSender(false); 
		mail.setSaveAsActivity(false); 

		try 
		{
			Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		} catch (Exception e) {
		    System.debug(e.getMessage());
		}
 	}

 	//Author: Rina Nachbas       Date:31.10.18      
    //Task:   MP-34:
    //email.sendEmailToAccountTeam('0016000000M3Rh0');
 	public void sendEmailToAccountTeam(Id accId){
 		sendEmailToAccountTeam(accId, null);
 	}

 	public void sendEmailToAccountTeam(Id accId, List<String> roles){

 		List<AccountTeamMember> accTeamList = new List<AccountTeamMember>();
 		Set<String> accTeamEmailList = new Set<String>();
        Id emailTargetObjId;

 		if(roles == null || roles.size() == 0){
 			accTeamList = [SELECT AccountId, TeamMemberRole, UserId, User.Email, Id, Account.owner.Email FROM AccountTeamMember where AccountId = :accId];
 		}
 		else {
 			accTeamList = [SELECT AccountId, TeamMemberRole, UserId, User.Email, Id, Account.owner.Email FROM AccountTeamMember where AccountId = :accId AND TeamMemberRole in :roles];
 		}

 		for(AccountTeamMember accTeamMember : accTeamList){
			accTeamEmailList.add(accTeamMember.User.Email);
 		}

 		//add account owner.Email
 		if(accTeamList.size() > 0){
			accTeamEmailList.add(accTeamList[0].Account.owner.Email);
            emailTargetObjId = accTeamList[0].UserId;
 		}
        else{ //if there is no account team to the partner account
            List<Account> acc = [select ownerId, owner.Email FROM Account where Id = :accId limit 1];
            if(acc.size()>0){
            	accTeamEmailList.add(acc[0].owner.Email);
                emailTargetObjId = acc[0].ownerId;
            }
        }

 		//add emails from MDT
 		for(PartnerEmail__mdt email : [Select Email__c  from PartnerEmail__mdt])
 			accTeamEmailList.add(email.Email__c);

 		//add this.recipients
 		if(this.recipients != null)
 			accTeamEmailList.addAll(this.recipients);

 		system.debug('##sendEmailToAccountTeam: recipients: ' + accTeamEmailList);
 			
 		if(accTeamEmailList.size() > 0){
 			sendEmailToTemplate(this.templateName, this.whatId, emailTargetObjId ,accTeamEmailList);
 		}
 	}
}