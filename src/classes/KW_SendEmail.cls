public class KW_SendEmail {
    public static void SendEmail (String Subject , String Description ){
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        // Set list of people who should get the email
        List<String> sendTo = new List<String>();
        sendTo.add('shay.siksik@skyboxsecurity.com');
        mail.setToAddresses(sendTo);
        
        // Set who the email is sent from
        mail.setReplyTo('shay.siksik@skyboxsecurity.com');
        mail.setSenderDisplayName('KiteworksError');
        
        // Set email contents - you can use variables!
        mail.setSubject(Subject);
                
        mail.setHtmlBody(Description);
        mail.setSaveAsActivity(false);
        mail.setUseSignature(false);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}