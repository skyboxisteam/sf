public class getCaseData {
	@AuraEnabled
    public static Case getCase(Id recordId){
        Case mycase = [SELECT Id, Owner.Name, IsClosed, Escalated__c, Escalated_by__c, Status, CaseNumber, ContactId, Contact.Name, Subject, CreatedDate, OwnerId, Reason, Status_last_modified__c, Who_did_you_speak_to__c, Version_build__c FROM Case WHERE id=:recordId];
        return mycase; 
    } 
    
    @AuraEnabled
    public static Case getCaseForEscalation(Id recordId) {
        Case mycase = [SELECT Id, Owner.Name, IsClosed, Escalated__c, Escalated_by__r.Name, Status, CaseNumber, ContactId, Contact.Name, Subject, CreatedDate, OwnerId, Reason, Status_last_modified__c, Who_did_you_speak_to__c, Version_build__c FROM Case WHERE id=:recordId];
        if(!mycase.Escalated__c){
            mycase.Escalated_by__c = UserInfo.getUserId();
            update mycase;
        }
        return mycase;
    }
    
    @AuraEnabled
    public static String closeCaseAndInsertComment(String stringJson, String commentJson){
        try{            
            Map<String, Object> caseObj = (Map<String, Object>) json.deserializeUntyped (stringJson);
            Case c = new Case();
            c.Id = (Id) caseObj.get('Id'); 
            c.Status = 'Closed';//(String) caseObj.get('Status'); 
            //c.Reason = (String) caseObj.get('Reason');
            //c.Who_did_you_speak_to__c = (String) caseObj.get('Who_did_you_speak_to__c');
            //c.Version_build__c = (String) caseObj.get('Version_build__c');
            update c;
            CaseComment cc = (CaseComment) json.deserialize (commentJson, CaseComment.class);
            insert cc;
        }catch(Exception ex){
			system.debug('ex: ' + ex.getMessage());  
            return ex.getMessage();
        }
        return 'done';
    } 
    
    @AuraEnabled
    public static List<String> getCaseReason(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = case.Reason.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        }
        
        return options;
    }
    
    @AuraEnabled
    public static List<String> getCaseStatuses(){
        List<String> options = new List<String>();
        List<CaseStatus> lst_closedStatuses = [Select Id, MasterLabel, ApiName From CaseStatus Where IsClosed = true Order By SortOrder ASC];        
        for(CaseStatus status: lst_closedStatuses){
            options.add(status.MasterLabel);
        }        
        return options;
    }   
    
}