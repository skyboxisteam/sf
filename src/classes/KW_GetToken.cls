global class KW_GetToken {
    
    public static HttpResponse MakeCallOut(String ReqBody , String host){
        // Creating Request and sending
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(host+'oauth/token');
        request.setMethod('POST');
        request.setHeader('content-Type', 'application/x-www-form-urlencoded');
        request.setBody(ReqBody);
        
        HttpResponse response = http.send(request);
        
        return response;
    }
    
    @future(callout=true)
    public static void  GetToken() {
        /// Get token and save in the Custom Settings
        // Get Customer setting info for host
        KiteWorks_Integration__c KW = KiteWorks_Integration__c.getValues('TestCloud');
        // Set UX Timestamp
        DateTime dateTimeNow = dateTime.now();
        String TimeStamp = ''+dateTimeNow.getTime()/1000;
        
        System.debug('KW GetToken Started. current Token: '+KW.Token__c );
        
        // Create a random number between 0-1000000
        Integer nonce = Math.round(Math.random()*1000000);
        
        // KW seperator of requests
        String KWseperator = '|@@|';
        
        // Creating Base string
        String base_string = KW.ClientApplicationID__c + KWseperator + KW.UserID__c + KWseperator + TimeStamp + KWseperator + nonce;
        //System.debug('base_string: ' + base_string);
        
        // Calculating HMAC Sha1 Hash between key - SigKey , data - base_string
        Blob mac = Crypto.generateMac('hmacSHA1', Blob.valueOf(base_string), Blob.valueOf(KW.SigKey__c)); 
        string hashed = EncodingUtil.convertToHex(mac);
        //System.debug('hasedValue: '+hashed);
        
        // Creating Authorization code
        String auth_code = EncodingUtil.base64Encode(Blob.valueof(KW.ClientApplicationID__c)) + KWseperator + EncodingUtil.base64Encode(Blob.valueof(KW.UserID__c)) + KWseperator + TimeStamp + KWseperator + nonce + KWseperator + hashed ;
        //System.debug('auth_code: '+auth_code);
        
        // Creating Request Body
        String ReqBody = 'code='+auth_code+'&client_secret='+KW.ClientSecretKey__c+'&grant_type=authorization_code&client_id='+KW.ClientApplicationID__c+'&redirect_uri='+KW.RedirectionURI__c ;
        System.debug('ReqBody: '+ReqBody);
        
        HttpResponse response = MakeCallOut( ReqBody , KW.Host__c);
        // Debug status
        System.debug( 'KW GetToken - response status:' + response.getStatus() );
        System.debug( 'KW GetToken - response body:' + response.getBody() );
        
        // If success - parsing token from JSON and update KW.Token__c
        if (response.getStatusCode() == 200) {
            // Deserialize the JSON string into collections of primitive data types.
            System.debug(response.getBody());
            JSONParser parser = JSON.createParser(response.getBody());
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                    String fieldName = parser.getText();
                    //System.debug('fieldName: ' + fieldName);
                    parser.nextToken();
                    if(fieldName == 'access_token'){
                        KW.Token__c = parser.getText();
                        System.debug( 'KW GetToken - New Token: '+KW.Token__c );
                        update KW;
                        break;
                    }
                }
            }
        }
        else {
            System.debug( 'KW Error in Get Token' + response.getStatus() );
            KW_SendEmail.SendEmail('KW Critical Issue in Get Token', 'CRITICAL - no token');
        }
        
    }
}