public with sharing class APIToIconduct 
{
  @future(callout=true)
    public static void callIconduct (String interfaceId, string Params){
        
        IconductSettings__c ic;
        if(Test.isRunningTest())
           ic = new IconductSettings__c(URL__c = 'test',Token__c = 'test',Username__c = 'test' );
        else
           ic = [select Token__c,URL__c,Username__c from IconductSettings__c limit 1 ];
        string url = ic.URL__c + interfaceId;    
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setBody('');
        req.setMethod('POST');
        req.setheader('token',ic.Token__c);
        req.setheader('username',ic.Username__c);
        req.setHeader('Content-Type', 'application/json; charset=utf-8');
        req.setbody('{ "parameters":[ '+Params+']}');
        req.setTimeout(60000); 
        String rBody = ''; 
        if(!Test.isRunningTest()){
            HttpResponse res = h.send(req);
         //   rBody = res.getBody();
        }else{ 
            // From Test method, we are not allowed to make a callout call. We will populate the text response body from test method using the public static variable "test_ResponseBody"
          //  rBody = test_ResponseBody;
        }
    
       
    }    
}