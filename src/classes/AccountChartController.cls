public with sharing class AccountChartController {

    public Account acc {get; set; }
    public Decimal SurveyScore {get; set; }
    public Decimal SurveyAmount {get; set; }
    public Decimal AvgCloseCase{get; set; }
    public String ClosestRenewalDateString {get; set; }
    public Decimal ClosestRenewalAmount {get; set; }
    public list<Data> LastYearCasesData{get;set;}
    public list<Data> OpenCasesData {get; set; }
    public list<Data> ProductUsage {get; set; }
    public list<Data> LicenseUsage  {get ; set; }
    public list<Task> RelevantTasks {get; set; }
    private map<integer,string> monthNumberToText= new map<integer,string>{1=>'Jan',2=>'Feb',3=>'Mar',4=>'Apr',5=>'May',6=>'Jun',7=>'Jul',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec'};
  

//--------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------Constractor-----------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------
   
    public AccountChartController(ApexPages.StandardController stdCon) 
    {
        OpenCasesData = new list<Data>();
        LastYearCasesData = new list<Data>();
        LicenseUsage  = new list<Data>();
        
       if(!test.isRunningTest())
         {
          stdCon.addFields(new list<String>{'ID','Sum_of_Opportunities__c','PS_Balance_new__c'});
          acc= (Account) stdCon.getRecord();
         }
        else 
           acc = [select id, Sum_of_Opportunities__c, PS_Balance_new__c from account limit 1];      
        runAccountData();
    }

//--------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------runAccountData-----------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------
    
 public void runAccountData()
  {
        setSurveyData();
        setCaseData(); 
        setOpportunityData();
    //    calcualteOpenCases();
   //     calcualteLastYear();
   //     findTasks();
   //     setLicenseUsage();
  }


//--------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------findTasks-----------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------

private void findTasks()
{
	RelevantTasks = new list<task>();
    RelevantTasks.addall([select ActivityDate,Owner.name,status,subject,type,isClosed,Description from Task where accountid =:acc.id and  isclosed = false order by ActivityDate   ]);
	RelevantTasks.addall([select ActivityDate,Owner.name,status,subject,type,isClosed, Description from Task where accountid =:acc.id and  isclosed = true order by ActivityDate desc limit 3   ]);

	
}
//--------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------setSurveyData-----------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------

   private void setSurveyData()
    {
      AggregateResult[] groupedResultsSurvey = [SELECT AVG(simplesurvey__Survey_Score__c)aver, Count(simplesurvey__Survey_Score__c)aver2 FROM simplesurvey__Survey__c where simplesurvey__Account__c =:acc.id and CreatedDate = LAST_N_DAYS:365];
        SurveyScore = returnAggregateResult(groupedResultsSurvey,0,'aver');
        SurveyAmount = returnAggregateResult(groupedResultsSurvey,0,'aver2');   
    }

//--------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------setCaseData-----------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------
   
   private void setCaseData()
   {
         AggregateResult[] groupedResultsAge = [SELECT AVG(Days_to_resolution__c)aver FROM case where Accountid =:acc.id  ]; 
        AvgCloseCase = returnAggregateResult(groupedResultsAge ,0,'aver'); 
   }

//--------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------setOpportunityData-----------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------

   private void setOpportunityData()
   {
        Opportunity[] opp = [SELECT CloseDate ,Amount
                          FROM Opportunity 
                          where AccountId =:acc.id  and isClosed = false and type in ('Support Renewal','Subscription Renewal','Multi-year renewal') 
                          Order By CloseDate asc limit 1  ];
       if(opp.size() > 0)
       {
         ClosestRenewalDateString = opp[0].closeDate.format();
         ClosestRenewalAmount = opp[0].amount;
        }
        else
        {
         ClosestRenewalDateString  = 'No Renewal';
         ClosestRenewalAmount =0.00;
        } 
   }

//--------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------returnAggregateResult-----------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------
   
   private Decimal returnAggregateResult( AggregateResult[] groupedResults, Integer Location,string name)
    {
    system.debug('###'+groupedResults);
     if(name != null)
     {
       Decimal result = double.valueOf(groupedResults[Location].get(name)) == null? 0.00:double.valueOf(groupedResults[Location].get(name));
       return result.setScale(2);
     }
       else
       return 0.00;
    
    }

//--------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------calcualteOpenCases-----------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------

    private void calcualteOpenCases()
    {
       map<string,Integer> statusMap = new map<string,Integer>();
       for(case cas: [select id,short_status__c from case where isclosed = false and Accountid =: acc.id])
         {
            if(!statusMap.containsKey(cas.short_status__c))
                statusMap.put(cas.short_status__c,1);
             else
                statusMap.put(cas.short_status__c,statusMap.get(cas.short_status__c)+1);
         }
         for(string s :statusMap.keySet())
            OpenCasesData.add(new Data(s,statusMap.get(s)));
            
    
    }

//--------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------calcualteLastYear-----------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------
    
     private void calcualteLastYear()
    {
        map<Integer,Integer> caseCountMap = new map<Integer,Integer>();
       Date d =  Date.today().adddays(-365).addmonths(1);
       for(case cas:[select id,CreatedDate  from case where  Accountid =: acc.id and CreatedDate = LAST_N_DAYS:365 order by CreatedDate ])
         {
            if(!caseCountMap.containsKey(cas.createddate.month()))
                caseCountMap.put(cas.createddate.month(),1);
             else
                caseCountMap.put(cas.createddate.month(),caseCountMap.get(cas.createddate.month())+1);
         }
         for( integer j = 0; j < 12; j++)//Integer s :caseCountMap.keySet())
           { 
         //  system.debug('### ' + caseCountMap.get(i));
            LastYearCasesData.add(new Data(monthNumberToText.get(d.month()),caseCountMap.get(d.month())==null?0:caseCountMap.get(d.month())));
              d = d.addMonths(1);
           } 
    
    }
     
//--------------------------------------------------------------------------------------------------------------------
//----------------------------------------------- Class Data -----------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------
    
      public class Data {
        public String name { get; set; }
        public String name2 { get; set; }
        public Integer data1 { get; set; }
        public Data(String name, Integer data1) {
            this.name = name;
            this.data1 = data1;
           
        }
        public Data(String name,string name2 ,Integer data1) {
            this.name = name;
            this.name2 = name2;
            this.data1 = data1;
           
        }
}

}