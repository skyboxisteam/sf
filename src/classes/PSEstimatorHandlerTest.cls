/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PSEstimatorHandlerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        ClsObjectCreator cls = new ClsObjectCreator();
         PSEstimatorHandler psh= new  PSEstimatorHandler();
        Account acc = cls.createAccount('test');
        opportunity opp = cls.createOpportunity(acc);
        PS_Estimation__c ps = new PS_Estimation__c(name = 'test', opportunity__c = opp.id, Server_Type__c = 'virtual', Difficulty__c = 'no_poc', Total_Number_of_Servers__c = 5, Number_of_Collection_Sources__c = 2, Number_of_Vendors__c = 5, NA_Licenses__c = 10  
);
        insert ps;
        
        ps.Get_Estimation__c = true;
     
        update ps;
        ps.JSON_Responce__c = '{"data": {"estimateDate": "2018-01-24T13:15:58", "high": 36.0, "low": 58.0, "normal": 42.0, "version": "1.0.1"}, "status": "success"}';
       
        psh.updateJsonResponce(ps,false); 
        
        
    }
}