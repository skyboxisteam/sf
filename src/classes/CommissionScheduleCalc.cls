public without sharing class CommissionScheduleCalc 
{
 
  public map<string,Commission_Rule_Assignment__c> mapComRule = new map<string,Commission_Rule_Assignment__c>();
  public map<id,decimal> mapColledted = new map<id,decimal>();
  public list<id> listOppId = new list<id>(); 
 // private string multiType ='Multi Year Adjustment';
//  private string multiCommissionFactor ='CommissionFactor';
 // private string multiQuoteFactor ='QuoteFactor';
 // private string collectionType ='Collection Days Limit';
//  private string collectionDays ='DaysLimit';
//  private string maxAmountType ='Max Amount';
 //private string maxAmount ='MaxAmount';
  //private string multiYearId = 'Multi Year';
  public decimal TotalCollected = 0;
  private list<Booking_Schedule__c> bookingScheduleList = new list<Booking_Schedule__c>();
  private map<id,decimal> collectedMap = new map<id,decimal>();
  
  
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------Constractor Commissiopm Plan-------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
   
  public CommissionScheduleCalc(list<id> comPlanList)  
  {
    for(Commission_Record__c cr : [select id,Opportunity__c from Commission_Record__c where Commission_Plan__c in :comPlanList])    
         listOppId.add(cr.Opportunity__c); 
  }
  
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------Cconstractor Booking Schedule---------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 
  public CommissionScheduleCalc(list<Booking_Schedule__c> bookingList)
  {  
    for(Booking_Schedule__c bs : bookingList) 
         listOppId.add(bs.Opportunity__c);                      
  }

  
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------Cconstractor Commission Record---------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 
  public CommissionScheduleCalc(list<Commission_Record__c> commissionRecordList)
  {  
     for(Commission_Record__c cr : commissionRecordList)    
         listOppId.add(cr.Opportunity__c);               
        
  }

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------Cconstractor Commission Schedule---------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 
  public CommissionScheduleCalc(list<Commission_Schedule__c> commissionScheduleList)
  {  
     for(Commission_Schedule__c cs : commissionScheduleList)    
         listOppId.add(cs.Opportunity__c);               
        
  }
  
  
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------setTotalCollected------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
    
   public void setTotalCollected( list<Booking_Schedule__c> bookingList)
      {
         for(Booking_Schedule__c bs : bookingList)
           collectedMap.put(bs.Opportunity__c ,collectedMap.containsKey(bs.Opportunity__c)? collectedMap.get(bs.Opportunity__c) + CommissionPlanCalc.ifnull(bs.Collected_Amount__c):CommissionPlanCalc.ifnull(bs.Collected_Amount__c)  );
        //   TotalCollected += bs.Collected_Amount__c;        
      }
    
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------createRuleMap----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
    
   public void createRuleMap(list<Commission_Record__c> listComRec)
    {
      list<id> comPlanId = new list<id>();
      for(Commission_Record__c cr :listComRec)
      {
        comPlanId.add(cr.Commission_Plan__c);
      }
      
      for(Commission_Rule_Assignment__c cr :CommissionMethods.getCommissionRule(comPlanId) )
      {
        mapComRule.put(cr.Commission_Plan__c + cr.type__c,cr); 
      }
    }

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------createRuleMap----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
    
   public void createRuleMap(list<Commission_Schedule__c> listComSch)
    {
      list<id> comPlanId = new list<id>();
      for(Commission_Schedule__c cs :listComSch)
      {
        comPlanId.add(cs.Commission_Plan__c);
      }
      
      for(Commission_Rule_Assignment__c cr :CommissionMethods.getCommissionRule(comPlanId) )
      {
        mapComRule.put(cr.Commission_Plan__c + cr.type__c,cr); 
      }
    }
    
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------deleteOldComSchedule----------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
    
  public void deleteOldComSchedule()
  {
     //system.debug('Deleted ' + listOppId );
         delete [select id from Commission_Schedule__c where Opportunity__c in : listOppId];
      }


//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getCommissionCredit----------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
  public decimal getCommissionCredit(Id planId,Boolean isMulti)
  {
    return isMulti ? CommissionMethods.getRuleValue(planId,Label.MultiYear,Label.QuotaFactor,mapComRule):100;
  }
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getAdvancable----------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
   
   public decimal getAdvancable( Booking_Schedule__c bs, Boolean commissionable)
   {
     // system.debug('*@#@* ' + getIsCommissinable(planId,bs));
       return  commissionable ? bs.Amount__c : bs.Collected_Amount__c == null ? 0 : bs.Collected_Amount__c  ; 
       //getIsCommissinable(planId,bs) ?  bs.Amount__c : bs.Collected_Amount__c ;       
                 
   }
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getIsCommissinable----------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
   
   public boolean getIsCommissinable(id planId, Booking_Schedule__c bs )
   {
   // system.debug('is commissionable ' + (bs.Commissinable__c && bs.Opportunity__r.Amount <= CommissionMethods.getRuleValue(planId,Label.MaxAmountRuleType,Label.MaxAmountRuleAttribute,mapComRule)));
     return ((bs.Commissinable__c && bs.Opportunity__r.Amount <= CommissionMethods.getRuleValue(planId,Label.MaxAmountRuleType,Label.MaxAmountRuleAttribute,mapComRule)) &&
        !(bs.Opportunity__r.Closedate.daysBetween(system.today()) > CommissionMethods.getRuleValue(planId,Label.CollectionDaysLimit,Label.DaysLimit,mapComRule) ||
              bs.opportunity__r.Collection_Risk__c || bs.opportunity__r.No_commission_until_collection__c)) || (bs.Amount__c == bs.Collected_Amount__c);
   }

 
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getQuotaFactoredAmount----------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
   
  
   public decimal getQuotaFactoredAmount(Commission_Record__c cr, Booking_Schedule__c bs,Boolean isMulti, list<Booking_Schedule__c> bookList)
   {
     
     decimal recuringAmount = isRecuring(cr,bs) ? bs.Opportunity__r.SBQQ__PrimaryQuote__r.Recurring_incentive__c : 0;
     //system.debug(CommissionMethods.getFactor(cr.Commission_Plan__c,isMulti, bs, bookList, mapComRule));
     return CommissionPlanCalc.ifnull(bs.amount__c)  * CommissionMethods.getFactor(cr.Commission_Plan__c,isMulti, bs, bookList, mapComRule) + recuringAmount;
   
   }
 //-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getCommissionFactoredAmount----------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
   public decimal getCommissionFactoredAmount(Commission_Record__c cr, Booking_Schedule__c bs, list<Booking_Schedule__c> bookList,Boolean isMulti, Decimal advanceAmount)
   {
     
     decimal recuringAmount = isRecuring(cr,bs) ? calculateAdvanceRecurring(bs.Opportunity__c, cr.Commission_Plan__c,bookList, bs) : 0;
     return advanceAmount * CommissionMethods.getFactor(cr.Commission_Plan__c, isMulti,bs, bookList,mapComRule) + recuringAmount;
  
   }

     public decimal getCommissionFactoredAmount(Commission_Schedule__c cs, Booking_Schedule__c bs, list<Booking_Schedule__c> bookList,Boolean isMulti, Decimal advanceAmount)
   {
     
     decimal recuringAmount = isRecuring(cs) ? calculateAdvanceRecurring(cs.Opportunity__c, cs.Commission_Plan__c,bookList, bs) : 0;
     return advanceAmount * CommissionMethods.getFactor(cs.Commission_Plan__c, isMulti,bs, bookList,mapComRule) + recuringAmount;
  
   }

   public Boolean isRecuring(Commission_Record__c cr, Booking_Schedule__c bs)
   {
    return bs.Type__c == Label.Perpetual  &&  
           bs.Opportunity__r.SBQQ__PrimaryQuote__c != null && 
           CommissionMethods.checkRuleExist(cr.Commission_Plan__c,Label.RecurringIncentive,mapComRule);
   }

     public Boolean isRecuring(Commission_Schedule__c cs)
   {
    return cs.Booking_Schedule__r.Type__c == Label.Perpetual  &&  
           cs.Opportunity__r.SBQQ__PrimaryQuote__c != null && 
           CommissionMethods.checkRuleExist(cs.Commission_Plan__c,Label.RecurringIncentive,mapComRule);
   }
  

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------calculateAdvanceRecurring----------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

 private decimal calculateAdvanceRecurring(Id oppId, Id comPlanId, list<Booking_Schedule__c> bookList, Booking_Schedule__c bsCurr)
 {
  decimal recAmount = 0;
  for(Booking_Schedule__c bs : bookList)
    if(bs.opportunity__c == oppId)
     recAmount += getAdvancable(bs, getIsCommissinable(comPlanId, bs ));
  
  return recAmount*bsCurr.Opportunity__r.SBQQ__PrimaryQuote__r.Recurring_incentive__c/bsCurr.Opportunity__r.amount;
 }   

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------createCommissionSchedule----------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
   
 public Commission_Schedule__c createCommissionSchedule(Commission_Record__c cr, Booking_Schedule__c bs, list<Booking_Schedule__c> bookList)
 {
     
      Commission_Schedule__c cs = new Commission_Schedule__c();
      cs.Booking_Schedule__c = bs.id;
      cs.Commission_Plan__c = cr.Commission_Plan__c;
      cs.Commission_Record__c = cr.id;
      cs.Opportunity__c = bs.Opportunity__c;
      cs.Credit__c = cr.Credited__c;
      cs.isMulti__c = CommissionMethods.isMulti(cr,bs,mapComRule);
      cs.Data_Conversion__c = bs.Data_Conversion__c;
      cs.Is_Advancable__c = getIsCommissinable(cr.Commission_Plan__c,bs);
      cs.Advanceable_Amount__c = getAdvancable(bs, cs.Is_Advancable__c);
      cs.Commission_Credit__c =  getCommissionCredit(cr.Commission_Plan__c, cs.isMulti__c ); 
   //   system.debug('Ismulti ' + cs.isMulti__c  +  'factor ' + getQuotaFactoredAmount(cr,bs,cs.isMulti__c , bookList) + ' Type  ' + bs.Type__c );
      cs.Quota_Amount__c = bs.Commissinable__c ? getQuotaFactoredAmount(cr,bs,cs.isMulti__c , bookList)*cr.Credited__c/100 :0;
      try
        {
         cs.Commission_Amount__c = bs.Commissinable__c ? getCommissionFactoredAmount(cr,bs,bookList,  cs.isMulti__c ,cs.Advanceable_Amount__c)*cr.Credited__c/100 : 0;
        }
     catch(exception ex)
      {
         system.debug(ex);

      }
      return cs;     
  
 
 }
 

 
 //-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getRatioMaxAmount----------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
 
 private decimal getRatioMaxAmount(id planId, Booking_Schedule__c bs)
 {
   
        return CommissionMethods.getRuleValue(planId,'Max Amount',Label.MaxAmountRuleAttribute,mapComRule)*bs.Amount__c/bs.Opportunity__r.Amount;
 }

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getCommissionRecords----------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

 public list<Commission_Record__c> getCommissionRecords()
    {       
        
        return [select id,
                       opportunity__c,
                       Commission_Plan__c,
                       Type__c ,
                       Credited__c                 
                from Commission_Record__c 
                where Opportunity__c in: listOppId];        
    }
    
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getBookingSchedules----------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
   public list<Booking_Schedule__c> getBookingSchedules()
    {       
        
        return [select id,
                       Type__c,
                       Type_Family__c,
                       Commissinable__c,
                       Collected_Amount__c,
                       Amount__c ,
                       Actual_Collection_Date__c,
                       Target_Collection_Date__c,
                       Data_Conversion__c,
                       Support_Renewal_Amount__c ,
                       Opportunity__c,
                       Opportunity__r.Closedate,
                       Opportunity__r.Target_Days_to_Collection__c,
                       Opportunity__r.Actual_Days_to_Collection__c,
                       Opportunity__r.No_commission_until_collection__c,
                       Opportunity__r.Collection_Risk__c,
                       Opportunity__r.Amount,                     
                       Opportunity__r.Effective_Order_Date__c,
                       Opportunity__r.SBQQ__PrimaryQuote__r.Recurring_incentive__c,
                       Opportunity__r.Is_renewal_for_commission_calculation__c,
                       opportunity__r.SBQQ__PrimaryQuote__r.SBQQ__SubscriptionTerm__c,
                       opportunity__r.SBQQ__PrimaryQuote__r.SBQQ__EndDate__c,
                       opportunity__r.SBQQ__PrimaryQuote__r.SBQQ__StartDate__c,
                        opportunity__r.Quota_Credit_Override__c
                from Booking_Schedule__c 
                where Opportunity__c in: listOppId];        
    }
 
   
}