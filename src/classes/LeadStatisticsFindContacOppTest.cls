/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LeadStatisticsFindContacOppTest {

    static testMethod void myUnitTest() {
        ClsObjectCreator cls = new ClsObjectCreator();
          Account acc = cls.createAccount('Test');
       contact ct = ClsObjectCreator.createContact('jeffry',acc.id);
        ct.Behavior_Score__c = 28;
        update ct;
       Opportunity opp = cls.createOpportunity(acc);
       opp.Main_contact__c = ct.id;
       update opp;
       ClsObjectCreator.createContactRoleTo(opp, 'Main');
        
       
        LeadStatisticsFindContacOpp sh1 = new LeadStatisticsFindContacOpp();      
     String sch = '0  00 1 3 * ?';
       system.schedule('Test', sch, sh1);
        
        
    }
}