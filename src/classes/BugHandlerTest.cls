@isTest
private class BugHandlerTest {
	
	@testSetup static void setupData() {
       
       ClsObjectCreator cls = new ClsObjectCreator();

       //create Account
        Account acc1 =  cls.createAccount('PartnerAccount1');
        
        //create Case
        Case case1 =  cls.createCasewithAccount(acc1.Id);
        Case case2 =  cls.createCasewithAccount(acc1.Id);

        Bug__c b1  = new Bug__c( Status__c = 'TO DO');
        insert b1;

        Bug_To_Case__c b2c1 = new Bug_To_Case__c(Bug__c=b1.Id, Case__c = case1.Id);
        Bug_To_Case__c b2c2 = new Bug_To_Case__c(Bug__c=b1.Id, Case__c = case2.Id);

        insert b2c1;insert b2c2;
   }
	
	@isTest static void createCaseCommentTest() {
		Bug__c b = [Select Id, Requirement__c, Status__c from Bug__c limit 1];


		List<Bug__c> bugTest = [Select Id, Requirement__c, Status__c from Bug__c ];
		System.assertEquals('TO DO', bugTest[0].Status__c);
		System.assertNotEquals('Support', bugTest[0].Requirement__c);

		//Test 1. bug.Status__c changed but bug.Requirement__c is NOT 'Support'
			b.Status__c = 'Unresolved';
			//b.Requirement__c = 'Support';
			update b;

		List<CaseComment> comments = [Select Id from CaseComment];
		System.assertEquals(0, comments.size());
		List<Case> cases = [Select Id, For_Support_Review__c from Case];
		System.assertEquals(cases[0].For_Support_Review__c, false);
		System.assertEquals(cases[1].For_Support_Review__c, false);

		//Test 2. bug.Status__c changed & bug.Requirement__c = 'Support'
		Test.startTest();
			b.Status__c = 'Resolved';
			b.Requirement__c = 'Support';
			update b;
		Test.stopTest();

		bugTest = [Select Id, Requirement__c, Status__c from Bug__c ];
		System.assertEquals('Resolved', bugTest[0].Status__c);
		System.assertEquals('Support', bugTest[0].Requirement__c);
		//comments = [Select Id from CaseComment];
		//System.assertEquals(2, comments.size());
		cases = [Select Id, For_Support_Review__c from Case];
		System.assertEquals(cases[0].For_Support_Review__c, true);
		System.assertEquals(cases[1].For_Support_Review__c, true);
	}
	
}