public with sharing class LicenseProduction 
{
   public list<license2__c> licesnesList = new list<license2__c>() ; 
   public list<id> listAccountId = new list<id>() ; 
   public list<Asset> listAsset = new list<Asset>() ; 
   public list<SBQQ__Subscription__c> listSubscriptions = new list<SBQQ__Subscription__c>() ; 
   public map<id,map<string,helpLicense>> mapLicense = new map<id,map<string,helpLicense>>() ; 
   private final string NA = 'Network Assurance';
   private final string vNA = 'Virtual Network Assurance';
   private final string FA = 'Firewall Assurance';
   private final string CM = 'Change Manager';
   private final string TM = 'Threat Mananger';
   private final string VC = 'Vulnerability Control';
   private final string PD  = 'Content Package';
   private final string ServerSW = 'Server Software';
   private final string ServerSWEnterpriseCode = 'SBV-ES';
   private final string ServerSWStandardCode = 'SBV-S';
   private final id recTypeId = [select id from recordtype where RecordType.Developername = 'Production' limit 1].id;
   set<string> setModule = new set<string>{NA,FA,TM,VC};
   
   /*public LicenseProduction(list<Account> listAccount ,map<id,Account> oldMap)
   {
      for(Account acc : listAccount )
        if(acc.Generate_License__c && !oldMap.get(acc.id).Generate_License__c) 
          listAccountId.add(acc.id);    
      
      if(!listAccountId.isEmpty())
         GenerateLicense();
   }  */  

    public LicenseProduction(list<License2__c> listLicense)
   {
      for(License2__c ls : listLicense )
      {
         listAccountId.add(ls.Account__c);  
       }

 system.debug('Accounts ' +listAccountId);
      licesnesList = listLicense;
      
   }    

  public LicenseProduction(Id accId)
   {
         listAccountId.add(accId);   
         buildLiceceMap();
          
   }    


/*  public void GenerateLicense()
  {
     listAsset = getAssets(listAccountId);
     system.debug(listAsset);
      if(!listAsset.isempty())
        for(Asset ass:listAsset )
        {
          system.debug( 'family prod' + ass.Product2.WDSB_Sub_Family__c);
           addLicenseMap(ass.accountid, ass.Product2.WDSB_Sub_Family__c == ServerSW ? ass.Product2.ProductCode :ass.Product2.WDSB_Sub_Family__c , ass.Quantity_Attribute__c, null);
    // system.debug('$$$ ' +mapLicense);
     }
       listSubscriptions = getSubscriptions(listAccountId);         
       system.debug('^^^ ' +listSubscriptions);
       if(!listSubscriptions.isempty())
          for(SBQQ__Subscription__c su :listSubscriptions )
          {
           system.debug('Familia ' + su.SBQQ__Product__r.WDSB_Sub_Family__c);
            addLicenseMap( su.SBQQ__Account__r.id,
                           su.SBQQ__Product__r.WDSB_Sub_Family__c == ServerSW ? su.SBQQ__Product__r.ProductCode : su.SBQQ__Product__r.WDSB_Sub_Family__c, 
                           su.Subscription_Quantity_Formula__c ,su.SBQQ__EndDate__c);
         } 
    system.debug('$$$ ' +mapLicense);
       if(!mapLicense.keyset().isempty())
         createLicense();
        
  } */

   public void GenerateLicense()
  {
    buildLiceceMap();
    system.debug('$$$ ' +mapLicense);
       if(!mapLicense.keyset().isempty())
         createLicense();
        
  } 

   public void buildLiceceMap()
   {
     listAsset = getAssets(listAccountId);
     system.debug(listAsset);
      if(!listAsset.isempty())
        for(Asset ass:listAsset )
        {
          system.debug( 'family prod' + ass.Product2.WDSB_Sub_Family__c);
           addLicenseMap(ass.accountid, ass.Product2.WDSB_Sub_Family__c == ServerSW ? ass.Product2.ProductCode :ass.Product2.WDSB_Sub_Family__c , ass.Quantity_Attribute__c, null);
    // system.debug('$$$ ' +mapLicense);
     }
       listSubscriptions = getSubscriptions(listAccountId);         
       system.debug('^^^ ' +listSubscriptions);
       if(!listSubscriptions.isempty())
          for(SBQQ__Subscription__c su :listSubscriptions )
          {
          // system.debug('Familia ' + su.SBQQ__Product__r.WDSB_Sub_Family__c);
            addLicenseMap( su.SBQQ__Account__r.id,
                           su.SBQQ__Product__r.WDSB_Sub_Family__c == ServerSW ? su.SBQQ__Product__r.ProductCode : su.SBQQ__Product__r.WDSB_Sub_Family__c, 
                           su.Subscription_Quantity_Formula__c ,su.SBQQ__EndDate__c);
         } 
   system.debug('Map ' + mapLicense);
   }

  public License2__c popLicense(License2__c ls)
  {
      system.debug(' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%' + ls);
            Id accId = ls.Account__c ; 
             system.debug('accId ' + accId);
            system.debug('Map2 ' + mapLicense.get(accId));
            system.debug('Map FA ' + mapLicense.get(accId).get(FA));
             ls.FA_license__c = mapLicense.get(accId).get(FA)!=null;
             ls.VC_license__c = mapLicense.get(accId).get(VC)!=null;
             ls.TM_license__c = mapLicense.get(accId).get(TM)!=null;
             ls.NA_License__c = mapLicense.get(accId).get(NA)!=null;
             ls.vNA_License__c = mapLicense.get(accId).get(vNA)!=null;
             ls.CM_License__c = mapLicense.get(accId).get(CM)!=null;     
             ls.Component_Limits_Servers__c = calcServerSW(accId);
             ls.Component_Limits_Managers__c = mapLicense.get(accId).get(ServerSWEnterpriseCode)!=null?9999:mapLicense.get(accId).get(ServerSWStandardCode)!=null?5:0;
             ls.Component_Limits_Collectors__c = mapLicense.get(accId).get(ServerSWEnterpriseCode)!=null?9999:mapLicense.get(accId).get(ServerSWStandardCode)!=null?5:0;
             ls.Content__c = mapLicense.get(accId).get(PD)!=null;           
             ls.FA_Nodes__c = mapLicense.get(accId).get(FA)!= null? mapLicense.get(accId).get(FA).amount:0;
             ls.VC_Nodes__c = mapLicense.get(accId).get(VC)!=null?mapLicense.get(accId).get(VC).amount:0;
             ls.NA_Nodes__c = mapLicense.get(accId).get(NA)!=null ?mapLicense.get(accId).get(NA).amount:0;
             ls.Virtual_Nodes__c = mapLicense.get(accId).get(vNA)!=null ?mapLicense.get(accId).get(vNA).amount:0;
             system.debug( 'NA ' +  ls.NA_Nodes__c + ' Exist ' +mapLicense.get(accId).get(NA));
             ls.recordtypeId = recTypeId;
             ls.Disable_license_limitation__c = true;
             //ls.Expiration_Date__c = !listSubscriptions.isempty()?listSubscriptions[0].SBQQ__EndDate__c:!listAsset.isempty()? date.newInstance(2100,1,1) :null;
             ls.FA_Expiration_Date__c  = getRelevantDate(mapLicense.get(accId).get(FA)!= null? mapLicense.get(accId).get(FA).experationDate:null , ls.FA_license__c,ls.Expiration_Date__c); 
             ls.NA_Expiration_Date__c  = getRelevantDate(mapLicense.get(accId).get(NA)!= null? mapLicense.get(accId).get(NA).experationDate:null, ls.NA_License__c,ls.Expiration_Date__c);  
             ls.vNA_Expiration_Date__c = getRelevantDate(mapLicense.get(accId).get(vNA)!= null? mapLicense.get(accId).get(vNA).experationDate:null, ls.vNA_License__c,ls.Expiration_Date__c);  
             ls.CM_Expiration_Date__c  = getRelevantDate(mapLicense.get(accId).get(CM)!= null? mapLicense.get(accId).get(CM).experationDate:null, ls.CM_License__c,ls.Expiration_Date__c);
             ls.VC_Expiration_Date__c  = getRelevantDate(mapLicense.get(accId).get(VC)!= null? mapLicense.get(accId).get(VC).experationDate:null, ls.VC_license__c,ls.Expiration_Date__c);
             ls.TM_Expiration_Date__c  = getRelevantDate(mapLicense.get(accId).get(TM)!= null? mapLicense.get(accId).get(TM).experationDate:null, ls.TM_license__c,ls.Expiration_Date__c);
             ls.Content_Expatriation_Date__c = getRelevantDate(mapLicense.get(accId).get(PD)!= null? mapLicense.get(accId).get(PD).experationDate:null, ls.Content__c,ls.Expiration_Date__c);
       system.debug(ls);
      return ls;
  }

  private void createLicense()
  {
      list<license2__c> result = new list<license2__c> ();
      for(license2__c ls : licesnesList)
           {
           //  License2__c ls = new license2__c();
             result.add( popLicense(ls));
           } 

           License2Genratation.Generate(result);

  }
/*
  private void createLicense()
  {
      list<license2__c> result = new list<license2__c> ();
      for(license2__c ls : licesnesList)
           {
           //  License2__c ls = new license2__c();
             Id accid = ls.Account__c ; 
            system.debug('Map ' + mapLicense.get(accId));
             ls.FA_license__c = mapLicense.get(accId).get(FA)!=null;
             ls.VC_license__c = mapLicense.get(accId).get(VC)!=null;
             ls.TM_license__c = mapLicense.get(accId).get(TM)!=null;
             ls.NA_License__c = mapLicense.get(accId).get(NA)!=null;
             ls.vNA_License__c = mapLicense.get(accId).get(vNA)!=null;
             ls.CM_License__c = mapLicense.get(accId).get(CM)!=null;     
             ls.Component_Limits_Servers__c = calcServerSW(accId);
             ls.Component_Limits_Managers__c = mapLicense.get(accId).get(ServerSWEnterpriseCode)!=null?9999:mapLicense.get(accId).get(ServerSWStandardCode)!=null?5:0;
             ls.Component_Limits_Collectors__c = mapLicense.get(accId).get(ServerSWEnterpriseCode)!=null?9999:mapLicense.get(accId).get(ServerSWStandardCode)!=null?5:0;
             ls.Content__c = mapLicense.get(accId).get(PD)!=null;           
             ls.FA_Nodes__c = mapLicense.get(accId).get(FA)!= null? mapLicense.get(accId).get(FA).amount:0;
             ls.VC_Nodes__c = mapLicense.get(accId).get(VC)!=null?mapLicense.get(accId).get(VC).amount:0;
             ls.NA_Nodes__c = mapLicense.get(accId).get(NA)!=null ?mapLicense.get(accId).get(NA).amount:0;
             ls.Virtual_Nodes__c = mapLicense.get(accId).get(vNA)!=null ?mapLicense.get(accId).get(vNA).amount:0;
             system.debug( 'NA ' +  ls.NA_Nodes__c + ' Exist ' +mapLicense.get(accId).get(NA));
             ls.recordtypeId = recTypeId;
             ls.Disable_license_limitation__c = true;
             //ls.Expiration_Date__c = !listSubscriptions.isempty()?listSubscriptions[0].SBQQ__EndDate__c:!listAsset.isempty()? date.newInstance(2100,1,1) :null;
             ls.FA_Expiration_Date__c  = getRelevantDate(mapLicense.get(accId).get(FA)!= null? mapLicense.get(accId).get(FA).experationDate:null , ls.FA_license__c,ls.Expiration_Date__c); 
             ls.NA_Expiration_Date__c  = getRelevantDate(mapLicense.get(accId).get(NA)!= null? mapLicense.get(accId).get(NA).experationDate:null, ls.NA_License__c,ls.Expiration_Date__c);  
             ls.vNA_Expiration_Date__c = getRelevantDate(mapLicense.get(accId).get(vNA)!= null? mapLicense.get(accId).get(vNA).experationDate:null, ls.vNA_License__c,ls.Expiration_Date__c);  
             ls.CM_Expiration_Date__c  = getRelevantDate(mapLicense.get(accId).get(CM)!= null? mapLicense.get(accId).get(CM).experationDate:null, ls.CM_License__c,ls.Expiration_Date__c);
             ls.VC_Expiration_Date__c  = getRelevantDate(mapLicense.get(accId).get(VC)!= null? mapLicense.get(accId).get(VC).experationDate:null, ls.VC_license__c,ls.Expiration_Date__c);
             ls.TM_Expiration_Date__c  = getRelevantDate(mapLicense.get(accId).get(TM)!= null? mapLicense.get(accId).get(TM).experationDate:null, ls.TM_license__c,ls.Expiration_Date__c);
             ls.Content_Expatriation_Date__c = getRelevantDate(mapLicense.get(accId).get(PD)!= null? mapLicense.get(accId).get(PD).experationDate:null, ls.Content__c,ls.Expiration_Date__c);
             result.add(ls);

           } 

           License2Genratation.Generate(result);

  }*/

  private boolean isNotNull(Id account, string family)
  {
    return mapLicense.get(account).get(family) !=null;
  }

  private decimal calcServerSW(Id accId)
  {
    return (mapLicense.get(accId).get(ServerSWEnterpriseCode) == null && mapLicense.get(accId).get(ServerSWStandardCode) == null)? null
            : (mapLicense.get(accId).get(ServerSWEnterpriseCode) == null ? 0 :mapLicense.get(accId).get(ServerSWEnterpriseCode).amount) + 
              (mapLicense.get(accId).get(ServerSWStandardCode) == null ? 0 :  mapLicense.get(accId).get(ServerSWStandardCode).amount);
  }
  private void addLicenseMap(Id accId,string family, decimal quantity, Date experation)
  {
     map<string,helpLicense> mapTemp = mapLicense.containsKey(accId)?mapLicense.get(accId):new map<string,helpLicense>() ;
     decimal priorQuantity = mapTemp.containsKey(family)?mapTemp.get(family).amount:0;
             if(mapTemp !=null)
             {             
                mapTemp.put(family,upsertHelpLicense(mapTemp.get(family),quantity,experation ) );                                  
                mapLicense.put(accId,mapTemp);
             }
             else
               mapLicense.put(accId,new map<string,helpLicense>{family => upsertHelpLicense(mapTemp.get(family),quantity,experation ) });
  } 
  
  private list<Asset> getAssets(list<id> listId)
     {
     
       return [select id,
                      accountId,
                        Quantity_Attribute__c,
                        Product2.WDSB_Sub_Family__c,
                        Product2.ProductCode                          
                 from Asset 
                 where Product2id != null and Product2.Family = 'Software License' and accountid in : listId ];
       
      }
      
   private list<SBQQ__Subscription__c> getSubscriptions(list<id> listId)
     {
     
       return [select id,
                      SBQQ__Account__r.id,
                        Subscription_Quantity_Formula__c,
                        SBQQ__Product__r.WDSB_Sub_Family__c,
                        SBQQ__Product__r.ProductCode,
                        SBQQ__EndDate__c                       
                 from SBQQ__Subscription__c 
                 where  SBQQ__Product__c !=null and SBQQ__Product__r.Family in( 'Software License', 'Content Dictionary') and SBQQ__Account__c in : listId and SBQQ__EndDate__c > Today];
     
      }
      
    private helpLicense upsertHelpLicense(helpLicense hl, decimal amount, Date experation)
    {
        if(hl == null)
          return new helpLicense(amount,  experation);
        else
        {
            hl.amount+=amount;
            hl.experationDate =  hl.experationDate == null || (experation != null && experation < hl.experationDate) ?experation: hl.experationDate;
            return hl;
        }
    }
    
    private Date getRelevantDate(Date componentDate , Boolean componentExist, Date licenseHeaderDate )
    {
      return componentDate != null ?  componentDate : componentExist && licenseHeaderDate !=null ? licenseHeaderDate :null; 
    }
    
    private class HelpLicense
    {
      public id accountId;
      public decimal amount ;
      public Date experationDate;
      
      public helpLicense(decimal amountParam, Date experationParam)
      {
        amount = amountParam;
        experationDate = experationParam;
      }
      
      
      
      
    }
   
}