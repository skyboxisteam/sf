global class CaseWeeklyBatch implements Database.Batchable<sObject>,Schedulable {
	//global final String defaultQuery = 'select id, name from Account where RecordTypeId = \'012600000009O6t\' ';
	String query;
	global final String defaultQuery = 'select id,Subject from Case WHERE (Owner.UserRole.Name=\'TLS\' OR Owner.Name=\'Incoming Cases - R&D\') AND Severity1__c = \'1\' AND (Status = \'Work In Progress\' OR Status = \'Pending Customer\'' +
	' OR Status = \'Assigned to R&D\') AND  Calc_Assigned_to_R_D_Date__c.addDays(7)< System.now()';

	global CaseWeeklyBatch() {
		query = defaultQuery;
	}
    global CaseWeeklyBatch(String s) {
        query = 'select id from Case LIMIT 200';
    }
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}
   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
  
   		List<Case> updateCaseList = new List<Case>();
	    for (sObject s : scope) {
			Case c = (Case)s;
			c.Weekly_update__c = true;
            System.debug(c.Subject);
			updateCaseList.add(c);
		}
		update updateCaseList;
	}
	global void execute(SchedulableContext sc){ Database.executeBatch(this,10); }

	global void finish(Database.BatchableContext BC) {
	}	
}