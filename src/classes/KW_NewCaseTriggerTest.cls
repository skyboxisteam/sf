@IsTest
public class KW_NewCaseTriggerTest {
    @IsTest
    private static void Test() {
       
        createTeamRole();
        CaseTriggerSettings__c cs = new CaseTriggerSettings__c(
        UserRoleName__c='TLS', 
        QueueName__c='Incoming Cases - R&D',
        Name='Case Owner Trigger',
        CaseTeamRoleName__c='R&D SupportTEST');
        insert cs;

        Test.startTest();
        Case caseItem = new Case();
        caseItem.Subject = 'Test Case';
        insert caseItem;
        Test.stopTest();
    }
    
    @future
    private static void createTeamRole()
    {
        CaseTeamRole ctr = new CaseTeamRole(Name='R&D SupportTEST', AccessLevel='Read');
        insert ctr;
    }
    
}