/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TerritoryLegacyUpdateBatchTest {

    static testMethod void TerritoryLegacyUpdateBatch() 
    {
           ClsObjectCreator cls = new ClsObjectCreator();

    
    
      Territory2Model ter2mod = [Select id from Territory2Model limit 1];
      system.debug('@@@ter2mod ' + ter2mod);
      Territory2Type ter2type = [select id from Territory2Type limit 1];
      system.debug('@@@ter2type ' + ter2type);

            ClsObjectCreator.createTerritory(ter2mod.Id,ter2type.Id,'TestTerritory1');
            ClsObjectCreator.createTerritory(ter2mod.Id,ter2type.Id,'TestTerritory2');
      list<Territory2> ter =  [select id ,Name,DeveloperName,Territory2ModelId,Territory2TypeId from Territory2 ];
      
      
      
   Test.startTest();
      Account acc = cls.createAccount('TestAcc1');
      Account acc2 = cls.createAccount('TestAcc2');

      ObjectTerritory2Association juncObj = cls.createJuncAccoutToTerritory(acc.Id,ter[0].Id,'Territory2Manual');
      
    
      
            TerritoryLegacyUpdateBatch h = new TerritoryLegacyUpdateBatch();
            String sch = '0 0 23 * * ?'; 
            String jobId = System.schedule('TerritoryLegacyUpdateBatch', sch, h); 
        
    Test.stopTest();

        
    }
}