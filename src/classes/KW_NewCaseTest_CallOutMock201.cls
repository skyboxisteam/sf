@isTest
global class KW_NewCaseTest_CallOutMock201 implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        //response.setHeader('Content-Type', 'application/json');
        response.setHeader('X-Accellion-Location', 'https://kw-upload1.skyboxsecurity.com/rest/folders/28362');
        response.setBody('{"blabla":"nanananan","uploadLink":"http://bla.com/asd123#2","request_file_ref":"123","created":"11/12/23","name":"blabla"}');
        response.setStatusCode(201);
        return response;
    }
}