public with sharing class CommissionPlanHandler {
	public CommissionPlanHandler() {
		
	}

	/**
    *@Author Rina Nachbas                           events: After Insert
    *@Date 20.11.18
    *@Task: CM-51
    *@Description : Create Default Rules to new commission plan
    */ 
	public static void createDefaultCommissionRule(List<Commission_Plan__c> newCP){
		List<Commission_Rule__c> crList = [select Id, Name from Commission_Rule__c where Default__c=true];
		List<Commission_Rule_Assignment__c> newCRAList = new List<Commission_Rule_Assignment__c>();
		for(Commission_Plan__c cp: newCP){
			for(Commission_Rule__c cr : crList){
				newCRAList.add(new Commission_Rule_Assignment__c(Commission_Plan__c=cp.Id,Commission_Rule__c=cr.Id, Name=cr.Name));	
			}
		}
		if(newCRAList.size()>0)
			insert newCRAList;
	}
}