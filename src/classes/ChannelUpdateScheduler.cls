global class ChannelUpdateScheduler implements Schedulable {
   
   global void execute(SchedulableContext ctx) {
      ChannelUpdateBatch batch = new ChannelUpdateBatch();
      batch.runBatch();
//      Database.executeBatch(batch);
   }   
}