public class CommissionRecordsExtension {
    
    public List<UserWrapper> selectedUser {get;set;}
    public static List<UserWrapper> allUser = new List<UserWrapper>();
    private Map<Id,UserWrapper> idObjectMap = new Map<Id,UserWrapper>();
    public List<UserWrapper> userWrapper {get;set;}
    public boolean userTable{ get;set;}
    public boolean selectedTable{ get;set;}
    public boolean submitBtn{ get;set;}
    public boolean selectAllObject{get;set;}
    public boolean noUserSelected{get;set;}
    public String selYear{ get;set;}
    public String selType{ get;set;}
    public String selCommissionTeam{ get;set;}
    public string theater = 'All';
    public List<Id> usersId{ get;set;}
    List<User> userList{get;set;}
    public Map<User,String> userCommPlan = new Map<User,String>();
    public Id recordId;
    List<Commission_Plan__c> commissionPlanList = new List<Commission_Plan__c>();
    Map<Id,List<selectOption>> userCommisionMap = new Map<Id,List<selectOption>>();
    public ApexPages.StandardSetController con {
        get {
            if(con == null) {
                con = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id, Name,Theater__c,(Select id,name from Commission_Plans__r) FROM User where id in (select Sales_User__c from Commission_Plan__c) Order By Name  ]));
                con.setPageSize(20);
            }
            return con;
        }
        set;
    }
          public Integer pageOffSet {
        get {
            return con.getPageSize();
        }
        set {
            con.setPageSize(value);
        }
    }
    
    List<String> role = new List<String>();
    public CommissionRecordsExtension(ApexPages.StandardController ctlr) {
        recordId = ctlr.getRecord().Id;   
        userWrapper = new List<UserWrapper>();
        selectedUser = new List<UserWrapper>();
        usersId = new List<Id>();
        selectedTable = false;
        userTable = false;
        submitBtn = true;
        selYear = '2016';
        allUser = new List<UserWrapper>();
        selectAllObject = false;
        idObjectMap = new Map<Id,UserWrapper>();
    }
     static final Set<String> rtlLanguages = new Set<String> {'iw'};
     public String rtlMagic {
        get {
            return rtlLanguages.contains(UserInfo.getLanguage()) ? 'rtlMagicFlip' : null;
        }
        set;
    }
    public void setTheater(String t){
        theater = t;
    }
    
    public string getSelectedYear(){
        return selYear;
    }
    public void setSelectedYear(String y){
        selYear = y;
    }   
    public List<selectOption> getYear() {
        List<selectOption> options = new List<selectOption>();
        Schema.DescribeFieldResult fieldResult =  Commission_Plan__c.Fiscal_Year__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    public List<selectOption> getType() {
        List<selectOption> options = new List<selectOption>();
        Schema.DescribeFieldResult fieldResult =  Commission_Record__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }  
   /* public Set<selectOption> getTheater() {
        Set<SelectOption> options = new Set<SelectOption>();
        Schema.DescribeFieldResult fieldResult =  User.Theater__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        Integer placeOfRelevantTheater = -1;
        Integer counter =0;
        string theaterFromCaommissionTeam = ''; 
          if(selCommissionTeam!=null)
           {
            Commission_Team__c com = [Select id, name,Theater__c  from Commission_Team__c where id=: selCommissionTeam] ;
            if(com !=null)
             for( Schema.PicklistEntry f : ple)
             {
                if(theaterFromCaommissionTeam ==f.getLabel())
                options.add(new SelectOption(f.getLabel(), f.getValue()));
                       
             } 
         }    
         
         
        options.add(new SelectOption('All', 'All'));
        for( Schema.PicklistEntry f : ple)
        {
            if(theaterFromCaommissionTeam !=f.getLabel())
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }   
        return options;
    } */
    
     public List<selectOption> getTheater() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult =  User.Theater__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         
        string theaterFromCaommissionTeam = '';       
        if(selCommissionTeam!=null && selCommissionTeam != 'All' && selCommissionTeam != 'NoTeamExist')
           {
            Commission_Team__c com = [Select id, name,Theater__c  from Commission_Team__c where id=: selCommissionTeam] ;
            if(com !=null)
             {
              theaterFromCaommissionTeam = com.Theater__c ;
              for( Schema.PicklistEntry f : ple)
                 if(theaterFromCaommissionTeam ==f.getLabel())
                  {
                   options.add(new SelectOption(f.getLabel(), f.getValue()));
                   break;
                  }
                 
              } 
            }    
    
        options.add(new SelectOption('All', 'All'));
      
        for( Schema.PicklistEntry f : ple)
        {
            if(theaterFromCaommissionTeam !=f.getLabel())
               options.add(new SelectOption(f.getLabel(), f.getValue()));
               
        }       
        return options;
    } 
   
    public List<selectOption> getCommissionTeam() {
        List<SelectOption> options = new List<SelectOption>();
        List<Commission_Team__c> commissionTeam;
        if(theater != 'All')
              commissionTeam = [Select id, name from Commission_Team__c where Theater__c =: theater];
        Else
            {
              commissionTeam = [Select id, name from Commission_Team__c];
              options.add(new SelectOption('All', 'All'));
            }
        for( Commission_Team__c cmsTeam : commissionTeam )
        {
            options.add(new SelectOption(cmsTeam.id, cmsTeam.name ));
        } 
        if(options.isempty())
             options.add(new SelectOption('NoTeamExist', 'No Team Available',false));    
        return options;
    } 
    
    public List<UserWrapper> getUsers() {
System.debug(idObjectMap.keySet());
        boolean var = false;
        userWrapper = new List<UserWrapper>();
        for(User user :(List<User>)con.getRecords()){
            
            if(idObjectMap.containsKey(user.id)){
                 userWrapper.add(idObjectMap.get(user.id));
            }else if(userCommisionMap.get(user.id) != null){
                 userWrapper.add(new UserWrapper(user,userCommisionMap.get(user.id),userCommPlan.get(user)));
            }
        }
        return userWrapper;
    }
    public PageReference searchUsers() {
        
        try {
            UserException ex = null;
           if(selCommissionTeam !='All' &&  selCommissionTeam !='NoTeamExist')
              {
                userList =  [Select Id, Name,Theater__c,(Select id,name from Commission_Plans__r ) FROM User where  id in (select Sales_User__c from Commission_Plan__c where Fiscal_Year__c=:selYear) and id in ( select Member__c from Commission_Team_Member__c where Commission_Team__c =:selCommissionTeam ) Order By Name  ];
                con = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id, Name,Theater__c,(Select id,name from Commission_Plans__r ) FROM User where  id in (select Sales_User__c from Commission_Plan__c where Fiscal_Year__c=:selYear) and id in ( select Member__c from Commission_Team_Member__c where Commission_Team__c =:selCommissionTeam ) Order By Name  ]));
                con.setPageSize(20);
              } 
           else if(theater!='All'){
                userList =  [Select Id, Name,Theater__c,(Select id,name from Commission_Plans__r ) FROM User where (Theater__c =:theater or Theater__c = 'Global') and id in (select Sales_User__c from Commission_Plan__c where Fiscal_Year__c=:selYear) Order By Name  ];
                con = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id, Name,Theater__c,(Select id,name from Commission_Plans__r ) FROM User where (Theater__c =:theater or Theater__c = 'Global') and id in (select Sales_User__c from Commission_Plan__c where Fiscal_Year__c=:selYear) Order By Name  ]));
                con.setPageSize(20);
                // con = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id, Name,Theater__c FROM User where Theater__c =:theater Order By Name  ]));
            }else{
                userList = [Select Id, Name,Theater__c,(Select id,name from Commission_Plans__r ) FROM User where Theater__c!=null and id in (select Sales_User__c from Commission_Plan__c where Fiscal_Year__c=:selYear) Order By Name  ];
                con = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id, Name,Theater__c,(Select id,name from Commission_Plans__r ) FROM User where Theater__c!=null and id in (select Sales_User__c from Commission_Plan__c where Fiscal_Year__c=:selYear) Order By Name  ]));
                con.setPageSize(20);
            }
            commissionPlanList = [select name,OwnerId,Sales_User__c,Fiscal_Year__c from Commission_Plan__c where Fiscal_Year__c=:selYear];
            
            //CM-16 - Added by RinaN  25.6.18
            Set<Id> cpWithPIORule = new Set<Id>();
            if(selType == 'PIO'){
                List<Commission_Rule_Assignment__c> commissionRuleAssList = [SELECT Id, Commission_Plan__c ,Name, Type__c
                                                                            FROM Commission_Rule_Assignment__c
                                                                            WHERE Commission_Plan__c in :commissionPlanList AND Type__c = 'Annual PIO Accelerator'];
                for(Commission_Rule_Assignment__c cra: commissionRuleAssList){
                    cpWithPIORule.add(cra.Commission_Plan__c);
                }
            }

            for(User user : userList){
                List<selectOption> temp = new  List<selectOption>();
                for(Commission_Plan__c cP :commissionPlanList){
                    if(cP.Sales_User__c == user.id && (selType != 'PIO'|| selType == 'PIO' && cpWithPIORule.contains(cP.Id))){
                        temp.add(new SelectOption(cP.Name,cP.Name));
                    }
                }
                if(temp.size()>0){
                      userCommisionMap.put(user.id,temp);  
                }
                
            }
            getUsers();
            if(userWrapper.size()==0){
                userTable = false;
                submitBtn = true;
                ex = new UserException();
                ex.setMessage('No users found');
                throw ex;
            }
            
        }catch (Exception ex) {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage()));
            return null;
        } 
        userTable = true;
        submitBtn = false;
        return null;
    }
    
    public void selectObject() {
        if( selectedUser==null ||  selectedUser.size()==0){
            selectedUser = new List<UserWrapper>();
        }
        if(idObjectMap == null || idObjectMap.size()==0){
            idObjectMap = new Map<id,UserWrapper>();
        }
        for(UserWrapper uW:userWrapper) {
            if(uW.checked == true) {
                if(idObjectMap.containsKey(uW.user.id)){continue;}
                selectedUser.add(new UserWrapper(uW.user,userCommisionMap.get(uW.user.id),uW.commissionPlan,uW.percent));
                idObjectMap.put(uW.user.id,uW);
            }else{
                idObjectMap.remove(uW.user.id);
            }
        }
    }         
    public void getSelectedUser(){
       noUserSelected = false;
       selectObject();
       selectedUser = new List<UserWrapper>(idObjectMap.values());
        try{
            UserException ex = null;
            if(selectedUser.size()==0){
                noUserSelected = true;
                userTable = true;
                selectedTable = false;
                ex = new UserException();
                ex.setMessage('No users selected');
                throw ex;   
            }else{
                userTable = false;
                selectedTable = true;
                noUserSelected = false;
            }
        }catch(Exception ex){
        }
      
    }
  
    public void back() {
      selectAllObject = false;
      userTable = true;
      submitBtn = true;
      selectedTable = false;
      userWrapper = null;
      selectedUSer = null;
      searchUsers();
    }
    public void backToSearch() {
      selectAllObject = false;
      userTable = false;
      submitBtn = true;
      selectedTable = false;
      userWrapper = null;
      selectedUSer = null;
   //   searchUsers();
    }
    public PageReference saveI() {
        selectedTable = false;
        userTable = false;
        List<String> planNames = new List<String>();
        Map<String,Integer> planCreditedMap = new Map<String,Integer>();
        for(UserWrapper op : selectedUser){
            planNames.add(op.commissionPlan);
            System.debug(op.percent);
            planCreditedMap.put(op.commissionPlan,Integer.valueof(op.percent));
        }
        commissionPlanList = [select id,Name from Commission_Plan__c where name in :planNames];
        System.debug('list');
        List<Commission_Record__c> commissionRecordList = new List<Commission_Record__c>();
        for(Commission_Plan__c cP : commissionPlanList){
            Commission_Record__c objectR = new Commission_Record__c();
            objectR.Opportunity__c = recordId; 
            objectR.Commission_Plan__c = cP.Id;
            objectR.Credited__c = Decimal.valueOf(planCreditedMap.get(cP.Name));
            objectR.Type__c = selType;//Added by RinaN 10.6.18 CM-16
            commissionRecordList.add(objectR);
        }
        System.debug('ins before');
        insert commissionRecordList;
        System.debug('ins after');
        PageReference opp = new PageReference('/' + recordId);
        opp.setRedirect(true);
        System.debug('save');
        return opp;
    }
    
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
    
    public void first() {
        selectObject();
        con.first();
        con.setPageSize(20);
        getUsers();
    
    }
    
    public void last() {
        selectObject();
        con.last();
        con.setPageSize(20);
        getUsers();

    }
    
    public void previous() {
        selectObject();
        con.previous();
        con.setPageSize(20);
         getUsers();
    //    prevObj();
    }
    
    public void next() {
        selectObject();
        con.next();
        con.setPageSize(20);
        getUsers();
      //  prevObj();
    }
     public PageReference  cancelOpp() {
     
      PageReference opp = new PageReference('/' + recordId);
      opp.setRedirect(true);
      return opp;
    }
    private class UserException extends Exception {}
   
    public class UserWrapper {
        public Boolean checked{ get; set; }
        public User user { get; set;}
        public String theater { get; set;}
        public String commissionPlan { get; set;}
        public Integer percent{ get; set;}
        public List<SelectOption> selectOptions{ get; set;}
      
         public UserWrapper(User u,List<SelectOption> sOp){
            user = u;
            selectOptions = sOp;
            checked = false;
        }
        public UserWrapper(User u,List<SelectOption> sOp,String cP){
            user = u;
            selectOptions = sOp;
            checked = false;
            commissionPlan = cP;
            percent = 100;
        }
         public UserWrapper(User u,List<SelectOption> sOp,String cP,Integer perc){
            user = u;
            selectOptions = sOp;
            checked = false;
            commissionPlan = cP;
            percent = perc;
        }
         public UserWrapper(User u,List<SelectOption> sOp,String cP,Integer perc,Boolean ch){
            user = u;
            selectOptions = sOp;
            checked = ch;
            commissionPlan = cP;
            percent = perc;
        }
    }
}