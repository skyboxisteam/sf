public with sharing class PSEstimatorHandler {

public string PendingEstimationStatus = 'Pending Estimation'; 
public string PendingSubmissionStatus = 'Pending Submission'; 
public string submitApprovalStatus = 'Submitted for Approval'; 
public string ErrorStatus = 'Estimation Error';
public string SEProfile = 'Skybox Pre-Sales User'; 
public string InterfaceId ='22079';
public string IconductParam;

public PSEstimatorHandler()
{
}

public PSEstimatorHandler(list<PS_Estimation__c> PSList,map<id,PS_Estimation__c> PSNewMap , map<id,PS_Estimation__c> PSOldMap,boolean isInsert, boolean isAfter)
  {
   system.debug('PSNewMap ' + PSNewMap + ' PSList' + PSList); 
    for(PS_Estimation__c pse: PSNewMap==null? PSList :getPSEstimation(PSNewMap.values()) )
    {
    	system.debug('After ' + isAfter + ' isInsert' + isInsert); 
     if(!isAfter && pse.opportunity__c != null &&(isInsert || (pse.opportunity__c != PSOldMap.get(pse.id).opportunity__c)))
       {
        pse.account__c = [select accountid from opportunity where id =: pse.opportunity__c limit 1].accountid;               
       }
     if(pse.Get_Estimation__c == true  )
     {
       updateJsonRequest(pse); 
      }
     if((!isinsert && (( pse.JSON_Responce__c <> PSOldMap.get(pse.id).JSON_Responce__c) && pse.JSON_Responce__c <>null )))
       
        updateJsonResponce(pse,isAfter);
    
    }
  }
     
   private void updateJsonRequest(PS_Estimation__c pse)
   {
       JSONGenerator gen = JSON.createGenerator(true);
       gen.writeStartObject();
       gen.writeStringField('submitDate',string.valueOfGmt(system.now()) ==null?string.valueOfGmt(datetime.now()):string.valueOfGmt(system.now()));
       
       gen.writeNumberField('fa_licenses',pse.FA_licenses__c==null?0:pse.FA_licenses__c );
       gen.writeNumberField('tm_licenses',pse.TM_Licenses__c==null?0:pse.TM_Licenses__c );     
       gen.writeNumberField('na_virtual_licenses',pse.NA_Virtual_Licesns__c==null?0:pse.NA_Virtual_Licesns__c); 
       gen.writeNumberField('na_licenses',pse.NA_Licenses__c==null?0:pse.NA_Licenses__c );    
       gen.writeNumberField('vc_licenses',pse.VC_Licenses__c==null?0:pse.VC_Licenses__c );
       gen.writeNumberField('numberOfCollectionSources',pse.Number_of_Collection_Sources__c==null?0:pse.Number_of_Collection_Sources__c );    
       gen.writeNumberField('numberOfVendors',pse.Number_of_Vendors__c==null?0:pse.Number_of_Vendors__c );   
       gen.writeNumberField('cloud_numberOfAccounts',pse.Number_of_Accounts__c==null?0:pse.Number_of_Accounts__c );    
       gen.writeNumberField('cloud_numberOfCloudProviders',pse.Number_of_Cloud_Providers__c==null?0:pse.Number_of_Cloud_Providers__c ); 
       gen.writeNumberField('vc_numberOfScanners',pse.Number_of_Scanners__c==null?0:pse.Number_of_Scanners__c ); 
       gen.writeBooleanField('is_cm',pse.Is_CM__c); 
       gen.writeBooleanField('is_tm_licensed',pse.Is_TM_Licensed__c);
       gen.writeBooleanField('is_cmdb',pse.IS_CM_DB__c);
       gen.writeBooleanField('is_vd',pse.Is_VD__c);
       gen.writeBooleanField('is_checkpoint',pse.Checkpoint_Firewalls__c);
       gen.writeBooleanField('is_custom_cmdb',pse.Is_Custom_CM_DB__c);   
       gen.writeBooleanField('is_lea',pse.Is_LEA__c);       
       gen.writeNumberField('lea_numberOfLogServers',pse.LEA_Number_Of_Log_Servers__c==null?0:pse.LEA_Number_Of_Log_Servers__c );
       gen.writeBooleanField('is_syslog',pse.Is_SYSLOG__c);
       gen.writeBooleanField('is_nrt',pse.Is_NRT__c);  
       gen.writeBooleanField('is_cmRuleCert',pse.Is_CM_Rule_Certificated__c);  
       gen.writeBooleanField('is_cloud',pse.Is_Cloud__c);   
       gen.writeBooleanField('is_repo',pse.Is_Repository__c); 
       gen.writeBooleanField('is_ips',pse.IPS_Devices__c);
       gen.writeBooleanField('is_routingDifficult',pse.Is_Repository__c);
       gen.writeBooleanField('is_networkMap',pse.Network_Map_Assistance_Required__c);
       gen.writeBooleanField('is_repoUnsupported',pse.Is_Repository_Supported__c);
       gen.writeBooleanField('is_complexWan',pse.Is_Complex_lex_Wan__c);    
       gen.writeStringField('difficulty',pse.Difficulty__c == null?'':pse.Difficulty__c);
       gen.writeStringField('estimatorRequestSource',pse.Request_Source__c == null?'':pse.Request_Source__c);
       gen.writeStringField('unsupportedRepoList',pse.Un_Supported_Repository_NAME_LOE_Ticket__c == null?'':pse.Un_Supported_Repository_NAME_LOE_Ticket__c);
       gen.writeBooleanField('is_L2',pse.Layer_2_Firewall_Modeling__c);
       gen.writeBooleanField('is_highLocCount',pse.More_than_10_Locations__c); 
       gen.writeStringField('platformType',pse.Server_Type__c ==null?'':pse.Server_Type__c);
       gen.writeNumberField('numOfServers',pse.Total_Number_of_Servers__c==null?0:pse.Total_Number_of_Servers__c);    
       gen.writeNumberField('numOfCollectors',pse.Total_Number_of_External_Collectors__c==null?0:pse.Total_Number_of_External_Collectors__c);     
       gen.writeBooleanField('is_ha',pse.HA_Required__c); 
       gen.writeBooleanField('unsupportedDevices',pse.UnSupported_Devices__c); 
       gen.writeStringField('unsupportedDevicesList',pse.List_All_Unsupported_Devices__c==null?'':pse.List_All_Unsupported_Devices__c);
       gen.writeStringField('sf_theater',pse.Opportunity__r.Account.Theater__c==null?'':pse.Opportunity__r.Account.Theater__c);
       gen.writeStringField('sf_account',pse.Opportunity__r.Account.Name==null?'':pse.Opportunity__r.Account.Name);
       gen.writeStringField('sf_submitterName',pse.CreatedBy.Name==null?'':pse.CreatedBy.Name);
       gen.writeStringField('sf_recordId',pse.id==null?'':pse.id);
       gen.writeStringField('sf_estimationName',pse.Name==null?'':pse.Name);
       gen.writeBooleanField('is_vdNetworkDevices',pse.Vulnerability_Detector_for_Network_Devic__c);
     
       
    
     pse.JSON__c = gen.getAsString();
     // Nadavg, 26.04.2018 Canceled status update  
   //  pse.Status__c = PendingEstimationStatus;
     pse.Estimation_Days__c = null;
     pse.PS_Comment__c = null;
     pse.JSON_Responce__c = null;
     pse.Message__c = null;    
     pse.Get_Estimation__c = false;
     update pse;
     system.debug('id is ' + pse.id);
     IconductParam = '{  "Key":"EstimationId","Value":"'+pse.id+'"}';
     APIToIconduct.callIconduct(InterfaceId,IconductParam );
    
    }
    
    
   public void updateJsonResponce(PS_Estimation__c pse, boolean isAfter)
     {
         string message ='';
         JSONParser parser = JSON.createParser(pse.JSON_Responce__c);
         while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if(fieldName == 'high'){
                        pse.High__c = decimal.valueof(parser.getText()) ;                      
                        continue;
                    }
                     if(fieldName == 'normal'){
                        pse.Normal__c = decimal.valueof(parser.getText()) ;                   
                        continue;
                    }
                     if(fieldName == 'low'){
                        pse.Low__c = decimal.valueof(parser.getText()) ;
                        continue;                                      
                    }
                 /*    if(fieldName == 'estimateDate'){
                        pse.Estimation_date__c = datetime.parse(parser.getText()) ;
                        continue;                                      
                    }*/
                     if(fieldName == 'status'  ){
                       if(parser.getText() == 'success')
                       {
                         // Nadavg, 26.04.2018 Canceled status update  
                    //    pse.Status__c =  PendingSubmissionStatus ;
                        pse.Message__c = 'Estimation Received';
                      // Nadavg, 26.04.2018 Canceled submit for approval  
                      //  submitForApproval(pse.id, pse.ownerId);
                         }
                       else
                        pse.Status__c = ErrorStatus ;
                        continue;
                     }
                      system.debug('####' + parser.getText() );
                      if(fieldName.contains('error') ){
                         message += parser.getText() + ' , ';
                           system.debug('$$$$' + parser.getText() );
                        continue;
                     }
                   
                    /*  if(fieldName == 'estimateDate'){
                        pse.Estimation_date__c =  DateTime.parse('11/6/2014 12:00 AM') ;
                        continue;
                      }*/
                    
                }
            }
              pse.Message__c = message.left(message.length()-3) ;
              update pse;
       }
   private list<PS_Estimation__c> getPSEstimation( list<PS_Estimation__c> listPSEst)
   {
      return [select id,
                    Name,
                     Status__c,
                     Message__c,
                     ownerId,
                     Normal__c,
                     Low__c,
                     High__c,
                     JSON_Responce__c,
                     FA_licenses__c,
                     TM_Licenses__c,
                     NA_Virtual_Licesns__c,
                     NA_Licenses__c,
                     VC_Licenses__c,
                     Number_of_Collection_Sources__c,
                     Number_of_Vendors__c,
                     Number_of_Accounts__c,
                     Number_of_Cloud_Providers__c,
                     Number_of_Scanners__c,
                     Is_CM__c,
                     Is_TM_Licensed__c,
                     IS_CM_DB__c,
                     Is_VD__c,
                     Checkpoint_Firewalls__c,
                     Is_Custom_CM_DB__c,
                     Is_LEA__c,
                     LEA_Number_Of_Log_Servers__c,
                     Is_SYSLOG__c,
                     Is_NRT__c,
                     Is_Cloud__c,
                     Is_Repository__c,
                     IPS_Devices__c,
                     Network_Map_Assistance_Required__c,
                     Is_Repository_Supported__c,
                     Vulnerability_Detector_for_Network_Devic__c,
                     Is_Complex_lex_Wan__c,
                     Difficulty__c ,
                     Request_Source__c ,
                     Un_Supported_Repository_NAME_LOE_Ticket__c ,
                     Layer_2_Firewall_Modeling__c,
                     More_than_10_Locations__c,
                     Server_Type__c ,
                     Total_Number_of_Servers__c,
                     Total_Number_of_External_Collectors__c,
                     UnSupported_Devices__c,
                     List_All_Unsupported_Devices__c,
                     JSON__c ,
                    Estimation_Days__c ,
                    PS_Comment__c,
                    Get_Estimation__c,
                    Is_CM_Rule_Certificated__c,
                    HA_Required__c,
                    Opportunity__c,
                    Opportunity__r.Account.Theater__c,
                    Opportunity__r.Account.Name,
                    CreatedBy.Name
                 FROM PS_Estimation__c
                 WHERE id in:listPSEst];
   
     
   }
    @future(callout=true)
   private static void submitForApproval(id PsID, id ownerid)
   {
   //  user owner = [select id from user where id=:ownerId limit 1];
    // system.runAs(owner)
     //{
       Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();                     
       req.setObjectId(PsID);
       Approval.ProcessResult result = Approval.process(req);
    // }               
   }
}