@isTest (SeeAllData=true)
public class ChannelUpdateBatchTest {
	static testMethod void ChannelUpdateBatchTest() {
		String query = 'select id, name from Account where RecordTypeId = \'012600000009O6t\' and Name = \'Accuvant, Inc.\' ';
    	ChannelUpdateBatch batch = new ChannelUpdateBatch(query);
    	
      	Database.executeBatch(batch, 10);
		
		System.assert(batch != null);
	}
}