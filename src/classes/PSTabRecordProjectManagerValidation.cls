public with sharing class PSTabRecordProjectManagerValidation {

public static void validate(map<id,PS_Tab_Record__c> psRecordMap)
 {
  map<id,PS_Tab_Record__c> psTabMap =new map<id,PS_Tab_Record__c>();
  list<PS_Tab_Record__c>  psRelevantList= [select id,type__c,Hours__c,PS_Tab__c,PS_Tab__r.type__c 
                             from PS_Tab_Record__c
                             where  id in:psRecordMap.keyset() and PS_Tab__r.type__c in ('Billable PS Days','Billable Training Days','Fixed Price')] ;
  
  for(PS_Tab_Record__c PTR :psRelevantList)
      psTabMap.put(PTR.PS_Tab__c,psRecordMap.get(PTR.id));
     
  AggregateResult[] billedCreaditSum = [select PS_Tab__c, SUM(Hours__c) Hours
                                      from PS_Tab_Record__c 
                                      where type__c in ('Credit','Billed') and PS_Tab__c in : psTabMap.keyset()
                                      group by PS_Tab__c];
  
   AggregateResult[] projectManagmentSum = [select PS_Tab__c, SUM(Hours__c) Hours
                                      from PS_Tab_Record__c 
                                      where type__c in ('Project Management') and PS_Tab__c in : psTabMap.keyset()
                                      group by PS_Tab__c];
                                      
   for(Integer i = 0;i<projectManagmentSum.size();i++)
     for(Integer j=0;j<billedCreaditSum.size();j++)
       if(projectManagmentSum[i].get('PS_Tab__c') == billedCreaditSum[j].get('PS_Tab__c') && (Decimal)projectManagmentSum[i].get('Hours') > (Decimal)billedCreaditSum[j].get('Hours')*0.2) 
          psTabMap.get(Id.valueOf(string.valueOf(projectManagmentSum[i].get('PS_Tab__c'))) ).addError('"Project Managent" cannot to be over 20% of "Creadit"+"Billed"');
 }

}