/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CPQMethodsTest {
  private static string renewal = 'Renewal';
  private static string amendment = 'Amendment';
  private static string newBusiness = 'New Business';
  private static string stageWon = 'Closed /Won';
  private static string stageReady2Book = 'Ready to book';

    @testSetup static void setupData() {
       
        Test.startTest();
       ClsObjectCreator cls = new ClsObjectCreator();
       Account a = cls.CreateAccount('Test');
       opportunity opp = cls.createOpportunity(a);
       SBQQ__Quote__c q = cls.createQuote(ClsObjectCreator.createContact('Test',a.id),'Draft');
       Contract c = cls.createContract(a,opp,q);
        Test.stopTest();

    }

    static testMethod void renewalQuoteTest() {
       Account  a = [select Id from Account Limit 1];
       Contract c = [select Id from Contract Limit 1];
       Contact con = [select Id from Contact Limit 1];
       Opportunity opp = [select Id from Opportunity Limit 1];
       //SBQQ__Quote__c q = [select Id from SBQQ__Quote__c Limit 1];
       //q.SBQQ__Opportunity2__c = opp.Id;
       //update q;
       //c.SBQQ__RenewalQuoted__c = true;
       //c.SBQQ__RenewalOpportunity__c = null;
       //Test.startTest();
       //System.debug('@@@renewalQuoteTest: before renewal: '+ c);
       //update c;
       //Test.stopTest();
//
       //List<SBQQ__Quote__c> qTest = [select Id from SBQQ__Quote__c];
       //System.debug('@@@renewalQuoteTest: qTest.size(): '+ qTest.size());

       SBQQ__Quote__c qu = new SBQQ__Quote__c(SBQQ__SubscriptionTerm__c = 13);
      qu.SBQQ__Status__c = 'Draft';
      qu.SBQQ__Account__c = a.Id;
      qu.WDSB_CustomerContact__c = con.id;
      qu.Pending_approval_timestamp__c = system.now() - 8;
      qu.SBQQ__Type__c = renewal;
      qu.SBQQ__Opportunity2__c = opp.Id;
      insert qu;
    }

    static testMethod void amendmentQuoteTest() {
       Account  a = [select Id from Account Limit 1];
       Contract c = [select Id from Contract Limit 1];
       Contact con = [select Id from Contact Limit 1];
       Opportunity opp = [select Id from Opportunity Limit 1];
       
       SBQQ__Quote__c qu = new SBQQ__Quote__c(SBQQ__SubscriptionTerm__c = 13);
      qu.SBQQ__Status__c = 'Draft';
      qu.SBQQ__Account__c = a.Id;
      qu.WDSB_CustomerContact__c = con.id;
      qu.Pending_approval_timestamp__c = system.now() - 8;
      qu.SBQQ__Type__c = amendment;
      qu.SBQQ__Opportunity2__c = opp.Id;
      insert qu;
    }

    static testMethod void newBusinessQuoteTest() {
       Account  a = [select Id from Account Limit 1];
       Contract c = [select Id from Contract Limit 1];
       Contact con = [select Id from Contact Limit 1];
       Opportunity opp = [select Id from Opportunity Limit 1];
       
       SBQQ__Quote__c qu = new SBQQ__Quote__c(SBQQ__SubscriptionTerm__c = 13);
      qu.SBQQ__Status__c = 'Draft';
      qu.SBQQ__Account__c = a.Id;
      qu.WDSB_CustomerContact__c = con.id;
      qu.Pending_approval_timestamp__c = system.now() - 8;
      qu.SBQQ__Type__c = newBusiness;
      qu.SBQQ__Opportunity2__c = opp.Id;
      insert qu;
    }
    
    static testMethod void myUnitTest() {
       Account  a = [select Id from Account Limit 1];
       Contract c = [select Id from Contract Limit 1];
       Opportunity opp2 = new Opportunity();
        opp2.name = 'test';
        opp2.AccountId = a.id;
        opp2.Type = 'Products';
        opp2.Amount = 56;
        opp2.CloseDate = date.today()+3;
        opp2.LeadSource = 'Direct Mail';
        opp2.StageName = 'Potential Opportunity';
        opp2.SBQQ__RenewedContract__c = c.id;
        
        Test.startTest();
        insert opp2;
        Test.stopTest();
    }

    //CPQ-123 
    //When quote approved, update related opp.WDSB_discount fields
    static testMethod void quoteApprovedUpdOppTest() {
   
      Opportunity opp = [select id from Opportunity Limit 1];
      SBQQ__Quote__c q = [select Id from SBQQ__Quote__c Limit 1];

      Integer i = 0;
      opp.WDSB_Appliance_Approved_discount_level__c = i++;
      opp.WDSB_Software_Approved_discount_level__c = i++;
      opp.WDSB_PS_Approved_discount_level__c = i++;
      opp.WDSB_Support_Approved_discount_level__c = i++;
      opp.WDSB_Training_Approved_discount_level__c = i++;
      update opp;

      Integer j = 2;
      q.WDSB_Appliance_Max_Discount__c = j++;
      q.WDSB_Software_License_Max_Discount__c = j++;
      q.WDSB_Professional_Services_Max_Discount__c = j++;
      q.WDSB_Technical_Support_Content_Max_Disc__c = null;
      q.WDSB_Training_Max_Discount__c = 0;
      update q;

      //check that opp flag before test is false
      opportunity  oppTest = [select WDSB_Software_Approved_discount_level__c from opportunity where Id = :opp.Id];
      System.assertEquals(1, oppTest.WDSB_Software_Approved_discount_level__c);
       //check that quote & opp are linked 
      SBQQ__Quote__c  qTest = [select SBQQ__Opportunity2__c, SBQQ__Status__c from SBQQ__Quote__c where Id = :q.Id];
      System.assertEquals(opp.Id, qTest.SBQQ__Opportunity2__c);
      System.assertNotEquals('Approved', qTest.SBQQ__Status__c);

      User u = [Select Id from User where profile.Name = 'Skybox Finance' and IsActive = true limit 1];
      
      Test.startTest();
        System.runAs(u) {

           q.SBQQ__Status__c = 'Approved';
           update q;
       }
      Test.stopTest();
      
      oppTest = [select WDSB_Appliance_Approved_discount_level__c,WDSB_Software_Approved_discount_level__c,
                WDSB_PS_Approved_discount_level__c, WDSB_Support_Approved_discount_level__c, WDSB_Training_Approved_discount_level__c
                from opportunity where Id = :opp.Id];
      System.assertEquals(q.WDSB_Appliance_Max_Discount__c, oppTest.WDSB_Appliance_Approved_discount_level__c);
      System.assertEquals(q.WDSB_Software_License_Max_Discount__c, oppTest.WDSB_Software_Approved_discount_level__c);
      System.assertEquals(q.WDSB_Professional_Services_Max_Discount__c, oppTest.WDSB_PS_Approved_discount_level__c);
      System.assertEquals(opp.WDSB_Support_Approved_discount_level__c, oppTest.WDSB_Support_Approved_discount_level__c);
      System.assertEquals(opp.WDSB_Training_Approved_discount_level__c, oppTest.WDSB_Training_Approved_discount_level__c);
        
    }

    //test CPQ-168 - Approved quote set related opp.Quote_Renewal_Approved__c = true flag
    static testMethod void quoteApprovedTest() {
    
      Opportunity opp = [select id from Opportunity Limit 1];
      SBQQ__Quote__c q = [select Id from SBQQ__Quote__c Limit 1];

      //check that opp flag before test is false
      opportunity  oppTest = [select Quote_Renewal_Approved__c from opportunity where Id = :opp.Id];
      System.assertEquals(false, oppTest.Quote_Renewal_Approved__c);
       //check that quote & opp are linked 
      SBQQ__Quote__c  qTest = [select SBQQ__Opportunity2__c, SBQQ__Status__c from SBQQ__Quote__c where Id = :q.Id];
      System.assertEquals(opp.Id, qTest.SBQQ__Opportunity2__c);
      System.assertNotEquals('Approved', qTest.SBQQ__Status__c);

      User u = [Select Id from User where profile.Name = 'Skybox Finance' and IsActive = true limit 1];
      
      Test.startTest();
        System.runAs(u) {

           q.SBQQ__Status__c = 'Approved';
           q.SBQQ__Type__c = CPQMethodsTest.renewal;
           update q;
       }
      Test.stopTest();
      
      oppTest = [select Quote_Renewal_Approved__c from opportunity where Id = :opp.Id];
      System.assertEquals(true, oppTest.Quote_Renewal_Approved__c);
    }

    //test CPQ-121 - Update Quote.StartDate - Upon Opportunity 'Closed /Won' OR 'Ready to book'
    static testMethod void opportunityClosedWonTest() {
    
      Opportunity opp = [select id from Opportunity Limit 1];
      SBQQ__Quote__c q = [select Id from SBQQ__Quote__c Limit 1];

     q.SBQQ__Status__c = 'Approved';
     update q;

      //check that opp stage before test is not Won/Ready2Book
      opportunity  oppTest = [select StageName, SBQQ__Contracted__c from opportunity where Id = :opp.Id];
       System.assertEquals(false, oppTest.SBQQ__Contracted__c);
      System.assertNotEquals(stageWon, oppTest.StageName);
      System.assertNotEquals(stageReady2Book, oppTest.StageName);
       
       //check that quote & opp are linked 
      SBQQ__Quote__c  qTest = [select SBQQ__Opportunity2__c, SBQQ__StartDate__c from SBQQ__Quote__c where Id = :q.Id];
      System.assertEquals(opp.Id, qTest.SBQQ__Opportunity2__c);
      System.assertEquals(null, qTest.SBQQ__StartDate__c);


      User u = [Select Id from User where profile.Name = 'Skybox Finance' and IsActive = true limit 1];
      
      Test.startTest();
        System.runAs(u) {

           opp.StageName = stageReady2Book;
           opp.Competitor_Displaced__c = 'No competitor displaced';//validation "Competitor displaced cannot be empty for opps greater than 80% probability"
           opp.Pilot_Status__c = 'No pilot needed';
           opp.Deal_Win_Loss_Reason__c = 'No Deal_Win_Loss_Reason__c';
           update opp;
       }
      Test.stopTest();
      
      //Check opp.SBQQ__Contracted__c was upadated to true - Upon Opportunity 'Closed /Won' OR 'Ready to book'
      oppTest = [select SBQQ__Contracted__c from opportunity where Id = :opp.Id];
      System.assertEquals(true, oppTest.SBQQ__Contracted__c);

      //Check Quote.StartDate was updated to date.Today() + 1: - Upon Opportunity 'Closed /Won' OR 'Ready to book'
      qTest = [select SBQQ__StartDate__c from SBQQ__Quote__c where Id = :q.Id];
      System.assertNotEquals(null, qTest.SBQQ__StartDate__c);
    }
}