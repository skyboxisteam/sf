/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SubscriptionViewTest {

    static testMethod void myUnitTest() {
       ClsObjectCreator cls = new ClsObjectCreator();
       Account a = cls.CreateAccount('Test');
       opportunity opp = cls.createOpportunity(a);
       SBQQ__Quote__c q = cls.createQuote(ClsObjectCreator.createContact('Test',a.id),'Draft');
       Contract c = cls.createContract(a,opp,q);
       SBQQ__Subscription__c sc= cls.createSubscription(a,c,cls.createProduct('name '));  
       Asset ass = cls.createAsset('test','test',a);
       SBQQ__SubscribedAsset__c sa = cls.createSubscribedAsset(sc,ass);
     //  list<SubscriptionView.wrapSub> sub = new  list<SubscriptionView.wrapSub>();
       
       SubscriptionView sv =  new SubscriptionView();
       sv.setrelevantId(a.id);
       Id i = sv.getrelevantId();
       sv.setaccountId(a.id);
       i = sv.getaccountId();
        sv.allowShowExp = false;
       sv.setaccountName('test');
       string s = sv.getaccountName();
       sv.setShowExpire();
       sv.wrapSubList= sv.getwrapSubList();
       sv.processSelected() ;
       sv.totalMap = sv.getTotalMap(date.today());
       
       
       
        
    }
}