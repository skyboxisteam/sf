public with sharing class CPQMethods 
{
  private list<opportunity> listOpportunity = new list<opportunity>();
  private list<SBQQ__Quote__c> listQuote = new list<SBQQ__Quote__c>();
  
  private map<id,contract> mapContract = new  map<id,contract>();
  private map<id,decimal> mapPrimaryAmount= new  map<id,decimal>();
  private boolean isInsert;  
  private string RoleInOpp = 'Unknown';
  private string stage = 'Skybox selected';
  private string stageWon = 'Closed /Won';
  private string stageReady2Book = 'Ready to book';
  private string subType = 'Standard or Premium Support';
  private string typeOpp = 'Support Renewal';
  private string renewal = 'Renewal';
  private string newBusiness = 'New Business';
  private string amendment = 'Amendment';



/**************************************************************************************************************************************************************
*                                                                                                                                                             *
*                                              Opportunity                                                                                                    *
*                                                                                                                                                             *
***************************************************************************************************************************************************************/
//CPQ-121
public void OpportunityClosedWon(list<opportunity> listOpp, map<id,opportunity> mapOpp)
   {
    //Update Quote.StartDate - Upon Opportunity 'Closed /Won' OR 'Ready to book'
    List<SBQQ__Quote__c> updateQuote = new List<SBQQ__Quote__c>();
    List<Opportunity> updateOpp = new List<Opportunity>();
    listOpportunity = getExtendedOpp(listOpp);
    for(opportunity opp : listOpportunity)
    {      
         if(opp.StageName != mapOpp.get(opp.Id).StageName && opp.SBQQ__PrimaryQuote__c != null && (opp.StageName == stageWon || opp.StageName == stageReady2Book)){
            updateQuote.add(new SBQQ__Quote__c(Id=opp.SBQQ__PrimaryQuote__c, SBQQ__StartDate__c= opp.SBQQ__PrimaryQuote__r.SBQQ__StartDate__c == null ? date.Today() + 1: opp.SBQQ__PrimaryQuote__r.SBQQ__StartDate__c));
            updateOpp.add(new Opportunity(Id=opp.ID, SBQQ__Contracted__c = true));
         }
     } 
     try
     {
      if(updateQuote.size() > 0)
        update updateQuote;
      if(updateOpp.size() > 0)
        update updateOpp;
     }
     catch(exception ex)
     {
        system.debug('@@@CPQMethods.OpportunityClosedWon Exception: ' + ex.getMessage());
     }
   }
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------OpportunityNamingConvention----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
  public void OpportunityNamingConvention(list<opportunity> listOpp)
   {
    listOpportunity = getExtendedOpp(listOpp);
    for(opportunity opp : listOpportunity)
    {      
         if(opp.SBQQ__RenewedContract__c != null  )     
           opp = updateNameConv(opp, opp.SBQQ__RenewedContract__r.WDSB_Distributor__c,opp.Account.Name,opp.SBQQ__RenewedContract__r.WDSB_Partner__c,opp.Closedate,renewal,renewal, opp.SBQQ__RenewedContract__r.Invoice__c );
         else if(opp.SBQQ__AmendedContract__c !=null && opp.SBQQ__RenewedContract__r.WDSB_Partner__c !=null)
              opp = updateNameConv(opp, opp.SBQQ__RenewedContract__r.WDSB_Distributor__c,opp.Account.Name,opp.SBQQ__AmendedContract__r.WDSB_Partner__c,opp.Closedate,getOppAddName(opp),'' , opp.SBQQ__AmendedContract__r.Invoice__c  ); 
         else if(opp.SBQQ__AmendedContract__c !=null && opp.SBQQ__AmendedContract__r.WDSB_Partner__c ==null)
               opp = updateNameConv(opp, opp.SBQQ__AmendedContract__r.WDSB_Distributor__c,opp.Account.Name,opp.SBQQ__AmendedContract__r.WDSB_Partner__c,opp.Closedate,getOppAddName(opp),'', opp.SBQQ__AmendedContract__r.Invoice__c   );       
         
     } 
     try
     {
       update listOpportunity;
     }
     catch(exception ex)
     {
       system.debug('@@@CPQMethods.OpportunityNamingConvention Exception: ' + ex.getMessage());
     }
   }


 //-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------updateNameConv----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
   private opportunity updateNameConv(opportunity opp, id disti, string accountName , id partnerId, date closedate, string OppNmaeAdd ,string  typeOf , string invoice)
    {
      opp.Override_validation_Rules__c = true;
      opp.Distributor__c = disti;
      opp.Name = accountName + ' - ' + string.valueof(closedate.addDays(1).year()) +' ' +OppNmaeAdd;//SF-1073 was string.valueof(closedate.year()+1) 
      opp.Partner__c = partnerId;
      opp.Invoice__c = invoice;
      if( typeOf == renewal)
      {
       opp.Skybox_Role_in_Partner_Opportunity__c = RoleInOpp;
       opp.StageName = stage;
       opp.Subtype__c = subType;
       opp.type = typeOpp;
       if(opp.SBQQ__RenewedContract__r.WDSB_MIgration_External_Id__c == null)
          opp.Sale_Channel__c = opp.SBQQ__AmendedContract__c != null ? opp.SBQQ__AmendedContract__r.SBQQ__Quote__r.SBQQ__Opportunity2__r.Sale_Channel__c : opp.SBQQ__RenewedContract__r !=null ?  opp.SBQQ__RenewedContract__r.SBQQ__Quote__r.SBQQ__Opportunity2__r.Sale_Channel__c:null;
      }
      system.debug('opp ' + opp);
      return opp;
    } 
 
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getOppAddName----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
   
   private string getOppAddName(opportunity opp)
   {
     return opp.SBQQ__RenewedContract__r.WDSB_Partner__c !=null?amendment + opp.SBQQ__RenewedContract__r.WDSB_Partner__r.name :amendment;
   }
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getExtendedOpp----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

   private list<opportunity> getExtendedOpp(list<opportunity> listOpp)
   {
     return [SELECT id,
                   Name,
                   Distributor__c,
                   Sale_Channel__c,
                   Partner__c,
                   StageName,
                   Subtype__c,
                   Type,
                   Skybox_Role_in_Partner_Opportunity__c,
                   Account.Name,
                   CloseDate,
                   SBQQ__RenewedContract__c,
                    SBQQ__RenewedContract__r.SBQQ__Quote__r.SBQQ__Opportunity2__r.Sale_Channel__c,
                    SBQQ__RenewedContract__r.WDSB_Partner__c,
                    SBQQ__RenewedContract__r.WDSB_MIgration_External_Id__c,   
                    SBQQ__RenewedContract__r.WDSB_Distributor__c,
                    SBQQ__RenewedContract__r.Invoice__c,
                    SBQQ__AmendedContract__c, 
                    SBQQ__AmendedContract__r.SBQQ__Quote__r.SBQQ__Opportunity2__r.Sale_Channel__c,                   
                    SBQQ__AmendedContract__r.WDSB_Partner__c,
                    SBQQ__AmendedContract__r.WDSB_Partner__r.name,
                    SBQQ__AmendedContract__r.WDSB_MIgration_External_Id__c,     
                    SBQQ__AmendedContract__r.WDSB_Distributor__c,
                    SBQQ__AmendedContract__r.Invoice__c,
                    SBQQ__PrimaryQuote__r.SBQQ__StartDate__c        
             FROM opportunity
             WHERE id in:listOpp ] ;
     
     
   }
   
/**************************************************************************************************************************************************************
*                                                                                                                                                             *
*                                              Quote                                                                                                          *
*                                                                                                                                                             *
***************************************************************************************************************************************************************/

//CPQ-123 
//When quote approved, update related opp.WDSB_discount fields
public void quoteApproved(list<SBQQ__Quote__c> listQuote, Map<Id, SBQQ__Quote__c> oldMap){
   system.debug('@@@quoteApproved');

  List<Opportunity> updateOpp = new List<Opportunity>();
  listQuote = getExtendedQuote(listQuote);
    
  List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
  String MyProflieName = PROFILE[0].Name;

  for(SBQQ__Quote__c qu : listQuote)
  {  
    if(qu.SBQQ__Opportunity2__c != null && qu.SBQQ__Status__c != oldMap.get(qu.Id).SBQQ__Status__c && qu.SBQQ__Status__c == 'Approved')
    {
        Opportunity opp = new Opportunity();
        opp.Override_validation_Rules__c = true;
        updateOppMaxDiscount('WDSB_Appliance_Approved_discount_level__c','WDSB_Appliance_Max_Discount__c', opp, qu);
        updateOppMaxDiscount('WDSB_Software_Approved_discount_level__c','WDSB_Software_License_Max_Discount__c', opp, qu);
        updateOppMaxDiscount('WDSB_PS_Approved_discount_level__c','WDSB_Professional_Services_Max_Discount__c', opp, qu);
        updateOppMaxDiscount('WDSB_Support_Approved_discount_level__c','WDSB_Technical_Support_Content_Max_Disc__c', opp, qu);
        updateOppMaxDiscount('WDSB_Training_Approved_discount_level__c','WDSB_Training_Max_Discount__c', opp, qu);
        updateOppMinAmount('WDSB_Last_Approved_Quote_Net_Amount__c','SBQQ__NetAmount__c', opp, qu);//CPQ-187 -Blanket approval to use the 25% variance
        //Quote_Renewal_Approved__c - Used to trigger Email Alert CPQ-168
        if(qu.SBQQ__Status__c == 'Approved' && qu.SBQQ__Type__c == Renewal && MyProflieName == 'Skybox Finance'){
          opp.Quote_Renewal_Approved__c = true;
          opp.Id = qu.SBQQ__Opportunity2__c;
        }
        if(opp.Id != null){
          updateOpp.add(opp);
        }
    }
    //Quote_Renewal_Approved__c - Used to trigger Email Alert CPQ-168
    else if(oldMap.get(qu.Id).SBQQ__Status__c == 'Approved' && qu.SBQQ__Status__c != 'Approved' && qu.SBQQ__Type__c == Renewal && MyProflieName == 'Skybox Finance'){
        updateOpp.add( new Opportunity( Quote_Renewal_Approved__c = false, Id = qu.SBQQ__Opportunity2__c,Override_validation_Rules__c = true));
    }

  }
  if(updateOpp.size() > 0){
    //comment the try block in order to show the Validation Rule error if exist
    //try{
        update updateOpp;
    //}
    //catch(exception ex){
    //  system.debug('@@@CPQMethods.quoteApproved Exception: ' + ex.getMessage());
    //}
  }
}
 private Opportunity updateOppMinAmount(String oppDiscount,String quoteDiscount, Opportunity opp, SBQQ__Quote__c qu) {
      if( qu.WDSB_Initiate_Approval_process__c   &&  ifnull(qu.getSobject('SBQQ__Opportunity2__r').get(oppDiscount),ifnull(qu.get(quoteDiscount), 0)) >= ifnull(qu.get(quoteDiscount), 0)){
          opp.Id = qu.SBQQ__Opportunity2__c;
          opp.put(oppDiscount, qu.get(quoteDiscount));
      }
      return opp;
 }
 private Opportunity updateOppMaxDiscount(String oppDiscount,String quoteDiscount, Opportunity opp, SBQQ__Quote__c qu) {
      if(ifnull(qu.getSobject('SBQQ__Opportunity2__r').get(oppDiscount),0) < ifnull(qu.get(quoteDiscount), 0)){
          opp.Id = qu.SBQQ__Opportunity2__c;
          opp.put(oppDiscount, qu.get(quoteDiscount));
      }
      return opp;
 }
 private decimal ifnull(object s1,decimal s2) {
    decimal result = (decimal)s1;
    if (s1 == null) { result = s2; }
    return result;
  }

//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------quoteNamingConvention----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
  public void quoteNamingConvention(list<SBQQ__Quote__c> listQuoteParam)
   {
    listQuote = getExtendedQuote(listQuoteParam);
    createMapPrimaryAmount();
    
    //System.debug('@@@quoteNamingConvention: listQuote before: '+ listQuote);
       
    for(SBQQ__Quote__c qu : listQuote)
    {  
      if(qu.SBQQ__Opportunity2__c != null)
        {
         QuoteVariableHelpClass quHelp = new QuoteVariableHelpClass(qu,renewal , amendment , newBusiness,mapPrimaryAmount );  
         qu = updateQuote(qu,quHelp);
       /*  if(qu.SBQQ__Type__c == renewal)     
              qu = updateQuote(qu, qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Distributor__c , qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Partner__c ,qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Quoted_For__c,true);
         else if(qu.SBQQ__Type__c == amendment)
              qu = updateQuote(qu, qu.SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Distributor__c , qu.SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Partner__c ,qu.SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Quoted_For__c,true); 
         else if(qu.SBQQ__Type__c == newBusiness)
              qu = updateQuote(qu, qu.SBQQ__Opportunity2__r.Distributor__c , qu.SBQQ__Opportunity2__r.Partner__c ,qu.WDSB_Paying_Entity__c,qu.SBQQ__Opportunity2__r.SBQQ__PrimaryQuote__c == null?true:false);  */     
        }
     } 
     
       //System.debug('@@@quoteNamingConvention: listQuote after: '+ listQuote);
     try
     {
       update listQuote;
     }
     catch(exception ex)
     {
       system.debug('Exc');
       system.debug('@@@CPQMethods.quoteNamingConvention Exception: ' + ex.getMessage());
     }
   }
   
  
 
 public class QuoteVariableHelpClass
 {
   Id distributor;
   Id resller;
   string quotedFor;
   string quoteType;
   boolean primaryQuote;
   boolean dealRegDistributor = false;
   boolean dealRegReseller = false;
   boolean pocDistributor = false;
   boolean pocReseller = false;
    SBQQ__Quote__c quote;
    
    public QuoteVariableHelpClass(SBQQ__Quote__c qu , string renewal , string amendment ,string newBusiness, map<id,decimal> mapPrimaryAmount)
    {
        //System.debug('@@@QuoteVariableHelpClass: mapPrimaryAmount:' + mapPrimaryAmount + ' mapPrimaryAmount.get(qu.SBQQ__Opportunity2__c): '+mapPrimaryAmount.get(qu.SBQQ__Opportunity2__c));
        //System.debug('@@@qu.SBQQ__Primary__c: '+ qu.SBQQ__Primary__c);
     // quoteType = qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__c  != null ? renewal : qu.SBQQ__Opportunity2__r.SBQQ__AmendedContract__c !=null?amendment:newBusiness;
       quote = qu;
       if(qu.SBQQ__Type__c == renewal)
       {
        //System.debug('@@@QuoteVariableHelpClass: qu.isClone(): '+qu.isClone());
        distributor = qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Distributor__c ;
        resller = qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Partner__c;
        quotedFor = qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Quoted_For__c ;         
        //1.On the first renwal creation the primary will be true beacause mapPrimaryAmount will return null - because no other primary yet.
        //after primary set to true, cpq code run and update it back to false, thus adding "|| qu.SBQQ__Primary__c == true"
        //2.On cloning mapPrimaryAmount - return false - there already primary quote for that opp.
        //primaryQuote = qu.isClone() ? false : true;// qu.isClone() not working - always return false
        primaryQuote = qu.SBQQ__Primary__c == true || mapPrimaryAmount.get(qu.SBQQ__Opportunity2__c) == null || mapPrimaryAmount.get(qu.SBQQ__Opportunity2__c) == 0; 
        dealRegDistributor = qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Distributor_Deal_Reg_Discount__c ;
        dealRegReseller = qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Reseller_Deal_Registration_Discount__c;
        pocDistributor = qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_POC_by_Distributor_Discount__c;
        pocReseller = qu.SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_POC_by_Partner_Discount__c;
        //System.debug('@@@q.primaryQuote: '+ primaryQuote);
       }
      
       else if(qu.SBQQ__Type__c == amendment)
       {
         distributor = qu.SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Distributor__c ;
         resller = qu.SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Partner__c;
         quotedFor = qu.SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Quoted_For__c ;
         primaryQuote = qu.SBQQ__Primary__c == true || mapPrimaryAmount.get(qu.SBQQ__Opportunity2__c) == null || mapPrimaryAmount.get(qu.SBQQ__Opportunity2__c) == 0; 
         dealRegDistributor = qu.SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Distributor_Deal_Reg_Discount__c ;
         dealRegReseller = qu.SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Reseller_Deal_Registration_Discount__c;
         pocDistributor = qu.SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_POC_by_Distributor_Discount__c;
         pocReseller = qu.SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_POC_by_Partner_Discount__c;
       }
      
       else
       {
         distributor =  qu.SBQQ__Opportunity2__r.Distributor__c ;
         resller = qu.SBQQ__Opportunity2__r.Partner__c;
         quotedFor = qu.WDSB_Paying_Entity__c ;
         primaryQuote =  qu.SBQQ__Primary__c == true || mapPrimaryAmount.get(qu.SBQQ__Opportunity2__c) == null || mapPrimaryAmount.get(qu.SBQQ__Opportunity2__c) == 0;
         dealRegDistributor = qu.WDSB_Dis_Deal_Reg_Discount__c ;
         dealRegReseller = qu.WDSB_DealReg__c;
         pocDistributor = qu.WDSB_POC_Distributor_Discount__c ;
         pocReseller =  qu.WDSB_POC_new__c;   
        }
  
    }
 }
 
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------MapPrimaries----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
    public void createMapPrimaryAmount()
    {
        set<Id> opp = new Set<Id>();
        for(SBQQ__Quote__c q :listQuote){
            opp.add(q.SBQQ__Opportunity2__c);
        }
            list<AggregateResult> aggrList = [SELECT SBQQ__Opportunity2__c, COUNT_DISTINCT(Id) 
                                              FROM  SBQQ__Quote__c 
                                              WHERE SBQQ__Primary__c = true and SBQQ__Opportunity2__c in :opp
                                              GROUP BY SBQQ__Opportunity2__c  ];
        
        for(AggregateResult ag :aggrList)
            mapPrimaryAmount.put((ID)ag.get('SBQQ__Opportunity2__c'), (decimal)ag.get('expr0'));
        
        
        System.debug('@@@createMapPrimaryAmount: aggrList: ' + aggrList + ' mapPrimaryAmount: '+mapPrimaryAmount+ ' listQuote:'+listQuote);
        
    }
    
 
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------updateQuote----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
  
   private SBQQ__Quote__c updateQuote(SBQQ__Quote__c qu,  QuoteVariableHelpClass q )
    {
        //System.debug('@@@updateQuote: qu: ' + qu + ' q: '+q);
        qu.SBQQ__Distributor__c = qu.SBQQ__Distributor__c == null ? q.distributor :  qu.SBQQ__Distributor__c;
        qu.SBQQ__Partner__c = qu.SBQQ__Partner__c == null ? q.resller :  qu.SBQQ__Partner__c;
        qu.SBQQ__Primary__c = q.primaryQuote;
        qu.SBQQ__WatermarkShown__c = true;
        qu.WDSB_Paying_Entity__c = qu.WDSB_Paying_Entity__c == null ? q.quotedFor : qu.WDSB_Paying_Entity__c;
        qu.EditLinesFieldSetName__c = qu.EditLinesFieldSetName__c == null ? q.quotedFor : qu.EditLinesFieldSetName__c;
        qu.WDSB_DealReg__c = q.dealRegReseller;
        qu.WDSB_Dis_Deal_Reg_Discount__c = q.dealRegDistributor;
        qu.WDSB_POC_Distributor_Discount__c = q.pocDistributor;
        qu.WDSB_POC_new__c = q.pocReseller;
        qu.ownerid = qu.SBQQ__Opportunity2__r.Owner.IsActive ?  qu.SBQQ__Opportunity2__r.ownerid : qu.ownerId;
         return qu;
    } 

   

   
//-------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------getExtendedQuote----------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

   private list<SBQQ__Quote__c> getExtendedQuote(list<SBQQ__Quote__c> listQuote)
   {
     return [SELECT id,
                   Name,
                   Ownerid,
                   SBQQ__Primary__c,
                   WDSB_Initiate_Approval_process__c,   
                   WDSB_Paying_Entity__c,
                   EditLinesFieldSetName__c,
                   WDSB_DealReg__c,
                   WDSB_Dis_Deal_Reg_Discount__c,
                   WDSB_POC_Distributor_Discount__c,
                   WDSB_POC_new__c,
                   SBQQ__Opportunity2__c,
                   SBQQ__Opportunity2__r.Ownerid,
                   SBQQ__Opportunity2__r.Owner.IsActive,
                   SBQQ__Opportunity2__r.Distributor__c,
                   SBQQ__Opportunity2__r.Partner__c,
                   SBQQ__Opportunity2__r.SBQQ__PrimaryQuote__c,
                   SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Distributor__c,
                   SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Partner__c,
                   SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Quoted_For__c,    
                   SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_POC_by_Distributor_Discount__c, 
                   SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_POC_by_Partner_Discount__c,
                   SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Reseller_Deal_Registration_Discount__c,
                   SBQQ__Opportunity2__r.SBQQ__RenewedContract__r.WDSB_Distributor_Deal_Reg_Discount__c  ,      
                   SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Distributor__c,
                   SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Partner__c,
                   SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Quoted_For__c,      
                   SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_POC_by_Distributor_Discount__c, 
                   SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_POC_by_Partner_Discount__c,   
                   SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Reseller_Deal_Registration_Discount__c,  
                   SBQQ__Opportunity2__r.SBQQ__AmendedContract__r.WDSB_Distributor_Deal_Reg_Discount__c,      
                   SBQQ__Distributor__c,
                   SBQQ__Partner__c,
                   SBQQ__Type__c,
                   SBQQ__Status__c,
                   WDSB_Appliance_Max_Discount__c,
                   SBQQ__Opportunity2__r.WDSB_Appliance_Approved_discount_level__c,
                   WDSB_Software_License_Max_Discount__c,
                   SBQQ__Opportunity2__r.WDSB_Software_Approved_discount_level__c,
                   WDSB_Professional_Services_Max_Discount__c,
                   SBQQ__Opportunity2__r.WDSB_PS_Approved_discount_level__c,
                   WDSB_Technical_Support_Content_Max_Disc__c,
                   SBQQ__Opportunity2__r.WDSB_Support_Approved_discount_level__c,
                   WDSB_Training_Max_Discount__c,
                   SBQQ__Opportunity2__r.WDSB_Training_Approved_discount_level__c,
                   SBQQ__NetAmount__c,
                   SBQQ__Opportunity2__r.WDSB_Last_Approved_Quote_Net_Amount__c

                   
             FROM SBQQ__Quote__c
             WHERE id in:listQuote ] ;
     
     
   }
}