@isTest
private class OpportunityHandlerTest
{	
	static ClsObjectCreator objCreator = new ClsObjectCreator();

	@testSetup static void setupData() 
	{
		
		User myAdminUser = [select id from user  where profile.Name='System Administrator' and isActive=true limit 1];
		
		//create Account
		Account acc =  objCreator.createAccount('TestAccount');
		//create opp
		Opportunity opp =  objCreator.createOpportunity(acc, 'test1');
		Opportunity opp2 =  objCreator.createOpportunity(acc, 'test2');
		//create Contact
		Contact con = ClsObjectCreator.createContact('Test', acc.Id);
		//create qoute
		SBQQ__Quote__c qoute = objCreator.createQuote(con, 'Draft');
		qoute.SBQQ__Opportunity2__c = opp.Id;
		update qoute;

		opp.SBQQ__PrimaryQuote__c = qoute.Id;
		update opp;

		Test.startTest();
		//create cp
		Commission_Plan__c cp = new Commission_Plan__c();
		cp.Sales_User__c = myAdminUser.Id;
		cp.Name = 'Rinas Commision Plan 2018';
		insert cp;
		Test.stopTest();
	}

	//Author: Rina Nachbas			Date:17.07.18
	//Task:   CPQ-127: Remove CPQ data(Renewed contract, Amended contract, Contracted, PrimaryQuote) upon clone opportunity	
	@isTest
	static void clearOpportunityFildsOnCloneTest()
	{
		// Given
		Account acc = [select id from Account Limit 1];
		Opportunity opp = [select id, name, AccountId, Type,Product__c,CloseDate,StageName from Opportunity Limit 1];//for clone has the requierd fields, needs to select them.
		SBQQ__Quote__c q =  [select id from SBQQ__Quote__c Limit 1];
		contract contr = objCreator.createContract(acc , opp, q );
		opp.SBQQ__Contracted__c = true;
		opp.SBQQ__RenewedContract__c = contr.Id;
		opp.SBQQ__AmendedContract__c = contr.Id;
		opp.SBQQ__PrimaryQuote__c = q.Id;
		Test.startTest();//To avoid too many soql
		update opp;
		Opportunity oppTest = [select ID, SBQQ__Contracted__c, SBQQ__RenewedContract__c, SBQQ__AmendedContract__c, SBQQ__PrimaryQuote__c from Opportunity where Id = :opp.Id];
		System.assertEquals(true, oppTest.SBQQ__Contracted__c);
		System.assertEquals(contr.Id, oppTest.SBQQ__RenewedContract__c);
		System.assertEquals(contr.Id, oppTest.SBQQ__AmendedContract__c);
		System.assertEquals(q.Id, oppTest.SBQQ__PrimaryQuote__c);
		
		// When
		//Test.startTest();
			Opportunity newOpp = opp.clone(false);
			insert newOpp;
		Test.stopTest();
		
		// Then
		oppTest = [select ID, SBQQ__Contracted__c, SBQQ__RenewedContract__c, SBQQ__AmendedContract__c,SBQQ__PrimaryQuote__c from Opportunity where Id = :newOpp.Id];
		System.assertEquals(false, oppTest.SBQQ__Contracted__c);
		System.assertEquals(null, oppTest.SBQQ__RenewedContract__c);
		System.assertEquals(null, oppTest.SBQQ__AmendedContract__c);
		System.assertEquals(null, oppTest.SBQQ__PrimaryQuote__c);
	}



	//Author: Rina Nachbas			Date:1.11.18
	//Task:   CM-64: If a user tries to change the probability (from higher than 95 to lower than 95)
	//				 and the opportunity has commission record which its type is different than PIO the user will get an error
	@isTest
	static void probabilityValidation1Test()
	{
		//User myAdminUser = [select id from user  where profile.Name='System Administrator' and isActive=true limit 1];
		Commission_Plan__c cp = [select id from Commission_Plan__c where Name = 'Rinas Commision Plan 2018' limit 1];
		Opportunity opp = [select id from Opportunity Limit 1];
		opp.Probability = 96;
		update opp;
		
		Opportunity oppTest = [select ID, Probability from Opportunity where Id = :opp.Id];
		System.assertEquals(96, oppTest.Probability);

		//create PIO CR
		Commission_Record__c crPIO = objCreator.createCommissionRecord(opp.Id, cp.Id, Label.PIO);

		Test.startTest();
			opp.Probability = 94;
			update opp;
		Test.stopTest();

		oppTest = [select ID, Probability from Opportunity where Id = :opp.Id];
		System.assertEquals(94, oppTest.Probability);
		System.debug('Succeed to update opp from higher than 95 to lower than 95, because it has not CR which its type is different than PIO');
		
	}

	
	//Author: Rina Nachbas			Date:1.11.18
	//Task:   CM-64: If a user tries to change the probability (from higher than 95 to lower than 95)
	//				 and the opportunity has commission record which its type is different than PIO the user will get an error
	@isTest
	static void probabilityValidation2Test()
	{
		//User myAdminUser = [select id from user  where profile.Name='System Administrator' and isActive=true limit 1];
		Commission_Plan__c cp = [select id from Commission_Plan__c where Name = 'Rinas Commision Plan 2018' limit 1];
		Opportunity opp = [select id from Opportunity Limit 1];
		opp.Probability = 96;
		update opp;
		
		Opportunity oppTest = [select ID, Probability from Opportunity where Id = :opp.Id];
		System.assertEquals(96, oppTest.Probability);

		//create Commission Record
		Commission_Record__c cr = objCreator.createCommissionRecord(opp.Id, cp.Id, Label.Regular);
		try {
			Test.startTest();
				opp.Probability = 94;
				update opp;
			Test.stopTest();
			System.assertEquals('Should Alert On: ' + Label.OpportunityProbabilityError, '');	 
		} catch(DMLException e){
			System.assertEquals(Label.OpportunityProbabilityError, e.getDmlMessage(0));
		}
	}

	//Author: Rina Nachbas			Date:1.11.18
	//Task:   CM-64: If the opportunity probability is less than 95% and a user tries to created a commission record will get an error.
	@isTest
	static void probabilityValidation3Test()
	{
		Commission_Plan__c cp = [select id from Commission_Plan__c where Name = 'Rinas Commision Plan 2018' limit 1];
		Opportunity opp = [select id from Opportunity Limit 1];
		opp.Probability = 94;
		update opp;
		
		Opportunity oppTest = [select ID, Probability from Opportunity where Id = :opp.Id];
		System.assertEquals(94, oppTest.Probability);

		//create Commission Record
		try {
			Test.startTest();

			//create PIO CR
			Commission_Record__c crPIO = objCreator.createCommissionRecord(opp.Id, cp.Id, Label.PIO);
			System.debug('Succeed to create CR which its type is PIO, when opportunity probability is less than 95%');
			List<Commission_Record__c> crTest = [select ID from Commission_Record__c];
			System.assertEquals(1,crTest.size());

			Commission_Record__c cr = objCreator.createCommissionRecord(opp.Id, cp.Id, Label.Regular);
			Test.stopTest();
			System.assertEquals('Should Alert On: ' + Label.CROpportunityProbabilityError, '');	 
		} catch(DMLException e){
			System.assertEquals(Label.CROpportunityProbabilityError, e.getDmlMessage(0));
		}

		List<Commission_Record__c> crTest = [select ID from Commission_Record__c];
		System.assertEquals(1,crTest.size());
	}
/*
	//Author: Rina Nachbas			Date:12.12.18
	//Task:   SF-1021: Opportunity cannot be moved to ready to book stage without signed SOW attached.
	@isTest
	static void validateSOWExistsOnReadyToBookUpdateTest()
	{
    	String readyToBook = 'Ready to book';

    	//Test 1: move Opp to ready to book stage with signed SOW attached
		Opportunity opp1 = [select id from Opportunity where Name = 'test1'];
		SOW__c sow1 = new SOW__c(Opportunity__c = opp1.Id);
		insert sow1;

		opp1.StageName = readyToBook;
		update opp1;
		
		Opportunity oppTest = [select ID, StageName from Opportunity where Id = :opp1.Id];
		System.assertEquals(readyToBook, oppTest.StageName);

		//Test 2: move Opp to ready to book stage without signed SOW attached
		Opportunity opp2 = [select id from Opportunity where name = 'test2'];
		try {
			Test.startTest();

			opp2.StageName = readyToBook;
			update opp2;

			Test.stopTest();
			System.assertEquals('Should Alert On: ' + Label.OpportunitySowMissingError, '');	 
		} catch(DMLException e){
			System.assertEquals(Label.OpportunitySowMissingError, e.getDmlMessage(0));
		}

		oppTest = [select ID, StageName from Opportunity where Id = :opp2.Id];
		System.assertNotEquals(readyToBook,oppTest.StageName);
	}
	//Author: Rina Nachbas			Date:12.12.18
	//Task:   SF-1021: Opportunity cannot be moved to ready to book stage without signed SOW attached.
	@isTest
	static void validateSOWExistsOnCloseWonUpdateTest()
	{
    	String readyToBook = 'Closed /Won';

    	//Test 1: move Opp to ready to book stage with signed SOW attached
		Opportunity opp1 = [select id from Opportunity where name = 'test1'];
		SOW__c sow1 = new SOW__c(Opportunity__c = opp1.Id);
		insert sow1;

		opp1.StageName = readyToBook;
		update opp1;
		
		Opportunity oppTest = [select ID, StageName from Opportunity where Id = :opp1.Id];
		System.assertEquals(readyToBook, oppTest.StageName);

		//Test 2: move Opp to ready to book stage without signed SOW attached
		Opportunity opp2 = [select id from Opportunity where name = 'test2'];
		try {
			Test.startTest();

			opp2.StageName = readyToBook;
			update opp2;

			Test.stopTest();
			System.assertEquals('Should Alert On: ' + Label.OpportunitySowMissingError, '');	 
		} catch(DMLException e){
			System.assertEquals(Label.OpportunitySowMissingError, e.getDmlMessage(0));
		}

		oppTest = [select ID, StageName from Opportunity where Id = :opp2.Id];
		System.assertNotEquals(readyToBook,oppTest.StageName);
	}
*/
	//Author: Rina Nachbas			Date:12.12.18
	//Task:   SF-1021: Opportunity cannot be moved to ready to book stage without signed SOW attached.
	/*@isTest
	static void validateSOWExistsOnReadyToBookInsertTest()
	{
    	String readyToBook = 'Ready to book';
    	//Test 3: create Opp  'ready to book' stage without signed SOW attached
		try {
			Test.startTest();

			Account acc =  objCreator.createAccount('TestAccount2');
			Opportunity opp3 =  objCreator.returnOpportunity(acc, 'test3');
			opp3.StageName = readyToBook;//cause: INVALID_CROSS_REFERENCE_KEY, invalid cross reference id: [] on Class.OpportunityBookingSchedule.creatFromOpportunity: line 73, column 1
			insert opp3;

			Test.stopTest();
			System.assertEquals('Should Alert On: ' + Label.OpportunitySowMissingError, '');	 
		} catch(DMLException e){
			System.assertEquals(Label.OpportunitySowMissingError, e.getDmlMessage(0));
		}

		List<Opportunity> oppListTest = [select ID, StageName from Opportunity];
		System.assertEquals(2,oppListTest.size());
    }*/

    //Author: Rina Nachbas			Date:26.12.18
	//Task:   SF-980: there must be a 'Pilot Scope' related to opp in order for the opp to be moved to any pilot status expect 'Projected' or 'No Pilot Needed'.
	@isTest
	static void pilotValidationTest()
	{
    	String noPilotNeeded = 'No pilot needed';
    	String projected = 'Projected';
    	String planned = 'Planned';
    	

    	//Test 1: move Opp to 'planned' with attached PilotScope
		Opportunity opp1 = [select id from Opportunity where name = 'test1'];
		Pilot_Scope__c ps1 = new Pilot_Scope__c(Opportunity__c = opp1.Id, name = 'psTest');
		insert ps1;

		Test.startTest(); //to avoid too many soql
		opp1.Pilot_Status__c = planned;
		update opp1;
		
		Opportunity oppTest = [select ID, Pilot_Status__c from Opportunity where Id = :opp1.Id];
		System.assertEquals(planned, oppTest.Pilot_Status__c);

		//Test 2: move Opp to 'planned' without attached PilotScope
		Opportunity opp2 = [select id from Opportunity where name = 'test2'];
		try {
			//Test.startTest();

			opp2.Pilot_Status__c = planned;
			update opp2;
 
			Test.stopTest();
			System.assertEquals('Should Alert On: ' + Label.OpportunityMissingPilotError, '');	 
		} catch(DMLException e){
			System.assertEquals(Label.OpportunityMissingPilotError, e.getDmlMessage(0));
		}

		oppTest = [select ID, Pilot_Status__c from Opportunity where Id = :opp2.Id];
		System.assertEquals(null,oppTest.Pilot_Status__c);

		//Test 3: move Opp to 'noPilotNeeded' without attached PilotScope
			opp2.Pilot_Status__c = noPilotNeeded;
			update opp2;

		oppTest = [select ID, Pilot_Status__c from Opportunity where Id = :opp2.Id];
		System.assertEquals(noPilotNeeded,oppTest.Pilot_Status__c);

    } 
    //Author: Rina Nachbas			Date:26.12.18
	//Task:   SF-980: there must be a 'Pilot Scope' related to opp in order for the opp to be moved to any pilot status expect 'Projected' or 'No Pilot Needed'.
	/*@isTest
	static void pilotValidationTest()
	{
    	String noPilotNeeded = 'No pilot needed';
    	String projected = 'Projected';
    	String planned = 'Planned';
    	//Test 4: create Opp 'Planned' pilot status without attached PilotScope
		try {
			Test.startTest();

			Account acc =  objCreator.createAccount('TestAccount2');
			Opportunity opp3 =  objCreator.returnOpportunity(acc, 'test3');
			opp3.Pilot_Status__c = planned;//cause: INVALID_CROSS_REFERENCE_KEY, invalid cross reference id: [] on Class.OpportunityBookingSchedule.creatFromOpportunity: line 73, column 1
			insert opp3;

			Test.stopTest();
			System.assertEquals('Should Alert On: ' + Label.OpportunityMissingPilotError, '');	 
		} catch(DMLException e){
			System.assertEquals(Label.OpportunityMissingPilotError, e.getDmlMessage(0));
		}

		List<Opportunity> oppListTest = [select ID, Pilot_Status__c from Opportunity];
		System.assertEquals(2,oppListTest.size());
    }*/
}