public with sharing class CommissionTableController {

public list<record> recordList {get;set;}
public Commission_plan__c comP {get;set;}

public CommissionTableController(ApexPages.StandardController stdCon) {
 	if(Test.isRunningTest())
 	{
 	 comP = new commission_plan__c();
 	 comP = [select id from Commission_plan__c limit 1];
 	 system.debug(comP);
 	}
 	else
     comP =  (Commission_plan__c) stdCon.getRecord();
    
   comP = [select id,Total_Commissionable_Adjusted__c,Super_Acc_Rate__c, Annual_Acc_Rate__c,Qualified_Billing_in_Super_Acceleration__c ,Qualified_Billing_in_Acceleration__c,Total_Credited_recurring_incentive__c,Total_Qualified_Billing_recurring_incent__c,Support_Multi_Year_Factor_Quota__c,Support_Multi_Year_Factor_Commission__c,Total_Credited_Adjusted__c,Adjust_Support_Multi_Year__c, Total_Credited_non_renewals__c,Total_Credited_renewals__c,Total_Credited_multi_year_renewals__c, Total_earning_up_to_quota__c,Total_Earning_Beyond_Quota__c,Quota_Attainment__c,Total_Commission_Earned__c
                  ,Total_Commissionable_non_renewals__c,Commission_Payable__c,Qualified_Billing_Up_To_Quota__c,Base_Rate__c,Total_Commissionable_renewals__c,Total_Commissionable_m_year_renewals__c,Advanceable_Flat_Rate__c,Advanceable_up_to_Quota__c,Advanceable_Beyond_Quota__c,Commission_Advancebale__c
           From Commission_plan__c where id =:comP.id ];
    
    list<commission_record__c> listComR = [select id,Advanceable_Amount__c,Is_Multi_Year_Renewal__c,Credited_Amount__c,Opportunity_Amount__c from commission_record__c where Commission_Plan__c =: comP.id];
   if(comP.Adjust_Support_Multi_Year__c ) 
   {
    recordList =  new list<record>();
    recordList.add(new record('Products (Perp & Subs) + PS + 1 Yr Renewal',getNonRenewQuote(comP),getNonRenewCommission(comP),getNonRenewQuote(comP),getOpp(listComR,false)));
   // recordList.add(new record('Renewal',getRenewQuote(comP),getRenewCommission(comP),getQuotaAttainmentMulti(comP),getnonRenewOpp(listComR,true)));
    recordList.add(new record('Multi Year Support & Content',getMultiYearQuote(comP),getMultiYearCommission(comP),getQuotaAttainmentMulti(comP),getOpp(listComR,true)));
    recordList.add(new record('Bonus Credit (Recurring Component)',getQuotaRecurring(comP),getCommissionRecurring(comP),getQuotaAttainmentRecurring(comP) ,getRecurringOpp(comP)));
    recordList.add(new record('Total Billing',getAllBookingQuote(comP),getAllBookingCommission(comP),getTotalQoutaAttainment(comP),getTotalOpp(comP,listComR)));
    recordList.add(new record('Quota Attainment',null,null,comP.Quota_Attainment__c,null));   
    recordList.add(new record('Commission Payable',getAllPayable(comP),getPayable(comP),null,null));   
    
      }
    }
  
  
  
public decimal getOpp(list<commission_record__c> listComR,boolean isMulti)  
  {
  	decimal sumOpp = 0 ;  
  	for(commission_record__c comR : listComR)
  	  if(comR.Is_Multi_Year_Renewal__c == isMulti )
  	    sumOpp += comR.Opportunity_Amount__c;
          
  	return sumOpp == null ? null : sumOpp.round();
  } 
  
public decimal getRecurringOpp(Commission_plan__c com)
{
	return null;
}

 public decimal getTotalOpp(Commission_plan__c com,list<commission_record__c> listR)
       {       	
       	   return getOpp(listR,true) == null? null:
       	         (getOpp(listR,true)+ getOpp(listR,false)).round();     	
       }    
//------------------------------------------------Attainment------------------------------------------------------------------

 public decimal getQuotaAttainmentMulti(Commission_plan__c com)  
   {
   	 return com.Total_Credited_multi_year_renewals__c == null? null:
   	        (com.Total_Credited_multi_year_renewals__c*com.Support_Multi_Year_Factor_Quota__c).round();
   }

 public decimal getQuotaAttainmentRecurring(Commission_plan__c com)  
   {
   	 return comP.Total_Credited_recurring_incentive__c == null?null :
   	        (comP.Total_Credited_recurring_incentive__c).round();
   }

  public decimal getTotalQoutaAttainment(Commission_plan__c com)
       {       	
       	 return getNonRenewQuote(com) == null ? null:
       	       (getQuotaAttainmentMulti(com) + getNonRenewQuote(com) +getQuotaAttainmentRecurring(comP)).round();     	
       }      

//-------------------------------------------------Quote----------------------------------------------------------------------------   
   
     public decimal getNonRenewQuote(Commission_plan__c com)
         {
        	return com.Total_Credited_non_renewals__c == null ? null:
               	   (com.Total_Credited_non_renewals__c + com.Total_Credited_renewals__c - com.Total_Credited_multi_year_renewals__c).round() ;
         }  
         
       public decimal getRenewQuote(Commission_plan__c com)
         {
        	return  com.Total_Credited_multi_year_renewals__c == null ? null:
        	       (com.Adjust_Support_Multi_Year__c == true ? (com.Total_Credited_multi_year_renewals__c) : com.Total_Credited_renewals__c ).round();
         }  
         
        public decimal getMultiYearQuote(Commission_plan__c com)
         {
        	return com.Total_Credited_multi_year_renewals__c == null ? null :
        	       (com.Total_Credited_multi_year_renewals__c*com.Support_Multi_Year_Factor_Commission__c/100).round();
         }  
       public decimal getFlatComQuote(Commission_plan__c com)
         {
        	return null;
         }  
        public decimal getAllBookingQuote(Commission_plan__c com)
         {
        	return  getNonRenewQuote(com) == null ? null :
        	       (com.Adjust_Support_Multi_Year__c == true ? ( getNonRenewQuote(com) + getMultiYearQuote(com) + getQuotaRecurring(com)) :com.Total_Credited_Adjusted__c ).round();
         }  
         
         
          public decimal getQuotaRecurring(Commission_plan__c com)
         {
        	return comP.Total_Credited_recurring_incentive__c == null ? null : comP.Total_Credited_recurring_incentive__c.round() ;
         } 
         
         public decimal getQuoteAttainment(Commission_plan__c com)
         {
        	return com.Quota_Attainment__c == null ? null : com.Quota_Attainment__c.round() ;
         }   
         
        public decimal getQuoteTotal(Commission_plan__c com)
         {
        	return com.Total_Commission_Earned__c == null ? null : com.Total_Commission_Earned__c.round() ;
         }  
         
 //-----------------------------------------------Commission----------------------------------------------------------------
         
       public decimal getNonRenewCommission(Commission_plan__c com)
         {
        	system.debug('com ' + com + ' is ' +com.Total_Commissionable_non_renewals__c );
        	return com.Total_Commissionable_non_renewals__c  == null ? null :
        	       (com.Total_Commissionable_non_renewals__c + com.Total_Commissionable_renewals__c - com.Total_Commissionable_m_year_renewals__c ).round() ;
         }  
         
       public decimal getRenewCommission(Commission_plan__c com)
         {
        	return  com.Total_Credited_renewals__c == null ? null : 
        	        (com.Adjust_Support_Multi_Year__c == true ?(com.Total_Credited_renewals__c - com.Total_Credited_multi_year_renewals__c) : com.Total_Commissionable_renewals__c).round() ;
         }  
         
        public decimal getMultiYearCommission(Commission_plan__c com)
         {
        	return  com.Total_Commissionable_m_year_renewals__c == null ? null : 
        	        (com.Adjust_Support_Multi_Year__c == true ? ((com.Support_Multi_Year_Factor_Commission__c/100)*com.Total_Commissionable_m_year_renewals__c)  :null).round();
         }  
       public decimal getFlatComCommission(Commission_plan__c com)
         {
        	return  com.Advanceable_Flat_Rate__c == null  ? null : 
        	         (com.Adjust_Support_Multi_Year__c == true && com.Advanceable_Flat_Rate__c != null ?com.Advanceable_Flat_Rate__c :null).round();
         }  
       public decimal getAllBookingCommission(Commission_plan__c com)
         {
        	return getNonRenewCommission(com)  == null ? null : 
        	        (com.Adjust_Support_Multi_Year__c == true ?  getNonRenewCommission(com) + getMultiYearCommission(com) + getCommissionRecurring(com) :com.Total_Commissionable_Adjusted__c).round() ;
         }       
         public decimal getCommissionRecurring(Commission_plan__c com)
         {
        	return   com.Total_Qualified_Billing_recurring_incent__c == null ? null : 
        	         com.Total_Qualified_Billing_recurring_incent__c.round() ;
         } 
         public decimal getCommissionAttainment(Commission_plan__c com)
         {
        	return null;
         }   
         
        public decimal getCommissionTotal(Commission_plan__c com)
         {
        	return com.Commission_Advancebale__c == null ? null : com.Commission_Advancebale__c.round() ;
         }   
         
//----------------------------------------------------Payable--------------------------------------------------------------------
public decimal getPayable(Commission_plan__c com)
{
	return com.Commission_Payable__c == null ? null : com.Commission_Payable__c.round();
	
}

public decimal getAllPayable(Commission_plan__c com)
{
		return  getMultiYearQuote(com)  == null ? null :
		       (com.Total_earning_up_to_quota__c + com.Annual_Acc_Rate__c * com.Qualified_Billing_in_Acceleration__c / 100 + getMultiYearQuote(com)*com.Base_Rate__c/100 + com.Super_Acc_Rate__c * (getNonRenewQuote(com) + getQuotaRecurring(com) - com.Qualified_Billing_Up_To_Quota__c - com.Qualified_Billing_in_Acceleration__c) / 100).round() ;
	
}
          
  
   public class record {
        public string fieldName {get; set;}
        public decimal quoteValue {get; set;}
        public decimal commissionValue {get; set;}
        public decimal quoteAttainmentValue {get; set;}
        public decimal oppTotalValue {get; set;}
 
        public record (string field, decimal quote, decimal commission, decimal quoteAttainment, decimal oppTotal ) 
         {
            fieldName = field ;           
            quoteValue = quote;
            commissionValue = commission;
            quoteAttainmentValue = quoteAttainment;
            oppTotalValue = oppTotal;
            
         } 
         
      
        
        
      }  
}