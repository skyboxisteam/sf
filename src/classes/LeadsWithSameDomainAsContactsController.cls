/****************************************************************************************
    Name              : LeadsWithSameDomainAsContactsController
    Revision History  :- 
    Created/Modified by         Created/Modified Date     Requested by      Related Task/Issue
    ----------------------------------------------------------------------------------------  
    1. Hernan 		               17/01/2012               		           [SW-4343]    
    ----------------------------------------------------------------------------------------
****************************************************************************************/

public class LeadsWithSameDomainAsContactsController {
	
	public List<Lead> leads {get; set;}
	public String pageErrorMsg	{get; set;}
	
	public LeadsWithSameDomainAsContactsController(ApexPages.Standardcontroller controller){
		pageErrorMsg = null;    	        
	    try{
	    	leads = new List<Lead>();
			Set<String> excludedDomains = Excluded_Email_Domains__c.getAll().keySet();						
			Set<String> contactsDomains = new Set<String>();
						
			for(Contact c : [SELECT Id, Email FROM Contact WHERE AccountId =: controller.getId() AND Email <> null AND Email LIKE '%@%']){
				String currentDomain = c.Email.substring(c.Email.indexOf('@') + 1);
				currentDomain = currentDomain.substring(0, currentDomain.indexOf('.'));	
				
				if(!excludedDomains.contains(currentDomain)){
					contactsDomains.add(currentDomain);
				}								
			}				
			
			if(!contactsDomains.isEmpty()){																																				
				String fieldsToQuery = '';
				
				Schema.FieldSet fs = Schema.SObjectType.Lead.fieldSets.getMap().get('Leads_With_Same_Domain_Columns');
				for(Schema.FieldSetMember field : fs.getFields()){
					if(!field.getFieldPath().equalsIgnoreCase('id') && !field.getFieldPath().equalsIgnoreCase('owner.name')){
						fieldsToQuery += ', ' + field.getFieldPath();
					}
				}				
				
				String query = 'SELECT Id, Owner.Name ' + fieldsToQuery;
				query += ' FROM Lead WHERE Email <> null AND (';
				for(String currentDomain : contactsDomains){
					query += ' Email LIKE \'%@' +  currentDomain + '.%\' OR';	
				}				
				query = query.substring(0, query.lastIndexOf('OR'));
				query += ' )';
				
				System.debug(query);										
				for(Lead l : Database.query(query)){								
					leads.add(l);						
				}												
			}
			
			itemsPerPage = 5;
			refreshPages();
		}catch(Exception e){
			String  errorMsg = e.getMessage();
            errorMsg += '\nStack Trace:' + e.getStackTraceString();
            errorMsg += '\nLine Number:' + e.getLineNumber();
            errorMsg += '\nException Type:' + e.getTypeName();            
            System.debug('errorMsg: ' + errorMsg);
	        pageErrorMsg = e.getMessage();             
	    }		
	}
	
	
	/////// Pagination controlling
	class ItemPage {
        public Boolean first {get; set;}
        public Boolean last {get; set;}
        public List<Lead> pageLeads {get; set;}
        public Integer pageNumber {get; set;}
        public Boolean isActive {get; set;}

        public ItemPage(List<Lead> mi, Integer pn)
        {
            pageLeads = mi;
            pageNumber = pn;
            isActive = false;
            first = false;
            last = false;
        }
    }


	// Paginator variables
	public ItemPage activePage           { get; set; }
    public List<ItemPage> pages 		  { get; set; }          
    public List<ItemPage> activePages    { get; set; }
    public Integer activePagesStartPNumber { get; set; }
    public Integer activePagesEndPNumber   { get; set; }
    public Boolean hasMorePagesAfter       { get; set; }
    public Boolean hasMorePagesBefore      { get; set; }
    public Integer pnum                    { get; set; }
    private static Integer pagesPerSetofActivePages = 50;
    public Integer itemsPerPage          { get; set; }     
	
	
	private void refreshPages(){					
		pages = populatePages(leads, itemsPerPage);	
		if (pages != null && pages.size() > 0) {
            pnum = 1;
            activePage = pages[0];
            pages[0].isActive = true;
            updateActivePages();
        }else{
        	pnum = null;
        	if(activepage == null){
        		activepage = new ItemPage(new List<Lead>(), 5);
        	}
        	activepage.pageLeads = new List<Lead>();
        }
	}
		
	public void nextPage(){		
		if (!activePage.last)
	        pnum = activePage.pageNumber + 1;
	      	updateActivePage();
    }

   public void prevPage(){   		
	      if (!activePage.first)
	        pnum = activePage.pageNumber - 1;
	      	updateActivePage();      
    }

    public List<ItemPage> populatePages(List<Lead> items, Integer ItemsPerPage){
        List<ItemPage> newPages = new List<ItemPage>();
        Integer itemsAdded = 0;
        Integer pagenumber = 0;

        // Adds items to itemspages until either all items are added or the page limit is reached
        if(items.size() > 0){        	       
	        while(itemsAdded < items.size()){
	            pagenumber++;
	            List<Lead> itemsToAdd = new List<Lead>();
	
	            for(Integer i=0; i < ItemsPerPage; i++) {
	                if(itemsAdded == items.size())
	                    break;
	
	                itemsToAdd.add(items[itemsAdded]);
	                itemsAdded++;
	            }
	
	            newPages.add(new ItemPage(itemsToAdd,pagenumber));
	            if(pagenumber == 1) {
	                newPages[0].first = true;
	            }
	        }
	        newPages[pagenumber-1].last=true;
        }               
        return newPages;
    }

    public void updateActivePages(){
        activePages = new List<ItemPage>();
        activePagesStartPNumber = pnum;
        Integer startIndex = pnum - 1;
        Integer endIndex = startIndex + pagesPerSetofActivePages;

        for (Integer i= startIndex; i < pages.size() && i < endIndex; i++)
            activePages.add(pages[i]);

        activePagesEndPNumber = activePages[activePages.size() - 1].pageNumber;

        if (activePagesEndPNumber == pages.size())
            hasMorePagesAfter = false;
        else
            hasMorePagesAfter = true;

        if (activePagesStartPNumber == 1)
            hasMorePagesBefore = false;
        else
            hasMorePagesBefore = true;

        setActivePage();
    }

    public void updateActivePagesBackwards(){
        List<ItemPage> tempActivePages = new List<ItemPage>();
        activePages = new List<ItemPage>();

        activePagesEndPNumber = pnum;
        Integer endIndex = pnum - 1;
        Integer startIndex = endIndex - pagesPerSetofActivePages - 1;

        for (Integer i= endIndex; (i >= 0) && (i > startIndex); i--)
            tempActivePages.add(pages[i]);

        for(Integer i = (tempActivePages.size() - 1); i >= 0; i--) {           
            activePages.add(tempActivePages[i]);
        }

        activePagesStartPNumber = activePages[0].pageNumber;

        if (activePagesEndPNumber == pages.size())
            hasMorePagesAfter = false;
        else
            hasMorePagesAfter = true;

        if (activePagesStartPNumber == 1)
            hasMorePagesBefore = false;
        else
            hasMorePagesBefore = true;

        setActivePage();
    }

    public void updateItemsPerPage(){
        pnum = 1;
        populatePages(leads, itemsPerPage);
        updateActivePages();
    }

    public void setActivePage(){
        Integer oldPN = activePage.pageNumber;
        activePage = pages[pnum - 1];
        pages[oldPN - 1].isActive = false;
        pages[pnum - 1].isActive = true;
    }

    public void updateActivePage(){
    	try{    		
	        setActivePage();
	        if (pnum > activePagesEndPNumber)
	            updateActivePages();
	        if (pnum < activePagesStartPNumber)
	            updateActivePagesBackwards();	        
        }catch(Exception e){
			pageErrorMsg = e.getMessage();				
		}
    }

	public void goToFirst(){		
        if (pages != null && pages.size() > 0)
            pnum = 1;
        	updateActivePage();        
    }

    public void goToLast(){
        if (pages != null && pages.size() > 0)
            pnum = pages.size();
        updateActivePage();
    }       
	/////// END Pagination controlling
	
	
	@isTest(SeeAllData=true) 
	static void LeadsWithSameDomainAsContactsController_Test(){		
		Account acc = new Account(Name = 'Test ', BillingCountryCode = 'US',BillingStateCode='TX', Theater__c = 'North America');
		insert acc;
		
		Contact con1 = new Contact(FirstName = 'Test', LastName = 'TestLastName', AccountId = acc.Id, Email = 'test1@testmail1.com');
		Contact con2 = new Contact(FirstName = 'Test', LastName = 'TestLastName', AccountId = acc.Id, Email = 'test2@testmail1.com');
		insert new List<Contact>{con1, con2};
		
		List<Lead> leads2Insert = new List<Lead>();
		for(Integer i = 0; i < 15; i++){
			Lead l = new Lead(FirstName = 'Test 1', LastName = 'Last', Email = 'test' + i + '@testmail1.com', Company = 'Company1');
			leads2Insert.add(l);
		}
		insert leads2Insert;				
										
		PageReference pageRef = Page.LeadsWithSameDomainAsContacts;
		pageRef.getParameters().put('id', acc.Id);
        Test.setCurrentPageReference(pageRef);
		 			    	          
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(acc);
		LeadsWithSameDomainAsContactsController controller = new LeadsWithSameDomainAsContactsController(sc);					
				
		controller.pnum = 1;
		controller.nextPage();
		controller.prevPage();
		controller.goToLast();	
		controller.updateActivePages();
		controller.updateActivePagesBackwards();
		controller.updateItemsPerPage();
		controller.goToFirst();			
	} 
}