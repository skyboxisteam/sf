public with sharing class QuoteSpecialApprovalController{
  
    String message;
    String origMessage;
    Boolean isError = false;
    Boolean isChanged = false;
    Boolean flag = false;
    SBQQ__Quote__c sq;
    Integer count = 0;
  
    public QuoteSpecialApprovalController(ApexPages.StandardController stdCon) {
     sq = (SBQQ__Quote__c) stdCon.getRecord();
     sq = getQuote(sq);
     origMessage = message = insertMessage(sq);
    
    }

            
    public PageReference checkMessageUpdate() {
        SBQQ__Quote__c  temp = getQuote(sq);
        message  = insertMessage(temp); 
        isChanged = flag || count > 5 ;   
        flag = message != origMessage  ; 
        count++;
        return null;
    }
         
     public string insertMessage(SBQQ__Quote__c q)
   {
    // string errorMsg= 'Error: PS in quote (' +q.PS_Days_in_Quote__c+' days) cannot be lower than Budgetary Estimation ('+q.PS_Budgetary_Days__c+' days), please add more PS days to quote or add final PS Estimation';
     string approvalStart = 'Approval: ';
     string approvalEnd = ' please amend the quote to avoid approval';
    // isError = q.PS_Quoted_less_than_PS_Budgetary__c;  - for P2S
    // return q.PS_Quoted_less_than_PS_Budgetary__c ? errorMsg : q.Special_Approval_needed__c == null ? '': approvalStart + q.Special_Approval_needed__c + approvalEnd;  - for P2S;
     return  q.Special_Approval_needed__c == null ? '': approvalStart + q.Special_Approval_needed__c + approvalEnd;
   }
   
     public string getmessage() {
        return message;
    }
    
    public Boolean getIsError () {
        return isError ;
    }
    
     public Boolean getIsChanged  () {
        return isChanged ;
    }
    
    private SBQQ__Quote__c  getQuote(SBQQ__Quote__c q)
    {
     // return [select id,PS_Days_in_Quote__c , PS_Budgetary_Days__c, PS_Quoted_less_than_PS_Budgetary__c, Special_Approval_needed__c from SBQQ__Quote__c  where id =: q.id];
      return [select id, Special_Approval_needed__c from SBQQ__Quote__c  where id =: q.id];
    }
   
}