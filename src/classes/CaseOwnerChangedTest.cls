@IsTest
public class CaseOwnerChangedTest {
	@IsTest
	private static void Test() {
		CaseTriggerSettings__c setting = new CaseTriggerSettings__c();
		setting.Name = 'Test';
		setting.QueueName__c = 'Test Queue';
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		CaseTeamRole ctr = new CaseTeamRole();
		System.runAs ( thisUser ) {
            ctr.Name = 'Test Role';
            Schema.DescribeFieldResult fieldResult = CaseTeamRole.AccessLevel.getDescribe();
            ctr.AccessLevel = fieldResult.getPicklistValues()[0].getValue();
            insert ctr;
        }

		caseteamtemplate ct;
		caseteamrole cr;
		caseteamtemplate c;
		try{
			cr=[select id from caseteamrole where name=:'Test Role'];
			ct=[select id,name from caseteamtemplate where name=:'test'];
			c=ct;
		} catch(Exception e) {
			if(ct==null) {
				//create case team template
				c=new caseteamtemplate(); 
				c.Description='test';
				c.Name='test';
				insert(c);
			}
		}
		//add case team member to pre defined case team
		caseteamtemplatemember cm=new caseteamtemplatemember();
		System.runAs ( thisUser ) {
			cm.memberid=UserInfo.getUserId();
			cm.teamroleid=cr.id;
			cm.teamtemplateid=c.id;
			insert(cm);
		}

		setting.PredefinedCaseTeamId__c = c.Id;
		User user = [SELECT Id, userRole.Name FROM User WHERE Id = :UserInfo.GetUserId()];
		setting.UserRoleName__c = user.userRole.Name;
        
        
		setting.CaseTeamRoleName__c = ctr.Name;

		setting.LastSupportOwnerRoleId__c = ctr.Id;
		System.runAs ( thisUser ) {
			insert setting;
		}
		Group g = new Group(Type = 'Queue', Name = setting.QueueName__c);
        System.runAs ( thisUser ) {
			insert g;
        }
		QueueSObject q = new QueueSObject(SobjectType = 'Case', QueueId = g.Id);
        System.runAs ( thisUser ) {
			insert q;
        }
		Case caseItem = new Case();
		caseItem.Subject = 'Test Case';
		caseItem.Last_Support_Owner_ID__c = user.Id;
        //try {
        System.runAs ( thisUser ) {
			insert caseItem;
		}
		caseItem.OwnerID = g.Id;
		caseItem.Last_Support_Owner_ID__c = '';
		Test.startTest();
		try {
			System.runAs ( thisUser ) {
				update caseItem;
			}
		} catch (Exception e) {
			System.debug(e);
		}

		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
		User u = new User(Alias = 'standt', Email='standarduser@skyboxsecurity.com',
			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', UserRoleId = '00E30000000i2UR',
			TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@skyboxsecurity.com');
		Map<Id,Id> testMap = new Map<Id,Id>();
		testMap.put(caseItem.Id, u.Id);
		FutureSetOldSupportOwnerOnCase.setupOldSupportOwners(testMap);

		Test.stopTest();
	}
}