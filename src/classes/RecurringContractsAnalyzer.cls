public with sharing class RecurringContractsAnalyzer {
  private ApexPages.StandardController ctrl;

  public RecurringContractsAnalyzer(ApexPages.StandardController ctrlParam){
    ctrl = ctrlParam;
  }
  
  public RecurringContractsAnalyzer() {
    
  }
  
  public List<Contract> getContracts() {
    return [SELECT id, Duration_months__c,  Monthly_Contract_Value__c, Monthly_New_Value__c, StartDate, EndDate
          FROM Contract 
          WHERE (RecordTypeId = '012600000009W1dAAE' or RecordTypeId = '012600000009W1YAAU' or RecordTypeId = '0120e000000JOlfAAG') and //CPQ Contract added
          Duration_months__c > 6];
  }
  
  public Integer getContractsNumber() {
      return getContracts().size();
  }

  public List<Record> getRecords() {
    List<Contract> contracts = getContracts();
    
    List<Record> records = new List<Record>();
    Date periodStart = Date.newInstance(2015, 1, 1);
    Date periodEnd = periodStart.addMonths(1).addDays(-1);
    Integer i = 0;
    
    while (periodEnd <= Date.today()) {
        Record r = new Record(periodEnd);
                 
        Integer n = 0;
        for (Integer j = 0 ; j < contracts.size() ; j++) {
            if (contracts[j].StartDate <= periodEnd && contracts[j].EndDate >= periodEnd ) {
                n++;
                r.MRR += contracts[j].Monthly_Contract_Value__c == null ? 0 : contracts[j].Monthly_Contract_Value__c;
                if(contracts[j].StartDate >= periodStart) //2017-09-01 issue SF-230
                    r.newMRR += contracts[j].Monthly_New_Value__c;
             

            }
         }
    
        r.numberOfContracts = n;
        r.renewedMRR = r.MRR - r.newMRR;
        
        r.churnedMRR = (i > 0 ? records[i-1].MRR - r.renewedMRR : 0);
        
        if (i > 0) {
            r.netRenewalRate = (records[i-1].MRR > 0 ? r.MRR / records[i-1].MRR : 1);
            r.churnRate = (records[i-1].MRR > 0 ? r.churnedMRR / records[i-1].MRR : 0);
            r.renewalRate = 1 - r.churnRate;
        }

        periodStart = periodStart.addMonths(1);
        periodEnd = periodStart.addMonths(1).addDays(-1);
        records.add(r);
        i++;
    }
 
    return records;
  }
  
  public class Record {
    public Date periodEnd {get; set; }
    public Decimal MRR { get; set; }
    public Decimal newMRR { get; set; }
    public Decimal churnedMRR { get; set; }
    public Decimal renewedMRR { get; set; }
    public Integer numberOfContracts { get; set; }
    public Decimal netRenewalRate { get; set; }
    public Decimal renewalRate { get; set; }
    public Decimal churnRate { get; set; }
    public String getPeriodEndString() {
        return periodEnd.year() + '-' + periodEnd.month();
    }
  
    public Record(Date periodEnd) {
        this.periodEnd = periodEnd;
        this.MRR = 0;
        this.newMRR = 0;
        this.churnedMRR = 0;
        this.renewedMRR = 0;
        this.netRenewalRate = 1;
        this.churnRate = 0;
        this.renewalRate = 1;
    }
  }

}