@isTest
private class LicenseHandlerTest {
	
	@testSetup static void setupData() {
       
       ClsObjectCreator cls = new ClsObjectCreator();

       //create Account
        Account acc1 =  cls.createAccount('PartnerAccount1');
        Account acc2 =  cls.createAccount('PartnerAccount2');
        
        //create Contact
        Contact con1 = ClsObjectCreator.createContact('Con1', acc1.Id);
        Contact con2 = ClsObjectCreator.createContact('Con2', acc1.Id);
        Contact con3 = ClsObjectCreator.createContact('Con3', acc2.Id);
        Case cs = cls.createCasewithAccount(acc1.id);

        Opportunity opp = cls.createOpportunity(acc1);

        License_Version__c lv = new License_Version__c( Default_version__c = true);
		insert lv;
   }
    
   //MP-24 : Eligible for generate license
	@isTest static void generateLicenseCondTest() {

		List<Contact> con = [Select Id from Contact];
		Case cs = [Select Id from Case limit 1];
        ClsObjectCreator.createLicenseDefault();
		//create License2 - Demo - first demo to contact
		License2__c liFirstDemo = ClsObjectCreator.createLicense2('Demo', con[0].Id, Date.today().addDays(1),'Partner Community');

		//create License2 - first Partner_Test to account 
		License2__c liFirstPartnerTest = ClsObjectCreator.createLicense2('Partner Test', con[0].Id, Date.today().addDays(1),'Partner Community');
        
        //create License2 - first support to account 
        License2__c liFirstSupport = ClsObjectCreator.createLicense2(cs);
		//Check both Demo, PartnerTest & support Licenses were created
		List<License2__c> licTest = [select Id from License2__c];
		 System.assertEquals(3, licTest.size());

		Test.startTest();
			//create License2 - Demo - Second demo to different contact
			License2__c liSecondDemo = ClsObjectCreator.createLicense2('Demo', con[1].Id, Date.today().addDays(1),'Partner Community');
			//create License2 - Second Partner_Test to different account 
			License2__c liSecPartnerTest = ClsObjectCreator.createLicense2('Partner Test', con[2].Id, Date.today().addDays(1),'Partner Community');//works only if con[2] really be con3...
		  
		Test.stopTest(); 
		licTest = [select Id from License2__c];
		System.assertEquals(5, licTest.size());

		try{
		//create License2 - Support - Second demo to same contact
		 License2__c liSecondSupport = ClsObjectCreator.createLicense2(cs);
		// System.assertEquals('Support License has been generated for this account in the last 3 months', '');
		}
		catch(DMLException e){
			System.assertEquals('You already have an active demo license', e.getDmlMessage(0));
		}

		try{
		//create License2 - Demo - Second demo to same contact
		 liSecondDemo = ClsObjectCreator.createLicense2('Demo', con[1].Id, Date.today().addDays(1),'Partner Community');
		 System.assertEquals('Should Alert On: You already have Demo', '');
		}
		catch(DMLException e){
			System.assertEquals('You already have an active demo license', e.getDmlMessage(0));
		}

		try{
		//create License2 - Second Partner_Test to account 
		 liSecPartnerTest = ClsObjectCreator.createLicense2('Partner Test', con[1].Id, Date.today().addDays(1),'Partner Community');
		 System.assertEquals('Should Alert On: You already have Partner Test', '');	 
		}
		catch(DMLException e){
			System.assertEquals('You already have an active partner test license', e.getDmlMessage(0));
		}

		//create License2 - Second Partner_Test to account But SourceCreation is NOT 'Partner Community'
		 liSecPartnerTest = ClsObjectCreator.createLicense2('Partner Test', con[1].Id, Date.today().addDays(1),'Internal');

    	licTest = [select Id from License2__c];
		System.assertEquals(7, licTest.size());

	}
	
	//MP-24 : Eligible for generate license
	@isTest static void enerateLicenseCondWithExperationDateTest() {
		
		List<Contact> con = [Select Id from Contact];
		
		//create License2 - Demo - first demo to contact
		License2__c lic1Demo = ClsObjectCreator.createLicense2('Demo', con[0].Id, Date.today().addDays(1),'Partner Community');
		lic1Demo.Expiration_Date__c = Date.today();
		update lic1Demo;

		Test.startTest();
			//create License2 - Demo - Second demo to same contact, but after license expaierd/today be expaierd
			License2__c lic2Demo = ClsObjectCreator.createLicense2('Demo', con[0].Id, Date.today().addDays(1),'Partner Community');
		Test.stopTest();

		List<License2__c> licTest = [select Id from License2__c];
		System.assertEquals(2, licTest.size());
	}

	//MP-24 : Eligible for generate license
	//@isTest static void updateLicenseTest() {
	//}

	//SF-925 : To be able to change the default install base amounts on production licenses
	//by setting production  License Generate_License__c default as false.
	/*@isTest static void productionLicenseTest() {
		List<Contact> con = [Select Id from Contact];
		con[0].email = 'rina@noEmail.com';
		update con[0];

		List<Account> acc = [Select Id from Account];
		List<Opportunity> opp = [Select Id from Opportunity];

		Test.startTest();

			//create License2 
			License2__c licPOC = ClsObjectCreator.createLicense2('POC', opp[0].Id);
        	License2__c licPartnerTest = ClsObjectCreator.createLicense2('Partner Test', con[0].Id);
			License2__c licProduction = ClsObjectCreator.createLicense2('Production', acc[0].Id);

		Test.stopTest();

		//check Generate_License__c default is true.
		List<License2__c> licTest = [select Id, Generate_License__c from License2__c where RecordType.Name = 'Partner Test'];
		System.assertEquals(true, licTest[0].Generate_License__c);

		//check production Generate_License__c default as false.
		licTest = [select Id, Generate_License__c from License2__c where RecordType.Name = 'Production'];
	  
        
	    ApexPages.StandardController sc = new ApexPages.StandardController(new License2__c(Account__c = acc[0].id));
	    PageReference pageRef = Page.License2Production;
	    Test.setCurrentPageReference(pageRef); // use setCurrentPageReference,
        ApexPages.currentPage().getParameters().put('CF00N0e000003JpNQ_lkid',acc[0].id);
         ApexPages.currentPage().getParameters().put('RecordType','0120e000000M5RS');

	    License2ProductionController lp = new License2ProductionController(sc);


		//System.assertEquals(false, licTest[0].Generate_License__c);
	}*/
	
}