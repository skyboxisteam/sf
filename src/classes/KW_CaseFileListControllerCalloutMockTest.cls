@isTest
global class KW_CaseFileListControllerCalloutMockTest implements HttpCalloutMock {
	
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"blabla":"nanananan","name":"test1","created":"11/11/1111","created":"11/12/23","name":"blabla","id":"123"}');
        response.setStatusCode(200);
        return response;
    }
}