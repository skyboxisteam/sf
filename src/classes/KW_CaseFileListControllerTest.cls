@isTest(SeeAllData=true)
public class KW_CaseFileListControllerTest {
    
    static testmethod void KW_CaseFileListControllerTest() {
    	User user = [SELECT Id, userRole.Name FROM User WHERE Id = :UserInfo.GetUserId()];

    	Case c = new Case();
		c.Subject = 'Test Case';
		c.Last_Support_Owner_ID__c = user.Id;
		insert c;

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new KW_CaseFileListControllerCalloutMockTest());
                
        KW_CaseFileListContoller.MakeCallOut(c.Id);
        
        Test.stopTest();
    }
    
}