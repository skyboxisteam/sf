/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true) 
private class AccountSubscriptionTest {

    static testMethod void myUnitTest() {
      
       ClsObjectCreator cls = new ClsObjectCreator();
    //   Account acc = cls.createAccount('test');
      /* opportunity opp = cls.createOpportunity(acc);
       Contract cr = cls.createContract(acc,opp);
       Product2 p = cls.createProduct('test');
       SBQQ__Subscription__c su = cls.createSubscription(acc,cr,p);
      
       Asset ass = cls.createAsset('test', 'SN');
       SBQQ__SubscribedAsset__c sua = cls.createSubscribedAsset(su,ass);*/
       SB_Subscribed_Asset_Copy__c sua = [select id,SB_Subscription_Copy__c from SB_Subscribed_Asset_Copy__c where SB_Subscription_Copy__c != null  limit 1 ];
 	   SB_Subscription_Copy__c su = [select id,Account__c from SB_Subscription_Copy__c where id =:sua.SB_Subscription_Copy__c limit 1];
 	   Account acc = [select id,name from account where id =: su.Account__c] ;
       ApexPages.StandardController sc = new ApexPages.StandardController(acc);
       AccountSubscription asu = new AccountSubscription(sc);
       asu.showExpiered = true;
       //asu.setShowExpire();
       //asu.clickRow();
       //asu.processSelected();
        
    }
}