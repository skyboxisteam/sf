public with sharing class RecordTypeSelectionController {
   
    @AuraEnabled
    public static string findRecordTypes(string objName){
        Set <id> setRTAvailable = new Set <id>();
        List<Schema.RecordTypeInfo> infos = Schema.SObjectType.Case.RecordTypeInfos; //find all Case record types       
        Schema.RecordTypeInfo defaultRT = null;
        List<RecordTypeWrapper> wrapperListTemp = new List<RecordTypeWrapper>();

        for (Schema.RecordTypeInfo info : infos) {   //check each one
            if(info.isDefaultRecordTypeMapping()){          	    
                defaultRT = info;
                continue;
            }
        	if (info.isAvailable() && !info.isMaster()){ 
              	//setRTAvailable.add(info.getRecordTypeId());                               
              	wrapperListTemp.add(new RecordTypeWrapper(info.getName(), info.getRecordTypeId()));
            }                           
        }
        
        List<RecordTypeWrapper> wrapperList = new List<RecordTypeWrapper>();
        if(defaultRT != null){
            wrapperList.add(new RecordTypeWrapper(defaultRT.getName(), defaultRT.getRecordTypeId()));
        }
        wrapperList.addAll(wrapperListTemp);
        
        /*
        string returnString='';
        string queryString='Select id, name from RecordType where sobjectType =: objName and IsActive=true and id in: setRTAvailable';
        List<sobject> recordList= Database.query(queryString);
        List<RecordTypeWrapper> wrapperList=new List<RecordTypeWrapper>();
        for(sobject sb : recordList)  {
            RecordTypeWrapper rw=new RecordTypeWrapper();
            rw.recordTypeLabel=string.valueof(sb.get('name'));
            rw.recordTypeId=string.valueof(sb.get('id'));
            wrapperList.add(rw);
        } 
		*/               
        return JSON.serialize(wrapperList);
    }
    
    public class RecordTypeWrapper{
        public string recordTypeLabel{get;set;}
        public string recordTypeId{get;set;}
        
        public RecordTypeWrapper(String recordTypeLabel, String recordTypeId){
            this.recordTypeLabel = recordTypeLabel;
            this.recordTypeId = recordTypeId;
        }
    }
}