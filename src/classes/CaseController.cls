public class CaseController {

    @AuraEnabled
    public static Id saveCase (Case cs) {
        upsert cs;
        return cs.Id;
    }
    
    @AuraEnabled
    public static List<String> getselectOptions(sObject objObject, string fld, Id recTypeId) {
      system.debug('objObject --->' + objObject);
      system.debug('fld --->' + fld);
      List <String> allOpts = new list < String > ();
      // Get the object type of the SObject.
      Schema.sObjectType objType = objObject.getSObjectType();
        
      //String query = 'SELECT RecordType.DeveloperName FROM ' + objType + ' WHERE Id = '
      // Describe the SObject using its object type.
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
     
      // Get a map of fields for the SObject
      map <String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
      // Get the list of picklist values for this field.
      
      if (fld == 'Type'){
        String recTypeDevName = [SELECT DeveloperName FROM RecordType WHERE Id =: recTypeId LIMIT 1].DeveloperName;
            system.debug('$$$ recTypeDevName: ' + recTypeDevName);
              if (recTypeDevName != null && Case_Type_Dependancy__c.getValues(recTypeDevName) != null){
                allOpts = (Case_Type_Dependancy__c.getValues(recTypeDevName).Values__c).split(',');
              }

      } else {
         list <Schema.PicklistEntry> values;
         values = fieldMap.get(fld).getDescribe().getPickListValues();
          for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
      }
      // Add these values to the selectoption list.
      
      system.debug('allOpts ---->' + allOpts);
      //allOpts.sort();
      return allOpts;
 }
  
}