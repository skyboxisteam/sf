@isTest
private class PMStatusTriggerTest
{
	//Author: Rina Nachbas			Date:16.07.18
	//Task:   SF-664	
	@isTest
	static void newPMStatus()
	{
		// Given
		ClsObjectCreator objCreator = new ClsObjectCreator();
		Account acc = objCreator.createAccount('My Account');
		Deployment__c d = objCreator.createDeployment(acc.Id, 'My Project');

		// When
		Test.startTest();
		//Test Create first PM_Status__c
		PM_Status__c pms1 = objCreator.createPMStatuses(d.Id, 'Status A');

		Test.stopTest();
		
		// Then
		Deployment__c depTest = [select Id, Last_PM_Status__c  from Deployment__c where Id = :d.Id];
		System.assertEquals('Status A', depTest.Last_PM_Status__c);
		
		//Test Create Second PM_Status__c
		PM_Status__c pms2 = objCreator.createPMStatuses(d.Id, 'Status B');
		depTest = [select Id, Last_PM_Status__c  from Deployment__c where Id = :d.Id];
		// Then
		System.assertEquals('Status B', depTest.Last_PM_Status__c);

	}

	@isTest
	static void deletePMStatus()
	{
		// Given
		ClsObjectCreator objCreator = new ClsObjectCreator();
		Account acc = objCreator.createAccount('My Account');
		Deployment__c d = objCreator.createDeployment(acc.Id, 'My Project');

		// When
		Test.startTest();
		//Create first PM_Status__c
		PM_Status__c pms1 = objCreator.createPMStatuses(d.Id, 'Status A');
		Deployment__c depTest = [select Id, Last_PM_Status__c  from Deployment__c where Id = :d.Id];
		System.assertEquals('Status A', depTest.Last_PM_Status__c);
		//Create Second PM_Status__c
		PM_Status__c pms2 = objCreator.createPMStatuses(d.Id, 'Status B');
		depTest = [select Id, Last_PM_Status__c  from Deployment__c where Id = :d.Id];
		System.assertEquals('Status B', depTest.Last_PM_Status__c);
		delete pms2;
		Test.stopTest();

		// Then
		depTest = [select Id, Last_PM_Status__c  from Deployment__c where Id = :d.Id];
		System.assertEquals('Status A', depTest.Last_PM_Status__c);

		// When
		delete pms1;
		// Then
		depTest = [select Id, Last_PM_Status__c  from Deployment__c where Id = :d.Id];
		System.assertEquals(null, depTest.Last_PM_Status__c);
	}

	@isTest
	static void updatePMStatus()
	{
		// Given
		ClsObjectCreator objCreator = new ClsObjectCreator();
		Account acc = objCreator.createAccount('My Account');
		Deployment__c d = objCreator.createDeployment(acc.Id, 'My Project');

		Test.startTest();
		//Create first PM_Status__c
		PM_Status__c pms1 = objCreator.createPMStatuses(d.Id, 'Status A');
		Deployment__c depTest = [select Id, Last_PM_Status__c  from Deployment__c where Id = :d.Id];
		System.assertEquals('Status A', depTest.Last_PM_Status__c);
		//Create Second PM_Status__c
		//need delay between objects creation - thus update trigger code to compare against '>='
		PM_Status__c pms2 = objCreator.createPMStatuses(d.Id, 'Status B');
		depTest = [select Id, Last_PM_Status__c  from Deployment__c where Id = :d.Id];
		System.assertEquals('Status B', depTest.Last_PM_Status__c);
		
		// When update last
		pms2.Status__c = 'Status C';
		update pms2;
		PM_Status__c pmTest = [select Status__c, createdDate  from PM_Status__c where Id =:pms2.Id];
		//System.debug('@@pmTest: ' + pmTest);
		Test.stopTest();

		// Then status updated
		depTest = [select Id, Last_PM_Status__c  from Deployment__c where Id = :d.Id];
		System.assertEquals('Status C', depTest.Last_PM_Status__c);

		//When update NOT last
		pms1.Status__c = 'Status D';
		update pms1;
		// Then status NOT updated
		depTest = [select Id, Last_PM_Status__c  from Deployment__c where Id = :d.Id];
		System.assertEquals('Status C', depTest.Last_PM_Status__c);
	}
}