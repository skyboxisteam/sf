/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 @isTest(SeeAllData=true)
private class License2GenratationTest {

    static testMethod void GenerateTest() {
       ClsObjectCreator cls = new ClsObjectCreator();
       Account acc= cls.createAccount('test');
       Opportunity opp = cls.createOpportunity(acc);
       id prodId = [select id from recordtype where SobjectType  = 'License2__c' and DeveloperName = 'Production' limit 1].id;
       Asset ass = [select id,
                      accountId,
                        Quantity_Attribute__c,
                        Product2.WDSB_Sub_Family__c,
                        Product2.ProductCode                          
                 from Asset 
                 where Product2id != null and Product2.Family = 'Software License' limit 1];

       SBQQ__Subscription__c s = [select id,
                      SBQQ__Account__r.id,
                        Subscription_Quantity_Formula__c,
                        SBQQ__Product__r.WDSB_Sub_Family__c,
                        SBQQ__Product__r.ProductCode,
                        SBQQ__EndDate__c                       
                 from SBQQ__Subscription__c 
                 where  SBQQ__Product__c !=null and SBQQ__Product__r.Family in( 'Software License', 'Content Dictionary')  and SBQQ__EndDate__c > Today limit 1];


       test.starttest();
         License2__c ls = cls.createLicense2(opp);
         License2__c lsSub = new License2__c(Account__c =  s.SBQQ__Account__r.id, recordtypeid = prodId );            
         insert lsSub;

         License2__c lsAss = new License2__c(Account__c =  ass.accountId, recordtypeid = prodId );
         insert lsAss;

        ApexPages.StandardController sc = new ApexPages.StandardController(new License2__c(Account__c =  ass.accountId, recordtypeid = prodId ));
        PageReference pageRef = Page.License2Production;
        Test.setCurrentPageReference(pageRef); // use setCurrentPageReference,
        ApexPages.currentPage().getParameters().put('CF00N0e000003JpNQ_lkid',ass.accountId);
        ApexPages.currentPage().getParameters().put('RecordType',prodId);

      License2ProductionController lpc = new License2ProductionController(sc);
      LicenseProduction lp = new LicenseProduction(ass.accountId);
      lp.GenerateLicense();
      lp.popLicense(lsAss);

       test.stoptest();

        
    }
}