global with sharing class AccountUnderSupportSchedule implements Schedulable {
	  
	  global void execute(SchedulableContext sc)
	  {
	    list<Account> result = new  list<Account>();
	    list<Account> accUnderList = [Select id from Account where Under_Support__c = false and id in (select SBQQ__Account__c from SBQQ__Subscription__c where SBQQ__EndDate__c >TODAY) ];
	    list<Account> accNotUnderList = [Select id from Account where Under_Support__c = true and id not in (select SBQQ__Account__c from SBQQ__Subscription__c where SBQQ__EndDate__c >TODAY) ];
        
        for(Account acc : accNotUnderList)
        {
          acc.Under_Support__c = false;
          result.add(acc);
        }

        for(Account acc : accUnderList)
        {
          acc.Under_Support__c = true;
          result.add(acc);
        }
      
       update result;
     

	  }
}