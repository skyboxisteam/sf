trigger CaseTrigger on Case (before update, after insert) {
	
  	if(trigger.isBefore && trigger.isUpdate)
    {
	 //CaseVMHandler cvh = new CaseVMHandler();
	 //cvh.createVMCloneRequest(trigger.new, trigger.oldMap);
    }

	
    if(trigger.isAfter && trigger.isInsert)
    {
    	//Task: SF-847 : Update last creared deployment in case
    	CaseHandler.updateCase(trigger.new, trigger.oldMap);
    }
    
}