trigger PSTabRecordTrigger on PS_Tab_Record__c (after insert, after update) {

     PSTabRecordProjectManagerValidation.validate(trigger.newMap);
   
}