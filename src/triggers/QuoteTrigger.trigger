trigger QuoteTrigger on SBQQ__Quote__c (after insert, before insert, before update, after update, before delete) {
        
    if(trigger.isinsert && trigger.isAfter)
    {
        CPQMethods cp = new CPQMethods();
        cp.quoteNamingConvention(trigger.new);
    }

    if(trigger.isBefore && !trigger.isDelete)
       QuoteHandler.manageDistResller(trigger.new);


    if (trigger.isUpdate && trigger.isAfter)
    {
      //CPQ-123
      CPQMethods cp = new CPQMethods();
      cp.quoteApproved(trigger.new, trigger.oldMap);
    }

    if(trigger.isBefore && trigger.isDelete)
      //SF-910
       QuoteDeleteHandler.validateDelete(trigger.old);
}