trigger CloneEngineerUserLookup on Deployment__c (after insert, after update) {
	
	Map<Id, Account> accountMap = new Map<Id, Account>();
	boolean relevant = false;
	for(Deployment__c deployment_i :trigger.new) {
 
			
		if(deployment_i.Deployment_Type__c =='Deployment by partner' ||deployment_i.Deployment_Type__c == 'Project' || deployment_i.Deployment_Type__c == 'Deployment' && deployment_i.Account__c != null) 
		  {
			if(accountMap.containsKey(deployment_i.Account__c) && deployment_i.PS_Owner__c != null ) {
				accountMap.get(deployment_i.Account__c).PS_Engineer_User__c = deployment_i.PS_Owner__c;
			} else {
				Account acc = new Account(Id = deployment_i.Account__c, PS_Engineer_User__c = deployment_i.PS_Owner__c); 
				accountMap.put(deployment_i.Account__c, acc);
			}
			if(accountMap.containsKey(deployment_i.Account__c) && deployment_i.Project_Manager__c != null) {
				accountMap.get(deployment_i.Account__c).Project_Manager__c = deployment_i.Project_Manager__c;
			} else {
				Account acc = new Account(Id = deployment_i.Account__c, Project_Manager__c = deployment_i.Project_Manager__c); 
				accountMap.put(deployment_i.Account__c, acc);
			}
		}
	}
	update accountMap.values();
}