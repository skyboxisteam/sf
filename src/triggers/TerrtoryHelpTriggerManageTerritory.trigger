trigger TerrtoryHelpTriggerManageTerritory on Territory_Help__c (after delete, after insert) 
{

   TerritoryHelpHandler handler;

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//-------------------------------------------------------------isInsert -----------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
 
     if(trigger.isInsert)
      {
        handler = new TerritoryHelpHandler(trigger.new);
        
      }
   
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//--------------------------------------------------------------isDelete -----------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
 
   if(trigger.isDelete)
      {
       list<id> relevantAccount = new list<id>();
       for(Territory_Help__c th : trigger.old)
         relevantAccount.add(th.account__c);
     system.debug('%%%' +relevantAccount);
     if(!relevantAccount.isEmpty())
     {
       list<Territory_Help__c> otherTerritories  = [select ID,name,Country_and_state_key__c,Update_Association__c,Account_territory_Association_id__c,Cause_of_association__c ,
                                                    Account__c,Territory_ID__c,Territory_Name__c,Country__c, State__c 
                                                    from Territory_Help__c 
                                                    where account__c IN : relevantAccount and  id Not IN :trigger.old
                                                    order by Update_Association__c desc];
                                                         
       handler = new TerritoryHelpHandler(otherTerritories);
     }
    }
    
                                                    
     
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
      
}