trigger CaseOwnerChangedToQueueOrTLSRole on Case (before update,after update) {
    if(trigger.isBefore && trigger.isUpdate) {
        CaseTriggerSettings__c triggerSetting = CaseTriggerSettings__c.getAll().Values()[0];
        Set<Id> ownerIdSet = new Set<Id>();
        for(Id Id_i : trigger.newMap.keySet()) {
            ownerIdSet.add(trigger.newMap.get(Id_i).OwnerId);
        }
        Map<Id, User> userMap =new Map<Id, User>([SELECT Id, userRole.Name FROM User WHERE Id IN :ownerIdSet]);
        if(triggerSetting != null) {            
            Set<String> userRoleNameSet = new Set<String>(triggerSetting.UserRoleName__c.split(','));
            Id QueueId = [SELECT Id FROM Group WHERE Type = 'Queue' AND Name = :triggerSetting.QueueName__c LIMIT 1].Id;            
            for(Id Id_i : trigger.newMap.keySet()) {
                System.debug(trigger.newMap.get(Id_i).OwnerId);
                if(trigger.newMap.get(Id_i).OwnerId != trigger.oldMap.get(Id_i).OwnerId 
                    && (trigger.newMap.get(Id_i).OwnerId == QueueId 
                        || (userMap.containsKey(trigger.newMap.get(Id_i).OwnerId) 
                            && userRoleNameSet.contains(userMap.get(trigger.newMap.get(Id_i).OwnerId).userRole.Name) == true))) {
                    trigger.newMap.get(Id_i).Status = 'Assigned to R&D';
                }
            }
        } else {
            System.debug('Please insert data to CaseTriggerSettings__c custom settings');
        }
    }
    if(trigger.isAfter && trigger.isUpdate) {
        Set<Id> caseIdSet = new Set<Id>();
        Set<Id> ownerIdSet = new Set<Id>();
        for(Id Id_i : trigger.newMap.keySet()) {
            ownerIdSet.add(trigger.newMap.get(Id_i).OwnerId);
        }
        Map<Id, User> userMap =new Map<Id, User>([SELECT Id, userRole.Name FROM User WHERE Id IN :ownerIdSet]);
        CaseTriggerSettings__c triggerSetting = CaseTriggerSettings__c.getAll().Values()[0];
        if(triggerSetting != null) {
            Set<String> userRoleNameSet = new Set<String>(triggerSetting.UserRoleName__c.split(','));
            Id QueueId = [SELECT Id FROM Group WHERE Type = 'Queue' AND Name = :triggerSetting.QueueName__c LIMIT 1].Id;
            for(Id Id_i : trigger.newMap.keySet()) {
                System.debug(trigger.newMap.get(Id_i).OwnerId);
                if(trigger.newMap.get(Id_i).OwnerId != trigger.oldMap.get(Id_i).OwnerId 
                    && (trigger.newMap.get(Id_i).OwnerId == QueueId 
                        || (userMap.containsKey(trigger.newMap.get(Id_i).OwnerId) 
                            && userRoleNameSet.contains(userMap.get(trigger.newMap.get(Id_i).OwnerId).userRole.Name) == true))) {
                    caseIdSet.add(Id_i);
                }
            }
            if(!caseIdSet.isEmpty()) {
                List<User> userList = [SELECT Id FROM User WHERE userRole.Name IN :userRoleNameSet AND isActive = true];
                List<CaseTeamMember> caseTeamMemberList = new List<CaseTeamMember>();
                Id teamMemberRoleId = [SELECT Id FROM CaseTeamRole WHERE Name = :triggerSetting.CaseTeamRoleName__c LIMIT 1].Id;
                List<CaseTeamMember> caseOldTeamMemberList = [SELECT ParentId, MemberId FROM CaseTeamMember WHERE ParentId IN :caseIdSet];
                Map<Id,Set<Id>> caseTeamMemberMap = new Map<Id,Set<Id>>();
                for(CaseTeamMember ctm_i : caseOldTeamMemberList) {
                    if(!caseTeamMemberMap.containsKey(ctm_i.ParentId)) {
                        caseTeamMemberMap.put(ctm_i.ParentId, new Set<Id>());
                    }
                    caseTeamMemberMap.get(ctm_i.ParentId).add(ctm_i.MemberId);
                }
                if(teamMemberRoleId != null) {
                    for(Id caseId_i : caseIdSet) {
                        for (User user_i : userList) {
                            Boolean contains = false;
                            if(caseTeamMemberMap.containsKey(caseId_i)) {
                                for(Id id_i : caseTeamMemberMap.get(caseId_i)) {
                                    if(id_i == user_i.Id) {
                                        contains = true;
                                        break;
                                    }
                                }
                            }
                            if(!contains) {
                                caseTeamMemberList.add(new CaseTeamMember( ParentId = caseId_i, MemberId = user_i.Id, TeamRoleId = teamMemberRoleId ));
                            }
                        }
                    }
                }
                if(String.isNotEmpty(triggerSetting.PredefinedCaseTeamId__c)) {
                    List<CaseTeamTemplateRecord> cttrDBList = [SELECT TeamTemplateId, 
                                                                      ParentId 
                                                                 FROM CaseTeamTemplateRecord 
                                                                 WHERE TeamTemplateId = :triggerSetting.PredefinedCaseTeamId__c 
                                                                       AND ParentId IN :caseIdSet];
                    Set<Id> caseToAddTeamSet = new Set<Id>();
                    for(Id caseId_i : caseIdSet) {
                        Boolean contains = false;
                        for(CaseTeamTemplateRecord cttr_i : cttrDBList) {
                            if(cttr_i.ParentId == caseId_i) {
                                contains = true;
                                break;
                            }
                        }
                        if(contains == false) {
                            caseToAddTeamSet.add(caseId_i);
                        }
                    }
                    List<CaseTeamTemplateRecord> cttrList = new List<CaseTeamTemplateRecord>();
                    for(Id caseId_i : caseToAddTeamSet) {
                        cttrList.add(new CaseTeamTemplateRecord(TeamTemplateId = triggerSetting.PredefinedCaseTeamId__c, ParentId = caseId_i));
                    }
                    insert cttrList;
                }
                insert caseTeamMemberList;
            }
        } else {
            System.debug('Please insert data to CaseTriggerSettings__c custom settings');
        }
    }
}