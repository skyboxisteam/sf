trigger BugTrigger on Bug__c (
	after update) {

		if (Trigger.isAfter && Trigger.isUpdate) {
			//Task: SF-733
	    	BugHandler.createCaseComment(Trigger.new, Trigger.oldMap);
	    
		}
}