trigger CommissionRecordTrigger on Commission_Record__c (after insert, before insert,after update, before delete) 
{
   BookingScheduleCommission bsc = new BookingScheduleCommission();
   if(trigger.isDelete)
     bsc.deletedCommission(trigger.old);
   else if(trigger.isAfter)
     bsc.createFromCommissionRecord(trigger.new);

   if(trigger.isInsert && trigger.isBefore){
     //RinaN, 1.11.18, CM-64
     CommissionRecordHandler.probabilityValidation(trigger.new);
   }
}