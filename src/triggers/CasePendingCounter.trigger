/******************************************************************************* 
Name              : CasePendingCounter
Description       : Calculates days in pending status
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Hernan                228/01/2013                                    [SW-4492]
*******************************************************************************/
trigger CasePendingCounter on Case (before update) {
    try{
        if( MonitorRecursionClass.getCasePendingCounterMonitor() == false ){
            MonitorRecursionClass.setCasePendingCounterMonitor(true);
            for( Integer i=0; i < trigger.size; i++ ){
                Case oldCase = trigger.old[i];
                Case newCase = trigger.new[i];
                
                if( oldCase.Status != newCase.Status && oldCase.Status != null && oldCase.Status.equalsIgnoreCase('pending customer') && newCase.Status != null && !newCase.Status.equalsIgnoreCase('pending customer')){
                    if( newCase.Pending_Customer_Start_Date__c != null){
                        Long sec = system.now().getTime() - newCase.Pending_Customer_Start_Date__c.getTime();
                        
                        decimal ms = sec;
                        system.debug('ms1: ' + ms);
                        system.debug('ms1: ' + ms/1000.0);
                        system.debug('ms1: ' + ms/1000.0/60.0);
                        system.debug('ms1: ' + ms/1000.0/60.0/60.0);
                        system.debug('ms1: ' + ms/1000.0/60.0/60.0/24.0);
                        
                        ms = ((((ms / 1000.0) /60.0)/60.0)/24.0);
                        system.debug('ms1: ' + ms);
                        if(newCase.Days_Pending_Customer__c == null){
                            newCase.Days_Pending_Customer__c = 0.0;
                        }
                        newCase.Days_Pending_Customer__c += ms;
                    }
                }
            }
        }
    }catch(Exception e){
        trigger.new[0].addError(e.getMessage());
    }
}