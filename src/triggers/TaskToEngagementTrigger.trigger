trigger TaskToEngagementTrigger on Task (after insert, after update, before delete) {
	
	list<Task> taskToUpsert = new  list<Task>();
	list<Task> taskToDelete = new  list<Task>();
	list<string> taskIdStringList = new list<string>();
	list<Id> whoIdList = new list<Id>();
	private final string MeetingSchedule = 'Meeting';
	private final string MeetingCompleted = 'Meeting Completed';
	set<string> relevantMeetingTypes =new set<string>{MeetingSchedule,MeetingCompleted}; 
	set<string> relevantObjectTypes =new set<string>{'Lead','Contact'}; 
	Map<string,Engagement__c> taskToEngagementMap = new Map<string,Engagement__c>();
	list<task> ListTask = [select id,type, whoid,createddate, Create_Lead_statistics__c,createdbyId , createdby.userRole.developername, ActivityDate, Task_OSR__c , who.type, Meeting_relevance__c, Status 
	                          from Task
	                          Where id in : trigger.isDelete?trigger.old:trigger.new]; 
	
	
	for(Task ts : ListTask)
	{
		system.debug('$$$ ts.who.type' + ts.whoid);
		system.debug('$$$ ts.type' + ts.type);
	 if(ts.whoId != null && relevantMeetingTypes.contains(ts.type) || ts.Create_Lead_statistics__c ==true)
	  {
	   taskIdStringList.add(ts.id);
	   whoIdList.add(ts.whoid);
	   if(trigger.isDelete)
	     taskToDelete.add(ts);
	   else
	     taskToUpsert.add(ts);
	  }
	}  
	  
	  
	 TaskToEngagementHandler tteh = new TaskToEngagementHandler(taskIdStringList,whoIdList);
	 if(trigger.isInsert || trigger.isUpdate)
	 {
	  list<Engagement__c> engagementToUpsert = tteh.convertToEngage(taskToUpsert,trigger.isInsert);
	   upsert engagementToUpsert;
	 }
	 if(trigger.isDelete){
	 	 list<Engagement__c> engagementToDelete =  tteh.engagementList;
	  
	//  system.debug('$$$' + engagementToUpsert);  
	 
	  delete engagementToDelete;
	     
	 }
	
	 
	 
	 
	     
	 
    
}