trigger CaseLastOwner on Case (before update) {
    Id QueueId = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName = 'Incoming_Case_Support'].Id;
    for(Integer i = 0; i < trigger.size; i++){
        if((trigger.old[i].OwnerId == QueueId) && (String.Valueof(trigger.new[i].OwnerId).startsWith('0056'))){
            trigger.new[i].Last_Owner__c = trigger.new[i].OwnerId;
        }
    }
}