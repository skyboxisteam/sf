trigger PMStatusTrigger on PM_Status__c (after insert, after delete, after update) {

	//Author: Rina Nachbas			Date:16.07.18
	//Task:   SF-664
	if(trigger.isInsert && trigger.isAfter){
		updateDeployment(trigger.new);
	}
	if(trigger.isUpdate || trigger.isDelete){
		List<PM_Status__c> pmStatusList = getDataForUpdateDeployment();
		updateDeployment(pmStatusList);
	}


	public List<PM_Status__c> getDataForUpdateDeployment(){
		List<PM_Status__c> pmTriggerList = trigger.isDelete ?  trigger.old : trigger.new;
		List<PM_Status__c> pmStatusList = new List<PM_Status__c>();
		//get the last PM_Status__c created for the Deployment__c obj
		Set<Id> deploymentSet = new Set<Id>();
		Map<Id, PM_Status__c> lastPMforDeployment = new Map<Id, PM_Status__c>();
		for(PM_Status__c pms : pmTriggerList){
			deploymentSet.add(pms.Deployment__c);
		}
		List<PM_Status__c> pmList = [select Id,Deployment__c,createdDate,Status__c from PM_Status__c where Deployment__c in :deploymentSet order by createdDate desc];
		for(PM_Status__c pm : pmList){
			if(lastPMforDeployment.get(pm.Deployment__c) == null || pm.createdDate >= lastPMforDeployment.get(pm.Deployment__c).createdDate){
				lastPMforDeployment.put(pm.Deployment__c, pm);
			}
		}
		if(trigger.isDelete){
			for(PM_Status__c pms : lastPMforDeployment.values()){
				pmStatusList.add(pms);
			}
			//cover situation where we deleted the last and only PM_Status__c
			Set<Id> deploymentsHavePMS = new Set<Id>();
			for(PM_Status__c pms : pmStatusList){
				deploymentsHavePMS.add(pms.Deployment__c);
			}
			for(PM_Status__c pms : pmTriggerList){
				if(!deploymentsHavePMS.contains(pms.Deployment__c)){
					pmStatusList.add(new PM_Status__c(Deployment__c = pms.Deployment__c, Status__c=''));
				}
			}
		}
		else{	
			for(PM_Status__c pms : pmTriggerList){
				//trigger called because Status__c was updated 
				if(pms.Status__c != trigger.oldMap.get(pms.Id).Status__c &&
					pms.Id == lastPMforDeployment.get(pms.Deployment__c).Id){
					pmStatusList.add(pms);
				}
			}
		}
		return pmStatusList;
	}

	public void updateDeployment(List<PM_Status__c> pmStatusList)
	{
		List<Deployment__c> depToUpdate = new List<Deployment__c>();
		for(PM_Status__c pms : pmStatusList)
		{
			depToUpdate.add(new Deployment__c( Id=pms.Deployment__c, Last_PM_Status__c=pms.Status__c));
		}
		if(depToUpdate.size() > 0)
		{
			update depToUpdate;
		}
	}

}