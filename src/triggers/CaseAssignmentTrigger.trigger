trigger CaseAssignmentTrigger on Case (after update) {
 
  list<case> NeedToAssignCases = new list<case>();
  list<case> ChangedOwnerCases = new list<case>();
  list<id> oldCaseOwners = new list<id>();
  list<id> newCaseOwners = new list<id>();
  
   
     for(case cs : trigger.new)
      {
       if(cs.Assign__c != trigger.oldMap.get(cs.id).Assign__c && cs.Assign__c != null && Ctrl_AssignCases.run ==true)
          NeedToAssignCases.add(cs);
       else if(cs.ownerid != trigger.oldMap.get(cs.id).ownerid )
         {
          ChangedOwnerCases.add(cs);
          newCaseOwners.add(cs.ownerID);
         }
      } 
        if(!NeedToAssignCases.isempty())
       {
         Ctrl_AssignCases runAllCase = new Ctrl_AssignCases(NeedToAssignCases);    
         runAllCase.runAssignmentProcess();
       }
       
        if(!ChangedOwnerCases.isempty())
       {
          for(case cs :trigger.oldmap.values())
              oldCaseOwners.add(cs.ownerid);
          map<id,user> oldUserOwners = new map<id,user> ([select id,name, isActive, contactId, UserRole.Developername, UserRoleId from user where id in : oldCaseOwners]); 
          map<id,user> newUserOwners = new map<id,user> ([select id,name, isActive, contactId, UserRole.Developername, UserRoleId from user where id in : newCaseOwners]);  
          Ctrl_AssignCases runAllCase = new Ctrl_AssignCases(ChangedOwnerCases);
          runAllCase.runChangeOwnerProcess(newUserOwners,oldUserOwners,trigger.oldmap);      
       }
        
       
}