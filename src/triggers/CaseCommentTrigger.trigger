trigger CaseCommentTrigger on CaseComment (after insert) {

    List<CaseComment> cms = new List<CaseComment>();
    List<Case> CasesToUpdate = new List<Case>();
    
    cms = [SELECT id, ParentID, parent.Last_Tier_4_Comment_Date__c, CreatedBy.UserRole.DeveloperName FROM CaseComment Where id IN :trigger.new];
    System.debug('cms list');
    for (CaseComment cm : cms){
          if(cm.CreatedBy.UserRole.DeveloperName == 'TLS'){
          Case c = new Case(Id=cm.ParentID, Last_Tier_4_Comment_Date__c = system.now());
          CasesToUpdate.add(c);
              }
    }
    if(!CasesToUpdate.isEmpty()){
    Update CasesToUpdate;
    }
}