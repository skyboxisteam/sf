/******************************************************************************* 
Name              : CaseTeamAddOwners
Description		  : Creates Case Team Members based on the Cases History
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by	      Related Task/Issue 			 
----------------------------------------------------------------------------------------
1. Hernan          		22/01/2013              	Adi		           	[SW-4341]
*******************************************************************************/
trigger CaseTeamAddOwners on Case (after update) {
	try{
		
		Map<Id, Set<Id>> caseByOwner = new Map<Id, Set<Id>>();
		Set<Id> case2Proc = new Set<Id>();
		
		for(Integer i=0; i<trigger.size; i++){
			Case oldCase = trigger.old[i];
			Case newCase = trigger.new[i];
			
			if( oldCase.OwnerId != newCase.OwnerId ){
				
				if(!caseByOwner.containsKey(oldCase.OwnerId)){
					caseByOwner.put(oldCase.OwnerId, new Set<Id>());
				}
				caseByOwner.get(oldCase.OwnerId).add(oldCase.Id);
				
				case2Proc.add(oldCase.Id);
			}
		}
		
		if(!case2Proc.isEmpty()){
			//Get the TeamRole Id to Use
			Id ctr2Use;
			for( CaseTeamRole ctr : [SELECT Id FROM CaseTeamRole WHERE Name = 'Assigned Support Engineer' LIMIT 1]){
				ctr2Use = ctr.Id;
			}
			
			if( ctr2Use != null){
				Map<Id, Set<String>> ownersInCT = new Map<Id, Set<String>>();
				
				// This is because in PRODUCTION we got an error about problem on relationship MEMBER
				Map<Id, String> caseTMNameById = new Map<Id, String>();
				for( CaseTeamMember ctm : [SELECT MemberId FROM CaseTeamMember WHERE ParentId IN :case2Proc] ){
					caseTMNameById.put(ctm.MemberId, '');
				}
				
				for( User us : [SELECT Id, Name FROM User WHERE Id IN :caseTMNameById.keySet()] ){
					caseTMNameById.put(us.Id, us.Name);
				}
								
				//Check if there are Team Members Created and get their Users so to not duplicate entries.
				//for( CaseTeamMember ctm : [SELECT ParentId, Member.Name, MemberId FROM CaseTeamMember WHERE ParentId IN :case2Proc] ){
				for( CaseTeamMember ctm : [SELECT ParentId, MemberId FROM CaseTeamMember WHERE ParentId IN :case2Proc] ){
					if(String.valueOf(ctm.MemberId).startsWith('005')){
						if(!ownersInCT.containsKey(ctm.ParentId) ){
							ownersInCT.put(ctm.ParentId, new Set<String>());
						}
						//ownersInCT.get(ctm.ParentId).add(ctm.Member.Name);
						ownersInCT.get(ctm.ParentId).add(caseTMNameById.get(ctm.MemberId));
					}
				}
				
				List<CaseTeamMember> ctm2Ins = new List<CaseTeamMember>();
				
				Map<String, Set<Id>> csByOwners = new Map<String, Set<Id>>();
				Set<String> userNames = new Set<String>();
				
				for( User us : [SELECT Id, Name FROM User WHERE isActive = true AND Name != 'R&D Support' AND Id IN :caseByOwner.keySet() AND ContactId = null AND IsPortalEnabled = false] ){
					for( Id caseId : caseByOwner.get(us.Id) ){
						if( (ownersInCT.containsKey(caseId) && !ownersInCT.get(caseId).contains(us.Name)) || ownersInCT.isEmpty() ){
							CaseTeamMember ctm = new CaseTeamMember( ParentId = caseId, MemberId = us.Id, TeamRoleId = ctr2Use );
							ctm2Ins.add(ctm);
						}
					}
				}
				/*
				for( CaseHistory ch : [SELECT CaseId, OldValue FROM CaseHistory WHERE CaseId IN :case2Proc AND Field = 'Owner'] ){
					String ownerName = String.valueOf(ch.OldValue);
					system.debug('ownerName: ' + ownerName);
					
					//If this conditions are met, we need save the Users names to query its Id
					//Here we check if the User is already a Team Member
					if( ownerName != null && ownerName != '' && ownerName != 'R&D Support' && ( (ownersInCT.containsKey(ch.CaseId) && !ownersInCT.get(ch.CaseId).contains(ownerName)) || ownersInCT.isEmpty() )){
						if( !csByOwners.containsKey( ownerName )){
							csByOwners.put( ownerName, new Set<Id>() );
						}
						csByOwners.get(ownerName).add(ch.CaseId);
						
						userNames.add(ownerName);
					}
				}
				
				//Query all Users we obtained
				for( User us : [SELECT Id, Name FROM User WHERE isActive = true AND Name IN :userNames ]){
					
					//For every User we have a list of Cases, we create a Team Member for each
					for( Id csId : csByOwners.get(us.Name) ){
						CaseTeamMember ctm = new CaseTeamMember( ParentId = csId, MemberId = us.Id, TeamRoleId = ctr2Use );
						ctm2Ins.add(ctm);
					}
				}
				*/
				if( !ctm2Ins.isEmpty() ){
					insert ctm2Ins;
				}
			}
		}
	}catch( Exception e ){
		String  errorMsg = e.getMessage();
		/*
		errorMsg += '\nStack Trace:' + e.getStackTraceString();
		errorMsg += '\nLine Number:' + e.getLineNumber();
		errorMsg += '\nException Type:' + e.getTypeName();
		*/            
		System.debug('errorMsg: ' + errorMsg);
		trigger.new[0].addError(errorMsg);
	}
}