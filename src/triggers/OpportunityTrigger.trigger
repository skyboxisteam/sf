trigger OpportunityTrigger on Opportunity (before insert , after insert, after update,before update,  before delete) {
 
  if(trigger.isAfter && trigger.isUpdate){
      OpportunitySearchLockDown.SearchLockDown(trigger.new, trigger.oldMap);
    
    CPQMethods cm = new CPQMethods();
    //CPQ-121
    cm.OpportunityClosedWon(trigger.new, trigger.oldMap);

     //RinaN 12.12.18 SF-1021
    // OpportunityHandler.validateSOWExistsOnReadyToBook(trigger.new, trigger.oldMap);
    OpportunityHandler.ignoreValidations(trigger.new, trigger.oldMap);
  }
 
  if(trigger.isAfter && trigger.isInsert)
  {
      CPQMethods cm = new CPQMethods();
      cm.OpportunityNamingConvention(trigger.new);
      
      //RinaN 10.7.18 SF-762
     OpportunityHandler.updateOppByOpportunityContactRole(trigger.new);
     ///SF-980
     OpportunityHandler.pilotValidation(trigger.new, trigger.oldMap);
     //RinaN 12.12.18 SF-1021
    // OpportunityHandler.validateSOWExistsOnReadyToBook(trigger.new, trigger.oldMap);
  }
  if(trigger.isBefore && trigger.isInsert)
  {
    //  insert new SFLog__c(Location__c='OpportunityTrigger:(trigger.isBefore && trigger.isInsert',msg__c=string.valueOf(trigger.new.size()));
      //RinaN, 17.7.18, CPQ-127:Remove CPQ data upon clone opportunity
      OpportunityHandler.clearOpportunityFildsOnClone(trigger.new);
  }     
  if(trigger.isBefore && trigger.isUpdate){
     
     //RinaN, 1.11.18, CM-64
     OpportunityHandler.probabilityValidation(trigger.new, trigger.oldMap);
     //MP-63
     OpportunityHandler.fieldUpdates(trigger.new, trigger.oldMap);
     ///SF-980
     OpportunityHandler.pilotValidation(trigger.new, trigger.oldMap);
   
  }
     
  if(trigger.isAfter)
  {
      system.debug(' is insert ?' + trigger.isInsert);
      system.debug(' is update ?' + trigger.isUpdate);
      system.debug(' is delete ?' + trigger.isdelete);
   
    system.debug('@@@OpportunityTrigger');
    //OpportunityBookingSchedule obs = new OpportunityBookingSchedule();
    //obs.creatFromOpportunity(trigger.new,trigger.oldMap, false, trigger.isInsert); 
    
    String jsonOldMap = JSON.serialize(Trigger.oldMap);
    String jsonNewList = JSON.serialize(trigger.new);
    OpportunityBookingSchedule.creatFromOpportunity(jsonNewList,jsonOldMap, false, trigger.isInsert, trigger.isUpdate);
  }
}