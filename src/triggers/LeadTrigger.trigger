trigger LeadTrigger on Lead (after insert, after update, before update) {

	if(trigger.isInsert && trigger.isAfter){
        //MP-63  - Send Email Alerts
        LeadHandler.sendAlert(trigger.new);
    }

    if(trigger.isUpdate && trigger.isAfter){
        //MP-63  - Send Email Alerts
        LeadHandler.sendAlert(trigger.new, trigger.oldMap);
    }

    if(trigger.isUpdate && trigger.isBefore){
        //MP-63  - Send Email Alerts
        LeadHandler.fieldUpdates(trigger.new, trigger.oldMap);
    }
}