trigger CommissionScheduleTrigger on Commission_Schedule__c (after insert) 
{
  CommissionPlanCalc cpc =new CommissionPlanCalc(trigger.new);
  cpc.calcAllComponents();    
}