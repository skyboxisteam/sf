trigger KW_NewCaseTrigger on Case (after insert) {
    Case NewCase = trigger.new[0];
    Case CaseToUpdate = [select id from case where id = :NewCase.id];
    System.debug('KW Got Triggered for new Case:' + NewCase.CaseNumber);

    if (!Test.isRunningTest()) {
        KW_NewCase.FolderCreation( string.valueOf(NewCase.CaseNumber));
    }
}