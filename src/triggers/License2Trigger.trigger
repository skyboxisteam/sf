trigger License2Trigger on License2__c (before insert, after insert, before update, after update) 
{
       
    LicenseHandler.sortToTypes(trigger.new,trigger.oldMap, trigger.isBefore,trigger.isInsert);
   
    if(trigger.isInsert && trigger.isBefore){
        //SF-925 - Move 'Must_Select_Account' VR to code so the validation will be before Update/Insert. - Must be first method
        LicenseHandler.validateRequierdFields(trigger.new);
      

        //MP-54  - link license to Account - afterInsert for VR to take effect befor code
        LicenseHandler.updateLicenseAccount(trigger.new);

         //MP-24  - generate license conditions
        LicenseHandler.generateLicenseCond(trigger.new);

        //update Partner_Account_Manage_SE__c- afterInsert for VR to take effect befor code
      //  LicenseHandler.updateLicense(trigger.new);
    }
    if(trigger.isInsert && trigger.isAfter){

        //MP-56  - Send internal POC alert - to accountTeam - after accountId is populated!
        LicenseHandler.sendAlert(trigger.new);
    }

    if(trigger.isUpdate && trigger.isBefore){
        //SF-925 - Move 'Must_Select_Account' VR to code so the validation will be before Update/Insert. - Must be first method
        LicenseHandler.validateRequierdFields(trigger.new);
        //MP-54  - link license to Account
        LicenseHandler.updateLicenseAccount(trigger.new, trigger.oldMap);
    }

}