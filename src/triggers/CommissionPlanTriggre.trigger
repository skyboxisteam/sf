trigger CommissionPlanTriggre on Commission_Plan__c (after insert) {
	if(trigger.isAfter && trigger.isInsert){
		//RinaN 20.11.18 CM-51
		CommissionPlanHandler.createDefaultCommissionRule(trigger.new);
	}
}