trigger CommissionRuleAssignment on Commission_Rule_Assignment__c (before insert, after insert ,after update, before delete) 
{
 
    CommissionRuleAssHandler crah = new CommissionRuleAssHandler(trigger.isdelete?trigger.old:trigger.new);
    
    if(trigger.isBefore && trigger.isInsert)
      crah.updateBeforeInsert();
    else
      if(trigger.isdelete)
        crah.reCalculateComPlan();
      else
        crah.calculateComPlan();
       
}