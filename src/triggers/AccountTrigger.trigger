trigger AccountTrigger on Account (after update) 
{
   //LicenseProduction sd = new LicenseProduction(trigger.new, trigger.oldmap); 

   if(Trigger.isAfter && Trigger.isUpdate)
    {
        //Task: MP-10 : Deactivate Partner Users
        AccountHandler.deactivatePartnerUsers(Trigger.new, Trigger.oldMap);
    }   
}