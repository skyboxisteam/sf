trigger ReleaseTrigger on Release__c (before update) {
	
	string IconductParam;
	string InterfaceId = '22188';
	
	for(Release__c rel:trigger.new )
	 if(rel.Get_Cards__c )
	  {
	     IconductParam = '{  "Key":"Board_Id","Value":"'+rel.Board_ID__c+'"},  {  "Key":"Release_Id","Value":"'+rel.id +'"} ';
	     APIToIconduct.callIconduct(InterfaceId,IconductParam );
	     rel.Get_Cards__c  = false;
	  }
    
}