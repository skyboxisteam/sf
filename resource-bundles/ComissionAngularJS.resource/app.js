
var CommissionApp = angular.module('CommissionApp', ['ngRoute', 'CommissionControllers']);
//CommissionApp.constant('appConfig', appConfig);
/**
 * assign global variables from visual force page 
 * into application constant object
 */
CommissionApp.constant('ENV_VAR', {
    COMMISSION_PLAN: commissionPlan,
    appResourceUrl: appResourceUrl
});


CommissionApp.config(['$routeProvider', '$sceProvider','$httpProvider','ENV_VAR',
    function($routeProvider, $sceProvider, appConfig, $httpProvider, offset, ENV_VAR ) {
        $sceProvider.enabled(false);
        $routeProvider.when('/', {
            //templateUrl: appConfig.appResourceUrl + 'partials/index.html',
            templateUrl: appResourceUrl + '/partials/index.html',
            controller: 'CommissionCtrl',
			reloadOnSearch: false 
        }).otherwise({
            redirectTo: '/'
        });

  }]);



// User Story 16842 : Task 16843
// Author Racheli Amrusi
// Description format to display date in case list
CommissionApp.filter('customDate', function($filter) {
   return function( value ) {
    var today = new Date().toDateString();
    var yesterday = new Date(new Date().setDate(new Date().getDate()-1)).toDateString();
    var tempdate = new Date(value).toDateString();
    if(today == tempdate)
       return $filter('date')(value, "HH:mm");
    else if (yesterday == tempdate)
         return 'אתמול';
    else return $filter('date')(value, "dd.MM.yy | HH:mm");
  }
});

CommissionApp.run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
    var original = $location.path;
    $location.path = function (path, reload) {
        if (reload === false) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path]);
    };
}])

