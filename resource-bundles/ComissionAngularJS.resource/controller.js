﻿var CommissionControllers = angular.module('CommissionControllers', ['ui.bootstrap','angular.filter']);


/***************************** MAIN CONTROLLER ********************************/
CommissionControllers.controller('CommissionCtrl',function ($scope) {

    //debugger;
    $scope.appResourceUrl = appResourceUrl;
    $scope.commissionPlan = commissionPlan;
    $scope.commissionScheduleList = commissionScheduleList;
    $scope.commissionRuleAssList = commissionRuleAssList;
    $scope.commissionSchedulePIOList = [];
    $scope.commissionScheduleNoPIOList = [];
    $scope.accelerator = acceleratorMap;
    $scope.recurringIncentiveMap = recurringIncentiveMap;
    $scope.isAdvanceableMap = isAdvanceableMap;
    $scope.valuesMap = valuesMap;
    $scope.isMultiYearMap = isMultiYearMap;
    $scope.labels = commissionPlanDetails.labels;
    $scope.Math = window.Math;
   

    //$scope.cpOppsAmnt = cpOppsAmnt;//13
    //$scope.commissionPlan.totalQualifiedBilling = commissionPlan.Up_to_quota_amount__c + commissionPlan.Beyond_quota_amount__c;//14
    //$scope.commissionPlan.totalCommissionDue = commissionPlan.Up_to_commission_amount__c + commissionPlan.Beyond_commission_amount__c;//15
    $scope.recurringIncentive = recurringIncentive;//8 - Sum cs.Advanceable_Recurring_incentive (Per Opportunity)
    $scope.baseRate = baseRate;//23
    $scope.baseRatePIO = baseRatePIO;
    $scope.multiYearFactor = multiYearFactor;
    $scope.superFactor = superFactor;
    $scope.superAddition = superAddition *1;
    $scope.advanceRecurringIncentive = advanceRecurringIncentive;
    //$scope.annualQuota = annualQuota;//formula field
    //$scope.H1Factor = H1Factor;//formula field
    //$scope.annualFactor = annualFactor;//formula field

    //PIO Fields Section 
    $scope.pioBaseRate = baseRatePIO;//$scope.valuesMap[$scope.labels.OnTargetCommission] / $scope.valuesMap[$scope.labels.AnnualPIOAccelerator];
    $scope.pioBaseRateH1Factior = $scope.pioBaseRate * $scope.valuesMap[$scope.labels.H1PIOAccelerator];
    $scope.pioBaseRateAnnualFactor = $scope.pioBaseRate * $scope.valuesMap[$scope.labels.AnnualPIOAccelerator+$scope.labels.Factor];
    $scope.pioBaseRateAnnualFactorH1Factior = $scope.pioBaseRate * $scope.valuesMap[$scope.labels.AnnualPIOAccelerator+$scope.labels.Factor] * $scope.valuesMap[$scope.labels.H1PIOAccelerator];

    $scope.opportunityAmountTotal = 0;//5 total & 51 total
    $scope.opportunityPIOAmountTotal = 0;//11,12,16 total PIO
    $scope.qualifiedAmount = 0;//6 Qualified Amount
    $scope.deduction = 0;//7
    $scope.hasPIO = false;
    $scope.hasMultyYear = false;
    $scope.hasSuperAcc = false;
    $scope.hasRenewal = false;
    $scope.commissionBreakDownTableRowSpan = 3;

    $scope.totalMultiYear = 0;
    $scope.qualifiedMultiYear = 0;
    $scope.qualifiedBillingForPaymentMultiYear = 0;
    $scope.totalNoMultiYear = 0;
    $scope.qualifiedNoMultiYear = 0;
    $scope.qualifiedNoBillingForPaymentMultiYear = 0;
    $scope.renewalTotal = 0;
    $scope.renewalQualified = 0;
    $scope.renewalQualifiedBillingForPayment = 0;
   
   $scope.init = function(){


        var items = []; //The temporary array to keep the values

        //caculate cs list for PIO table and cs list for other opp type
      //+ calculate total Opportunity_Amount__c by CS - DISTINCT by opp//5+11 total
        angular.forEach($scope.commissionScheduleList, function(value, key) {
          //console.log(key + ': ' + value);
          if(value.Commission_Record_Type__c == "PIO")
            $scope.commissionSchedulePIOList.push(value);
          else
            $scope.commissionScheduleNoPIOList.push(value);

           if(items.indexOf(value.Opportunity__c) === -1) { //Check if the values is not already in the temp array
                items.push(value.Opportunity__c); //Push it to array
                if(value.Commission_Record_Type__c == "PIO"){
                    $scope.opportunityPIOAmountTotal += value.Opportunity_Amount__c; 
                }
                else{
                    $scope.opportunityAmountTotal += value.Opportunity_Amount__c;    
                }
            }

            value.Opportunity__r.CreatedDate = value.Opportunity__r.CreatedDate.split('T')[0];
        });

       //caculate if CP has PIO rule - to show PIO data on the page
       //+ parse Json value to look as nicely text
        angular.forEach($scope.commissionRuleAssList, function(value, key) {
          //console.log(key + ': ' + value);
          if(value.Rule__c)//is undefined when Rule's json value not exist
            value.Rule__c = value.Rule__c.replace(/}|{| |\n|"/g,'').replace(/,/g,'<br>').replace(/:/g,' = ').replace(/([A-Z])/g, " $1");//parse Json value to look as nicely text + split camelCasedWord
          if(value.Type__c == "Annual PIO Accelerator")
            $scope.hasPIO = true;
        else if(value.Type__c == $scope.labels.RenewalAdjustment)
            $scope.hasRenewal = true;
          else if(value.Type__c == "Multi Year"){
            $scope.hasMultyYear = true;
            $scope.commissionBreakDownTableRowSpan++;
          }
          else if(value.Type__c == "Super Accelerator"){
            $scope.hasSuperAcc = true;
            $scope.commissionBreakDownTableRowSpan++;
          }
        });

        
        angular.forEach($scope.commissionScheduleNoPIOList, function(value, key) {
            //debugger;
          //console.log(key + ': ' + value);
          //$scope.qualifiedAmount+= value.Advanceable_Amount__c * (value.Credit__c/100); //6
            debugger;
          if($scope.isMultiYearMap[value.Id]) { //value.Type__c.contains( "Multi Year") && $scope.hasMultyYear CM-84
            $scope.deduction+= value.Commission_Amount__c;//7
            $scope.totalMultiYear+= value.Booking_Schedule__r.Amount__c;
            $scope.qualifiedMultiYear+= value.Quota_Amount__c;
            $scope.qualifiedBillingForPaymentMultiYear+= value.Commission_Amount__c;
          }
          else if(value.Opportunity__r.Is_renewal_for_commission_calculation__c && $scope.hasRenewal){
            $scope.renewalTotal+= value.Booking_Schedule__r.Amount__c;
            $scope.renewalQualified+= value.Quota_Amount__c;
            $scope.renewalQualifiedBillingForPayment+= value.Commission_Amount__c;
          }
          else{
            $scope.totalNoMultiYear+= value.Booking_Schedule__r.Amount__c;
            $scope.qualifiedNoMultiYear+= value.Quota_Amount__c;
            $scope.qualifiedNoBillingForPaymentMultiYear+= value.Commission_Amount__c;
          }
      });

        //$scope.qualifiedNoBillingForPaymentMultiYear+= $scope.advanceRecurringIncentive*1; // add Commission_Amount__c's the advanced recurring 
        //$scope.totalNoMultiYear+= $scope.recurringIncentive*1; // add Commission_Amount__c's the advanced recurring 
        //$scope.qualifiedNoMultiYear+= $scope.recurringIncentive*1; // add Commission_Amount__c's the advanced recurring 
        //$scope.qualifiedAmount = $scope.qualifiedAmount - $scope.deduction; //6. Sum (cs.Advanceable_Amount__c  *cs.Credit__c) - Sum cs.Billing_Credit_Amount__c (where type == ‘Multi Year’)
        $scope.deduction = $scope.deduction ;//* multiYearFactor;
        $scope.qualifiedAmount = $scope.qualifiedNoMultiYear;//$scope.getSum($scope.commissionScheduleNoPIOList,'Commission_Amount__c');//updated 6
        $scope.totalQualified = $scope.qualifiedAmount; //10 = 6+8
        $scope.totalAmount = $scope.commissionPlan.Up_to_commission_amount__c + $scope.commissionPlan.Beyond_commission_amount__c + $scope.baseRate * $scope.deduction * $scope.hasMultyYear + commissionPlan.Up_to_quota_rate__c * superFactor * superAddition * $scope.hasSuperAcc; //28 = 19 + 22 + 25 +superAmnt
        $scope.commissionPlan.commissionEarning = $scope.totalAmount  ;//1 = 28 = 19+22+25 +superAmnt
        $scope.commissionPlan.PIOEarning  = commissionPlan.Up_to_quota_amount_PIO__c + commissionPlan.Beyond_quota_amount_PIO__c; //2
        $scope.commissionPlan.paymentDue = $scope.commissionPlan.commissionEarning + $scope.commissionPlan.PIOEarning - $scope.commissionPlan.Commission_Paid__c;//4.   1+2-3



        //update 'current' of special AnnualPIOAccelerator + AnnualPIOAccelerator which we already calculated the total here at $scope code 
        //+ update attainment value
        //angular.forEach($scope.accelerator,function(value, key){
        //    if (key == $scope.labels.AnnualAccelerator) {
        //        value.current = $scope.totalQualified;
        //    }
        //    else if(key == $scope.labels.AnnualPIOAccelerator){
        //        value.current = $scope.commissionPlan.Sum_PIO__c;
        //    }
        //     value.attainment = value.current/value.target; // update attainment
        //});

        $("td").tooltip({container:'body'});


        //To update the text of toggle show more/less when it's closed/opend
        $(document).ready(function(){
          $("#detailSection").on("hide.bs.collapse", function(){
            $(".link").html('<span class="glyphicon glyphicon-collapse-down"></span> Show More');
          });
          $("#detailSection").on("show.bs.collapse", function(){
            $(".link").html('<span class="glyphicon glyphicon-collapse-up"></span> Show Less');
          });
        });
    }

    $scope.getSum = function(items, fieldName, fieldName2) {
        //debugger;

        return items.length > 0 ? items
            .map(function(item) { return fieldName2? item[fieldName][fieldName2]||0: item[fieldName]||0 ; })
            .reduce(function(accumulator, b) { return accumulator + b; }, 0) : 0;
  };

//get count of items distincted by 'fieldToDistinct' - how many different fieldToDistinct are in the items list
  $scope.getCount = function(items, fieldToDistinct){
    var tempItems = []; //The temporary array to keep the values
    angular.forEach(items, function(value, key) {
        if(tempItems.indexOf(value[fieldToDistinct]) === -1) { //Check if the values is not already in the temp array
            tempItems.push(value.Opportunity__c); //Push it to array
        }
    });
    return tempItems.length;
  }

  $scope.ExportToExcel = function(){
    debugger;

    var table_div = $( '.Opportunities' ).clone() // clone table
            .removeAttr( 'data-prot class' ) // remove class and data-prot attributes
            .find( 'thead' ) // find thead
            .find( 'tr' ) // find rows
            .first().remove().end() // remove first row (table title & toolbar)
            .last().remove().end() // remove last row in header (filter bar)
            .unwrap().end()// remove thead, end tr
            .end()// end thead, now we are in table again
            .find( 'tfoot' ).remove().end()// remove tfoot, back to table
            .find( '.chk' ).remove().end()// remove checkboxes, back to table
            .find( 'tbody' ).children( 'tr' ).unwrap().end()// remove tbody, end tr
            .end() // end tbody
            .find( 'th[class],td[class]' ).removeAttr( 'class style' )// find cells, remove class and style attributes
            .removeAttr( 'data-name data-type data-sortable data-filterable data-editable' ) // remove data- attributes
            .end()// end th, td cells, now we are in table again
            .wrap( '<div></div>' ).parent(); // wrap table with div and select that div
        
        // save table as excel
        window.open( 'data:application/vnd.ms-excel,' + encodeURIComponent( table_div.html() ) );       
  }
    


    //main controller init method
    $scope.init();
});


/******************************** DIRECTIVES **********************************/
/*
CommissionControllers.directive('keyEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.keyEnter);
                });
                event.preventDefault();
            }
        });
    };
});
*/

